<?function numar($s, $z) {
   switch($z) {
      case 1:
         switch($s) {
         	case 0: $numar = "zero"; break;
            case 1: $numar = "o "; break;
            case 2: $numar = "doua "; break;
            case 3: $numar = "trei "; break;
            case 4: $numar = "patru "; break;
            case 5: $numar = "cinci "; break;
            case 6: $numar = "sase "; break;
            case 7: $numar = "sapte "; break;
            case 8: $numar = "opt "; break;
            case 9: $numar = "noua ";
         }
         break;
      case 2:
         switch($s) {
         	case 0: $numar = "zero"; break;
            case 1: $numar = "zece"; break;
            case 2: $numar = "doua"; break;
            case 3: $numar = "trei"; break;
            case 4: $numar = "patru"; break;
            case 5: $numar = "cinci"; break;
            case 6: $numar = "sai"; break;
            case 7: $numar = "sapte"; break;
            case 8: $numar = "opt"; break;
            case 9: $numar = "noua";
         }
         break;
      case 3:
         switch($s) {
         	case 0: $numar = "zero"; break;
            case 1: $numar = "unu "; break;
            case 2: $numar = "doi "; break;
            case 3: $numar = "trei "; break;
            case 4: $numar = "patru "; break;
            case 5: $numar = "cinci "; break;
            case 6: $numar = "sase "; break;
            case 7: $numar = "sapte "; break;
            case 8: $numar = "opt "; break;
            case 9: $numar = "noua ";
         }
         break;
      case 4:
         switch($s) {
         	case 0: $numar = "zero"; break;
            case 1: $numar = "un"; break;
            case 2: $numar = "doi"; break;
            case 3: $numar = "trei"; break;
            case 4: $numar = "pai"; break;
            case 5: $numar = "cinci"; break;
            case 6: $numar = "sai"; break;
            case 7: $numar = "sapte"; break;
            case 8: $numar = "opt"; break;
            case 9: $numar = "noua";
         }
         break;
   }
   return $numar;
}
function valoare_in_litere($nr) {
   $bnr=split(",", $nr);
   $cstr="";
   for ($i=0;$i<count($bnr);$i++) {
      $nr=str_replace(array(",","."), "", $bnr[$i]);
      $sute="";
      $zeci="";
      $uni="";
      $un = array("miliard ", "miliarde ", "milion ", "milioane ", "mie ", "mii ", "", "");
      $sir = "";
      $s = "1";
      $num = 4;
      while ($s != "") {
         $s = substr($nr, -3);
         if ($s=="") break;
         $nr = (strlen($nr)-3>0)?substr($nr, 0, strlen($nr)-3):"";
         $ss = "";
         if ($s!="") {
            switch(strlen($s)) {
               case 3:
                  $sute = substr($s, 0, 1);
                  $zeci = substr($s, 1, 1);
                  $uni = substr($s, 2, 1);
                  $ss = numar($sute, 1) . (($sute==1)?"suta ":(($sute>1)?" sute ":""));
                  if ($zeci==1 && $uni!=0)
                     $ss=$ss.numar($uni, 4)."sprezece ".$un[$num*2];
                  else if ($zeci==1 && $uni==0)
                     $ss="zece ".$un[$num*2];
                  else {
                     $ss=$ss.numar($zeci, 2) . (($zeci!=0)?"zeci ":"") . (($uni!="0") ? "si ".numar($uni,3):"") . (($s!="000")?$un[$num*2-1]:"");
                  }
                  $sir = $ss . $sir;
                  break;
               case 2:
                  $sute = 0;
                  $zeci = substr($s, 0, 1);
                  $uni = substr($s, 1, 1);
                  if ($zeci == 1 && $uni != 0)
                     $ss = $ss . numar($uni, 4) . "sprezece " . $un[$num*2-1];
                  else if ($zeci == 1 && $uni == 0)
                     $ss = "zece " . $un[$num * 2];
                  else
                     $ss = $ss . numar($zeci, 2) . (($zeci!=0)?"zeci ":"") . (($uni!="0")?"si ".numar($uni,3):"") . $un[$num*2-1];
                  $sir = $ss . $sir;
                  break;
               case 1:
                  $sute = 0;
                  $zeci = 0;
                  $uni = substr($s, 0, 1);
                  $sir = numar($uni, ($num==3)?1:3) . (($uni==1)?$un[($num-1)*2]:$un[($num-1)*2+1]) . $sir;
                  break;
            }
            $num--;
         }
      }
      if ($i==0)
         $cstr.=$sir." lei ";
      else if ($i==1)
         $cstr.=" si ".$sir." bani";
   }
   return $cstr;
}

//echo smarty_modifier_transf("5944,65");
function roundup ($value, $dp)
{ 
    $offset = pow (10, -($dp + 1)) * 5;
    return round ($value + $offset, $dp);
} 
function dont_touch_the_precision($val, $pre = 0)
{
    return number_format((float) (int) ($val * pow(10, $pre)) / pow(10, $pre) ,$pre,'.','');
    
}

function luna_cifre_litere ($luna){ 
	$an_actual=date("Y");
    if($luna=="01"){$val="ianuarie";}
    if($luna=="02"){$val="fabruarie";}
    if($luna=="03"){$val="martie";}
    if($luna=="04"){$val="aprilie";}
    if($luna=="05"){$val="mai";}
    if($luna=="06"){$val="iunie";}
    if($luna=="07"){$val="iulie";}
    if($luna=="08"){$val="august";}
    if($luna=="09"){$val="septembrie";}
    if($luna=="10"){$val="octombrie";}
    if($luna=="11"){$val="noiembrie";}
    if($luna=="12"){$val="decembrie";}
    return $val;
} 
?>
