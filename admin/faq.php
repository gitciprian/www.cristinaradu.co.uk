<?include("top_admin.php");?>
<div class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">				
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['FaqPageTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['FaqPageSubtitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>	
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="span3">
							<ul class="ver-inline-menu tabbable margin-bottom-10">
								<li class="active"><a href="#tab_1" data-toggle="tab"><i class="icon-briefcase"></i> <?php echo $lang['FaqPageTabTitle1'];?></a><span class="after"></span></li>
								<li><a href="#tab_2" data-toggle="tab"><i class="icon-group"></i> <?php echo $lang['FaqPageTabTitle2'];?></a></li>
								<li><a href="#tab_3" data-toggle="tab"><i class="icon-leaf"></i> <?php echo $lang['FaqPageTabTitle3'];?></a></li>
								<li><a href="#tab_4" data-toggle="tab"><i class="icon-info-sign"></i> <?php echo $lang['FaqPageTabTitle4'];?></a></li>
								<li><a href="#tab_5" data-toggle="tab"><i class="icon-plus"></i> <?php echo $lang['FaqPageTabTitle5'];?></a></li>
							</ul>
						</div>
						<div class="span9">
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="accordion in collapse" id="accordion1" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1"><?php echo $lang['FaqPageTab1SubTitle1'];?></a>
											</div>
											<div id="collapse_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab1Text1'];?>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2"><?php echo $lang['FaqPageTab1SubTitle2'];?></a>
											</div>
											<div id="collapse_2" class="accordion-body collapse">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab1Text2'];?>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3"><?php echo $lang['FaqPageTab1SubTitle3'];?></a>
											</div>
											<div id="collapse_3" class="accordion-body collapse">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab1Text3'];?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_2">
									<div class="accordion in collapse" id="accordion2" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">
												    <?php echo $lang['FaqPageTab2SubTitle1'];?>
												</a>
											</div>
											<div id="collapse_2_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab2Text1'];?>
												</div>
											</div>
										</div>
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2">
													<?php echo $lang['FaqPageTab2SubTitle2'];?>
												</a>
											</div>
											<div id="collapse_2_2" class="accordion-body collapse">
												<div class="accordion-inner">
                                                    <?php echo $lang['FaqPageTab2Text2'];?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_4">
									<div class="accordion in collapse" id="accordion4" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_1">
													<?php echo $lang['FaqPageTab3SubTitle1'];?>
												</a>
											</div>
											<div id="collapse_4_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab3Text1'];?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_5">
									<div class="accordion in collapse" id="accordion5" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse_5_1">
													<?php echo $lang['FaqPageTab4SubTitle1'];?>
												</a>
											</div>
											<div id="collapse_5_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab4SubText1'];?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_3">
									<div class="accordion in collapse" id="accordion3" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
													<?php echo $lang['FaqPageTab5SubTitle1'];?>
												</a>
											</div>
											<div id="collapse_3_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<?php echo $lang['FaqPageTab5SubText1'];?>
												</div>
											</div>
										</div>
									</div></div>
									</div>
								</div>
							</div>
						</div>
						<!--end span9-->                                   
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER--> 
		</div>
		<!-- END PAGE -->    
	</div>
	  <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<script src="assets/scripts/app.js"></script>      
	<script>
		jQuery(document).ready(function() {    
		   App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
<?include('footer.php');?>