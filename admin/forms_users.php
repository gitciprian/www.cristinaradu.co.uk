<?include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->

<body class="page-header-fixed" onload="startTime()"> 
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<?include('bara_sus.php');?>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<?include('meniu.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"><br/>	<br/>	
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php">Home</a><i class="icon-angle-right"></i></li>
							<li><a href="#">Cereri din site</a></li>
							<li class="pull-right no-text-shadow"><i class="icon-calendar"></i><span><?echo luni_in_romana($luna_in_litere);?></span>	</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
				
				
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Cereri venite din formulare site</div>
								<div class="actions">
								<!--	<a href="#" class="btn blue"><i class="icon-pencil"></i> Add</a> -->
									<div class="btn-group">
										<a class="btn green" href="#" data-toggle="dropdown"><i class="icon-cogs"></i> Export <i class="icon-angle-down"></i></a>
										<ul class="dropdown-menu pull-right">
										<?/*
											<li><a href="#"><i class="icon-pencil"></i> Exporta ...</a></li>
											<li><a href="#"><i class="icon-trash"></i> Sterge</a></li>
											<li><a href="#"><i class="icon-ban-circle"></i> Ban</a></li>
											<li class="divider"></li>
										*/?>
											<li><a href="exporta_utilizatori.php?tip=custom&i=30"><i class="i"></i> Exporta datele la xls</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th></th>
											<th style="width:8px;">Id</th>
											<th>Detalii client</th>
											<th class="hidden-480">Detalii cerere</th>
											<th>Status</th>
											<th style="width:140px">Actiune</th>
											
											<!-- culori status --
												 <span class="label label-success">Approved</span>
											     <span class="label label-warning">Suspended</span>
											     <span class="label label-inverse">Blocked</span>
											-->
										</tr>
									</thead>
									<tbody>
									<?php
					                 	$sqlp="SELECT *  from abonati order by id DESC";
										$resultp=mysql_query($sqlp);
										while($rows=mysql_fetch_array($resultp)){?>
											<tr class="odd gradeX">
												<td style="width:1px"></td>
												<td><?echo $rows['id'];?></td>
												<td>
													<?echo $rows['nume_client'];?><br/>
													<?if($rows['companie_client']!=""){echo $rows['companie_client']."<br/>";}?>
													<?if($rows['telefon_client']!=""){echo $rows['telefon_client']."<br/>";}?>													
												</td>
												<td class="hidden-480">
													Tip cerere: <?echo ucfirst(str_replace("-"," ",$rows['tip_formular']));?><br/>
													Din data: <?echo Datasiora($rows['data_inregistrare']);?><br/>													
												<a href="mailto:<?echo $rows['email_client'];?>"><?echo $rows['email_client'];?></a></td>
												<td>
													<?if($rows['status']=="noua"){?><span class="label label-success">Cerere noua</span><?}?>
													<?if($rows['status']=="procesare"){?><span class="label label-warning">Cerere in lucru</span><?}?>
													<?if($rows['status']=="finalizata"){?><span class="label label-inverse">Cerere finalizata</span><?}?>
												</td>
												<td>
													<a href="afisare.php?afisare=exporta_pdf&id=<?php echo $rows['id'];?>" target="_blank" title="Vezi detalii complete cerere" onclick="NewWindow(this.href,'name','800','700','yes');return false" class="btn green icn-only"><i class="icon-user icon-white"></i></a>
													<a href="afisare.php?afisare=mod_status_cerere&id=<?php echo $rows['id'];?>" title="Modifica status cerere" onclick="NewWindow(this.href,'name','600','500','yes');return false" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></a>													
													<a class="btn red icn-only" href="actiune.php?pagini=sterge_cerere_site&id=<?php echo $rows['id'];?>" title="Sterge aceasta cerere" onclick="return confirm( 'Sigur, sigur doresti stergerea acestei cereri?  Exista si varianta editarii statusului.' )"><i class="icon-remove icon-white"></i></a>
												</td>																					
											</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->


				
				
				<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/table-managed.js"></script>     
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableManaged.init();
		});
	</script>
	<?include('footer.php');?>