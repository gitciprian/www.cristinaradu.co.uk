<?php
//include database class
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}
include_once 'db.php';
$db = new DB();
$id = $_GET['id'];
?>
<!DOCTYPE HTML>
<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drag&amp;Drop Reorder</title>
    <link href="assets/css/image-order.css" rel="stylesheet" type="text/css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript">
        $(document).ready(function(){
            $('.reorder_link').on('click',function(){
                $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
                $('.reorder_link').html('Salveaza ordonare');
                $('.reorder_link').attr("id","save_reorder");
                $('#reorder-helper').slideDown('slow');
                $('.image_link').attr("href","javascript:void(0);");
                $('.image_link').css("cursor","move");
                $("#save_reorder").click(function( e ){
                    if( !$("#save_reorder i").length ){
                        $(this).html('').prepend('<img src="images/refresh-animated.gif"/>');
                        $("ul.reorder-photos-list").sortable('destroy');
                        $("#reorder-helper").html( "Se reorganizeaza imaginile - Aceasta operatie poate dura cateva momente. Nu inchideti pagina pana cand nu aveti confirmarea finala." ).removeClass('light_box').addClass('notice notice_error');

                        var h = [];
                        $("ul.reorder-photos-list li").each(function() {  h.push($(this).attr('id').substr(9));  });

                        $.ajax({
                            type: "POST",
                            url: "orderUpdate.php",
                            data: {ids: " " + h + ""},
                            success: function(){
                                window.location.reload();
                            }
                        });
                        return false;
                    }
                    e.preventDefault();
                });
            });
        });
	</script>
</head>
<body>
<div style="text-align: center; margin:50px auto">
	<a href="javascript:void(0);" class="btn outlined mleft_no reorder_link" id="save_reorder">Ordoneaza imaginile</a>
	<a href="galerie.php?id=<?php echo $_GET['id'];?>&w=<?php echo $_GET['w'];?>&np=<?php echo $_GET['np'];?>" class="btn" id="save_reorder">Inapoi la galerie</a>
	<div id="reorder-helper" class="light_box" style="display:none;">1. Click pe fotografie si pozitionati unde doriti.<br>2. Click 'Salveaza ordonare' la final.</div>
	<div class="gallery">
		<ul class="reorder_ul reorder-photos-list">
			<?php
			//Fetch all images from database
			$images = $db->getRows($id);
			if(!empty($images)){
				foreach($images as $row){?>
					<li id="image_li_<?php echo $row['id']; ?>" class="ui-sortable-handle"><a href="javascript:void(0);" style="float:none;" class="image_link"><img src="https://<?php echo $_SERVER['HTTP_HOST']."/public/images/gallery/thumbs/".$row['image']; ?>" alt=""></a></li>
				<?php } } ?>
		</ul>
	</div>
</div>
</body>
</html>
