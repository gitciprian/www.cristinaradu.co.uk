<?include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<body class="page-header-fixed">
	<div class="header navbar navbar-inverse navbar-fixed-top">	
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container row-fluid">	
		<div class="page-sidebar nav-collapse collapse">			     
			<?include('meniu.php');?>	
			<?
			 if(isset($_GET['id'])){$id_parinte=$_GET['id'];}else{$id_parinte=$id_utilizator;}	
			 
				$sql_parinte_actual="SELECT *  from u_parinti where id_utilizator='$id_parinte'";					
				$result_parinte_actual=mysql_query($sql_parinte_actual);
				$row_parinte_actual=mysql_fetch_array($result_parinte_actual);

				 $nume_utilizator=$row_parinte_actual['nume_utilizator'];	 
				 $tip_utilizator=$row_parinte_actual['tip_utilizator'];	 
				 $email_utilizator=$row_parinte_actual['email_utilizator'];	 
				 $cnp_utilizator=$row_parinte_actual['cnp_utilizator'];	 
				 $imagine=$row_parinte_actual['imagine'];	 
				 $ocupatie=$row_parinte_actual['ocupatie'];	 
				 $data_activare_cont=$row_parinte_actual['data_activare_cont'];	 
				 $tara=$row_parinte_actual['tara'];	 
				 $judet=$row_parinte_actual['judet'];	 
				 $adresa=$row_parinte_actual['adresa'];	 
				 $descriere_profil=$row_parinte_actual['descriere_profil'];	 
				 $telefon=$row_parinte_actual['telefon'];	 
				 $email_secundar=$row_parinte_actual['email_secundar'];
				 $notificare_evenimente=$row_parinte_actual['notificare_evenimente'];
				 $notificare_plata=$row_parinte_actual['notificare_plata'];
				 $notificare_optionale=$row_parinte_actual['notificare_optionale'];
				 $nume_secundar=$row_parinte_actual['nume_secundar'];
				 $telefon_secundar=$row_parinte_actual['telefon_secundar'];
				 				 
				 
				 	$query_copil="SELECT count(id) as total_record  from u_copii where id_parinte='$id_parinte'";
		 			$res_copil=mysql_query($query_copil) or die(mysql_error());
	     			$row_copil=mysql_fetch_array($res_copil); 
		 			$numar_copii=$row_copil['total_record'];
		 			if($numar_copii=="0"){$nmc="Nu exista copii inregistrati pe acest cont";}
		 			if($numar_copii=="1"){$nmc="1 copil inregistrat";}
		 			if($numar_copii>"1"){$nmc=$numar_copii." copii inregistrati";}
		 		?>
					
		</div>		
		<div class="page-content">
			<div class="container-fluid">			
				<div class="row-fluid">
					<div class="span12">
						<h3 class="page-title">Profilul dvs. <small> <?echo $nume_utilizator;?></small></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i> <a href="index.php">Home</a> <i class="icon-angle-right"></i></li>
							<li><a href="#">Profilul meu</a><i class="icon-angle-right"></i></li>							
						</ul>						
					</div>
				</div>
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom tabbable-full-width">
							<ul class="nav nav-tabs"><?/* class="active"*/?>								
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Date generale</a></li>
								<li><a href="#tab_1_2" data-toggle="tab">Informatii</a></li>
								<li><a href="#tab_1_3" data-toggle="tab">Setari cont</a></li>
								<?/*<li><a href="#tab_1_4" data-toggle="tab">Optionale pentru copilul meu</a></li>	*/?>							
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li><img src="assets/img/profile/profile-img.png" alt="" /> <?/*<a href="#" class="profile-edit">shimba</a>*/?></li>										
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<h1><?echo $nume_utilizator;?></h1>
												<?echo $descriere_profil;?><br/>
												<p><a href="http://www.art-play.ro.ro">www.art-play.ro.ro</a></p>
												<ul class="unstyled inline">
													<li><i class="icon-map-marker"></i> <?echo $tara;?></li>
													<li><i class="icon-map-marker"></i> <?echo $judet;?></li>
													<li><i class="icon-map-marker"></i> <?echo $adresa;?></li>
													<li><i class="icon-calendar"></i> Inregistrat la <?echo Datasiora($data_activare_cont);?></li>
													<li><i class="icon-briefcase"></i> <?echo $ocupatie;?></li>													
													<li><i class="icon-heart"></i> <?echo $nmc;?></li>
												</ul>
											</div>
											<!--end span8-->
											<div class="span4">
												<div class="portlet sale-summary">
													<div class="portlet-title">
														<div class="caption">Parinte pentru</div>
														<div class="tools"><a class="reload" href="javascript:;"></a></div>
													</div>
													<div class="portlet-body">
													<?if($numar_copii>"0"){?>
														<ul class="unstyled">		
														<?	$sql_copil="select id, id_grupa, nume_copil from u_copii where id_parinte='$id_parinte'";
															$result_copil=mysql_query($sql_copil);
															while($row_copil=mysql_fetch_array($result_copil)){ $id_grupa=$row_copil['id_grupa'];															
																$sql_grupa="select id, nume_grupa from parametri_grupe where id='$id_grupa'";
																$result_grupa=mysql_query($sql_grupa);
																$row_grupa=mysql_fetch_array($result_grupa);?>											
															<li>
																<span class="sale-info"><?echo $row_copil['nume_copil'];?> <i class="icon-img-up"></i></span> 
																<span class="sale-num"><?echo $row_grupa['nume_grupa'];?></span>
															</li>
															<?}?>
														</ul>		
													<?}else{?>Nu sunt copii inregistrati pentru acest parinte<?}?>												
													</div><br/><br/><br/>
												</div>
											</div>
											<!--end span4-->
										</div>
										<!--end row-fluid-->		
										
										<?if($numar_copii>"0"){?>								
										<div class="tabbable tabbable-custom tabbable-custom-profile">											
											<ul class="nav nav-tabs">
												<?	$fgfgfgf=1; $fgfdsgfgf=1;
													$sql_copil="select id, id_grupa, nume_copil from u_copii where id_parinte='$id_parinte'";
													$result_copil=mysql_query($sql_copil);
													while($row_copil=mysql_fetch_array($result_copil)){ $id_grupa=$row_copil['id_grupa'];
												?>
												<li <? if($fgfgfgf++=="1"){?>class="active"<?}?>><a href="#tab_1_1<?echo $fgfdsgfgf++;?>" data-toggle="tab">Optionale selectate pentru <?echo $row_copil['nume_copil'];?></a></li>
												<?}?>												
											</ul>
											
											<div class="tab-content">
											<?	$fgfgfgf_re=1; $fgfdsgfgf_re=1;
												$sql_copil="select id, id_grupa, nume_copil from u_copii where id_parinte='$id_parinte'";
												$result_copil=mysql_query($sql_copil);
												while($row_copil=mysql_fetch_array($result_copil)){ $id_grupa=$row_copil['id_grupa'];
											?>
												<div class="tab-pane <? if($fgfgfgf_re++=="1"){?>active<?}?>" id="tab_1_1<?echo $fgfdsgfgf_re++;?>">
													<div class="portlet-body" style="display: block;">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th width="120"><i class="icon-briefcase"></i> Activitate</th>
																	<th class="hidden-phone"><i class="icon-question-sign"></i> Descriere</th>
																	<th width="120"><i class="icon-bookmark"></i> Tarif</th>																	
																</tr>
															</thead>
															<tbody>
															<?	$sql_act="select * from repartizare_copii_optiuni where id_parinte='$id_parinte' and  id_copil='{$row_copil['id']}'";
																$result_act=mysql_query($sql_act);
																while($row_act=mysql_fetch_array($result_act)){	
																	$id_optiune=$row_act['id_optiune'];
																	
																	$sql_grupa_opt="select id, nume_activitate, pret_activitate,descriere from parametri_optionale where id='$id_optiune'";
																	$result_grupa_opt=mysql_query($sql_grupa_opt);
																	$row_grupa_opt=mysql_fetch_array($result_grupa_opt);?>	
																<tr>
																	<td><a href="#"><?echo $id_optiune;?></a></td>
																	<td class="hidden-phone"><b><?echo $row_grupa_opt['nume_activitate'];?></b> (<?echo $row_grupa_opt['descriere'];?>)</td>
																	<td><?echo $row_grupa_opt['pret_activitate'];?> lei <span class="label label-success label-mini">Inclus</span></td>																
																</tr>
															<?}?>
															</tbody>
														</table>
													</div>
												</div>											
												<?}?>
											</div>
										</div><?}?>								
									<!--end span9-->
								</div></div>
								<!--end tab-pane-->
								<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
									<div class="span2"><img src="assets/img/profile/profile-img.png" alt="" /> <?/*<a href="#" class="profile-edit">editeaza</a>*/?></div>
									<ul class="unstyled span10">
										<li>Contact parinte</li>
										<li><span>Numele dvs:</span> <?echo $nume_utilizator;?></li>										
										<li><span>Tara:</span> <?echo $tara;?></li>
										<li><span>Localitate:</span> <?echo $judet;?></li>
										<li><span>Localitate:</span> <?echo $adresa;?></li>
										<li><span>Ocupatia:</span> <?echo $ocupatie;?></li>
										<li><span>Email:</span> <a href="mailto:<?echo $email_utilizator;?>"><?echo $email_utilizator;?></a></li>										
										<li><span>Telefon:</span> <?echo $telefon;?></li>
										<li><span>Meniuni:</span> <?echo $nmc;?></li>
										<li>&nbsp;</li>
										<li>Contact secundar</li>
										<li><span>Nume:</span> <?echo $nume_secundar;?></li>
										<li><span>Email:</span> <a href="mailto:<?echo $email_secundar;?>"><?echo $email_secundar;?></a></li>										
										<li><span>Telefon:</span> <?echo $telefon_secundar;?></li>
									</ul>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active"><a data-toggle="tab" href="#tab_1-1"><i class="icon-cog"></i> Date personale</a><span class="after"></span></li>
												<?/*	<li ><a data-toggle="tab" href="#tab_2-2"><i class="icon-picture"></i> Schimba imagine</a></li> */?>
													<li ><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Schimba parola</a></li>
													<li ><a data-toggle="tab" href="#tab_4-4"><i class="icon-eye-open"></i> Mentiuni speciale</a></li>
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
															<form action="actiune.php?pagini=editeaza_parinte_scurt" method="post" onsubmit="target_popup(this)">
															<input type="hidden" name="id_parinte" value="<?echo $id_parinte;?>"/>
															<p>Datele de mai jos sunt necesare pentru informari si contacte daca nevoia o cere. Va rugam furnizati date valide.</p><br/>
																<label class="control-label">Nume parinte*</label>
																<input type="text" name="nume_utilizator" value="<?echo $nume_utilizator;?>" placeholder="Numele dvs" class="m-wrap span8" required />
																<label class="control-label">CNP parinte*</label>
																<input type="text" name="cnp_utilizator" value="<?echo $cnp_utilizator;?>" placeholder="CNP dvs" class="m-wrap span8" required />
																<label class="control-label">Email*</label>
																<input type="email" name="email_utilizator" placeholder="Email" value="<?echo $email_utilizator;?>" class="m-wrap span8" required/>
																<label class="control-label">Telefon*</label>
																<input type="text"  name="telefon" placeholder="Tefelon" value="<?echo $telefon;?>" class="m-wrap span8" required/>																
																<label class="control-label">Occupation</label>
																<input type="text" placeholder="Ocupatia" name="ocupatie" value="<?echo $ocupatie;?>" class="m-wrap span8" />
																<label class="control-label">Tara</label>
																<input type="text" name="tara" value="<?echo $tara;?>" class="m-wrap span8" />
																<label class="control-label">Localitate</label>
																<input type="text" name="judet" value="<?echo $judet;?>"  class="m-wrap span8" />
																<label class="control-label">Adresa</label>
																<input type="text" name="adresa" value="<?echo $adresa;?>"  class="m-wrap span8">
																<br/>
																<label class="control-label">Contact secundar</label>
																<label class="control-label">Nume contact</label>
																<input type="text" placeholder="Daca este cazul" name="nume_secundar" value="<?echo $nume_secundar;?>" class="m-wrap span8" />
																<label class="control-label">Email contact</label>
																<input type="email" placeholder="Daca este cazul" name="email_secundar" value="<?echo $email_secundar;?>" class="m-wrap span8"/>
																<label class="control-label">Telefon Contact</label>
																<input type="text" placeholder="Daca este cazul" name="telefon_secundar" value="<?echo $telefon_secundar;?>"class="m-wrap span8"/>		
																
																<div class="submit-btn">
																	<input type="submit" class="btn green" value="Modifica date"/>
																	<input type="reset" class="btn" value="Renunt"/>
																</div>
															</form>
														</div>
													</div>
													<?/*
													<div id="tab_2-2" class="tab-pane">
														<div style="height: auto;" id="accordion2-2" class="accordion collapse">
															<form action="actiune.php?pagini=editeaza_parinte_scurt" method="post" onsubmit="target_popup(this)">
																<p>Datele de mai jos sunt necesare pentru informari si contacte daca nevoia o cere. Va rugam furnizati date valide</p>
																<br />
																<div class="controls">
																	<div class="thumbnail" style="width: 291px; height: 170px;">
																		<img src="http://www.placehold.it/291x170/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
																	</div>
																</div>
																<div class="space10"></div>
																<div class="fileupload fileupload-new" data-provides="fileupload">
																	<div class="input-append">
																		<div class="uneditable-input">
																			<i class="icon-file fileupload-exists"></i> 
																			<span class="fileupload-preview"></span>
																		</div>
																		<span class="btn btn-file">
																		<span class="fileupload-new">Select file</span>
																		<span class="fileupload-exists">Change</span>
																		<input type="file" class="default" />
																		</span>
																		<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
																	</div>
																</div>
																<div class="clearfix"></div>
																<div class="controls">
																	<span class="label label-important">NOTE!</span>
																	<span>You can write some information here..</span>
																</div>
																<div class="space10"></div>
																<div class="submit-btn">
																	<a href="#" class="btn green">Submit</a>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													*/?>
													<div id="tab_3-3" class="tab-pane">
														<div style="height: auto;" id="accordion3-3" class="accordion collapse">														
															<script>
															function checkPass()
															{
															    //Store the password field objects into variables ...
															    var pass1 = document.getElementById('pass1');
															    var pass2 = document.getElementById('pass2');
															    //Store the Confimation Message Object ...
															    var message = document.getElementById('confirmMessage');
															    //Set the colors we will be using ...
															    var goodColor = "#66cc66";
															    var badColor = "#ff6666";
															    //Compare the values in the password field 
															    //and the confirmation field
															    if(pass1.value == pass2.value){
															        //The passwords match. 
															        //Set the color to the good color and inform
															        //the user that they have entered the correct password 
															        pass2.style.backgroundColor = goodColor;
															        message.style.color = goodColor;
															        message.innerHTML = "Parolele se potrivesc!"
															    }else{
															        //The passwords do not match.
															        //Set the color to the bad color and
															        //notify the user.
															        pass2.style.backgroundColor = badColor;
															        message.style.color = badColor;
															        message.innerHTML = "Parolele nu sunt identice!"
															    }
															}  
															</script>
															<form action="settings.php" method="post" name = "myForm" onsubmit="target_popup(this)">
																<input name="id" type="hidden" value="<?echo $id_parinte;?>">
																<input name="Submit" type="hidden" value="Schimba">
																<label class="control-label">Parola actuala</label>
																<input name="oldpwd" type="password" class="m-wrap span8" required/>
																<label class="control-label">Parola noua</label>
																<input name="newpwd" type="password" class="m-wrap span8"  id="pass1" required/>
																<label class="control-label">Rescrie parola noua</label>
																<input name="newpwd2" type="password" class="m-wrap span8" required id="pass2" onkeyup="checkPass(); return false;"/><br/>
																<span id="confirmMessage" class="confirmMessage"></span><br/><br/>
																<div class="submit-btn">
																	<input type="submit" class="btn green" value="Schimba parola"/>
																	<input type="reset" class="btn" value="Renunt"/>
																</div>
															</form>
															
														</div>
													</div>
													<div id="tab_4-4" class="tab-pane">
														<div style="height: auto;" id="accordion4-4" class="accordion collapse">
															<form action="actiune.php?pagini=notificari_parinti" method="post" onsubmit="target_popup(this)">
															<input type="hidden" name="id_parinte" value="<?echo $id_parinte;?>"/>
															
																<div class="profile-settings row-fluid">
																	<div class="span9"><p>Doresc sa primesc notificari atunci cand se organizeaza evenimente</p></div>
																	<div class="control-group span3">
																		<div class="controls">
																			<label class="radio"><input type="radio" name="notificare_evenimente" value="da" <?if($notificare_evenimente=="da"){?>checked<?}?> />da</label>
																			<label class="radio"><input type="radio" name="notificare_evenimente" value="nu" <?if($notificare_evenimente=="nu"){?>checked<?}?> />nu</label>  
																		</div>
																	</div>
																</div>
																<div class="profile-settings row-fluid">
																	<div class="span9"><p>Doresc sa primesc notificari de plata (la final de luna)</p></div>
																	<div class="control-group span3">
																		<div class="controls">
																			<label class="radio"><input type="radio" name="notificare_plata" value="da" <?if($notificare_plata=="da"){?>checked<?}?> />da</label>
																			<label class="radio"><input type="radio" name="notificare_plata" value="nu" <?if($notificare_plata=="nu"){?>checked<?}?> />nu</label>  
																		</div>
																	</div>
																</div>
																<div class="profile-settings row-fluid">
																	<div class="span9"><p>Doresc sa primesc notificari privind activitatile optionale</p></div>
																	<div class="control-group span3">
																		<div class="controls">
																			<label class="radio"><input type="radio" name="notificare_optionale" value="da" <?if($notificare_optionale=="da"){?>checked<?}?> />da</label>
																			<label class="radio"><input type="radio" name="notificare_optionale" value="nu" <?if($notificare_optionale=="nu"){?>checked<?}?> />nu</label>  
																		</div>
																	</div>
																</div>
																
																<div class="space5"></div>
																<div class="submit-btn">
																	<input type="submit" class="btn green" value="Schimba setari"/>
																	<input type="reset" class="btn" value="Renunt"/>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
															<script>
															function target_popup(form) {
															    window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
															    form.target = 'formpopup';
															}
															</script>
								<!--end tab-pane-->
								<?/*
								<div class="tab-pane" id="tab_1_4">							
									<div class="row-fluid add-portfolio">
										<div class="pull-left">
											<span>Lista activitatilor optionale.</span>											
										</div>							
									</div>	
									<p>Se pot alege optionalele dorite din lista de mai jos, tot ce trebuie sa faceti este sa dati click pe butonul Adauga/Modifica si optiunea va fi adaugata sau scoasa.<br/> Se vor alege pentru fiecare copil in parte.</p><br/>								
										<?	$sql_act="select * from parametri_optionale where activa='da' order by id desc";
											$result_act=mysql_query($sql_act);
											while($row_act=mysql_fetch_array($result_act)){												
											
												$queryid="SELECT count(id) as total_record from repartizare_copii_optiuni where id_optiune='{$row_act['id']}' ";
											 	$resid=mysql_query($queryid) or die(mysql_error());
										     	$rowid=mysql_fetch_array($resid); 
											 	$nrc44tpate=$rowid['total_record'];
										 
										?>										
									<!--end add-portfolio-->
									<div class="row-fluid portfolio-block">
										<div class="span5 portfolio-text">
											<img src="assets/img/profile/portfolio/kids-icon.png" alt="" />
											<div class="portfolio-text-info">
												<h4><?echo $row_act['nume_activitate'];?></h4>
												<p><?echo nl2br($row_act['descriere']);?></p>
											</div>
										</div>
										<div class="span5" style="overflow:hidden;">
											<div class="portfolio-info">
												Data<br/>
												Start <?echo Datasc($row_act['data_start_activitate']);?> <br/>Final <?echo Datasc($row_act['data_end_activitate']);?>
											</div>
											<div class="portfolio-info">
												Participanti
												<span><?echo $nrc44tpate;?></span>
											</div>
											<div class="portfolio-info">
												Pret/copil
												<span><?echo $row_act['pret_activitate'];?> lei</span>
											</div>
										</div>
										<div class="span2 portfolio-btn">
											<?
												$queryid="SELECT count(id) as total_record from repartizare_copii_optiuni where id_parinte='$id_parinte' and  id_optiune='{$row_act['id']}' ";
											 	$resid=mysql_query($queryid) or die(mysql_error());
										     	$rowid=mysql_fetch_array($resid); 
											 	$nrc44=$rowid['total_record'];
										 	?>
											<a href="afisare.php?afisare=repartizare_optionale&idp=<?echo $id_parinte;?>&ido=<?echo $row_act['id'];?>&no=<?echo base64_encode($row_act['nume_activitate']);?>" onclick="NewWindow(this.href,'name','700','500','yes');return false" class="btn bigicn-only"><?if($nrc44>"0"){?><span>Modifica</span><?}else{?><span>Adauga</span><?}?></a>                      
										</div>
									</div>
									<!--end add-portfolio-->									
									<?}?>
								</div>
								*/?>
								                            
										</div>
									</div>
								</div>
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER--> 
		</div>
		<!-- END PAGE -->    
	</div>
	 <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>      
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
		});
	</script>
<?include('footer.php');?>