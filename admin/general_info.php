<?include("top_admin.php");
$sqlp       = "select * from general_info where id='1'";
$resultp    = mysqli_query($mysqli,$sqlp);
$row_general= mysqli_fetch_array($resultp);?>

<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">				
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title">General variables <small> texts and values</small>
						</h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php">Dashboard</a><span class="icon-angle-right"></span></li>
							<li><a href="#">General info</a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>	
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<div class="span3">
							<ul class="ver-inline-menu tabbable margin-bottom-10">
								<li  class="active"><a href="#tab_1" data-toggle="tab"><i class="icon-briefcase"></i> Front page and images</a><span class="after"></span></li>
								<li><a href="#tab_2" data-toggle="tab"><i class="icon-leaf"></i> Tracking codes</a></li>
								<li><a href="#tab_3" data-toggle="tab"><i class="icon-info-sign"></i> Forms</a></li>
								<li><a href="#tab_5" data-toggle="tab"><i class="icon-gift"></i> General contact details</a></li>
							</ul>
						</div>
						<div class="span9">
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="accordion in collapse" id="accordion1" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1"> Front page data</a>
											</div>
											<div id="collapse_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<form action="actiune.php?pagini=general_1" class="form-horizontal" method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id" value="1" />

														<h4>Meta data</h4>

														<div class="control-group">
															<label class="control-label">Page Meta title (SEO) [RO]</label>
															<div class="controls">
																<input type="text" name="meta_title" value="<?php echo  $row_general['meta_title'];?>" class="span9 m-wrap popovers" maxlength="256" data-trigger="hover" data-content="Meta title - very important for SEO" data-original-title="Meta title SEO" />
															</div>
														</div>

                                                        <div class="control-group">
                                                            <label class="control-label">Page Meta title (SEO) [EN]</label>
                                                            <div class="controls">
                                                                <input type="text" name="meta_title_en" value="<?php echo  $row_general['meta_title_en'];?>" class="span9 m-wrap popovers" maxlength="256" data-trigger="hover" data-content="Meta title - very important for SEO" data-original-title="Meta title SEO" />
                                                            </div>
                                                        </div>

														<div class="control-group">
															<label class="control-label">Meta description (SEO) [RO]</label>
															<div class="controls">
																<input type="text" name="meta_description" value='<?php echo  $row_general['meta_description'];?>' class="span9 m-wrap popovers" maxlength="256" data-trigger="hover" data-content="Meta description. Hidden for visitors - visible for search engines" data-original-title="Meta Description SEO" />
															</div>
														</div>

                                                        <div class="control-group">
                                                            <label class="control-label">Meta description (SEO) [EN]</label>
                                                            <div class="controls">
                                                                <input type="text" name="meta_description_en" value='<?php echo  $row_general['meta_description_en'];?>' class="span9 m-wrap popovers" maxlength="256" data-trigger="hover" data-content="Meta description. Hidden for visitors - visible for search engines" data-original-title="Meta Description SEO" />
                                                            </div>
                                                        </div>
														<hr/>
                                                        <?php /*
                                                            <h4>Imagini despre noi</h4>
                                                            <a href="edit_img_art_central.php?id=1|3|750|380|general_info|about_banner_1"  target="_blank" class="btn blue">Incarca banner (about 1 750x380px)</a><br/><br/>
                                                            <img src="http://www.agro-star-sud.ro/public/images/<?php echo $row_general['about_banner_1'];?>" class="img-responsive">
                                                            <div class="clearfix"></div><br/>
                                                            <a href="edit_img_art_central.php?id=1|3|750|380|general_info|about_banner_2"  target="_blank" class="btn blue">Incarca banner (about 2 750x380px)</a><br/><br/>
                                                            <img src="http://www.agro-star-sud.ro/public/images/<?php echo $row_general['about_banner_2'];?>" class="img-responsive">
                                                            <div class="clearfix"></div><br/>
                                                            <a href="edit_img_art_central.php?id=1|3|750|380|general_info|about_banner_3"  target="_blank" class="btn blue">Incarca banner (about 3 750x380px)</a><br/><br/>
                                                            <img src="http://www.agro-star-sud.ro/public/images/<?php echo $row_general['about_banner_3'];?>" class="img-responsive">
                                                            <div class="clearfix"></div><br/>
                                                            <a href="edit_img_art_central.php?id=1|3|750|380|general_info|about_banner_4"  target="_blank" class="btn blue">Incarca banner (about 4 750x380px)</a><br/><br/>
                                                            <img src="http://www.agro-star-sud.ro/public/images/<?php echo $row_general['about_banner_4'];?>" class="img-responsive">
                                                            <div class="clearfix"></div><br/>
                                                            <br/>
                                                        */?>

														<div class="form-actions">
                                                            <button type="submit" class="btn blue">Update details</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="reset" class="btn">Reset</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_2">
									<div class="accordion in collapse" id="accordion2" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">Trackers and footer codes</a>
											</div>
											<div id="collapse_2_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<form action="actiune.php?pagini=general_2" class="form-horizontal" method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id" value="1" />													
														<br/><br/>														
														<div class="control-group">
															<label class="control-label">Webmaster Tools</label>
															<div class="controls">
																<input type="text" name="webmaster_code" value="<?php echo  $row_general['webmaster_code'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Webmaster Tools meta" data-original-title="Optional" />
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Analitycs and other trackers</label>
															<div class="controls">
																<textarea name="trackers_code" class="span9 m-wrap popovers"  style="height:100px" data-trigger="hover" data-content="This section will be present on all page" data-original-title="Website traffic monitoring" ><?php echo  $row_general['trackers_code'];?></textarea>
															</div>
														</div>
														<div class="form-actions">
                                                            <button type="submit" class="btn blue">Update details</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button type="reset" class="btn">Reset</button>
                                                        </div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="tab-pane" id="tab_3">
									<div class="accordion in collapse" id="accordion3" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">Automated response for forms</a>
											</div>											
											<div id="collapse_3_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<form action="actiune.php?pagini=general_3" class="form-horizontal" method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id" value="1" />

														<div class="control-group">	
															<h4>Email and auto-response content</h4>
														</div>										
														<div class="control-group">
															<label class="control-label">Reply to<br/> (auto-response sent from)</label>
															<div class="controls">
																<input type="text" name="auto_response_email" value="<?php echo  $row_general['auto_response_email'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="From this address the auto-response will be sent" data-original-title="Email address" />
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Auto-response (RO)</label>
															<div class="controls">
																<textarea name="auto_response_content" class="span9 m-wrap popovers"  style="height:200px" data-trigger="hover" data-content="This content will serve as a auto-response for those who ask for an enquiry" data-original-title="Auto-response message" ><?php echo  $row_general['auto_response_content'];?></textarea>
															</div>
														</div>

                                                        <div class="control-group">
															<label class="control-label">Auto-response (EN)</label>
															<div class="controls">
																<textarea name="auto_response_content_en" class="span9 m-wrap popovers"  style="height:200px" data-trigger="hover" data-content="This content will serve as a auto-response for those who ask for an enquiry" data-original-title="Auto-response message" ><?php echo  $row_general['auto_response_content_en'];?></textarea>
															</div>
														</div>

														<div class="form-actions">
															<button type="submit" class="btn blue">Update details</button>&nbsp;&nbsp;&nbsp;&nbsp;
															<button type="reset" class="btn">Reset</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="tab-pane" id="tab_5">
									<div class="accordion in collapse" id="accordion2" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse_5_1"> General contact details</a>
											</div>
											<div id="collapse_5_1" class="accordion-body collapse in">
												<div class="accordion-inner">
													<form action="actiune.php?pagini=general_5" class="form-horizontal" method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id" value="1" />													
														<br/><br/>														
														<div class="control-group">
															<label class="control-label">Email sending address</label>
															<div class="controls">
																<input type="text" name="email_send_address" value="<?php echo  $row_general['email_send_address'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Default email address. From this address all the form auto-response will be sent." data-original-title="Form email address" />
															</div>
														</div>																													
														<div class="control-group">
															<label class="control-label">General phone</label>
															<div class="controls">
																<input type="text" name="general_phone" value="<?php echo  $row_general['general_phone'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in contact page" data-original-title="Main contact phone" />
															</div>
														</div>
                                                        <div class="control-group">
                                                            <label class="control-label">General mobile</label>
                                                            <div class="controls">
                                                                <input type="text" name="general_mobile" value="<?php echo  $row_general['general_mobile'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in contact page" data-original-title="Main mobile phone" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
															<label class="control-label">General contact email</label>
															<div class="controls">
																<input type="text" name="general_email" value="<?php echo  $row_general['general_email'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in contact page" data-original-title="General email " />
															</div>
														</div>


                                                        <div class="control-group">
                                                            <label class="control-label">Facebook URL</label>
                                                            <div class="controls">
                                                                <input type="text" name="link_facebook" value="<?php echo  $row_general['link_facebook'];?>" class="span9 m-wrap popovers" placeholder="https://" data-trigger="hover" data-content="Shown in contact page" data-original-title="Facebook URL" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Youtube URL</label>
                                                            <div class="controls">
                                                                <input type="text" name="link_youtube" value="<?php echo  $row_general['link_youtube'];?>" class="span9 m-wrap popovers" placeholder="https://" data-trigger="hover" data-content="Shown in contact page" data-original-title="Youtube URL" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Instagram URL</label>
                                                            <div class="controls">
                                                                <input type="text" name="link_instagram" value="<?php echo  $row_general['link_instagram'];?>" class="span9 m-wrap popovers" placeholder="https://" data-trigger="hover" data-content="Shown in contact page" data-original-title="Instagram URL" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Pinterest URL</label>
                                                            <div class="controls">
                                                                <input type="text" name="link_pinterest" value="<?php echo  $row_general['link_pinterest'];?>" class="span9 m-wrap popovers" placeholder="https://" data-trigger="hover" data-content="Shown in contact page" data-original-title="Pinterest URL" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Twitter URL</label>
                                                            <div class="controls">
                                                                <input type="text" name="link_twitter" value="<?php echo  $row_general['link_twitter'];?>" class="span9 m-wrap popovers" placeholder="https://" data-trigger="hover" data-content="Shown in contact page" data-original-title="Twitter URL" />
                                                            </div>
                                                        </div>

                                                        <div class="control-group">
                                                            <label class="control-label">General address (RO)</label>
                                                            <div class="controls">
                                                                <input type="text" name="general_address" value="<?php echo  $row_general['general_address'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in contact page" data-original-title="Main address" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">General address (EN)</label>
                                                            <div class="controls">
                                                                <input type="text" name="general_address_en" value="<?php echo  $row_general['general_address_en'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in contact page" data-original-title="Main address" />
                                                            </div>
                                                        </div>

                                                        <div class="control-group">
                                                            <label class="control-label">Footer slogan (RO)</label>
                                                            <div class="controls">
                                                                <input type="text" name="footer_slogan" value="<?php echo  $row_general['footer_slogan'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in footer" data-original-title="General footer slogan" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Footer slogan (EN)</label>
                                                            <div class="controls">
                                                                <input type="text" name="footer_slogan_en" value="<?php echo  $row_general['footer_slogan_en'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in footer" data-original-title="General footer slogan" />
                                                            </div>
                                                        </div>

                                                        <div class="control-group">
                                                            <label class="control-label">Footer general (RO)</label>
                                                            <div class="controls">
                                                                <input type="text" name="general_footer" value="<?php echo  $row_general['general_footer'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in footer" data-original-title="General footer text" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Footer general (EN)</label>
                                                            <div class="controls">
                                                                <input type="text" name="general_footer_en" value="<?php echo  $row_general['general_footer_en'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Shown in footer" data-original-title="General footer text" />
                                                            </div>
                                                        </div>


                                                        <div class="form-actions">
															<button type="submit" class="btn blue">Save details</button>&nbsp;&nbsp;&nbsp;&nbsp;
															<button type="reset" class="btn">Reset</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>

								<?php /*
								<div class="tab-pane" id="tab_4">
									<div class="accordion in collapse" id="accordion4" style="height: auto;">
										<div class="accordion-group">
											<div class="accordion-heading">
												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_1">Titluri si texte</a>
											</div>
											<div id="collapse_2_1" class="accordion-body collapse in">
												<div class="accordion-inner">
												<h4>Probabil or sa fie</h4>

													<form action="actiune.php?pagini=general_4" class="form-horizontal" method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id" value="1" />													
														<br/><br/>														
														<div class="control-group">
															<label class="control-label">Titlu "Activitati"</label>
															<div class="controls">
																<input type="text" name="titlu_activitati" value="<?php echo  $row_general['titlu_activitati'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare pe prima pagina sub continut. Langa articole" data-original-title="Titlul activitati" />
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Text "Activitati"</label>
															<div class="controls">
																<textarea name="text_activitati" class="span9 m-wrap popovers"  style="height:100px" data-trigger="hover" data-content="Text activitati" data-original-title="Text activitati" ><?php echo  $row_general['text_activitati'];?></textarea>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Titlu "Contacte"</label>
															<div class="controls">
																<input type="text" name="titlu_contacte" value="<?php echo  $row_general['titlu_contacte'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Aceasta va fi setata ca cea de pe care se vor trimite raspunsurile automate din formulare" data-original-title="Titlu contacte" />
															</div>
														</div>
														<div class="control-group">
															<label class="control-label">Titlu infomatii utile</label>
															<div class="controls">
																<input type="text" name="titlu_utile" value="<?php echo  $row_general['titlu_utile'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in footer site" data-original-title="Titlu informatii utile" />
															</div>
														</div>		
														<div class="control-group">
															<label class="control-label">Titlu servicii extra</label>
															<div class="controls">
																<input type="text" name="titlu_extra" value="<?php echo  $row_general['titlu_extra'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in footer site" data-original-title="Titlu servicii extra" />
															</div>
														</div>		
														<div class="control-group">
															<label class="control-label">Titlu programe lunare</label>
															<div class="controls">
																<input type="text" name="titlu_programe" value="<?php echo  $row_general['titlu_programe'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in footer site" data-original-title="Titlu programe lunare" />
															</div>
														</div>		
														<div class="control-group">
															<label class="control-label">Subitlu in pagina "oferta educationala"</label>
															<div class="controls">
																<input type="text" name="slogan_servicii" value="<?php echo  $row_general['slogan_servicii'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in pagina de servicii ca subtitlu" data-original-title="Subtitlu apgina" />
															</div>
														</div>																
														<div class="control-group">
															<label class="control-label">Galerie imagini 1</label>
															<div class="controls">
																<input type="text" name="titlu_galerie_1" value="<?php echo  $row_general['titlu_galerie_1'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in pagina de contact" data-original-title="Titlu galerie" />
															</div>
														</div>		
														<div class="control-group">
															<label class="control-label">Galerie imagini 2</label>
															<div class="controls">
																<input type="text" name="titlu_galerie_2" value="<?php echo  $row_general['titlu_galerie_2'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in pagina de contact" data-original-title="Titlu galerie" />
															</div>
														</div>	
														<div class="control-group">
															<label class="control-label">Galerie imagini 3</label>
															<div class="controls">
																<input type="text" name="titlu_galerie_3" value="<?php echo  $row_general['titlu_galerie_3'];?>" class="span9 m-wrap popovers"  data-trigger="hover" data-content="Apare in pagina de contact" data-original-title="Titlu galerie" />
															</div>
														</div>	
														<div class="form-actions">
															<button type="submit" class="btn blue">Modifica datele paginii</button>&nbsp;&nbsp;&nbsp;&nbsp;
															<button type="reset" class="btn">Reseteaza</button>     
														</div>
													</form>

												</div>
											</div>
										</div>
									</div>
								</div>
 							*/ ?>
							</div>
						</div>
						<!--end span9-->                          
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER--> 
		</div>
		<!-- END PAGE -->    
	</div>
	  <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<script src="assets/scripts/app.js"></script>      
	<script>
		jQuery(document).ready(function() {    
		   App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
<?include('footer.php');?>