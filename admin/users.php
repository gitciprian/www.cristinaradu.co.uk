<? include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->
<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">				
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['AllUsersMainTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['AllUsersMainSubtitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">	
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i><?php echo $lang['AllUsersBoxTitle'];?></div>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<?php $hh = "";?>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
									<thead>
										<tr>
											<th width="5%"><?php echo $lang['AllUsersTablehead1'];?></th>
											<th width="35%"><?php echo $lang['AllUsersTablehead2'];?></th>
											<th class="hidden-480" align="center"><?php echo $lang['AllUsersTablehead3'];?></th>
											<th class="hidden-480" align="center"><?php echo $lang['AllUsersTablehead4'];?></th>
											<th align="center"><?php echo $lang['AllUsersTablehead5'];?></th>
											<th style="display:none;"></th>
											<th style="display:none;"></th>																			
										</tr>
									</thead>									
									<tbody>
									 <?php
					                 	$sqlp       = "SELECT *  from u_login where user_type in ('1','2','3') order by id DESC";
										$resultp    = mysqli_query($mysqli, $sqlp) or die(mysqli_error());
										while($rows = mysqli_fetch_array($resultp)){

										 	$idt    = $rows['id'];
											$queryid="SELECT count(id) as total_record  from u_auth where user_id = '$idt'";
										 	$resid  = mysqli_query($mysqli, $queryid) or die(mysqli_error());
									     	$rowid  = mysqli_fetch_array($resid);
										 	$nrc    = $rowid['total_record'];

										 	?>
											<td><?echo $rows['id'];?></td>
											<td>
												<?echo $rows['user_name'];?><br/>
												<?echo $rows['user_email'];?><br/>
										<?php	if($nrc>"0"){
													$sqlpy      = "SELECT *  from u_auth where user_id ='$idt' order by user_id desc";
													$resultpy   = mysqli_query($mysqli, $sqlpy) or die(mysqli_error());
													$row_user   = mysqli_fetch_array($resultpy);?>
											        <?php echo $lang['AllUsersInfo1'];?>: <?php echo Datasiora($row_user['login_at']);?>
												<?}else{?>
											        <?php echo $lang['AllUsersInfo2'];?>
												<?}?>
											</td>
											<td class="hidden-480" align="center">
												<b><?php echo ($rows['user_type'] == 1 ? 'Admin' : 'User');?></b><br/>
												<?php echo $lang['AllUsersInfo3'];?>: <b><?php if($rows['user_permision'] == "1"){?><b>Yes/Da</b><?}else{?>No/Nu<?}?></b><br/>
												<?php echo $lang['AllUsersInfo4'];?>: <?echo Datasiora($rows['created_at']);?>
											</td>
											<td class="hidden-480" align="left">
												<?echo $rows['user_phone'];?><br/>
												<?echo $rows['secondary_email'];?><br/>
												<?echo $rows['user_country'];?> > <?echo $rows['user_address'];?><br/>
											</td>
											<td width="136">
												<div class="btn-toolbar">
													<div class="btn-group">
														<a class="btn green" href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> <i class="icon-user"></i> <?php echo $lang['AllUsersTablehead5'];?> <i class="icon-angle-down"></i></a>
														<ul class="dropdown-menu">
															<li><a href="add_user.php?id=<?php echo $rows['id'];?>" title="<?php echo $lang['AllUsersInfo6'];?>"><i class="icon-pencil"></i> <?php echo $lang['AllUsersInfo6'];?></a></li>
															<li><a href="actiune.php?pagini=block&id=<?php echo $rows['id'];?>&nr=<?php echo $rows['user_permision'];?>" title="<?php echo $lang['AllUsersInfo7'];?>" onclick="return confirm( '<?php echo $lang["AllUsersInfo8"];?>')"><i class="icon-ban-circle"></i> <?php echo $lang['AllUsersInfo9'];?></a></li>
														</ul>
													</div>												
												</div>
											</td>
											<td style="display:none;"><?php echo $lang['AllUsersInfo5'];?>:</td>
											<td style="display:none;"><p><?php echo $rows['user_notes'];?></p></td>
										</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/table-advanced.js"></script>     
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableAdvanced.init();
		});
	</script>
	<?include('footer.php');?>