<?include("top_admin.php");?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL STYLES -->
	<script type="text/javascript">
        function addFile(){
            var root=document.getElementById('mytab').getElementsByTagName('tr')[0].parentNode;
            var oR = cE('tr');var oC = cE('td');var oI = cE('input'); var oS=cE('span')
            cA(oI,'type','file');cA(oI,'name','file[]');
            oS.style.cursor='pointer';
            oS.onclick=function(){
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode)
            }
            oS.appendChild(document.createTextNode(' Sterge'));
            oC.appendChild(oI);oC.appendChild(oS);oR.appendChild(oC);root.appendChild(oR);
        }
        function cE(el){
            this.obj =document.createElement(el);
            return this.obj
        }
        function cA(obj,att,val){
            obj.setAttribute(att,val);
            return
        }
	</script>
	<script type='text/javascript'>
        function popup( url, width, height )
        {
            width = screen.width * width;
            height = screen.height * height;
            var params = "toolbar=no,width=" + width + ",height=" + height + ",left=0,top=0, screenX=100,screenY=100,status=no,scrollbars=yes,resize=yes";
            window.open( url, "Max", params );
            return false;
        }
        //<a href='#' onclick="popup('http://www.day-by-day.us/events/calendar.html','.6','1');">Events</a>
	</script>

	<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('bara_sus.php');?>
	</div>
	<div class="page-container">
		<div class="page-sidebar nav-collapse collapse">
			<?php include('meniu.php');?>
			<div class="page-content">

				<?php if(!isset($_GET['id'])){?>
				<!-- BEGIN PAGE CONTAINER-->
				<div class="container-fluid">
					<!-- BEGIN PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<h3 class="page-title">Site partners <small> settings, image and content</small>
							</h3>
							<ul class="breadcrumb">
								<li><i class="icon-home"></i><a href="index.php">Dashboard</a><span class="icon-angle-right"></span></li>
								<li><a href="#">Add partner details</a><span class="icon-angle-right"></span></li>
							</ul>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption"><i class="icon-reorder"></i>Page details</div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="javascript:;" class="reload"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form action="actiune.php?pagini=add_partner" class="form-horizontal" method="POST" enctype="multipart/form-data">

										<div class="control-group">
											<label class="control-label">Partner name</label>
											<div class="controls">
												<input type="text" class="span6 m-wrap" name="part_name" maxlength="256" required/>
												<span class="help-inline">This will be shown on the page</span>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Partner visibility</label>
											<div class="controls">
												<select class="span3 m-wrap" name="part_active" data-placeholder="Select active status" tabindex="1">
													<option value="yes">yes</option>
													<option value="no">no</option>
												</select>
												<span class="help-inline">This property will determine if this partner will be visible on the website</span>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Partner order</label>
											<div class="controls">
												<input type="number" step="1" min="0" max="999" name="part_order" class="span3 m-wrap popovers" data-trigger="hover" data-content="Numerical value. (Will be displayed from small to big [ASC])" data-original-title="Partner order in group" />
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Partner logo</label>
											<div class="controls">
												<input type="file" name="fileToUpload" id="fileToUpload" class="span3 m-wrap popovers" data-trigger="hover" data-content="Partner logo" data-original-title="Partner logo" />
												<span>You can upload partner logo.</span>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Partner details</label>
											<div class="controls">
												<textarea class="span9 ckeditor m-wrap" name="part_details" rows="10"></textarea>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Partner website URL</label>
											<div class="controls">
												<input type="text" name="part_link" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="If is set will be shown" data-original-title="Partner website" />
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Partner notes</label>
											<div class="controls">
												<input type="text" name="part_notes" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="Only for back-end. (max 256 char). Attribute available only for admins" data-original-title="Partner notes" />
											</div>
										</div>
										<div class="form-actions">
											<button type="submit" class="btn blue">Add partner details</button>&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="reset" class="btn">Reset</button>
										</div>
									</form>
									<!-- END FORM-->
								</div>
							</div>
							<!-- END SAMPLE FORM PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT-->
					<?}else{?>

					<?php
					$id         = $_GET['id'];
					$sqlp       = "SELECT * from partners where id='$id'";
					$resultp    = mysqli_query($mysqli,$sqlp);
					$rowp       = mysqli_fetch_array($resultp);

					?>
					<!-- BEGIN PAGE CONTAINER-->
					<div class="container-fluid">
						<!-- BEGIN PAGE HEADER-->
						<div class="row-fluid">
							<div class="span12">
								<h3 class="page-title">Update partner details <small>content and logo</small>
								</h3>
								<ul class="breadcrumb">
									<li><i class="icon-home"></i><a href="index.php">Dashboard</a><span class="icon-angle-right"></span></li>
									<li><a href="#">Update partner</a><span class="icon-angle-right"></span></li>
								</ul>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption"><i class="icon-reorder"></i>Update details</div>
										<div class="tools">
											<a href="javascript:;" class="collapse"></a>
											<a href="javascript:;" class="reload"></a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="actiune.php?pagini=update_partner" class="form-horizontal" method="POST" enctype="multipart/form-data">
											<input type="hidden" name="id" value="<?php echo $id;?>" />
											<div class="control-group">
												<label class="control-label">Partner name</label>
												<div class="controls">
													<input type="text" class="span6 m-wrap" name="part_name" value="<?php echo $rowp['part_name'];?>" maxlength="256" required/>
													<span class="help-inline">This will be shown on the page</span>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Partner visibility</label>
												<div class="controls">
													<select class="span3 m-wrap" name="part_active" data-placeholder="Select active status" tabindex="1">
														<option value="yes" <?php echo ($rowp['part_active'] == "yes" ? 'selected' : '');?>>yes</option>
														<option value="no" <?php echo ($rowp['part_active'] == "no" ? 'selected' : '');?>>no</option>
													</select>
													<span class="help-inline">This property will determine if this partner will be visible on the website</span>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Partner order</label>
												<div class="controls">
													<input type="number" step="1" min="0" max="999" name="part_order" value="<?php echo $rowp['part_order'];?>" class="span3 m-wrap popovers" data-trigger="hover" data-content="Numerical value. (Will be displayed from small to big [ASC])" data-original-title="Partner order in group" />
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Partner logo</label>
												<div class="controls">
													<input type="file" name="fileToUpload" id="fileToUpload" class="span3 m-wrap popovers" data-trigger="hover" data-content="Partner logo" data-original-title="Partner logo" />
													<span>You can upload partner logo.</span>
													<div class="clearfix"></div><br/>
													<img src="<?php echo ($rowp['part_image'] == "" ?  'http://via.placeholder.com/350x150?text=admin placeholder' : '../public/images/logos/'.$rowp['part_image']);?>" class="img-responsive">
													<div class="clearfix"></div><br/>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Partner details</label>
												<div class="controls">
													<textarea class="span9 ckeditor m-wrap" name="part_details" rows="10"><?php echo $rowp['part_details'];?></textarea>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Partner website URL</label>
												<div class="controls">
													<input type="text" name="part_link" value="<?php echo $rowp['part_link'];?>" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="If is set will be shown" data-original-title="Partner website" />
												</div>
											</div>

											<div class="control-group">
												<label class="control-label">Partner notes</label>
												<div class="controls">
													<input type="text" name="part_notes" value="<?php echo $rowp['part_notes'];?>" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="Only for back-end. (max 256 char). Attribute available only for admins" data-original-title="Partner notes" />
												</div>
											</div>


											<div class="form-actions">
												<button type="submit" class="btn blue">Update page details</button>&nbsp;&nbsp;&nbsp;&nbsp;
												<button type="reset" class="btn">Reset</button>
											</div>
										</form>


										<!-- END FORM-->
									</div>
								</div>
								<!-- END SAMPLE FORM PORTLET-->
							</div>
						</div>
						<!-- END PAGE CONTENT-->

						<?}?>
					</div>
					<!-- END PAGE CONTAINER-->
				</div>
				<!-- END PAGE -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="//cdn.ckeditor.com/4.4.1/full/ckeditor.js"></script>
			<!--<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script> -->
			<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
			<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
			<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
			<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
			<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
			<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
			<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
			<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="assets/scripts/app.js"></script>
			<script src="assets/scripts/form-components.js"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
                jQuery(document).ready(function() {
                    // initiate layout and plugins
                    App.init();
                    FormComponents.init();
                });
			</script>
			<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
			<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
			<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
			<!--[if lt IE 9]>
			<script src="assets/plugins/excanvas.min.js"></script>
			<script src="assets/plugins/respond.min.js"></script>
			<![endif]-->
			<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
			<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
			<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
			<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
			<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
			<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="assets/scripts/app.js" type="text/javascript"></script>
			<script src="assets/scripts/index.js" type="text/javascript"></script>
			<script src="assets/scripts/tasks.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
                jQuery(document).ready(function() {
                    App.init(); // initlayout and core plugins
                    Index.init();
                    Index.initJQVMAP(); // init index page's custom scripts
                    Index.initCalendar(); // init index page's custom scripts
                    Index.initCharts(); // init index page's custom scripts
                    Index.initChat();
                    Index.initMiniCharts();
                    Index.initDashboardDaterange();
                    Index.initIntro();
                    Tasks.initDashboardWidget();
                });
			</script>
			<!-- END JAVASCRIPTS -->
<?include('footer.php');?>