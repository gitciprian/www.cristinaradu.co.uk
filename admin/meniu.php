<?php if($_SESSION['user_type'] == "1"){?>
	<ul class="page-sidebar-menu">
		<li><div class="sidebar-toggler hidden-phone"></div></li>
		<li class="start">
			<a href="index.php"><i class="icon-home"></i><span class="title"><?php echo $lang['Dashboard'];?></span><span class="selected"></span></a>
		</li>
		<li class="start">
			<a href="../index.php" target="_blank"><i class="icon-gift"></i><span class="title"><?php echo $lang['ToWebsite'];?></span><span class="selected"></span></a>
		</li>

		<li <?php if($activa == "add_user" or $activa == "users"){?> class="active"<?php }?>>
			<a href="javascript:;"><i class="icon-th"></i></i><span class="title"><?php echo $lang['Users'];?></span><span class="arrow "></span></a>
			<ul class="sub-menu">
				<li <?php if($activa == "add_user"){?>class="active"<?php }?>><a href="add_user.php"><?php echo $lang['AddUser'];?></a></li>
				<li <?php if($activa == "users"){?>class="active"<?php }?>> <a href="users.php"><?php echo $lang['AllUsers'];?></a></li>
			</ul>
		</li>

        <li <? if($activa=="add_gallery" || $activa=="all_galleries"){?> class="active"<?}?>>
            <a href="javascript:;"><i class="icon-folder-open"></i><span class="title"><?php echo $lang['MenuGallery'];?></span><span class="arrow "></span></a>
            <ul class="sub-menu">
                <li <?php if($activa=="add_gallery"){?>class="active"<?}?>><a href="add_gallery.php"><?php echo $lang['MenuGalleryFoto'];?></a></li>
                <li <?php if($activa=="all_galleries"){?>class="active"<?}?>><a href="all_galleries.php"><?php echo $lang['MenuAllGalleries'];?></a></li>
            </ul>
        </li>

		<li <?php if($activa == "add_page" or $activa == "all_pages"){?> class="active"<?php }?>>
			<a href="javascript:;"><i class="icon-bookmark-empty"></i><span class="title"><?php echo $lang['WebsitePages'];?></span><span class="arrow "></span></a>
			<ul class="sub-menu">
                <li <?php if($activa == "add_page"){?>class="active"<?php }?>><a href="add_page.php?tip=4"><?php echo $lang['AddArticle'];?></a></li>
                <?php /*<li <?php if($activa == "add_page"){?>class="active"<?php }?>><a href="add_page.php?tip=3"><?php echo $lang['AddActivity'];?></a></li> */?>
				<li <?php if($activa == "all_pages"){?>class="active"<?php }?>><a href="all_pages.php"><?php echo $lang['AllWebsitePages'];?></a></li>
			</ul>
		</li>
        <?php /*
        <li <?php if($activa == "add_partner" or $activa == "all_partners"){?> class="active"<?php }?>>
            <a href="javascript:;"><i class="icon-bookmark-empty"></i><span class="title">Website partners</span><span class="arrow "></span></a>
            <ul class="sub-menu">
                <li <?php if($activa == "add_partner"){?>class="active"<?php }?>><a href="add_partner.php">Add partner</a></li>
                <li <?php if($activa == "all_partners"){?>class="active"<?php }?>><a href="all_partners.php">See all partners</a></li>
            </ul>
        </li>
        <li <?php if($activa == "add_message" or $activa == "messages"){?> class="active"<?php }?>>
            <a href="javascript:;"><i class="icon-th"></i></i><span class="title">Messages and Docs</span><span class="arrow "></span></a>
            <ul class="sub-menu">
                <li <?php if($activa == "add_message"){?>class="active"<?php }?>><a href="add_message.php">Create new message</a></li>
                <li <?php if($activa == "messages"){?>class="active"<?php }?>> <a href="messages.php">Previous messages</a></li>
            </ul>
        </li>
        <li <?php if($activa == "calendar" or $activa == "add_calendar"){?> class="active"<?php }?>>
            <a href="javascript:;">	<i class="icon-bookmark-empty"></i> <span class="title"><?php echo $lang['MenuCalendar'];?></span><span class="arrow"></span></a>
            <ul class="sub-menu">
                <li <?php if($activa == "add_calendar"){?>class="active"<?php }?>><a href="add_calendar.php"><?php echo $lang['MenuCalendarAdd'];?></a></li>
                <li <?php if($activa == "calendar"){?>class="active"<?php }?>><a href="calendar.php"><?php echo $lang['MenuCalendarAll'];?></a></li>
            </ul>
        </li>

        */?>

        <li <?php if($activa=="pagina_speciala" or $activa=="galerie" or $activa=="tradu"){?> class="active"<?php }?>>
            <a href="javascript:;"><i class="icon-bookmark"></i><span class="title">Pagini traduceri</span><span class="arrow "></span></a>
            <ul class="sub-menu">
				<?php foreach ($array_lb as $lbb) {?>
                    <li <?php if($activa=="tradu"){?> class="active"<?php }?>><a href="tradu.php?lb=<?php echo $lbb;?>">Fisier limba <?php echo $lbb;?></a></li>
				<?php }?>
            </ul>
        </li>

        <li <?php if($activa == "add_slider" or $activa == "all_slides"){?> class="active"<?php }?>>
            <a href="javascript:;">	<i class="icon-bookmark-empty"></i> <span class="title"><?php echo $lang['FrontPagSlider'];?></span><span class="arrow"></span></a>
            <ul class="sub-menu">
                <li <?php if($activa == "add_slider"){?>class="active"<?php }?>><a href="add_slider.php"><?php echo $lang['AddSliderImage'];?></a></li>
                <li <?php if($activa == "all_slides"){?>class="active"<?php }?>><a href="all_slides.php"><?php echo $lang['EditSliderImage'];?></a></li>
            </ul>
        </li>


		<li <?php if($activa == "form_fileupload"){?> class="active"<?php }?>>
			<a href="javascript:;"><i class="icon-table"></i><span class="title"><?php echo $lang['FileUpload'];?></span><span class="arrow "></span></a>
			<ul class="sub-menu">
				<li  <?php if($activa == "form_fileupload"){?>class="active"<?php }?>><a href="form_fileupload.php"><?php echo $lang['LoadFilesOnTheServer'];?></a></li>
			</ul>
		</li>
		<li <?php if($activa == "faq" or $activa == "general_info"){?> class="active"<?php }?>>
			<a href="javascript:;"><i class="icon-gift"></i><span class="title"><?php echo $lang['SiteInformation'];?></span><span class="arrow "></span></a>
			<ul class="sub-menu">
				<li <?php if($activa == "general_info"){?> class="active"<?php }?>><a href="general_info.php"><?php echo $lang['GeneralInfo'];?></a></li>
                <li <?php if($activa == "faq"){?> class="active"<?php }?>><a href="faq.php"><?php echo $lang['GoodToKnow'];?></a></li>
			</ul>
		</li>
	</ul>
	<?php }?>
</div>