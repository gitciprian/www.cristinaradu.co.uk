<?php
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}
require_once('config.php');

$id         = $_GET['id'];
$watermark  = $_GET['w'];
$np         = $_GET['np'];
$queryid    = "SELECT count(id) as total_record  from gallery where project_id = '$id' and folder = 'image'";
$resid      = mysqli_query($mysqli,$queryid) or die(mysqli_error());
$rowid      = mysqli_fetch_array($resid);
$nrc        = $rowid['total_record'];
$upload_max_size = ini_get('upload_max_filesize');

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Image gllery</title>
        <link rel="stylesheet" href="assets/css/styles-galerie.css?v=1" />
        <!--[if lt IE 9]>
          <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
     <script language="javascript">
        var win = null;
        function NewWindow(mypage,myname,w,h,scroll){
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings =
        'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
        win = window.open(mypage,myname,settings)
        }
    </script>

    </head>    
    <body onbeforeunload="refreshAndClose();">
	<header>
		<h2 style="text-align:center">Image gallery for "<?php echo base64_decode($_GET['np']);?>"</h2>
        <h3 style="text-align:center">
            <?php if($nrc == 0){
                echo 'This project does not have a gallery yet.';
            }?>
        </h3>
	</header>		
	<div id="dropbox">
		<span class="message">Drag and drop your images here. <br /><i>(max 50 images at once)</i></span><br/>
		<p style="text-align:center">Server max upload limit <?php echo $upload_max_size;?>b/image</p>
	</div>

<script src="https://code.jquery.com/jquery-1.6.3.min.js"></script>
<script src="assets/js/jquery.filedrop.js"></script>
<!--   <script src="assets/js/script.js"></script>    am pus mai jos sa se faca id cu php-->
<script type="text/javascript">
$(function(){	
	var dropbox = $('#dropbox'), message = $('.message', dropbox);	
	dropbox.filedrop({		
		paramname:'pic',		
		maxfiles: 50,
    	maxfilesize: 500,
		url: 'post_file.php?id=<?php echo $id;?>&w=<?php echo $watermark;?>&np=<?php echo $_GET['np'];?>',
		uploadFinished:function(i,file,response){
			$.data(file).addClass('done');			
		},		
    	error: function(err, file) {
			switch(err) {
				case 'BrowserNotSupported':
					showMessage('Your browser is not supported. Please try another one!');
					break;
				case 'TooManyFiles':
					alert('Too many files. Upload limit is 50 at once');
					break;
				case 'FileTooLarge':
					alert(file.name+' to large! Please upload images with a size smaller than 500 Mb.');
					break;
				default:
					break;
			}
		},
		beforeEach: function(file){
			if(!file.type.match(/^image\//)){
				alert('Allowd extensions (jpg, jpeg, png, gif)!');
				return false;			
			}
		},
		uploadStarted:function(i, file, len){
			createImage(file);
		},		
		progressUpdated: function(i, file, progress) {
			$.data(file).find('.progress').width(progress);
		}    	 
	});	
	var template = '<div class="preview">'+'<span class="imageHolder">'+'<img />'+'<span class="uploaded"></span>'+'</span>'+'<div class="progressHolder">'+'<div class="progress"></div>'+'</div>'+'</div>'; 		
	function createImage(file){
		var preview = $(template), 	image = $('img', preview);			
		var reader = new FileReader();		
		image.width = 100;
		image.height = 100;		
		reader.onload = function(e){			
			image.attr('src',e.target.result);
		};
		reader.readAsDataURL(file);		
		message.hide();
		preview.appendTo(dropbox);		
		$.data(file,preview);
	}
	function showMessage(msg){
		message.html(msg);
	}
});

    $(document).ready(function(){
        $("#checkAll").click(function () {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $("#deleteAllImages").click(function () {

            var checkValues = $('.checkItem:checkbox:checked').map(function()
            {
                return $(this).data("id");
            }).get();

            if(checkValues.length > 0) {

                $.ajax({
                    url: 'actiune.php?pagini=delete_mass_image&id=<?php echo $id;?>&np=<?php echo $np;?>',
                    type: 'post',
                    data: {ids: checkValues},
                    success: function (data) {
                        location.reload();
                    }
                });
            }else{
                alert("EROARE! Nu aveti imagini selectate.");
            }

        });

    });
</script>
<?php if($nrc>"0"){?>

    <div style="margin: auto; text-align: center">
        <a href="order.php?id=<?php echo $id;?>&w=<?php echo $watermark;?>&np=<?php echo $_GET['np'];?>" class="order-url">Ordoneaza imaginilile</a><br/><br/>
    </div>

    <header><h2 style="text-align:center">Page images for "<?php echo base64_decode($_GET['np']);?>"</h2></header>
    <div style="width:1000px;text-align:center;margin:auto">
            <table width="1000" align="center" cellpadding="5" cellspacing="5">
                <tr>
                    <td colspan="3" align="left">
                        <label><input type="checkbox" id="checkAll"> <span style="font-size:25px; cursor:pointer">Selecteaza toate imaginile</span></label>
                    </td>
                    <td colspan="2" align="left">
                        <button type="button" class="button-cls" id="deleteAllImages" onclick="confirm('Sigur vrei sa stergi aceste imagini!');">Sterge imaginile selectate</button>
                    </td>
                </tr>
                <tr><td colspan="5">&nbsp;</td></tr>
                <tr>
                    <td></td>
                    <td>Image</td>
                    <td>Details</td>
                    <td>Visibility</td>
                    <td width="100" align="right">Actions</td>
                </tr>
                <?php	$sqlp    = "SELECT * FROM gallery WHERE project_id = '$id' and folder = 'image' ORDER BY img_order ASC";
                        $resultp = mysqli_query($mysqli,$sqlp);
                        while($row_general =mysqli_fetch_array($resultp)){?>
                            <tr>
                                <td><input type="checkbox" class="checkItem" data-id="<?php echo $row_general['id'];?>"></td>
                                <td><a href="https://<?php echo $_SERVER['HTTP_HOST'];?>/public/images/gallery/<?php echo $row_general['image'];?>" onclick="NewWindow(this.href,'name','800','600','yes');return false" title="Zoom image"><img src="https://<?php echo $_SERVER['HTTP_HOST'];?>/public/images/gallery/thumbs/<?echo $row_general['image'];?>" width="200" style="border-radius:5px"/></a></td>
                                <td align="left">
                                    Image id:  <?php echo $row_general['id'];?><br/>
                                    Extension: <?php echo $row_general['doc_type'];?><br/>
                                    Alt tag: <?php echo $row_general['doc_alt'];?> <?echo $row_general['id'];?><br/>
                                    Raw URL: https://<?php echo $_SERVER['HTTP_HOST'].'/public/images/gallery/'.$row_general['image'];?><br/>
                                    Uploaded at: <?php echo date( 'm/d/Y g:i A', strtotime($row_general['created_at']));?><br/>
                                </td>
                                <td align="center"><?php echo $row_general['img_active'];?></td>
                                <td align="right">
                                    <a href="actiune.php?pagini=delete_gallery_image&id=<?php echo $row_general['id'];?>&n=<?php echo urlencode(base64_encode($row_general['image']));?>" onclick="NewWindow(this.href,'name','400','300','yes');return false" title="Delete image"><img src="all/img/118.png" style="width:30px;float:right;"/></a>
                                    <a href="afisare.php?afisare=change_details&id=<?php echo $row_general['id'];?>&pr=<?php echo $id;?>" title="Update details" onclick="NewWindow(this.href,'name','800','600','yes');return false"><img src="all/img/2.png" style="width:30px;float:right;margin-left:5px"/></a>
                                </td>
                            </tr>
                        <?php }?>
            </table>
    </div>
<?php }?>
</body>
</html>