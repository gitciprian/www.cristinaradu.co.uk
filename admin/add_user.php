<?include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL STYLES -->
    <script type="text/javascript">
    function addFile(){
    var root=document.getElementById('mytab').getElementsByTagName('tr')[0].parentNode;
    var oR = cE('tr');var oC = cE('td');var oI = cE('input'); var oS=cE('span')
    cA(oI,'type','file');cA(oI,'name','file[]');
    oS.style.cursor='pointer';
    oS.onclick=function(){
    this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode)
    }
    oS.appendChild(document.createTextNode(' Sterge'));
    oC.appendChild(oI);oC.appendChild(oS);oR.appendChild(oC);root.appendChild(oR);
    }
    function cE(el){
    this.obj =document.createElement(el);
    return this.obj
    }
    function cA(obj,att,val){
    obj.setAttribute(att,val);
    return
    }
    </script>
    <script type='text/javascript'>
    function popup( url, width, height )
    {
       width = screen.width * width;
       height = screen.height * height;
       var params = "toolbar=no,width=" + width + ",height=" + height + ",left=0,top=0, screenX=100,screenY=100,status=no,scrollbars=yes,resize=yes";
       window.open( url, "Max", params );
       return false;
    }
    //<a href='#' onclick="popup('http://www.day-by-day.us/events/calendar.html','.6','1');">Events</a>
    </script>
<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">	
		
		<?if(!isset($_GET['id'])){?>			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['UsersMainTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['UsersMainSubtitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">						
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $lang['UsersTopFormTitle'];?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>									
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="actiune.php?pagini=add_user" class="form-horizontal" method="POST" enctype="multipart/form-data">
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserName'];?></label>
										<div class="controls">
											<input type="text" class="span6 m-wrap" name="user_name" required/>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"><?php echo $lang['FormCompanyName'];?></label>
										<div class="controls">
											<input type="text" class="span6 m-wrap" name="company_name"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormLoginEmail'];?></label>
										<div class="controls">
											<input type="email" name="user_email" class="span6 m-wrap popovers" required data-trigger="hover" data-content="<?php echo $lang['FormLoginEmailHintText'];?>" data-original-title="<?php echo $lang['FormLoginEmailHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormInitialPassword'];?></label>
										<div class="controls">
											<input type="text" name="user_password" class="span6 m-wrap popovers" required data-trigger="hover" data-content="<?php echo $lang['FormInitialPasswordHintText'];?>" data-original-title="<?php echo $lang['FormInitialPasswordHintTitle'];?>" />
										</div>
									</div>	
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormNotificariEmail'];?></label>
										<div class="controls">
											<select class="span3 m-wrap" name="notificare">											
												<option value="nu"><?php echo $lang['FormNotificariEmailOpt1'];?></option>
												<option value="da"><?php echo $lang['FormNotificariEmailOpt2'];?></option>
											</select>
											<span class="help-inline"><?php echo $lang['FormNotificariEmailHint'];?></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserType'];?></label>
										<div class="controls">
											<select class="span3 m-wrap" name="user_type">
												<option value="1"><?php echo $lang['FormUserTypeOpt1'];?></option>
												<option value="2"><?php echo $lang['FormUserTypeOpt2'];?></option>
											</select>
											<span class="help-inline"><?php echo $lang['FormUserTypeHint'];?></span>
										</div>
									</div>	
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserActive'];?></label>
										<div class="controls">
											<select class="span3 m-wrap" name="user_permision">
												<option value="1"><?php echo $lang['FormUserActiveOpt1'];?></option>
												<option value="0"><?php echo $lang['FormUserActiveOpt2'];?></option>
											</select>
											<span class="help-inline"><?php echo $lang['FormUserActiveHint'];?></span>
										</div>
									</div>
                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['FormUserLanguage'];?></label>
                                        <div class="controls">
                                            <select class="span3 m-wrap" name="user_language">
                                                <option value="ro"><?php echo $lang['FormUserLanguageOpt1'];?></option>
                                                <option value="en"><?php echo $lang['FormUserLanguageOpt2'];?></option>
                                            </select>
                                            <span class="help-inline"><?php echo $lang['FormUserLanguageHint'];?></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
										<label class="control-label"><?php echo $lang['FormSecondaryEmail'];?></label>
										<div class="controls">
											<input type="text" name="secondary_email" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormSecondaryEmailHintText'];?><" data-original-title="<?php echo $lang['FormSecondaryEmailHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserPhone'];?></label>
										<div class="controls">
											<input type="text" name="user_phone" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormUserPhoneHintText'];?>" data-original-title="<?php echo $lang['FormUserPhoneHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserCountry'];?></label>
										<div class="controls">
											<input type="text" name="user_country" class="span3 m-wrap popovers" value="England" data-trigger="hover" data-content="<?php echo $lang['FormUserCountryHintText'];?>" data-original-title="<?php echo $lang['FormUserCountryHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserAddress'];?></label>
										<div class="controls">
											<input type="text" name="user_address" class="span9 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormUserAddressHintText'];?>" data-original-title="<?php echo $lang['FormUserAddressHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserNotes'];?></label>
										<div class="controls">
											<input type="text" name="user_notes" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['FormUserNotesHintText'];?>" data-original-title="<?php echo $lang['FormUserNotesHintTitle'];?>" />
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn blue"><?php echo $lang['FormSubmitBtnText'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn"><?php echo $lang['FormResetBtn'];?></button>
									</div>
								</form>
								<!-- END FORM-->  
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>				
				<!-- END PAGE CONTENT--> 
			<?}else{?>
			
            <?php
                $id         = $_GET['id'];
                $sqlp       = "select * from u_login where id='$id'";
                $resultp    = mysqli_query($mysqli, $sqlp);
                $rowp       = mysqli_fetch_array($resultp);
            ?>
			<!-- BEGIN PAGE CONTAI
			NER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['EditUsersMainTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['EditUsersMainSubtitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">						
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $lang['EditUsersTopFormTitle'];?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>									
									<a href="javascript:;" class="reload"></a>									
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="actiune.php?pagini=edit_user" class="form-horizontal" method="POST" enctype="multipart/form-data">
									<input type="hidden" name="id" value="<?php echo $id;?>" />
									<input type="hidden" name="ps" value="<?php echo $rowp['user_password'];?>" />
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserName'];?></label>
										<div class="controls">
											<input type="text" class="span6 m-wrap" name="user_name" value="<?php echo  htmle($rowp['user_name']);?>" required/>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"><?php echo $lang['FormCompanyName'];?></label>
										<div class="controls">
											<input type="text" class="span6 m-wrap" name="company_name" value="<?php echo  htmle($rowp['company_name']);?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormLoginEmail'];?></label>
										<div class="controls">
											<input type="email" name="user_email" value="<?php echo  htmle($rowp['user_email']);?>" class="span6 m-wrap popovers" required data-trigger="hover" data-content=<?php echo $lang['FormLoginEmailHintText'];?>" data-original-title="<?php echo $lang['FormLoginEmailHintTitle'];?>" />
										</div>
									</div>						
									<div class="control-group">
										<label class="control-label"><?php echo $lang['EditUsersFormPassword']?></label>
										<div class="controls">
											<input type="text" name="user_password" placeholder="" class="span6 m-wrap popovers"  data-trigger="hover" data-content="<?php echo $lang['EditUsersFormPasswordHintTitle']?>" data-original-title="<?php echo $lang['EditUsersFormPasswordHintText']?>" />
										</div>
									</div>										
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserType'];?></label>
										<div class="controls">
											<select class="span3 m-wrap" name="user_type">
												<option value="1" <?php if($rowp['user_type']=="1"){?>selected<?}?>><?php echo $lang['FormUserTypeOpt1'];?></option>
												<option value="2" <?php if($rowp['user_type']=="2"){?>selected<?}?>><?php echo $lang['FormUserTypeOpt2'];?></option>
											</select>
											<span class="help-inline"><?php echo $lang['FormUserTypeHint'];?></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserActive'];?></label>
										<div class="controls">
											<select class="span3 m-wrap" name="user_permision">
												<option value="1" <?php if($rowp['user_permision']=="1"){?>selected<?}?>><?php echo $lang['FormUserActiveOpt1'];?></option>
												<option value="0" <?php if($rowp['user_permision']=="0"){?>selected<?}?>><?php echo $lang['FormUserActiveOpt2'];?></option>
											</select>
											<span class="help-inline"><?php echo $lang['FormUserActiveHint'];?></span>
										</div>
									</div>
                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['FormUserLanguage'];?></label>
                                        <div class="controls">
                                            <select class="span3 m-wrap" name="user_language">
                                                <option value="ro" <?php if($rowp['user_language']=="ro"){?>selected<?}?>><?php echo $lang['FormUserLanguageOpt1'];?></option>
                                                <option value="en" <?php if($rowp['user_language']=="en"){?>selected<?}?>><?php echo $lang['FormUserLanguageOpt2'];?></option>
                                            </select>
                                            <span class="help-inline"><?php echo $lang['FormUserLanguageHint'];?></span>
                                        </div>
                                    </div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormSecondaryEmail'];?></label>
										<div class="controls">
											<input type="text" name="secondary_email" value="<?php echo  htmle($rowp['secondary_email']);?>" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormSecondaryEmailHintText'];?>" data-original-title="<?php echo $lang['FormSecondaryEmailHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserPhone'];?></label>
										<div class="controls">
											<input type="text" name="user_phone" value="<?php echo  htmle($rowp['user_phone']);?>" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormUserPhoneHintText'];?>" data-original-title="<?php echo $lang['FormUserPhoneHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserCountry'];?></label>
										<div class="controls">
											<input type="text" name="user_country" value="<?php echo  htmle($rowp['user_country']);?>" class="span3 m-wrap popovers" value="Romania" data-trigger="hover" data-content="<?php echo $lang['FormUserCountryHintText'];?>" data-original-title="<?php echo $lang['FormUserCountryHintTitle'];?>" />
										</div>
									</div>
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserAddress'];?></label>
										<div class="controls">
											<input type="text" name="user_address" value="<?php echo  htmle($rowp['user_address']);?>" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormUserAddressHintText'];?>" data-original-title="<?php echo $lang['FormUserAddressHintTitle'];?>" />
										</div>
									</div>									
									<div class="control-group">
										<label class="control-label"><?php echo $lang['FormUserNotes'];?></label>
										<div class="controls">
											<input type="text" name="user_notes" class="span9 m-wrap popovers" value="<?php echo  htmle($rowp['user_notes']);?>" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['FormUserNotesHintText'];?>" data-original-title="<?php echo $lang['FormUserNotesHintTitle'];?>" />
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn blue"><?php echo $lang['EditFormSubmitBtnText'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn"><?php echo $lang['EditFormResetBtn'];?></button>
									</div>
								</form>
								<!-- END FORM-->  
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>				
				<!-- END PAGE CONTENT-->         
			
			<?}?>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
		   FormComponents.init();
		});
	</script>
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/tasks.js" type="text/javascript"></script>        
	<!-- END PAGE LEVEL SCRIPTS -->  
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   Index.init();
		   Index.initJQVMAP(); // init index page's custom scripts
		   Index.initCalendar(); // init index page's custom scripts
		   Index.initCharts(); // init index page's custom scripts
		   Index.initChat();
		   Index.initMiniCharts();
		   Index.initDashboardDaterange();
		   Index.initIntro();
		   Tasks.initDashboardWidget();
		});
	</script>
	<!-- END JAVASCRIPTS -->   
	<?include('footer.php');?>