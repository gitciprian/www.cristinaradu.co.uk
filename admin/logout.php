<?php
session_start(); 
unset($_SESSION['auth']);
unset($_SESSION['user_email']);
unset($_SESSION['user_name']);
unset($_SESSION['user_type']);
unset($_SESSION['user_id']);
unset($_SESSION['random_key']);
unset($_SESSION['user_file_ext']);
header("Location: login.php");
?>
