		<div class="navbar-inner">
			<div class="container-fluid">			
				<a class="brand" href="index.php" style="color:#fff"><h3 style="margin:-10px 0 0 0;padding:0;font-size:20px">Site&nbsp;admin</h3></a>
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse"><img src="assets/img/menu-toggler.png" alt="" /></a>  
				<ul class="nav pull-right">
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" src="assets/img/avatar.png" width="30"/>
						<span class="username">&nbsp;&nbsp;<?php echo $_SESSION['user_name'];?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
                            <?if($_SESSION['user_type']=="1"){?>
                                <li><a href="add_user.php?id=<?php echo $_SESSION['user_id'];?>"><i class="icon-user"></i> <?php echo $lang['MyProfile'];?></a></li>
                            <?}?>
							<li class="divider"></li>
							<li><a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i> <?php echo $lang['FullScreen'];?></a></li>
							<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
						</ul>
					</li>					
				</ul>
			</div>
		</div>