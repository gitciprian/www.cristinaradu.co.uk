<?include("top_admin.php");?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->
	<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>
	</div>
<div class="page-container">
	<div class="page-sidebar nav-collapse collapse">
		<?include('meniu.php');?>
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<h3 class="page-title">Previous messges <small> content and details</small>
						</h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php">Dashboard</a><span class="icon-angle-right"></span></li>
							<li><a href="#">Select message</a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">

						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i>
									All website messages
								</div>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<?$hh="cum a deceatada";?>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
									<thead>
									<tr>
										<th width="5%">ID</th>
										<th width="45%">Page details</th>
										<th class="hidden-480" align="center">Status</th>
										<th class="hidden-480" align="center">Dates</th>
										<th align="center">Actions</th>
										<th style="display:none;"></th>
										<th style="display:none;"></th>
									</tr>
									</thead>
									<tbody>
									<?php

									$sqlp       = "SELECT *  from messages WHERE id > 0 ";
									$sqlp      .= " order by id DESC ";
									$resultp    = mysqli_query($mysqli,$sqlp);
									while($rows = mysqli_fetch_array($resultp)){

									    $id_proiect = $rows['id'];

										$sqla       = "SELECT *  from gallery WHERE id > 0 AND project_id = '$id_proiect' AND folder ='attach'";
										$sqla      .= " order by id DESC ";
										$resulta    = mysqli_query($mysqli,$sqla);
										$attach_num = mysqli_num_rows($resulta);


										?>
										<tr>
											<td><?echo $rows['id'];?></td>
											<td>
												Subject: <?php echo $rows['subject'];?><br/>
												Type: <?php echo ($rows['notification'] == 'nu' ? 'without email' : 'with email');?><br/>
                                                Attachments: <?php echo $attach_num;?>
											</td>
											<td class="hidden-480" align="center">
												Sent to users:<br/>
                                                <?php

                                                $users      = json_decode($rows['users'], true);
                                                $how_many   = count($users);

                                                if($how_many > 0) {

	                                                foreach ( $users as $id_user ) {

		                                                $sqlm    = "SELECT user_email FROM u_login WHERE  id = '$id_user' ";
		                                                $resultm = mysqli_query( $mysqli, $sqlm );
		                                                $rowm    = mysqli_fetch_array( $resultm );
		                                                echo $rowm['user_email']."<br/>";
	                                                }
                                                }else{
                                                    echo 'No user was selected';
                                                }
                                                ?>

											</td>
											<td class="hidden-480" align="left">
												Created at: <?php echo Datasiora($rows['created_at']);?><br/>
												Created by: <?php echo $rows['created_by'];?>
											</td>
											<td width="90">
												<a href="add_message.php?id=<?php echo $rows['id'];?>" title="See more details" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></a>
												<a class="btn red icn-only" href="actiune.php?pagini=delete_message&id=<?php echo $rows['id'];?>" title="Delete this messag" onclick="return confirm( 'Are you sure that you want to delete this message? NOTE: If emails were sent they cannot be undone' )"><i class="icon-remove icon-white"></i></a>
											</td>
											<td style="display:none;"></td>
                                            <td style="display:none;">
	                                            <?php while($rowa = mysqli_fetch_array($resulta)){?>
                                                    Attachment: <a href="../public/images/messages/<?php echo $rowa['image'];?>" target="_blank"><?php echo $rowa['doc_alt'];?> (<?php echo $rowa['doc_type'];?>)</a><br/>
	                                            <?php }?>
                                                <hr/>
												Notes: <?php echo $rows['notes'];?>
											</td>
										</tr>
									<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>
	<![endif]-->
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/table-advanced.js"></script>
	<script>
        jQuery(document).ready(function() {
            App.init();
            TableAdvanced.init();
        });
	</script>
<?include('footer.php');?>