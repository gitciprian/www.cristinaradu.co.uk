<?php include("top_admin.php");?>
<body class="page-header-fixed" onload="startTime()">
<div class="header navbar navbar-inverse navbar-fixed-top">
    <?php include('bara_sus.php');?>
</div>
<div class="page-container">
    <div class="page-sidebar nav-collapse collapse">
        <?include('meniu.php');?>
        <div class="page-content">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">
                        <?php $lb = $_GET['lb'];?>
                        <h3 class="page-title">Fisier traduceri pentru limba <?echo $lb;?><small> texte si coduri</small>
                        </h3>
                        <ul class="breadcrumb">
                            <li><i class="icon-home"></i><a href="index.php">Home</a><span class="icon-angle-right"></span></li>
                            <li><a href="#">Traduceri limba <?echo $lb;?></a><span class="icon-angle-right"></span></li>
                        </ul>
                    </div>
                </div>
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
                        <h2>CTRL+F pentru a gasi cuvantul cautat </h2>
                        <?php
                        $filename       = isset($_GET['file']) ? $_GET['file'] : '';
                        $directory      = ($fisier_limba."texte.".$lb.".php");
                        $fn             = $directory . $filename;

                        if (isset($_POST['content']))
                        {
                            $content    = stripslashes($_POST['content']);
                            $fp         = fopen($fn,"w") or die ("Fisierul nu poate fi deschis in modul de scriere!");
                            fputs($fp,$content);
                            fclose($fp) or die ("Fisierul nu poate fi salvat!");
                        }?>
                        <form action="<?php echo $_SERVER["PHP_SELF"] ?>?lb=<?echo $lb;?>" method="post">
                            <textarea class="span12" style="min-height:500px" name="content"><?php readfile($fn); ?></textarea>
                            <hr />
                            <input type="submit" value="Aplica traducerea">
                        </form>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
    <!--[if lt IE 9]>
    <script src="assets/plugins/excanvas.min.js"></script>
    <script src="assets/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
    <!-- END CORE PLUGINS -->
    <script src="assets/scripts/app.js"></script>
    <script>
        jQuery(document).ready(function() {
            App.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->
    <?php include('footer.php');?>
