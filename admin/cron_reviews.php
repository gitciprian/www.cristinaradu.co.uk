<?php
include 'config.php';

$token      = "8048c9da87f01fb2ba4d57268dd53736";

if($token == $_GET['t']) {

	$curl_content = '';
	$url          = 'http://www.checkatrade.com/EliteHeatingandPlumbingLtd/Reviews.aspx?sort=0&page=1#results';

	function curl_like_browser( $url ) {

		$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
		$ch    = curl_init();
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_VERBOSE, true );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT, $agent );
		curl_setopt( $ch, CURLOPT_URL, $url );
		$result = curl_exec( $ch );

		return $result;
	}


	if ( $curl_content == "" ) {
		$curl_content = curl_like_browser( $url );
	}

//var_dump($curl_content);

	$total_reviews_boom     = explode( "name=\"results\"", $curl_content );
	$total_reviews_kboom    = explode( " reviews", $total_reviews_boom[1] );
	$total_reviews          = filter_var( strip_tags( $total_reviews_kboom[0] ), FILTER_SANITIZE_NUMBER_INT );

	$total_pages_to_curl    = (int) ( 1 + ( $total_reviews / 30 ) );

	$total_info_boom        = explode( "<div class=\"feedback-list__count push--top\">", $curl_content );
	$total_info_kboom       = explode( "<p>", $total_info_boom[1] );
	$total_info             = trim( strip_tags( $total_info_kboom[0] ) );


	$sql = "UPDATE  general_info set checkatrade_total_reviews = '$total_reviews', checkatrade_total_pages = '$total_pages_to_curl', checkatrade_latest_reviews = '$total_info'	WHERE id = '1'";
	$update = mysqli_query($mysqli, $sql);

	if($update) {

		$sqla        = "TRUNCATE s_reviews";
		$truncate    = mysqli_query($mysqli, $sqla);

		if($truncate) {

			for ( $i = 1; $i <= $total_pages_to_curl; $i ++ ) {
				$url = 'http://www.checkatrade.com/EliteHeatingandPlumbingLtd/Reviews.aspx?sort=0&page=' . $i . '#results';

				$curl_content = curl_like_browser( $url );

				// start getting content
				$total_content_boom = explode( "<div class=\"feedback-list__text-column\">", $curl_content );

				foreach ( $total_content_boom as $content ) {
					// get review title
					$review_title_boom  = explode( "<div class=\"feedback-list__title summary\">", $content );
					$review_title_kboom = explode( "</a></div>", $review_title_boom[1] );
					$review_title       = trim( strip_tags( $review_title_kboom[0] ) );
					$review_title        = str_replace("'", "&#39;", $review_title);

					$review_desc_boom  = explode( "<p class=\"feedback-list__description description\">&ldquo;", $content );
					$review_desc_kboom = explode( "&rdquo;</p>", $review_desc_boom[1] );
					$review_desc        = trim( strip_tags( $review_desc_kboom[0] ) );
					$review_desc        = str_replace("'", "&#39;", $review_desc);

					$review_author_boom  = explode( "<span class=\"feedback-list__customer\">&ndash; ", $content );
					$review_author_kboom = explode( "( ", $review_author_boom[1] );
					$review_author = trim( strip_tags( $review_author_kboom[0] ) );

					$review_date_boom  = explode( "</span> ) ", $content );
					$review_date_kboom = explode( "</span>", $review_date_boom[1] );
					$review_date = trim( strip_tags( $review_date_kboom[0] ) );

					$review_score_boom  = explode( "<div class=\"feedback-list__overall\" title=\"Average score out of 10\"><span>", $content );
					$review_score_kboom = explode( "</span></div>", $review_score_boom[1] );
					$review_score = trim( strip_tags( $review_score_kboom[0] ) );

					if($review_title !="") {

						$sql    = "INSERT INTO s_reviews (review_title, review_content, review_date, review_author, review_score) VALUE ('".$review_title."', '".$review_desc."', '".$review_date."', '".$review_author."', '".$review_score."')";
						$insert = mysqli_query( $mysqli, $sql );

					}

				}

			}
		}
	}

}else{
	echo 'no token present';
}
