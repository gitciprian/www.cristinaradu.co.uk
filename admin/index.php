<?php include("top_admin.php");?>
<body class="page-header-fixed" onLoad="startTime()"> 
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php include('bara_sus.php');?>
	</div>	
	<div class="page-container">		
		<div class="page-sidebar nav-collapse collapse">
			<?include('meniu.php');?>		
		<div class="page-content">
		<?if($_SESSION['user_type'] == "1"){?>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12"><br/>	<br/>	
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php">Home</a><i class="icon-angle-right"></i></li>
							<li><a href="#"><?php echo $lang['Dashboard'];?></a></li>
							<li class="pull-right no-text-shadow"><i class="icon-calendar"></i><span><?php echo luni_in_romana($luna_in_litere);?></span>	</li>
						</ul>
					</div>
				</div>
				<div id="dashboard">
					<div class="row-fluid">
                        <div class="span3 responsive" data-tablet="span3" data-desktop="span3">
							<div class="dashboard-stat blue">
								<div class="visual"><i class="icon-comments"></i></div>
								<div class="details">
									<div class="number">
									<?	$queryid    = "SELECT COUNT(DISTINCT user_email) as total_record  from u_login  ";
									 	$resid      = mysqli_query($mysqli, $queryid) or die(mysqli_error());
								     	$rowid      = mysqli_fetch_array($resid);
									 	$nrc        = $rowid['total_record'];?>
									 	<? echo $nrc; ?>
									</div>
									<div class="desc"><?php echo $lang['Users'];?></div>
								</div>
								<a class="more" href="users.php"><?php echo $lang['MoreDetails'];?> <i class="m-icon-swapright m-icon-white"></i></a>
							</div>
						</div>
                        <div class="span3 responsive" data-tablet="span3" data-desktop="span3">
							<div class="dashboard-stat green">
								<div class="visual"><i class="icon-user"></i></div>
								<div class="details">
									<div class="number">
									<?php
										$queryid    = "SELECT count(id) as total_record  from pages WHERE page_type != '4' ";
									 	$resid      = mysqli_query($mysqli, $queryid) or die(mysqli_error());
								     	$rowid      = mysqli_fetch_array($resid);
									 	$nrc        = $rowid['total_record'];?>
									 	<?php echo $nrc; ?>
									</div>
                                    <div class="desc"><?php echo $lang['Pages'];?></div>
								</div>
								<a class="more" href="all_pages.php"><?php echo $lang['MoreDetails'];?> <i class="m-icon-swapright m-icon-white"></i></a>
							</div>
						</div>

						<div class="span3 responsive" data-tablet="span3" data-desktop="span3">
							<div class="dashboard-stat yellow">
								<div class="visual">
									<i class="icon-bar-chart"></i>
								</div>
								<div class="details">
									<div class="number">
									<?php

                                        $queryid    = "SELECT count(id) as total_record  from pages WHERE page_type = '4' ";
									 	$resid      = mysqli_query($mysqli, $queryid) or die(mysqli_error());
								     	$rowid      = mysqli_fetch_array($resid);
									 	$nrc        = $rowid['total_record'];?>
									 	<?php echo $nrc; ?>
									</div>
									<div class="desc"><?php echo $lang['Articles'];?></div>
								</div>
								<a class="more" href="all_pages.php?t=4&a=no"><?php echo $lang['MoreDetails'];?> <i class="m-icon-swapright m-icon-white"></i>
								</a>                 
							</div>
						</div>
                        <div class="span3 responsive" data-tablet="span3" data-desktop="span3">
                            <div class="dashboard-stat purple">
                                <div class="visual"><i class="icon-globe"></i></div>
                                <div class="details">
                                    <div class="number">#</div>
                                    <div class="desc">Analytics</div>
                                </div>
                                <a class="more" href="https://www.google.com/analytics/" target="_blank"><?php echo $lang['MoreDetails'];?> <i class="m-icon-swapright m-icon-white"></i></a>
                            </div>
                        </div>
					</div>                    
			    </div>
			<?}?>
		</div>
	</div>
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<script src="assets/scripts/app.js" type="text/javascript"></script>	
	<script src="assets/scripts/tasks.js" type="text/javascript"></script>        
	<!-- END PAGE LEVEL SCRIPTS -->  
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   Index.init();
		   Index.initJQVMAP(); // init index page's custom scripts
		   Index.initCalendar(); // init index page's custom scripts
		   Index.initCharts(); // init index page's custom scripts
		   Index.initChat();
		   Index.initMiniCharts();
		   Index.initDashboardDaterange();
		   Index.initIntro();
		   Tasks.initDashboardWidget();
		});
	</script>
<?include('footer.php');?>