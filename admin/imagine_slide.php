<?php
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}
require_once('config.php');
require "languages/lang.".$_SESSION['user_language'].".php";

if (!isset($_SESSION['random_key']) || strlen($_SESSION['random_key'])==0){
    $_SESSION['random_key'] = strtotime(date('Y-m-d H:i:s')); 
	$_SESSION['user_file_ext']= "";
}

#########################################################################################################
# CONSTANTS																								#
# You can alter the options below																		#
#########################################################################################################
$upload_dir         = "../public/images/slides"; 	// The directory for the images to be saved in
$contact_path       = "slides"; 	                // The directory for the images to be saved in the database
$upload_path        = $upload_dir."/";				// The path to where the image will be saved
$large_image_prefix = "original_"; 			        // The prefix name to large image
$thumb_image_prefix = "custom_";			        // The prefix name to the thumb image
$large_image_name   = $large_image_prefix.$_SESSION['random_key'];     // New name of the large image (append the timestamp to the filename)
$thumb_image_name   = $thumb_image_prefix.$_SESSION['random_key'];     // New name of the thumbnail image (append the timestamp to the filename)
$max_file           = "50"; 						// Maximum file size in MB
$image_id           = $_GET['id'];
$max_width          = "2000";						// Max width allowed for the large image
$thumb_width        = "1920";						// Width of thumbnail image
$thumb_height       = "1080";						// Height of thumbnail image

// Only one of these image types should be allowed for upload
$allowed_image_types    = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
$allowed_image_ext      = array_unique($allowed_image_types); // do not change this
$image_ext              = "";	// initialise variable, do not change this.


foreach ($allowed_image_ext as $mime_type => $ext) {
    $image_ext.= strtoupper($ext)." ";
}
function sanitazeStringForUrl($string){
    $str = strtolower(trim($string));
    $str = preg_replace('@[^a-zA-Z0-9]@', ' ', $str);
    $str = preg_replace('/(\s\s+|\-\-+)/', ' ', $str);
    return str_replace(' ','-',$str);
}

##########################################################################################################
# IMAGE FUNCTIONS																						 #
# You do not need to alter these functions																 #
##########################################################################################################
function resizeImage($image,$width,$height,$scale) {
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType      = image_type_to_mime_type($imageType);
	$newImageWidth  = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage       = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source = imagecreatefromgif($image);
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source = imagecreatefromjpeg($image);
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
	
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$image); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$image,80);
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$image);  
			break;
    }
	
	chmod($image, 0777);
	return $image;
}
//You do not need to alter these functions
function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType      = image_type_to_mime_type($imageType);
	
	$newImageWidth  = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage       = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source = imagecreatefromgif($image);
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source = imagecreatefromjpeg($image);
			break;
	    case "image/png":
		case "image/x-png":
			$source = imagecreatefrompng($image);
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,80);
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}
//You do not need to alter these functions
function getHeight($image) {
	$size       = getimagesize($image);
	$height     = $size[1];
	return $height;
}
//You do not need to alter these functions
function getWidth($image) {
	$size       = getimagesize($image);
	$width      = $size[0];
	return $width;
}

//Image Locations
$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
$thumb_image_location = $upload_path.$thumb_image_name.$_SESSION['user_file_ext'];

//Create the upload directory with the right permissions if it doesn't exist
if(!is_dir($upload_dir)){
	mkdir($upload_dir, 0777);
	chmod($upload_dir, 0777);
}

//Check to see if any images with the same name already exist
if (file_exists($large_image_location)){
	if(file_exists($thumb_image_location)){
		$thumb_photo_exists = "<img src=\"".$upload_path.$thumb_image_name.$_SESSION['user_file_ext']."\" alt=\"Thumbnail Image\"/>";
	}else{
		$thumb_photo_exists = "";
	}
   	$large_photo_exists = "<img src=\"".$upload_path.$large_image_name.$_SESSION['user_file_ext']."\" alt=\"Large Image\" />";
} else {
   	$large_photo_exists = "";
	$thumb_photo_exists = "";
}

if (isset($_POST["upload"])) { 
	//Get the file information
	$userfile_name = $_FILES['image']['name'];
	$userfile_tmp = $_FILES['image']['tmp_name'];
	$userfile_size = $_FILES['image']['size'];
	$userfile_type = $_FILES['image']['type'];
	$filename = basename($_FILES['image']['name']);
	$file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
	
	//Only process if the file is a JPG, PNG or GIF and below the allowed limit
	if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {
		
		foreach ($allowed_image_types as $mime_type => $ext) {
			//loop through the specified image types and if they match the extension then break out
			//everything is ok so go and check file size
			if($file_ext==$ext && $userfile_type==$mime_type){
				$error = "";
				break;
			}else{
				$error = "Only some extensions  <strong>".$image_ext."</strong> are allowed<br />";
			}
		}
		//check if the file size is above the allowed limit
		if ($userfile_size > ($max_file*1048576)) {
			$error.= "Max iamge size ".$max_file." MB";
		}
		
	}else{
	
		$error= '<h4>Image not uploaded. Please uplaod an image.</h4>';
	}
	//Everything is ok, so we can upload the image.
	if (strlen($error) == 0){
		
		if (isset($_FILES['image']['name'])){
			//this file could now has an unknown file extension (we hope it's one of the ones set above!)
			$large_image_location = $large_image_location.".".$file_ext;
			$thumb_image_location = $thumb_image_location.".".$file_ext;
			
			//put the file ext in the session so we know what file to look for once its uploaded
			$_SESSION['user_file_ext']=".".$file_ext;
			
			move_uploaded_file($userfile_tmp, $large_image_location);
			chmod($large_image_location, 0777);
			
			$width = getWidth($large_image_location);
			$height = getHeight($large_image_location);
			//Scale the image if it is greater than the width set above
			if ($width > $max_width){
				$scale = $max_width/$width;
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
			}else{
				$scale = 1;
				$uploaded = resizeImage($large_image_location,$width,$height,$scale);
			}
			//Delete the thumbnail file so the user can create a new one
			if (file_exists($thumb_image_location)) {
				unlink($thumb_image_location);
			}

			$db_image_names     = str_replace($upload_dir,$contact_path,$thumb_image_location);
								
			$query = "update slider set  image ='$db_image_names' where id='$image_id'";
			mysqli_query($mysqli,$query);
     		
		}		
		//Refresh the page to show the new uploaded image
		header("location:".$_SERVER["PHP_SELF"]."?id=".$_GET['id']);
		exit();
	}
}

if (isset($_POST["upload_thumbnail"]) && strlen($large_photo_exists)>0) {
	//Get the new coordinates to crop the image.
	$x1 = $_POST["x1"];
	$y1 = $_POST["y1"];
	$x2 = $_POST["x2"];
	$y2 = $_POST["y2"];
	$w = $_POST["w"];
	$h = $_POST["h"];
	//Scale the image to the thumb_width set above
	$scale = $thumb_width/$w;
	$cropped = resizeThumbnailImage($thumb_image_location, $large_image_location,$w,$h,$x1,$y1,$scale);
	//Reload the page again to view the thumbnail
	header("location:".$_SERVER["PHP_SELF"]."?id=".$_GET['id']);
	exit();
}


if ($_GET['a']=="delete" && strlen($_GET['t'])>0){

	$large_image_location = $upload_path.$large_image_prefix.$_GET['t'];
	$thumb_image_location = $upload_path.$thumb_image_prefix.$_GET['t'];
	
	if (file_exists($large_image_location)) {
		unlink($large_image_location);
	}
	if (file_exists($thumb_image_location)) {
		unlink($thumb_image_location);
	}
	header("location:".$_SERVER["PHP_SELF"]."?id=".$_GET['id']);
	exit(); 
}
if ($_GET['a']=="sterge" && strlen($_GET['ff'])>0){
//get the file locations 
	$large_image_location = $_GET['ff'];
	$thumb_image_location = $_GET['ff'];
	//echo $large_image_location;
	//die();
	
	if (file_exists($large_image_location)) {
		unlink($large_image_location);
	}
	if (file_exists($thumb_image_location)) {
		unlink($thumb_image_location);
	}
	header("location:".$_SERVER["PHP_SELF"]."?id=".$_GET['id']);
	exit(); 
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]--><!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]--><!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]--><!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Content management system</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="assets/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="../images/favicon.ico" />
	<script type="text/javascript" src="js/jquery-pack.js"></script>
	<script type="text/javascript" src="js/jquery.imgareaselect.min.js"></script>	
	<script language="javascript">
	var win = null;
	function NewWindow(mypage,myname,w,h,scroll){
	LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
	TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
	settings =
	'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
	win = window.open(mypage,myname,settings)
	}
	</script>
	<script type='text/javascript'>
	function popup( url, width, height )
	{
	   width = screen.width * width;
	   height = screen.height * height;
	   var params = "toolbar=no,width=" + width + ",height=" + height + ",left=0,top=0, screenX=100,screenY=100,status=no,scrollbars=yes,resize=yes";
	   window.open( url, "Max", params );
	   return false;
	}
	//<a href='#' onclick="popup('http://www.day-by-day.us/events/calendar.html','.6','1');">Events</a>
	</script>
</head>
<body class="page-header-fixed page-sidebar-fixed page-sidebar-closed">
	<div class="header navbar navbar-inverse navbar-fixed-top">		
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">		
		<div class="page-sidebar nav-collapse collapse">		
			<?include('meniu.php');?>		
		<div class="page-content">		
			<div class="container-fluid">				
				<div class="row-fluid">
					<div class="span12"><br/><br/>	
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php">Dashboar</a><i class="icon-angle-right"></i></li>
							<li><a href="#">Crop images to set sise</a></li>
						</ul>
					</div>
				</div>
				
					<?php
					//Only display the javacript if an image has been uploaded
					if(strlen($large_photo_exists)>0){
						$current_large_image_width  = getWidth($large_image_location);
						$current_large_image_height = getHeight($large_image_location);?>
					<script type="text/javascript">
					function preview(img, selection) { 
						var scaleX = <?php echo $thumb_width;?> / selection.width; 
						var scaleY = <?php echo $thumb_height;?> / selection.height; 
						
						$('#thumbnail + div > img').css({ 
							width: Math.round(scaleX * <?php echo $current_large_image_width;?>) + 'px', 
							height: Math.round(scaleY * <?php echo $current_large_image_height;?>) + 'px',
							marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
							marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
						});
						$('#x1').val(selection.x1);
						$('#y1').val(selection.y1);
						$('#x2').val(selection.x2);
						$('#y2').val(selection.y2);
						$('#w').val(selection.width);
						$('#h').val(selection.height);
					} 
					
					$(document).ready(function () { 
						$('#save_thumb').click(function() {
							var x1 = $('#x1').val();
							var y1 = $('#y1').val();
							var x2 = $('#x2').val();
							var y2 = $('#y2').val();
							var w = $('#w').val();
							var h = $('#h').val();
							if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
								alert("Trebuie sa efectuati o selectie pe imagine");
								return false;
							}else{
								return true;
							}
						});
					}); 
					$(window).load(function () { 
						$('#thumbnail').imgAreaSelect({ aspectRatio: '1:<?php echo $thumb_height/$thumb_width;?>', onSelectChange: preview });
					});
					</script>
					<?php }?>
					
					<table width="94%" align="center">	
						<tr>
							<td colspan="2" align="center"><h1 style="margin:10px 0">Crop image</h1></td>
						</tr>

						<tr>
							<td colspan="2" align="center">
                                <?php
                                    $directory      = $upload_path;
                                    $handle         = opendir($directory);
                                        $filename   = readdir($handle);
                                        $p          = array();
                                        $count      = 0;
                                        while (($filename = readdir($handle))!==false) {
                                            if ($filename != "." && $filename != ".." && strpos ($filename,".jpg")) {
                                            $p[] = $filename;
                                         $count++;
                                        }
                                       }
                                    echo "<br style='clear:both'/>";
                                        closedir($handle);
                                    ?>
                                    <script type="text/javascript">
                                        function showContent(vThis)
                                        {
                                            vParent = vThis.parentNode;
                                            vSibling = vParent.nextSibling;
                                            while (vSibling.nodeType==3) {
                                            vSibling = vSibling.nextSibling;
                                        };
                                            if(vSibling.style.display == "none")
                                        {
                                            vThis.src="all/img/128.png";
                                            vThis.alt = "Hide images";
                                            vSibling.style.display = "block";
                                            } else {
                                            vSibling.style.display = "none";
                                            vThis.src="all/img/127.png";
                                            vThis.alt = "Show iamges";
                                        }
                                        return;
                                        }
                                    </script>
                                <span><img src="all/img/127.png" title="Use previous loaded images"  width="40" style="cursor:pointer;" onclick="showContent(this);" /></span>
                                <div style="padding:2px; display:none; border:1px solid #f4f4f4">
                                    <h3>Only images wider than 1900px are shown</h3>
                                       <?php
                                        for ($i = 1; $i < $count; $i++) {
                                        list($width, $height, $type, $attr) = getimagesize($directory.$p[$i]);
                                            if($width > "1900"){?>
                                                <div style="float:left;padding:3px; margin:3px;border:1px solid #ccc;width:300px">
                                                    <img src="<?echo $directory.$p[$i];?>" style="height:180px"/><br/>
                                                    <div style="width:auto;owerflow:auto">
                                                        <div style="background:#f4f4f4;border:0;padding:2px;margin:2px;overflow:auto;font-size:10px"><?echo $fata_link;?><?echo $directory.$p[$i];?></div>
                                                    </div>
                                                    <a title="Delete this image" href="<?echo $_SERVER["PHP_SELF"];?>?a=sterge&ff=<?echo $directory.$p[$i];?>&id=<?echo $_GET['id'];?>"><img src="all/img/118.png"  border="0" width="20"/></a>
                                                    <br/>
                                                    Resolution: <?echo $width;?>x<?echo $height;?> px
                                                </div>
                                            <?}?>
                                        <?}?>
                                </div>
							</td>
						</tr>
						<tr valign="bottom">
							<td align="left" width="70%">
								<form  name="photo" enctype="multipart/form-data" action="<?php echo $_SERVER["PHP_SELF"];?>?id=<?echo $_GET['id'];?>" method="post">								
			                      	<input type="hidden" name="upload" value="Upload" />   
			                        <h3>Upload image:	<input type="file" name="image" />	</h3>
								 	<p>	
			                        	<button class="btn blue span4">Upload <i class="m-icon-swapright m-icon-white"></i></button>
			                        </p>			                         	
					 			</form>
							</td>
							<td align="right"><a href="all_slides.php" title="Back to slides" class="btn green span4">Back to slides</a></td>
						</tr>

						<tr>
							<td colspan="2"><hr/><br/></td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<?php
									if(strlen($error)>0){
										echo "<ul><li><strong>Error!</strong></li><li>".$error."</li></ul>";
									}
									if(strlen($large_photo_exists)>0 && strlen($thumb_photo_exists)>0){?>										
										<table align="center" >
											<tr valign="top">
												<td><?php echo $large_photo_exists;?></td>
											</tr>
											<tr><td height="20"></td></tr>
											<tr>	
												<td align="left">
													<?php echo $thumb_photo_exists;?><div style="clear:both"></div><br/>
													<a href="<?echo $_SERVER["PHP_SELF"]."?a=delete&t=".$_SESSION['random_key'].$_SESSION['user_file_ext'];?>&id=<?echo $_GET['id'];?>" style="margin-left:0;width:<?echo $thumb_width-26;?>px" class="btn red">Delete results</a><div style="clear:both"></div><br/>
													<a href="all_slides.php" class="btn blue" style="margin-left:0;width:<?echo $thumb_width-26;?>px">Keep slide and go back to all slides</a>
												</td>
											</tr>											
										</table><br/><br/>
										<?
										//Clear the time stamp session and user file extension
										$_SESSION['random_key']= "";
										$_SESSION['user_file_ext']= "";
									}else{
											if(strlen($large_photo_exists)>0){?>											
											<div align="center">
												<span class="label label-important">NOTE! Crop the image by creating a section on the below layer. The page oferflow is intended to scale the image in a 1:1 ratio</span><div style="clear:both"></div><br/>
												<img src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" id="thumbnail" alt="create thumbnail"/>

												<br style="clear:both;"/><br/>
												<form name="thumbnail" action="<?php echo $_SERVER["PHP_SELF"];?>?id=<?echo $_GET['id'];?>" method="post">
													<input type="hidden" name="x1" value="" id="x1" />
													<input type="hidden" name="y1" value="" id="y1" />
													<input type="hidden" name="x2" value="" id="x2" />
													<input type="hidden" name="y2" value="" id="y2" />
													<input type="hidden" name="w" value="" id="w" />
													<input type="hidden" name="h" value="" id="h" />
													<input type="submit" class="btn blue span4" name="upload_thumbnail" value="Save cropped section" id="save_thumb" />
												</form>
											</div>
										<hr />
										<?	} ?>
										<?	} ?>
										<br/><br/>
							</td>
						</tr>	 
						</table>	
										
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>	
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>	
	<script src="assets/scripts/app.js"></script>      
	<script>
		jQuery(document).ready(function() {    
		   App.init();
		});
	</script>
</div>
</body>
</html>