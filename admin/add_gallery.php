<?include("top_admin.php");?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL STYLES -->
	<script type='text/javascript'>
        function popup( url, width, height )
        {
            width = screen.width * width;
            height = screen.height * height;
            var params = "toolbar=no,width=" + width + ",height=" + height + ",left=0,top=0, screenX=100,screenY=100,status=no,scrollbars=yes,resize=yes";
            window.open( url, "Max", params );
            return false;
        }
        //<a href='#' onclick="popup('http://www.day-by-day.us/events/calendar.html','.6','1');">Events</a>
	</script>
	<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>
	</div>
	<div class="page-container">
		<div class="page-sidebar nav-collapse collapse">
			<?include('meniu.php');?>
			<div class="page-content">

				<?if(!isset($_GET['id'])){?>
				<!-- BEGIN PAGE CONTAINER-->
				<div class="container-fluid">
					<!-- BEGIN PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<h3 class="page-title"><?php echo $lang['AllCalendarPageTitle'];?></h3>
							<ul class="breadcrumb">
								<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
								<li><a href="#"><?php echo $lang['AllCalendarPageSubtitle'];?></a><span class="icon-angle-right"></span></li>
							</ul>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption"><i class="icon-reorder"></i><?php echo $lang['AllCalendarFormTitle'];?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="javascript:;" class="reload"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form action="actiune.php?pagini=add_gallery" class="form-horizontal" method="POST" enctype="multipart/form-data">

                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['FormCalendarLocatie'];?></label>
                                            <div class="controls">
                                                <input type="text" name="gallery_name"  class="span6 m-wrap popovers" required data-trigger="hover" data-content="<?php echo $lang['FormCalendarLocatieHint'];?>" data-original-title="<?php echo $lang['FormCalendarLocatieHintTitle'];?>" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['FormCalendarLocatieEN'];?></label>
                                            <div class="controls">
                                                <input type="text" name="gallery_name_en"  class="span6 m-wrap popovers" required data-trigger="hover" data-content="<?php echo $lang['FormCalendarLocatieHint'];?>" data-original-title="<?php echo $lang['FormCalendarLocatieHintTitle'];?>" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Tip galerie</label>
                                            <div class="controls">
                                                <select class="span2 m-wrap" name="gallery_type">
                                                    <option value="1">Foto</option>
                                                    <option value="2">Video</option>
                                                </select>
                                                <span class="help-inline">Se va face repartizarea galeriilor.</span>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['CalendarFormActive'];?></label>
                                            <div class="controls">
                                                <select class="span2 m-wrap" name="gallery_active">
                                                    <option value="yes"><?php echo $lang['CalendarFormActiveOpt1'];?></option>
                                                    <option value="no"><?php echo $lang['CalendarFormActiveOpt2'];?></option>
                                                </select>
                                                <span class="help-inline"><?php echo $lang['CalendarFormActiveHint'];?></span>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['FormCalendarOrder'];?></label>
                                            <div class="controls">
                                                <input type="text" name="gallery_order" class="span2 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormCalendarOrderHint'];?>" data-original-title="<?php echo $lang['FormCalendarOrderHintTitle'];?>" />
                                            </div>
                                        </div>
										<div class="control-group">
											<label class="control-label"><?php echo $lang['SlideFormNotes'];?></label>
											<div class="controls">
												<input type="text" name="notes" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['SlideFormNotesHintText'];?>" data-original-title="" />
											</div>
										</div>

										<div class="form-actions">
											<button type="submit" class="btn blue"><?php echo $lang['FormEditCalendarBtn'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="reset" class="btn"><?php echo $lang['FormResetCalendarBtn'];?></button>
										</div>
									</form>
									<!-- END FORM-->
								</div>
							</div>
							<!-- END SAMPLE FORM PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT-->
					<?}else{?>

					<?php
						$id     = $_GET['id'];
						$sqlp   = "SELECT * FROM gallery_details WHERE id='$id'";
						$resultp= mysqli_query($mysqli,$sqlp);
						$rowp   = mysqli_fetch_array($resultp);
					?>
					<!-- BEGIN PAGE CONTAINER-->
					<div class="container-fluid">
						<!-- BEGIN PAGE HEADER-->
						<div class="row-fluid">
							<div class="span12">
								<h3 class="page-title"><?php echo $lang['EditAllCalendarPageTitle'];?></h3>
								<ul class="breadcrumb">
									<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
									<li><a href="#"><?php echo $lang['EditAllCalendarPageSubtitle'];?></a><span class="icon-angle-right"></span></li>
								</ul>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption"><i class="icon-reorder"></i><?php echo $lang['EditCalendarPageSubTitle'];?></div>
										<div class="tools">
											<a href="javascript:;" class="collapse"></a>
											<a href="javascript:;" class="reload"></a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="actiune.php?pagini=edit_gallery" class="form-horizontal" method="POST" enctype="multipart/form-data">
											<input type="hidden" name="id" value="<?php echo $rowp['id'];?>" >

											<div class="control-group">
												<label class="control-label"><?php echo $lang['FormCalendarLocatie'];?></label>
												<div class="controls">
													<input type="text" name="gallery_name" value="<?php echo $rowp['gallery_name'];?>" class="span6 m-wrap popovers" required data-trigger="hover" data-content="<?php echo $lang['FormCalendarLocatieHint'];?>" data-original-title="<?php echo $lang['FormCalendarLocatieHintTitle'];?>" />
												</div>
											</div>

                                            <div class="control-group">
                                                <label class="control-label"><?php echo $lang['FormCalendarLocatieEN'];?></label>
                                                <div class="controls">
                                                    <input type="text" name="gallery_name_en" value="<?php echo $rowp['gallery_name_en'];?>" class="span6 m-wrap popovers" required data-trigger="hover" data-content="<?php echo $lang['FormCalendarLocatieHint'];?>" data-original-title="<?php echo $lang['FormCalendarLocatieHintTitle'];?>" />
                                                </div>
                                            </div>


                                            <div class="control-group">
                                                <label class="control-label">Tip galerie</label>
                                                <div class="controls">
                                                    <select class="span2 m-wrap" name="gallery_type">
                                                        <option value="1" <?php if($rowp['gallery_type'] == "1"){?> selected<?php }?>>Foto</option>
                                                        <option value="2" <?php if($rowp['gallery_type'] == "2"){?> selected<?php }?>>Video</option>
                                                    </select>
                                                    <span class="help-inline">Se va face repartizarea galeriilor.</span>
                                                </div>
                                            </div>

											<div class="control-group">
												<label class="control-label"><?php echo $lang['CalendarFormActive'];?></label>
												<div class="controls">
													<select class="span2 m-wrap" name="gallery_active">
														<option value="yes" <?php if($rowp['gallery_active'] == "yes"){?> selected<?php }?>><?php echo $lang['CalendarFormActiveOpt1'];?></option>
														<option value="no" <?php if($rowp['gallery_active'] == "no"){?> selected<?php }?>><?php echo $lang['CalendarFormActiveOpt2'];?></option>
													</select>
													<span class="help-inline"><?php echo $lang['CalendarFormActiveHint'];?></span>
												</div>
											</div>

											<div class="control-group">
												<label class="control-label"><?php echo $lang['FormCalendarOrder'];?></label>
												<div class="controls">
													<input type="text" name="gallery_order" value="<?php echo $rowp['gallery_order'];?>" class="span2 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['FormCalendarOrderHint'];?>" data-original-title="<?php echo $lang['FormCalendarOrderHintTitle'];?>" />
												</div>
											</div>

											<div class="control-group">
												<label class="control-label"><?php echo $lang['SlideFormNotes'];?></label>
												<div class="controls">
													<input type="text" name="notes" value="<?php echo $rowp['notes'];?>"  class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['SlideFormNotesHintText'];?>" data-original-title="<?php echo $lang['SlideFormNotesHintTitle'];?>" />
												</div>
											</div>


											<div class="form-actions">
												<button type="submit" class="btn blue"><?php echo $lang['FormEditCalendarBtn'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
												<button type="reset" class="btn"><?php echo $lang['FormResetCalendarBtn'];?></button>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
								<!-- END SAMPLE FORM PORTLET-->
							</div>
						</div>
						<!-- END PAGE CONTENT-->

						<?}?>
					</div>
					<!-- END PAGE CONTAINER-->
				</div>
				<!-- END PAGE -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
			<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
			<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
			<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
			<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
			<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
			<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
			<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="assets/scripts/app.js"></script>
			<script src="assets/scripts/form-components.js"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
                jQuery(document).ready(function() {
                    // initiate layout and plugins
                    App.init();
                    FormComponents.init();
                });
			</script>
			<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
			<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
			<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
			<!--[if lt IE 9]>
			<script src="assets/plugins/excanvas.min.js"></script>
			<script src="assets/plugins/respond.min.js"></script>
			<![endif]-->
			<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
			<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
			<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
			<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
			<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
			<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="assets/scripts/app.js" type="text/javascript"></script>
			<script src="assets/scripts/tasks.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
                jQuery(document).ready(function() {
                    App.init(); // initlayout and core plugins
                    Index.init();
                    Index.initJQVMAP(); // init index page's custom scripts
                    Index.initCalendar(); // init index page's custom scripts
                    Index.initCharts(); // init index page's custom scripts
                    Index.initChat();
                    Index.initMiniCharts();
                    Index.initDashboardDaterange();
                    Index.initIntro();
                    Tasks.initDashboardWidget();
                });
			</script>
			<!-- END JAVASCRIPTS -->
<?include('footer.php');?>