<?include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<!-- END PAGE LEVEL STYLES -->
<script type="text/javascript"> 
function addFile(){ 
var root=document.getElementById('mytab').getElementsByTagName('tr')[0].parentNode; 
var oR = cE('tr');var oC = cE('td');var oI = cE('input'); var oS=cE('span') 
cA(oI,'type','file');cA(oI,'name','file[]'); 
oS.style.cursor='pointer'; 
oS.onclick=function(){ 
this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode) 
} 
oS.appendChild(document.createTextNode(' Sterge')); 
oC.appendChild(oI);oC.appendChild(oS);oR.appendChild(oC);root.appendChild(oR); 
} 
function cE(el){ 
this.obj =document.createElement(el); 
return this.obj 
} 
function cA(obj,att,val){ 
obj.setAttribute(att,val); 
return 
} 
</script> 
<script type='text/javascript'>
function popup( url, width, height )
{
   width = screen.width * width;
   height = screen.height * height;
   var params = "toolbar=no,width=" + width + ",height=" + height + ",left=0,top=0, screenX=100,screenY=100,status=no,scrollbars=yes,resize=yes";
   window.open( url, "Max", params );
   return false;
}
//<a href='#' onclick="popup('http://www.day-by-day.us/events/calendar.html','.6','1');">Events</a>
</script>

<body class="page-header-fixed">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">	
		
		<?if(!isset($_GET['id'])){?>			
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['AddPageTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['AddPageSubTitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">						
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $lang['AddPageFormTitle'];?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>									
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="actiune.php?pagini=add_pagina" class="form-horizontal" method="POST" enctype="multipart/form-data">
									<input type="hidden" name="tip" value="<?php echo $_GET['tip'];?>"/>

                                    <div class="control-group">
                                        <label class="control-label">Nume <?php echo strtolower(page_type($_GET['tip'],$_SESSION['user_language']));?> (RO)</label>
                                        <div class="controls">
                                            <input type="text" class="span6 m-wrap" name="page_title" maxlength="256" required/>
                                            <span class="help-inline"><?php echo $lang['EditPageFormTitleHint'];?></span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Nume <?php echo strtolower(page_type($_GET['tip'],$_SESSION['user_language']));?> (EN) </label>
                                        <div class="controls">
                                            <input type="text" class="span6 m-wrap" name="page_title_en" maxlength="256" required/>
                                            <span class="help-inline"><?php echo $lang['EditPageFormTitleHint'];?></span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormActive'];?></label>
                                        <div class="controls">
                                            <select class="span3 m-wrap" name="page_active" data-placeholder="Select active status" tabindex="1">
                                                <option value="yes"><?php echo $lang['EditPageFormActiveOpt1'];?></option>
                                                <option value="no"><?php echo $lang['EditPageFormActiveOpt2'];?></option>
                                            </select>
                                            <span class="help-inline"><?php echo $lang['EditPageFormActiveHint'];?></span>
                                        </div>
                                    </div>

                                    <?php if($_SESSION['user_type'] == 1){?>
                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['EditPageFormApproved'];?></label>
                                            <div class="controls">
                                                <select class="span3 m-wrap" name="page_approved" data-placeholder="Select approval" tabindex="1">
                                                    <option value="1"><?php echo $lang['EditPageFormApprovedOpt1'];?></option>
                                                    <option value="2"><?php echo $lang['EditPageFormApprovedOpt2'];?></option>
                                                </select>
                                                <span class="help-inline"><?php echo $lang['EditPageFormApprovedHint'];?></span>
                                            </div>
                                        </div>
                                    <?php }?>

									<div class="control-group">
										<label class="control-label"><?php echo $lang['EditPageFormOrder'];?></label>
										<div class="controls">
											<input type="number" step="1" min="0" max="999" name="page_order" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['EditPageFormOrderHintText'];?>" data-original-title="<?php echo $lang['EditPageFormOrderHintTitle'];?>" />
										</div>
									</div>

                                    <div class="control-group">
                                        <label class="control-label">Tip watermark</label>
                                        <div class="controls">
                                            <select class="span3 m-wrap" name="gallery_watermark">
                                                <option value="0">Fara watermark</option>
                                                <option value="1">de culoare alba</option>
                                                <option value="2">de culoare neagra</option>
                                            </select>
                                            <span class="help-inline">Ce fel de watermark se va adauga la imaginile din galerie</span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">Categorie galerie</label>
                                        <div class="controls">
                                            <select class="span3 m-wrap" name="partner_id" data-placeholder="Select partner" >
                                                <?php
                                                $sqlm       = "SELECT id, gallery_type,  gallery_name  FROM gallery_details WHERE gallery_active = 'yes' ORDER BY id DESC ";
                                                $resultm    = mysqli_query($mysqli,$sqlm);
                                                while($rowm = mysqli_fetch_array($resultm)){?>
                                                    <option value="<?php echo $rowm['id'];?>"><?php echo $rowm['gallery_name'];?> (<?php echo galleryType($rowm['gallery_type']);?>)</option>
                                                <?php };?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaTitle'];?> (RO)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_title" class="span9 m-wrap popovers" data-trigger="hover" maxlength="256" data-content="<?php echo $lang['EditPageFormMetaTitleHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaTitleHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaTitle'];?> (EN)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_title_ro" class="span9 m-wrap popovers" data-trigger="hover" maxlength="256" data-content="<?php echo $lang['EditPageFormMetaTitleHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaTitleHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaDesc'];?> (RO)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_description" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['EditPageFormMetaDescHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaDescHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaDesc'];?> (EN)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_description_en" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['EditPageFormMetaDescHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaDescHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormContent'];?> (RO)</label>
                                        <div class="controls">
                                            <textarea class="span9 ckeditor m-wrap" name="page_content" rows="10"></textarea>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormContent'];?> (EN)</label>
                                        <div class="controls">
                                            <textarea class="span9 ckeditor m-wrap" name="page_content_en" rows="10"></textarea>
                                        </div>
                                    </div>

									<div class="control-group">
										<label class="control-label"><?php echo $lang['EditPageFormImage'];?></label>
										<div class="controls">
											<?php echo $lang['EditPageFormImageNote'];?>
										</div>
									</div>

									<?php if($_GET['tip'] == "4"){?>
                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['EditPageFormImageAction7'];?></label>
                                            <div class="controls">
                                                <?php echo $lang['EditPageFormImageAction8'];?>
                                            </div>
                                        </div>
									<?php }?>

									<div class="control-group">
										<label class="control-label"><?php echo $lang['EditPageFormNotes'];?></label>
										<div class="controls">
											<input type="text" name="notes" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['EditPageFormNotesHintText'];?>" data-original-title="<?php echo $lang['EditPageFormNotesHintTitle'];?>" />
										</div>
									</div>

									<?php
                                        if($_GET['tip'] == "3"){
                                            $page_extras = json_decode('{"scurta_descriere":"","scurta_descriere_en":"","youtube_video_url":"","url_material_download":""}', true);
                                            $how_many    = count($page_extras);

                                            if($how_many > 0){
                                                ?>
                                                <hr/>
                                                <h3><?php echo $lang['EditPageFormCustom'];?></h3>
                                                <?php foreach ($page_extras as $k => $v){?>
                                                    <div class="control-group">
                                                        <label class="control-label"><?php echo read_key($k);?></label>
                                                        <div class="controls">
                                                            <input type="text" name="page_extras[<?php echo $k;?>]" class="span12 m-wrap popovers" value="<?php echo $v;?>" maxlength="256" data-placement="top" data-trigger="hover" data-content="<?php echo $lang['EditPageFormCustomHintText'];?>" data-original-title="<?php echo $lang['EditPageFormCustomHintTitle'];?>" />
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            <?php }?>
                                    <?php }?>

									<?php
									if($_GET['tip'] == "4"){
										$page_extras = json_decode('{"scurta_descriere":"","scurta_descriere_en":"","parola_galerie":"","taguri_galerie":"","autor":"Valentin Hudubet"}', true);
										$how_many    = count($page_extras);

										if($how_many > 0){
											?>
                                            <hr/>
                                            <h3><?php echo $lang['EditPageFormCustom'];?></h3>
											<?php foreach ($page_extras as $k => $v){?>
                                                <div class="control-group">
                                                    <label class="control-label"><?php echo read_key($k);?></label>
                                                    <div class="controls">
                                                        <input type="text" name="page_extras[<?php echo $k;?>]" class="span12 m-wrap popovers" value="<?php echo $v;?>" maxlength="256" data-placement="top" data-trigger="hover" data-content="<?php echo $lang['EditPageFormCustomHintText'];?>" data-original-title="<?php echo $lang['EditPageFormCustomHintTitle'];?>" />
                                                    </div>
                                                </div>
											<?php }?>
										<?php }?>
									<?php }?>


									<div class="form-actions">
										<button type="submit" class="btn blue"><?php echo $lang['EditPageFormBtnAdd'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn"><?php echo $lang['EditPageFormBtnReset'];?></button>
									</div>
								</form>
								<!-- END FORM-->  
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>				
				<!-- END PAGE CONTENT--> 
			<?}else{?>
			
<?php
$id         = $_GET['id'];
$sqlp       = "SELECT * from pages where id='$id'";
$resultp    = mysqli_query($mysqli,$sqlp);
$rowp       = mysqli_fetch_array($resultp);
$partner_id = $rowp['partner_id'];

?>
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['EditPageTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['EditPageSubTitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">						
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $lang['EditPageFormTitle'];?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>									
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form action="actiune.php?pagini=update_page" class="form-horizontal" method="POST" enctype="multipart/form-data">
									<input type="hidden" name="id" value="<?php echo $id;?>" />
									<input type="hidden" name="tip" value="<?php echo $rowp['page_type'];?>" />

                                    <div class="control-group">
                                        <label class="control-label"><?php echo ucfirst(page_type($rowp['page_type'],$_SESSION['user_language']));?> titlu <br/>(RO)</label>
                                        <div class="controls">
                                            <input type="text" class="span6 m-wrap" name="page_title" value="<?php echo $rowp['page_title'] ;?>" maxlength="256" required/>
                                            <span class="help-inline"><?php echo $lang['EditPageFormTitleHint'];?></span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo ucfirst(page_type($rowp['page_type'],$_SESSION['user_language']));?> titlu <br/>(EN)</label>
                                        <div class="controls">
                                            <input type="text" class="span6 m-wrap" name="page_title_en" value="<?php echo $rowp['page_title_en'] ;?>" maxlength="256" required/>
                                            <span class="help-inline"><?php echo $lang['EditPageFormTitleHint'];?></span>
                                        </div>
                                    </div>

                                    <?php if($rowp['page_type'] > 2){?>
                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['EditPageFormActive'];?></label>
                                            <div class="controls">
                                                <select class="span3 m-wrap" name="page_active" data-placeholder="Select active status" tabindex="1">
                                                    <option value="yes" <?php echo ($rowp['page_active'] == "yes" ? 'selected' : '') ;?>><?php echo $lang['EditPageFormActiveOpt1'];?></option>
                                                    <option value="no" <?php echo ($rowp['page_active'] == "no" ? 'selected' : '') ;?>><?php echo $lang['EditPageFormActiveOpt2'];?></option>
                                                </select>
                                                <span class="help-inline"><?php echo $lang['EditPageFormActiveHint'];?></span>
                                            </div>
                                        </div>

                                        <?php if($_SESSION['user_type'] == 1){?>
                                            <div class="control-group">
                                                <label class="control-label"><?php echo $lang['EditPageFormApproved'];?></label>
                                                <div class="controls">
                                                    <select class="span3 m-wrap" name="page_approved" data-placeholder="Select approval" tabindex="1">
                                                        <option value="1" <?php echo ($rowp['page_approved'] == "1" ? 'selected' : '') ;?>><?php echo $lang['EditPageFormApprovedOpt1'];?></option>
                                                        <option value="2" <?php echo ($rowp['page_approved'] == "2" ? 'selected' : '') ;?>><?php echo $lang['EditPageFormApprovedOpt2'];?></option>
                                                    </select>
                                                    <span class="help-inline"><?php echo $lang['EditPageFormApprovedHint'];?></span>
                                                </div>
                                            </div>
                                        <?php }?>

                                        <div class="control-group">
                                            <label class="control-label"><?php echo $lang['EditPageFormOrder'];?></label>
                                            <div class="controls">
                                                <input type="number" step="1" min="0" max="999" name="page_order" value="<?php echo $rowp['page_order'] ;?>" class="span3 m-wrap popovers" data-trigger="hover" data-content="<?php echo $lang['EditPageFormOrderHintText'];?>" data-original-title="<?php echo $lang['EditPageFormOrderHintTitle'];?>" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Tip watermark</label>
                                            <div class="controls">
                                                <select class="span3 m-wrap" name="gallery_watermark">
                                                    <option value="0" <?php if($rowp['gallery_watermark'] == "0"){?> selected<?php }?>>Fara watermark</option>
                                                    <option value="1" <?php if($rowp['gallery_watermark'] == "1"){?> selected<?php }?>>de culoare alba</option>
                                                    <option value="2" <?php if($rowp['gallery_watermark'] == "2"){?> selected<?php }?>>de culoare neagra</option>
                                                </select>
                                                <span class="help-inline">Ce fel de watermark se va adauga la imaginile din galerie</span>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label">Categorie galerie</label>
                                            <div class="controls">
                                                <select class="span3 m-wrap" name="partner_id" data-placeholder="Select partner" >
				                                    <?php
				                                    $sqlm       = "SELECT id, gallery_type,  gallery_name  FROM gallery_details WHERE gallery_active = 'yes' ORDER BY id DESC ";
				                                    $resultm    = mysqli_query($mysqli,$sqlm);
				                                    while($rowm = mysqli_fetch_array($resultm)){?>
                                                        <option value="<?php echo $rowm['id'];?>" <?php echo ($partner_id == $rowm['id'] ? 'selected' : '') ;?>><?php echo $rowm['gallery_name'];?> (<?php echo galleryType($rowm['gallery_type']);?>)</option>
				                                    <?php };?>
                                                </select>
                                            </div>
                                        </div>

                                        <?php /*
                                            <div class="control-group">
                                                <label class="control-label">Page associated with a partner</label>
                                                <div class="controls">
                                                    <select class="span3 m-wrap" name="partner_id" data-placeholder="Select partner" tabindex="1">
                                                        <option value="0" <?php echo ($partner_id == 0 ? 'selected' : '') ;?>>not applicable</option>
                                                        <?php
                                                            $sqlm       = "SELECT id, user_name  FROM u_login WHERE user_type IN ('2','3') AND user_permision = 1 ";
                                                            $resultm    = mysqli_query($mysqli,$sqlm);
                                                            while($rowm = mysqli_fetch_array($resultm)){
                                                        ?>
                                                            <option value="<?php echo $rowm['id'];?>" <?php echo ($partner_id == $rowm['id'] ? 'selected' : '') ;?>><?php echo $rowm['user_name'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                         */ ?>

                                    <?php }else{ // set inputs preset defaults?>

                                        <input type="hidden" name="page_active" value="yes">
                                        <input type="hidden" name="page_approved" value="0">
                                        <input type="hidden" name="page_order" value="0">
                                        <input type="hidden" name="partner_id" value="0">

                                    <?php }?>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaTitle'];?> (RO)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_title" value="<?php echo $rowp['page_meta_title'] ;?>" class="span9 m-wrap popovers" data-trigger="hover" maxlength="256" data-content="<?php echo $lang['EditPageFormMetaTitleHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaTitleHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaTitle'];?> (EN)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_title_en" value="<?php echo $rowp['page_meta_title_en'] ;?>" class="span9 m-wrap popovers" data-trigger="hover" maxlength="256" data-content="<?php echo $lang['EditPageFormMetaTitleHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaTitleHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaDesc'];?> (RO)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_description" value="<?php echo $rowp['page_meta_description'] ;?>" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['EditPageFormMetaDescHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaDescHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormMetaDesc'];?> (EN)</label>
                                        <div class="controls">
                                            <input type="text" name="page_meta_description_en" value="<?php echo $rowp['page_meta_description_en'] ;?>" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['EditPageFormMetaDescHintText'];?>" data-original-title="<?php echo $lang['EditPageFormMetaDescHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormContent'];?> (RO)</label>
                                        <div class="controls">
                                            <textarea class="span9 ckeditor m-wrap" name="page_content" rows="10"><?php echo $rowp['page_content'] ;?></textarea>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormContent'];?> (EN)</label>
                                        <div class="controls">
                                            <textarea class="span9 ckeditor m-wrap" name="page_content_en" rows="10"><?php echo $rowp['page_content_en'] ;?></textarea>
                                        </div>
                                    </div>

                                    <div class="control-group">
										<label class="control-label"><?php echo $lang['EditPageFormImage'];?></label>
										<div class="controls">                        

                                            <img src="<?php echo ($rowp['main_image'] != "" ? '../public/images/'.$rowp['main_image'] : 'https://via.placeholder.com/350x150?text=admin placeholder');?>" class="span3" style="border:2px solid #999;margin-bottom:10px"/>
                                            <div style="clear:both"></div>
                                            <div class="btn-group">
                                                <div class="btn-group">
                                                    <button class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><?php echo $lang['EditPageFormImageActions'];?> <i class="icon-angle-down"></i></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a style="cursor:pointer" title="<?php echo $lang['EditPageFormImageAction1'];?>" onclick="popup('edit_img_art_central.php?id=<?php echo $rowp['id'];?>&s=da','.9','.76');"><?php echo $lang['EditPageFormImageAction2'];?></a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="actiune.php?pagini=delete_page_image&id=<?php echo $rowp['id'];?>" title="<?php echo $lang['EditPageFormImageAction3'];?>" onclick="return confirm( '<?php echo $lang["EditPageFormImageAction4"];?>' )" ><?php echo $lang['EditPageFormImageAction5'];?></a></li>
                                                    </ul>
                                                    <div style="width:57px;float:left"></div>
                                                </div>
                                                <?php if($rowp['page_type'] > 3){?>
                                                    <?php if($rowp['page_approved'] == 1){?>
                                                        <a href="galerie.php?id=<?php echo $rowp['id'];?>&w=<?php echo $rowp['gallery_watermark'];?>&np=<?php echo  base64_encode($rowp['page_title']);?>" class="btn red" target="_blank"><?php echo $lang['EditPageFormImageAction6'];?></a>
	                                                <?php }?>
                                                <?php }?>
                                            </div>

										</div>
									</div>

									<?php if($id == 1){?>
                                        <div class="control-group">
                                            <label class="control-label">Imagine fundal</label>
                                            <div class="controls">
                                                <a href="edit_img_art_central.php?id=<?php echo $id;?>|3|1920|750|pages|bg_image"  target="_blank" class="btn blue">Incarca imagine fundal (1920x750px)</a><br/><br/>
                                                <img src="https://www.valentinhudubet.com/public/images/<?php echo $rowp['bg_image'];?>" class="img-responsive" style="max-width: 600px">
                                            </div>
                                        </div>
									<?php }else{ ?>

                                        <?php if($rowp['page_type'] > 2){?>

                                            <div class="control-group">
                                                <label class="control-label">Imagine fundal</label>
                                                <div class="controls">
                                                    <a href="edit_img_art_central.php?id=<?php echo $id;?>|3|1980|600|pages|bg_image"  target="_blank" class="btn blue">Incarca imagine header (1980x600px)</a><br/><br/>
                                                    <img src="https://www.valentinhudubet.com/public/images/<?php echo $rowp['bg_image'];?>" class="img-responsive" style="max-width: 600px">
                                                </div>
                                            </div>

                                        <?php } else {?>

                                            <div class="control-group">
                                                <label class="control-label">Imagine fundal</label>
                                                <div class="controls">
                                                    <a href="edit_img_art_central.php?id=<?php echo $id;?>|3|1920|550|pages|bg_image"  target="_blank" class="btn blue">Incarca imagine header (1920x550px)</a><br/><br/>
                                                    <img src="https://www.valentinhudubet.com/public/images/<?php echo $rowp['bg_image'];?>" class="img-responsive" style="max-width: 600px">
                                                </div>
                                            </div>

										<?php }?>
									<?php }?>

                                    <div class="control-group">
                                        <label class="control-label"><?php echo $lang['EditPageFormNotes'];?></label>
                                        <div class="controls">
                                            <input type="text" name="notes" class="span9 m-wrap popovers" value="<?php echo htmle($rowp['notes']);?>" maxlength="250" data-trigger="hover" data-content="<?php echo $lang['EditPageFormNotesHintText'];?>" data-original-title="<?php echo $lang['EditPageFormNotesHintTitle'];?>" />
                                        </div>
                                    </div>

                                    <?php if($rowp['page_type'] > 3 && $rowp['page_approved'] == 2){?>
                                        <hr/>
                                        <h3>Incarca video la galerie</h3>
                                        <div class="control-group">
                                            <label class="control-label">Adauga url video <a href="" title="Adauga inca unul" id="addScnt"><img src="all/img/95.png" style="width: 40px; border: 0;"> </a></label>
                                            <div class="controls" id="p_scents">
                                                <input type="text" id="p_scnt"  class="span9 m-wrap popovers" name="video_codes[]" value="" placeholder="Adauga cod video din youtube" style="margin-bottom: 10px"/>
                                            </div>
                                        </div>

                                        <?php if($rowp['video_codes'] !=''){

                                            $boom   = explode(", ",$rowp['video_codes']);
                                            foreach ($boom as $video_cod){
                                                if($video_cod !=''){?>
                                                    <span class="<?php echo $video_cod;?>">
                                                        <iframe height="180" src="https://www.youtube.com/embed/<?php echo $video_cod;?>" frameborder="0" allowfullscreen></iframe>
                                                        <img title="Sterge acest video" src="all/img/118.png" class="deleteExistingVideo" data-id="<?php echo $video_cod;?>">
                                                        <input type="hidden" name="video_codes[]" value="<?php echo $video_cod;?>">
                                                    </span>
                                                <?php }?>
                                            <?php }?>
                                        <?php }?>
                                    <?php }?>

                                    <?php
                                        $page_extras = json_decode($rowp['page_extras'], true);
                                        $how_many    = count($page_extras);

                                        if($how_many > 0){
                                    ?>
                                        <hr/>
                                        <h3><?php echo $lang['EditPageFormCustom'];?></h3>
                                            <?php foreach ($page_extras as $k => $v){?>
                                                <div class="control-group">
                                                    <label class="control-label"><?php echo read_key($k);?></label>
                                                    <div class="controls">
                                                        <input type="text" name="page_extras[<?php echo $k;?>]" class="span12 m-wrap popovers" value="<?php echo $v;?>" maxlength="256" data-placement="top" data-trigger="hover" data-content="<?php echo $lang['EditPageFormCustomHintText'];?>" data-original-title="<?php echo $lang['EditPageFormCustomHintTitle'];?>" />
                                                    </div>
                                                </div>
                                            <?php }?>
                                    <?php }?>

									<div class="form-actions">
										<button type="submit" class="btn blue"><?php echo $lang['EditPageFormBtnEdit'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="reset" class="btn"><?php echo $lang['EditPageFormBtnReset'];?></button>
									</div>
								</form>


								<!-- END FORM-->  
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>				
				<!-- END PAGE CONTENT-->         
			
			<?}?>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="//cdn.ckeditor.com/4.4.1/full/ckeditor.js"></script>	
		<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/form-components.js"></script>     
	<!-- END PAGE LEVEL SCRIPTS -->

	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/tasks.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		$(document).ready(function() {
		   App.init();
            // adaugare multipla de inputs

            $(function() {
                var scntDiv = $('#p_scents');
                var i = $('#p_scents p').size() + 1;

                $('#addScnt').live('click', function() {
                    $('<div class="hldr"><input type="text" id="p_scnt" class="span9 m-wrap popovers" name="video_codes[]" value="" placeholder="Adauga cod video din youtube" /><a href="" class="remScnt" title="Sterge intrare"><img src="all/img/118.png" style="width:35px"></a></div>').appendTo(scntDiv);
                    i++;
                    return false;
                });

                $('.remScnt').live('click', function() {
                    if( i > 1 ) {
                        $(this).parents('div.hldr').remove();
                        i--;
                    }
                    return false;
                });

		    });

            $('.deleteExistingVideo').click(function(e){
                e.preventDefault();

                var videoCode       = $(this).data("id");
                $("input[value='"+videoCode+"']").remove();
                $("span."+videoCode).remove();

            });

        });

	</script>
            <style type="text/css">
               .hldr{margin-bottom: 10px}
                a.remScnt,  a.remScnt:hover {text-decoration: none;}
               .deleteExistingVideo{
                   cursor: pointer;
                   z-index: 200;
                   position: absolute;
                   margin-left: -180px;
                   margin-top: 110px;
               }
            </style>
	<!-- END JAVASCRIPTS -->   
	<?include('footer.php');?>