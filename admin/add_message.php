<?include("top_admin.php");?>
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<script type="text/javascript">
        function addFile(){
            var root=document.getElementById('mytab').getElementsByTagName('tr')[0].parentNode;
            var oR = cE('tr');var oC = cE('td');var oI = cE('input'); var oS=cE('span')
            cA(oI,'type','file');cA(oI,'name','file[]');
            oS.style.cursor='pointer';
            oS.onclick=function(){
                this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode)
            }
            oS.appendChild(document.createTextNode(' Remove'));
            oC.appendChild(oI);oC.appendChild(oS);oR.appendChild(oC);root.appendChild(oR);
        }
        function cE(el){
            this.obj =document.createElement(el);
            return this.obj
        }
        function cA(obj,att,val){
            obj.setAttribute(att,val);
            return
        }
	</script>
	<!-- END PAGE LEVEL STYLES -->
	<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>
	</div>
	<div class="page-container">
		<div class="page-sidebar nav-collapse collapse">
			<?include('meniu.php');?>
			<div class="page-content">

				<?if(!isset($_GET['id'])){?>
				<!-- BEGIN PAGE CONTAINER-->
				<div class="container-fluid">
					<!-- BEGIN PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<h3 class="page-title">Messages <small> send notification to users</small>
							</h3>
							<ul class="breadcrumb">
								<li><i class="icon-home"></i><a href="index.php">Dashboard</a><span class="icon-angle-right"></span></li>
								<li><a href="#">Message details</a><span class="icon-angle-right"></span></li>
							</ul>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption"><i class="icon-reorder"></i>Page details</div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
										<a href="javascript:;" class="reload"></a>
									</div>
								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form action="actiune.php?pagini=create_message" class="form-horizontal" method="POST" enctype="multipart/form-data">

										<div class="control-group">
											<label class="control-label">Message subject</label>
											<div class="controls">
												<input type="text" class="span6 m-wrap" name="subject" maxlength="256" required/>
												<span class="help-inline">This will also be the email subject</span>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Notification type</label>
											<div class="controls">
												<select class="span3 m-wrap" name="notification" data-placeholder="Select active status" tabindex="1">
													<option value="yes">Website message and email</option>
													<option value="no">Only website message - no email</option>
												</select>
												<span class="help-inline">This property will determine if the message will be send via email or not</span>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Send notification to selected partners</label>
											<div class="controls">

												<?php
													$sqlm       = "SELECT id, user_name, user_email FROM u_login WHERE  user_permision = 1 ";
													$resultm    = mysqli_query($mysqli,$sqlm);
													while($rowm = mysqli_fetch_array($resultm)){
												?>
														<label><input type="checkbox" name="users[]" value="<?php echo $rowm['id'];?>" autocomplete="off" />  <strong><?php echo $rowm['user_name'];?></strong> (<?php echo $rowm['user_email'];?>)</label>
												<?php }?>


											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Message files </label>
											<div class="controls">
												<table id="mytab"  border="0">
													<tr>
														<td><input type="button" class="span12" onclick="addFile()" value="To upload more files click here" style="cursor:pointer;background:#4b8df8;color:#fff;font-size:14px;border:0px;"/>	</td>
													</tr>
													<tr>
														<td>
														<span class="btn btn-file">
															<span class="fileupload-new">Select file to upload (PDF, DOC(x), XLS(x), PPT, ZIP, RAR, etc)</span>
															<input type="file" class="default" name="file[]" />
														</span>
														</td>
													</tr>
												</table>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Message content</label>
											<div class="controls">
												<textarea class="span9 ckeditor m-wrap" name="content" rows="10"></textarea>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Message notes</label>
											<div class="controls">
												<input type="text" name="notes" class="span9 m-wrap popovers" maxlength="250" data-trigger="hover" data-content="Only for back-end. (max 256 char). Attribute available only for admins" data-original-title="Message notes" />
											</div>
										</div>
										<div class="form-actions">
											<button type="submit" class="btn blue">Send message</button>&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="reset" class="btn">Reset</button>
										</div>
									</form>
									<!-- END FORM-->
								</div>
							</div>
							<!-- END SAMPLE FORM PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT-->
					<?}else{?>

					<?php
					$id         = $_GET['id'];
					$sqlp       = "SELECT * from messages where id='$id'";
					$resultp    = mysqli_query($mysqli,$sqlp);
					$rowp       = mysqli_fetch_array($resultp);
					$project_id = $rowp['id'];

					?>
					<!-- BEGIN PAGE CONTAINER-->
					<div class="container-fluid">
						<!-- BEGIN PAGE HEADER-->
						<div class="row-fluid">
							<div class="span12">
								<h3 class="page-title">See message details <small>content and attachments</small>
								</h3>
								<ul class="breadcrumb">
									<li><i class="icon-home"></i><a href="index.php">Dashboard</a><span class="icon-angle-right"></span></li>
									<li><a href="#">Message details</a><span class="icon-angle-right"></span></li>
								</ul>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span12">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption"><i class="icon-reorder"></i>Message details</div>
										<div class="tools">
											<a href="javascript:;" class="collapse"></a>
											<a href="javascript:;" class="reload"></a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->

                                        <div class="control-group">
                                            <label class="control-label">Message subject:  <?php echo $rowp['subject'] ;?></label>
                                        </div>

                                        <br/>
                                        <div class="control-group">
                                            <label class="control-label">This message was sent to:</label>
                                            <div class="controls">
                                                <?php
                                                    $users      = json_decode($rowp['users'], true);
                                                    $how_many   = count($users);

                                                    if($how_many > 0) {

                                                        foreach ( $users as $id_user ) {

                                                            $sqlm    = "SELECT user_email FROM u_login WHERE  id = '$id_user' ";
                                                            $resultm = mysqli_query( $mysqli, $sqlm );
                                                            $rowm    = mysqli_fetch_array( $resultm );
                                                            echo $rowm['user_email']."<br/>";
                                                        }
                                                    }else{
                                                        echo 'No user was selected';
                                                    }
                                                ?>
                                            </div>
                                        </div>

                                        <br/>
                                        <div class="control-group">
                                            <label class="control-label">Message attachments:</label>
                                            <div class="controls">
												<?php
                                                    $sqla       = "SELECT *  from gallery WHERE id > 0 AND project_id = '$project_id' AND folder ='attach'";
                                                    $sqla      .= " order by id DESC ";
                                                    $resulta    = mysqli_query($mysqli,$sqla);
                                                    $attach_num = mysqli_num_rows($resulta);
												?>

                                                <?php if($attach_num > 0){?>
	                                                <?php while($rowa = mysqli_fetch_array($resulta)){?>
                                                        Attachment: <a href="../public/images/messages/<?php echo $rowa['image'];?>" target="_blank"><?php echo $rowa['doc_alt'];?> (<?php echo $rowa['doc_type'];?>)</a><br/>
	                                                <?php }?>
                                                <?php } else {?>
                                                    No attachments were present
                                                <?php }?>
                                            </div>
                                        </div>



                                        <br/>
                                        <div class="control-group">
                                            <label class="control-label">Message content</label>
                                            <div class="controls">
                                                <blockquote  class="blockquote" style=""><?php echo $rowp['content'];?></blockquote >
                                            </div>
                                        </div>

                                        <br/>
                                        <div class="control-group">
                                            <label class="control-label">Message notes</label>
                                            <div class="controls">
                                                <?php echo htmle($rowp['notes']);?>
                                            </div>
                                        </div>

										<!-- END FORM-->
									</div>
								</div>
								<!-- END SAMPLE FORM PORTLET-->
							</div>
						</div>
						<!-- END PAGE CONTENT-->

						<?}?>
					</div>
					<!-- END PAGE CONTAINER-->
				</div>
				<!-- END PAGE -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="//cdn.ckeditor.com/4.4.1/full/ckeditor.js"></script>
			<!--<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script> -->
			<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
			<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
			<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
			<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
			<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
			<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
			<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
			<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript" ></script>
			<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
			<script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript" ></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="assets/scripts/app.js"></script>
			<script src="assets/scripts/form-components.js"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
                jQuery(document).ready(function() {
                    // initiate layout and plugins
                    App.init();
                    FormComponents.init();
                });
			</script>
			<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
			<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
			<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
			<!--[if lt IE 9]>
			<script src="assets/plugins/excanvas.min.js"></script>
			<script src="assets/plugins/respond.min.js"></script>
			<![endif]-->
			<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
			<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
			<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
			<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
			<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
			<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
			<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
			<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
			<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<script src="assets/scripts/app.js" type="text/javascript"></script>
			<script src="assets/scripts/index.js" type="text/javascript"></script>
			<script src="assets/scripts/tasks.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
                jQuery(document).ready(function() {
                    App.init(); // initlayout and core plugins
                    Index.init();
                    Index.initJQVMAP(); // init index page's custom scripts
                    Index.initCalendar(); // init index page's custom scripts
                    Index.initCharts(); // init index page's custom scripts
                    Index.initChat();
                    Index.initMiniCharts();
                    Index.initDashboardDaterange();
                    Index.initIntro();
                    Tasks.initDashboardWidget();
                });
			</script>
			<!-- END JAVASCRIPTS -->
<?include('footer.php');?>