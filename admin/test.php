<?php
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}

$upload_max_size = ini_get('upload_max_filesize');

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Image gllery</title>
	<link rel="stylesheet" href="assets/css/styles-galerie.css" />
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>

<div id="dropbox">
	<span class="message">Drag and drop your images here. <br /><i>(max 20 images at once)</i></span><br/>
	<?php if($upload_max_size < "5"){?><p style="text-align:center">Server max upload limit <?php echo $upload_max_size;?>b/image</p><?}?>
</div>

<?php if($upload_max_size<5)
{
	$upload_max_size=$upload_max_size;}else{ $upload_max_size = 5;
}
?>

<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
<script src="assets/js/jquery.filedrop.js"></script>
<!--   <script src="assets/js/script.js"></script>    am pus mai jos sa se faca id cu php-->
<script type="text/javascript">
    $(function(){
        var dropbox = $('#dropbox'), message = $('.message', dropbox);
        dropbox.filedrop({
            paramname:'pic',
            maxfiles: 20,
            maxfilesize: <?echo $upload_max_size;?>,
            url: 'post_file.php?id=<?echo $_GET['id'];?>&np=<?php echo $_GET['np'];?>',
            uploadFinished:function(i,file,response){
                $.data(file).addClass('done');
            },
            error: function(err, file) {
                switch(err) {
                    case 'BrowserNotSupported':
                        showMessage('Your browser is not supported. Please try another one!');
                        break;
                    case 'TooManyFiles':
                        alert('Too many files. Upload limit is 20 at once');
                        break;
                    case 'FileTooLarge':
                        alert(file.name+' to large! Please upload images with a size smaller than 5 mb.');
                        break;
                    default:
                        break;
                }
            },
            beforeEach: function(file){
                if(!file.type.match(/^image\//)){
                    alert('Allowed extensions (jpg, jpeg, png, gif)!');
                    return false;
                }
            },
            uploadStarted:function(i, file, len){
                createImage(file);
            },
            progressUpdated: function(i, file, progress) {
                $.data(file).find('.progress').width(progress);
            }
        });
        var template = '<div class="preview">'+'<span class="imageHolder">'+'<img />'+'<span class="uploaded"></span>'+'</span>'+'<div class="progressHolder">'+'<div class="progress"></div>'+'</div>'+'</div>';
        function createImage(file){
            var preview = $(template), 	image = $('img', preview);
            var reader = new FileReader();
            image.width = 100;
            image.height = 100;
            reader.onload = function(e){
                image.attr('src',e.target.result);
            };
            reader.readAsDataURL(file);
            message.hide();
            preview.appendTo(dropbox);
            $.data(file,preview);
        }
        function showMessage(msg){
            message.html(msg);
        }
    });
</script>
</body></html>