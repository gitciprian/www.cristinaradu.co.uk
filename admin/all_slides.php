<?include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->
<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">				
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['AllSlidesPageTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['AllSlidesPageSubTitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">					
						
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i><?php echo $lang['AllSlidesFormTitle'];?></div>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<?$hh="cum a deceatada";?>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
									<thead>
										<tr>
											<th width="5%"><?php echo $lang['AllSlidesTable1'];?></th>
											<th width="45%"><?php echo $lang['AllSlidesTable2'];?></th>
											<th class="hidden-480" align="center"><?php echo $lang['AllSlidesTable3'];?></th>
											<th class="hidden-480" align="center"><?php echo $lang['AllSlidesTable4'];?></th>
											<th align="center"><?php echo $lang['AllSlidesTable44'];?></th>
											<th style="display:none;"></th>
											<th style="display:none;"></th>										
										</tr>
									</thead>									
									<tbody>
									 <?php
					                 	$sqlp       = "SELECT *  from slider  order by id desc";
										$resultp    = mysqli_query($mysqli,$sqlp);
										while($rows = mysqli_fetch_array($resultp)){?>
										<tr>
											<td><?echo $rows['id'];?></td>
											<td>
												<?php echo $rows['slide_title'];?><br/>
												<?php echo $rows['slide_name'];?><br/>
												<?php echo ($rows['slide_link'] !="" ? '<a href="'.$rows["slide_link"].'" target="_blank">Slide link</a>' : ''); ?>
											</td>
											<td class="hidden-480" align="center">
												<b><?echo $rows['active_slide'];?></b>
											</td>
											<td class="hidden-480" align="left">
												<?php echo $rows['slide_order'];?> <?php echo $lang['AllSlidesTable5'];?>
											</td>
											<td width="90">
												<a href="add_slider.php?id=<?php echo $rows['id'];?>" title="<?php echo $lang['AllSlidesTable6'];?>" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></a>
												<a class="btn red icn-only" href="actiune.php?pagini=delete_slide&id=<?php echo $rows['id'];?>" title="Delete this slide" onclick="return confirm( '<?php echo $lang["AllSlidesTable7"];?>' )"><i class="icon-remove icon-white"></i></a>
											</td>
											<td style="display:none;"><img style="border:1px solid #ccc" src="../public/images/<?echo $rows['image'];?>" width="100%"/></td>
											<td style="display:none;"><? echo $rows['notes'];?></td>
										</tr>
										<?}?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/table-advanced.js"></script>     
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableAdvanced.init();
		});
	</script>
	<?include('footer.php');?>