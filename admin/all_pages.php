<?include("top_admin.php");?>
<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->
<body class="page-header-fixed" onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?include('bara_sus.php');?>		
	</div>	
	<div class="page-container">	
		<div class="page-sidebar nav-collapse collapse">			
			<?include('meniu.php');?>
		<div class="page-content">				
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->   
				<div class="row-fluid">
					<div class="span12">		
						<h3 class="page-title"><?php echo $lang['AllPagesTitle'];?></h3>
						<ul class="breadcrumb">
							<li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><span class="icon-angle-right"></span></li>
							<li><a href="#"><?php echo $lang['AllPagesSubTitle'];?></a><span class="icon-angle-right"></span></li>
						</ul>
					</div>
				</div>				
				<div class="row-fluid">
					<div class="span12">
                        <?php if(isset($_GET['ids'])){?>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                                <strong>NOTE!</strong> This page indicates the projects that are requested to be deleted by partners/engineers. The decision is up to the admin to do so.
                            </div>
                        <?php }?>
						
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i>
                                    <?php echo $lang['AllPagesFormTitle'];?>
                                </div>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
							</div>
							<div class="portlet-body">
								<?$hh="cum a deceatada";?>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
									<thead>
										<tr>
											<th width="5%"><?php echo $lang['AllPagesTable1'];?></th>
											<th width="45%"><?php echo $lang['AllPagesTable2'];?></th>
											<th class="hidden-480" align="center"><?php echo $lang['AllPagesTable3'];?></th>
											<th class="hidden-480" align="center"><?php echo $lang['AllPagesTable4'];?></th>
											<th align="center"><?php echo $lang['AllPagesTable44'];?></th>
											<th style="display:none;"></th>
											<th style="display:none;"></th>										
										</tr>
									</thead>									
									<tbody>
									 <?php
                                        $tip        = (isset($_GET['t']) && $_GET['t'] !='' ? $_GET['t'] : '' );
									    $page_approved  = (isset($_GET['a']) && $_GET['a'] !='' ? $_GET['a'] : '' );
									    $ids        = (isset($_GET['ids']) && $_GET['ids'] !='' ? str_replace("|",", ",$_GET['ids']) : '' );

                                       $sqlp       = "SELECT *  from pages WHERE id > 0";
                                        if($tip !=''){
                                         $sqlp      .= " AND page_type ='$tip' ";
                                        }
                                         if($ids !=''){
                                             $sqlp      .= " AND id IN ($ids) ";
                                         }
                                         /*
                                        if($page_approved !=''){
                                         $sqlp      .= " AND page_approved ='$page_approved' ";
                                        }
                                         */
                                        $sqlp      .= " order by id DESC ";
										$resultp    = mysqli_query($mysqli,$sqlp);
										while($rows = mysqli_fetch_array($resultp)){

										    $partner_id = $rows['partner_id'];

										    if($partner_id > 0) {
											    $sqlm    = "SELECT id, gallery_name  FROM gallery_details WHERE id = '$partner_id'  ";
											    $resultm = mysqli_query( $mysqli, $sqlm );
											    $rowm    = mysqli_fetch_array( $resultm );
											    $gallery_name   = $rowm['gallery_name'];
										    }else{
											    $gallery_name = "";
                                            }
									 ?>
										<tr>
											<td><?echo $rows['id'];?></td>
											<td>
												<?php echo $lang['AllPagesTable11'];?>: <?php echo $rows['page_title'];?><br/>
												<?php echo $lang['AllPagesTable12'];?>: <b><?php echo page_type($rows['page_type'],$_SESSION['user_language']);?> <?php echo galleryType($rows['page_approved']);?></b><br/>
                                                <?php echo $gallery_name;?>
											</td>
											<td class="hidden-480" align="center">
												<?php echo $lang['AllPagesTable13'];?>: <b><?php echo $rows['page_active'];?></b><br/>
												<?php echo $lang['AllPagesTable14'];?>: <?php echo ($rows['page_order'] == 0 ? $lang['AllPagesTable17'] : $rows['page_order']);?><br/>
											</td>
											<td class="hidden-480" align="left">
												<?php echo $lang['AllPagesTable15'];?>: <?php echo Datasiora($rows['created_at']);?><br/>
												<?php echo $lang['AllPagesTable16'];?>: <?php echo $rows['created_by'];?>
											</td>
											<td width="140">
												<a href="add_page.php?id=<?php echo $rows['id'];?>" title="<?php echo $lang['AllPagesTable6'];?>" class="btn blue icn-only"><i class="m-icon-swapright m-icon-white"></i></a>
                                                <?php if($rows['page_type'] > 3 && $rows['page_approved'] == 1){?>
                                                    <a href="galerie.php?id=<?php echo $rows['id'];?>&w=<?php echo $rows['gallery_watermark'];?>&np=<?php echo  base64_encode($rows['page_title']);?>" target="_blank" title="<?php echo $lang['AllPagesTable9'];?>" class="btn green icn-only"><i class="m-icon-swapright m-icon-white"></i></a>
												<?php }?>
											    <?php if($rows['page_type'] > 2){?>
                                                <a class="btn red icn-only" href="actiune.php?pagini=delete_page&id=<?php echo $rows['id'];?>" title="<?php echo $lang['AllPagesTable7'];?>" onclick="return confirm( '<?php echo $lang["AllPagesTable5"];?>' )"><i class="icon-remove icon-white"></i></a>
											    <?php }?>
                                                <?php if(isset($_GET['ids']) && $_GET['ids'] !=''){?>
                                                    <a href="actiune.php?pagini=cancelRemoval&id=<?php echo $rows['id'];?>" title="<?php echo $lang['AllPagesTable10'];?>" ><?php echo $lang['AllPagesTable10'];?></a>
                                                <?php }?>
                                            </td>
											<td style="display:none;"><?php if($rows['main_image'] !=""){?><img src="<?php echo "../public/images/".$rows['main_image'];?>" width="400" style="border:2px solid #999"/><?}else{?><b><?php echo $lang['AllPagesTable8'];?></b><?}?></td>
											<td style="display:none;"><p><?php echo $rows['notes'];?></p></td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->         
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->  
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/table-advanced.js"></script>     
	<script>
		jQuery(document).ready(function() {       
		   App.init();
		   TableAdvanced.init();
		});
	</script>
	<?include('footer.php');?>