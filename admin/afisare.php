<?include("top_admin.php");?>
<body class="page-header-fixed page-sidebar-fixed page-sidebar-closed"  onload="startTime()">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<?include('bara_sus.php');?>
	</div>
	<div class="page-container">		
		<div class="page-sidebar nav-collapse collapse">			      
			<?php include('meniu.php');?>
		<div class="page-content">				
			
  <? switch ($_REQUEST['afisare']){
	
     case 'change_details':
     
	 	$id     = $_REQUEST['id'];
		$sqlp   = "select * from gallery where id = $id";
		$resultp= mysqli_query($mysqli, $sqlp);
		$rowp   = mysqli_fetch_array($resultp);

	     $sqlg   = "select page_extras from pages where id = '5'";
	     $resultg= mysqli_query($mysqli, $sqlg);
	     $rowg   = mysqli_fetch_array($resultg);

	     $page_extras   = json_decode($rowg['page_extras'], true);

		?>
				<div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12"><br/>	<br/>
                            <ul class="breadcrumb">
                                <li><i class="icon-home"></i><a href="index.php"><?php echo $lang['Dashboard'];?></a><i class="icon-angle-right"></i></li>
                                <li><a href="#"><?php echo $lang['DisplayPageTitle'];?></a></li>
                                <li class="pull-right no-text-shadow"><i class="icon-calendar"></i><span><?echo luni_in_romana($luna_in_litere);?></span>	</li>
                            </ul>
                        </div>
                    </div>
                    <form action="actiune.php?pagini=change_image_details" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $id;?>"/>
                        <input type="hidden" name="project_id" value="<?php echo (isset($_GET['pr']) && $_GET['pr'] > 0 ? $_GET['pr'] : 0);?>"/>
                        <div class="control-group">
                            <label class="control-label"><?php echo $lang['DisplayPageFormAlt'];?></label>
                            <div class="controls">
                                <input type="text" name="doc_alt" value="<?php echo $rowp['doc_alt'];?>" class="span6 m-wrap" />
                                <span class="help-inline"><?php echo $lang['DisplayPageFormAltHint'];?></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"><?php echo $lang['DisplayPageFormVisibility'];?></label>
                            <div class="controls">
                                <select class="span3 m-wrap" name="img_active" data-placeholder="Is this image present?" tabindex="1">
                                    <option value="yes" <?php if($rowp['img_active']=="yes"){?> selected<?php }?>><?php echo $lang['DisplayPageFormVisiOpt1'];?></option>
                                    <option value="no" <?php if($rowp['img_active']=="no"){?> selected<?php }?>><?php echo $lang['DisplayPageFormVisiOpt2'];?></option>
                                </select>
                                <span class="help-inline"><?php echo $lang['DisplayPageFormVisiHint'];?></span>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Detalii imagine (RO)</label>
                            <div class="controls">
                                <textarea id="ckeditor" class="span9 ckeditor m-wrap" name="image_details" rows="10"><?php echo $rowp['image_details'];?></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Detalii imagine (EN)</label>
                            <div class="controls">
                                <textarea id="ckeditor1" class="span9 ckeditor m-wrap" name="image_details_en" rows="10"><?php echo $rowp['image_details_en'];?></textarea>
                            </div>
                        </div>


                        <?php /* if($_GET['pr'] < 9990){?>
                            <div class="control-group">
                                <label class="control-label"><?php echo $lang['DisplayPageFormConvertImg'];?></label>
                                <div class="controls">
                                    <select class="span3 m-wrap" name="main_image" data-placeholder="" tabindex="1">
                                        <option value="no"><?php echo $lang['DisplayPageFormConvertOpt1'];?></option>
                                        <option value="yes"><?php echo $lang['DisplayPageFormConvertOpt2'];?></option>
                                    </select>
                                    <span class="help-inline"><?php echo $lang['DisplayPageFormConvertHint'];?></span>
                                </div>
                            </div>
                            <input type="hidden" name="img_class" value="">

                        <?php }else{ ?>

                            <input type="hidden" name="main_image" value="no">
                            <div class="control-group">
                                <label class="control-label"><?php echo $lang['DisplayPageFormGal'];?></label>
                                <div class="controls">
                                    <select class="span3 m-wrap" name="img_class" data-placeholder="" tabindex="1">
                                        <?php foreach ($page_extras as $class):?>
                                            <option value="<?php echo sanitazeStringForUrl(strtolower($class));?>" <?php echo ($rowp['img_class'] == sanitazeStringForUrl(strtolower($class)) ? 'selected' : '');?>><?php echo $class;?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <span class="help-inline"><?php echo $lang['DisplayPageFormGalHint'];?></span>
                                </div>
                            </div>
                        <?php } */?>

                    <div class="form-actions">
                        <button type="submit" class="btn blue"><?php echo $lang['DisplayPageEditDetails'];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="reset" class="btn"><?php echo $lang['DisplayPageFormResetBtn'];?></button>
                    </div>
                    </form>
				</div>
            <? break;


            
            
}?>
				
			
		</div>		
	</div>
	<!-- END CONTAINER -->
<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
    <script src="//cdn.ckeditor.com/4.4.1/full/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'ckeditor', {
            toolbar: [
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike',  '-', 'CopyFormatting', 'RemoveFormat' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                { name: 'links', items: [ 'Link', 'Unlink'] },
            ]
        });
        CKEDITOR.replace( 'ckeditor1', {
            toolbar: [
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike',  '-', 'CopyFormatting', 'RemoveFormat' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                { name: 'links', items: [ 'Link', 'Unlink'] },
            ]
        });
    </script>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>   
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>  
	<script src="assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
	<script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/index.js" type="text/javascript"></script>
	<script src="assets/scripts/tasks.js" type="text/javascript"></script> 
	<!-- END PAGE LEVEL SCRIPTS -->  
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   Index.init();
		   Index.initJQVMAP(); // init index page's custom scripts
		   Index.initCalendar(); // init index page's custom scripts
		   Index.initCharts(); // init index page's custom scripts
		   Index.initChat();
		   Index.initMiniCharts();
		   Index.initDashboardDaterange();
		   Index.initIntro();
		   Tasks.initDashboardWidget();
		});
	</script>
	<?include('footer.php');?>