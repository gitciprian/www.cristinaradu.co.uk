<?php
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}

header('Content-Type: text/html; charset=utf-8');
require_once('config.php');
require "functions.php";
require '../application/libs/php-mailer/PHPMailerAutoload.php';
error_reporting(0);
$suser  = $_SESSION['user_name'];
$credit = "";


$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'mail.smtp2go.com';
$mail->SMTPAuth = true;
$mail->Username = 'safe-ro-mail';
$mail->Password = 'PYXaz6VS3OzH%fT$^3w9';
$mail->SMTPSecure = 'TLS';
$mail->Port = 587;
$mail->isHTML(true);
$mail->setFrom('info@eliteheat.plumbing', 'Elite Heating and Plumbing');
$mail->addReplyTo('info@eliteheat.plumbing', 'Elite Heating and Plumbing');
//$mail->addBCC('ciprian@cipdesign.ro');


switch ($_GET['pagini']) {

    case'add_pagina':


		$page_extras    = array();
	    $created_by     = $suser;
	    $main_image     = '';
        $page_type      = $_POST['tip'];
        $page_title     = $mysqli->real_escape_string($_POST['page_title']);
	    $page_active    = $mysqli->real_escape_string($_POST['page_active']);
	    $page_approved  = $mysqli->real_escape_string($_POST['page_approved']);
	    $page_order     = $mysqli->real_escape_string($_POST['page_order']);
	    $partner_id     = $mysqli->real_escape_string($_POST['partner_id']);
	    $notes          = $mysqli->real_escape_string($_POST['notes']);
	    $gallery_watermark      = $mysqli->real_escape_string($_POST['gallery_watermark']);
	    $page_meta_title        = $mysqli->real_escape_string($_POST['page_meta_title']);
	    $page_meta_description  = $mysqli->real_escape_string($_POST['page_meta_description']);
	    $page_content           = $mysqli->real_escape_string($_POST['page_content']);

	    // for english
	    $page_title_en              = $mysqli->real_escape_string($_POST['page_title_en']);
	    $page_meta_title_en         = $mysqli->real_escape_string($_POST['page_meta_title_en']);
	    $page_meta_description_en   = $mysqli->real_escape_string($_POST['page_meta_description_en']);
	    $page_content_en            = $mysqli->real_escape_string($_POST['page_content_en']);

	    if(isset($_POST['page_extras'])){
		    foreach ($_POST['page_extras'] as $k => $v){
			    $page_extras[$k]   = $mysqli->real_escape_string($v);
		    }
	    }
	    $page_extras = json_encode($page_extras);

        $sql        = "INSERT INTO pages (page_title_en, page_meta_title_en, page_meta_description_en, page_content_en, gallery_watermark, partner_id, page_type, page_active, page_approved, page_order, page_meta_title, page_meta_description, page_title, page_content, page_extras, main_image, created_at, created_by, notes) VALUE ('$page_title_en', '$page_meta_title_en', '$page_meta_description_en', '$page_content_en', '$gallery_watermark', '$partner_id', '$page_type', '$page_active', '$page_approved', '$page_order', '$page_meta_title', '$page_meta_description', '$page_title', '$page_content', '$page_extras', '$main_image', NOW(), '$created_by', '$notes')";
        $insert     = mysqli_query($mysqli, $sql);

        if($insert) {
	        $id_baza = mysqli_insert_id( $mysqli );
	        echo ' Page created. Redirecting ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=edit_img_art_central.php?id=' . $id_baza . '&s=da" >';
        }else{
	        echo ' Error! Page NOT created ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_pages.php" >';
        }

        break;

	case'update_page':

		$id             = $_POST['id'];
		$page_extras    = array();
		$page_type      = $_POST['tip'];
		$cate_video     = count($_POST['video_codes']); // if is set
		$video_codes    = '';
		$page_title     = $mysqli->real_escape_string($_POST['page_title']);
		$page_active    = $mysqli->real_escape_string($_POST['page_active']);
		$page_approved  = $mysqli->real_escape_string($_POST['page_approved']);
		$page_order     = $mysqli->real_escape_string($_POST['page_order']);
		$partner_id     = $mysqli->real_escape_string($_POST['partner_id']);
		$notes          = $mysqli->real_escape_string($_POST['notes']);
		$gallery_watermark      = $mysqli->real_escape_string($_POST['gallery_watermark']);
		$page_meta_title        = $mysqli->real_escape_string($_POST['page_meta_title']);
		$page_meta_description  = $mysqli->real_escape_string($_POST['page_meta_description']);
		$page_content           = $mysqli->real_escape_string($_POST['page_content']);

		// for english
		$page_title_en              = $mysqli->real_escape_string($_POST['page_title_en']);
		$page_meta_title_en         = $mysqli->real_escape_string($_POST['page_meta_title_en']);
		$page_meta_description_en   = $mysqli->real_escape_string($_POST['page_meta_description_en']);
		$page_content_en            = $mysqli->real_escape_string($_POST['page_content_en']);

		if(isset($_POST['page_extras'])){
			foreach ($_POST['page_extras'] as $k => $v){
				$page_extras[$k]   = htmlentities($mysqli->real_escape_string($v));
			}
		}
		$page_extras            = json_encode($page_extras);

		if($cate_video > 0){
			foreach ($_POST['video_codes'] as $video_code){
				$video_codes    .= $video_code.", ";
			}
		}

		$sql = "UPDATE  pages set 
	            partner_id		= '$partner_id', 
	            page_type		= '$page_type', 
	            page_active		= '$page_active', 
	            page_approved	= '$page_approved', 
	            page_order		= '$page_order',
	            gallery_watermark	='$gallery_watermark',
	            video_codes		='$video_codes',
	            page_meta_title	= '$page_meta_title',
	            page_meta_description	= '$page_meta_description',
	            page_title		= '$page_title',
	            page_content	= '$page_content',
	            page_extras		= '$page_extras',
	            updated_at		= NOW(),	                   
	            notes			= '$notes',	            
	            page_title_en			= '$page_title_en',
	            page_meta_title_en		= '$page_meta_title_en',
	            page_meta_description_en = '$page_meta_description_en',
	            page_content_en			= '$page_content_en'
	            	  	           
	  	where id = '$id'";
		$update     = mysqli_query($mysqli, $sql);

		if($update){
			echo 'Page updated ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_pages.php" >';
		}else{
			echo ' Error! Page NOT updated ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_pages.php" >';
		}


		break;

	case'add_gallery':

		$gallery_active     = $mysqli->real_escape_string($_POST['gallery_active']);
		$gallery_order      = $mysqli->real_escape_string($_POST['gallery_order']);
		$gallery_type       = $mysqli->real_escape_string($_POST['gallery_type']);
		$gallery_name       = $mysqli->real_escape_string($_POST['gallery_name']);
		$gallery_name_en    = $mysqli->real_escape_string($_POST['gallery_name_en']);
		$notes              = $mysqli->real_escape_string($_POST['notes']);


        $sql        = "INSERT INTO gallery_details (created_at, gallery_active, gallery_order, gallery_type, gallery_name, gallery_name_en, notes) VALUE (NOW(),  '$gallery_active', '$gallery_order', '$gallery_type', '$gallery_name', '$gallery_name_en', '$notes')";
        $insert     = mysqli_query($mysqli, $sql);

        if($insert) {
	        echo ' Entry created. Redirecting ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_galleries.php" >';
        }else{
	        echo ' Error! Page NOT created ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_galleries.php" >';
        }

        break;

    case'edit_gallery':

	    $id                 = $mysqli->real_escape_string($_POST['id']);
        $gallery_active     = $mysqli->real_escape_string($_POST['gallery_active']);
		$gallery_order      = $mysqli->real_escape_string($_POST['gallery_order']);
		$gallery_type       = $mysqli->real_escape_string($_POST['gallery_type']);
		$gallery_name       = $mysqli->real_escape_string($_POST['gallery_name']);
	    $gallery_name_en    = $mysqli->real_escape_string($_POST['gallery_name_en']);
		$notes              = $mysqli->real_escape_string($_POST['notes']);


	    $sql = "UPDATE  gallery_details set 
	            gallery_active	= '$gallery_active',
	            gallery_order	= '$gallery_order', 	  
	            gallery_type	= '$gallery_type', 	  
	            gallery_name	= '$gallery_name',
	            gallery_name_en	= '$gallery_name_en', 	  	            	  	             	              
	            notes			= '$notes'	           
	  	where id = '$id'";
	    $update     = mysqli_query($mysqli, $sql);

        if($update) {
	        echo ' Entry updated. Redirecting ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_galleries.php" >';
        }else{
	        echo ' Error! Page NOT updated ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_galleries.php" >';
        }

        break;




	case 'create_message':

		$subject        = $_POST['subject'];
		$notification   = $_POST['notification'];
		$users          = $_POST['users'];
		$users          = json_encode($users);
		$content        = $_POST['content'];
		$notes          = $_POST['notes'];


		$sqla   = "INSERT INTO messages (subject, notification, users, content, notes, created_at, created_by) VALUES ('$subject', '$notification', '$users', '$content', '$notes', NOW(), '$suser')";
		$insert = mysqli_query($mysqli,$sqla);

		if($insert) {

			$id_baza    = mysqli_insert_id($mysqli);
			$nr_rand    = rand(100000,150000);
			$folder     = "attach";
			$dir        = $_SERVER['DOCUMENT_ROOT']."/public/images/messages/";
			$th_dir     = $_SERVER['DOCUMENT_ROOT']."/public/images/messages/tmp/";


			foreach ($_FILES['file']['error'] as $key => $error)
			{
				if ($error == UPLOAD_ERR_OK)
				{
					$tmp_name       = $_FILES['file']['tmp_name'][$key];
					$name           = $_FILES['file']['name'][$key];
					$tip_document   = $_FILES['file']['type'][$key];
					$ext            = strtolower(strstr($name, "."));
					$nm             = explode($ext, $name);
					$random_number  = sanitazeStringForUrl($nm[0]."-".$nr_rand);
					$nume_document  = ucfirst($nm[0]);
					move_uploaded_file($tmp_name, $dir.$random_number.$ext) or die('upload error');
					chmod($dir.$random_number.$ext, 0777);

					// use aas attachemnts
					$arr_poza_mare[]  = array($random_number.$ext=>$dir.$random_number.$ext);

					$sqlat   = "INSERT INTO gallery (project_id, doc_type, doc_alt, image, img_active, created_at, folder) values ('$id_baza', '$tip_document','$nume_document', '".$random_number.$ext."','yes',NOW(),'$folder')";
					mysqli_query($mysqli,$sqlat);

				}
			}

			if ( $notification == "yes" ) {

				$html   = '
				<html>
				<head>
				<title>'.$subject.'</title>
				<style type="text/css">body {max-width:1000px;}</style>
				</head>
				<body>					
					'.$content.'									
					<h3 style="text-align:left; margin:auto; color:#999">Best regards,<br/> ' . $_SESSION['user_name'] . '</h3>					
				</body>
				</html>';

				$how_many = count($_POST['users']);

				if($how_many > 0){

					foreach ($_POST['users'] as $id_user) {

						$sqlm       = "SELECT user_email FROM u_login WHERE  id = '$id_user' ";
						$resultm    = mysqli_query($mysqli,$sqlm);
						$rowm       = mysqli_fetch_array($resultm);

						if (filter_var(trim($rowm['user_email']), FILTER_VALIDATE_EMAIL)) {

							$mail->addAddress(trim($rowm['user_email']));
							$mail->Subject  = $subject;
							$mail->Body     = $html;

							if(count($arr_poza_mare) > 0){
								foreach ($arr_poza_mare as $attach) {
									foreach ($attach as $attachment) {
										$mail->AddAttachment( $attachment );
									}
								}
							}

							if ($mail->send()) {
								echo 'Notification email sent to '.$rowm['user_email'].'<br/>';
							} else {
								echo 'Error! Notification email not sent to '.$rowm['user_email'].'<br/>';
							}
						}
					}
				}
			}

			echo 'Message sent ...   <META HTTP-EQUIV=Refresh CONTENT="5; URL=messages.php" >';
		}

		break;

    case'add_user':

	    $user_name          = $_POST['user_name'];
	    $user_email         = $_POST['user_email'];
	    $user_password      = $_POST['user_password'];
	    $notificare         = $_POST['notificare'];
	    $user_language      = $_POST['user_language'];
	    $company_name       = $_POST['company_name'];
	    $user_type          = $_POST['user_type'];
	    $user_permision     = $_POST['user_permision'];
	    $secondary_email    = $_POST['secondary_email'];
	    $user_phone         = $_POST['user_phone'];
	    $user_country       = $_POST['user_country'];
	    $user_address       = $_POST['user_address'];
	    $user_notes         = $_POST['user_notes'];
		$parola             = mc_encrypt($user_password);
	    $html               = '';


	    $sqla   = "INSERT INTO u_login (user_name, company_name,  user_type, user_password, user_email, secondary_email, user_country, user_address, user_notes, user_phone, user_permision, user_language) VALUES ('$user_name', '$company_name', '$user_type', '$parola', '$user_email', '$secondary_email', '$user_country', '$user_address', '$user_notes', '$user_phone', '$user_permision','$user_language')";
        $insert = mysqli_query($mysqli,$sqla);

        if($insert) {

	        if ( $notificare == "da" ) {

	        	switch ($_SESSION['user_language']){
			        case 'en':
				            $subiect        = "New user account";
				            $html   .= '
								<h4 style="text-align:left; ">Hello ' . $user_name . '</h4><br/>
								<p style="text-align:left; margin-bottom:6px">Starting now, you have an account for www.valentinhudubet.com</p>
								<p style="text-align:left; margin-bottom:6px">You can connect to your account using the below credentials</p>
								<p style="text-align:left; margin-bottom:6px">You can save the login credential or store this email for future uses.</p>					
				                <p style="text-align:left; margin-bottom:6px">
				                    *********************** Login details ***********************<br/>
				                    URL: https://www.valentinhudubet.com/admin/<br/>
				                    User: ' . $user_email . '<br/>
				                    Password: ' . $user_password . '<br/>
				                    **********************************************************<br/>
				                </p><br/>	             	
								<h3 style="text-align:left; margin:auto; color:#999">Best regards,<br/> ' . $_SESSION['user_name'] . '</h3>
							';
				            $display_ok     = 'Notification email sent.';
				            $display_error  = 'Error! Notification email not sent.';
			        	break;

			        case 'ro':
				        $subiect        = "Nou cont creat";
				        $html   .= '
								<h4 style="text-align:left; ">Salut ' . $user_name . '</h4><br/>
								<p style="text-align:left; margin-bottom:6px">Incepand cu acest moment aveti un cont la www.valentinhudubet.com</p>
								<p style="text-align:left; margin-bottom:6px">Va puteti conecta la cont folosind datele de mai jos</p>
								<p style="text-align:left; margin-bottom:6px">Deasemenea, puteti salva aceste date pentru accesarile viitoare.</p>					
				                <p style="text-align:left; margin-bottom:6px">
				                    *********************** Login details ***********************<br/>
				                    URL: http://www.valentinhudubet.com/admin/<br/>
				                    Utilizator: ' . $user_email . '<br/>
				                    Parola: ' . $user_password . '<br/>
				                    **********************************************************<br/>
				                </p><br/>	             	
								<h3 style="text-align:left; margin:auto; color:#999">Cu stima,<br/> ' . $_SESSION['user_name'] . '</h3>
							';
				        $display_ok     = 'Notificare email trimisa';
				        $display_error  = 'Eroare! Notificarea pe email nu a putut fi trimisa.';
				        break;
		        }

		        $reply          = $_SESSION['user_email'];
		        $mesaj          = '
					<html>
					<head>
					<title>'.$subiect.'</title>
					<style type="text/css">body {width:780px;}</style>
					</head>
					<body>
						<img src="https://www.valentinhudubet.com/public/images/logo-black.png" ><br/>
						'.$html.'
					</body>
					</html>
					';

		        $headere = "MIME-Version: 1.0\r\n";
		        $headere .= "Content-type: text/html; charset=iso-8859-1\r\n";
		        $headere .= "From: ".$_SESSION['user_name']." <$reply>\r\n";
		        $headere .= "Reply-To: $reply";


		        if(mail( $user_email, $subiect, $mesaj, $headere )){
		            echo $display_ok;
                }else{
			        echo $display_error;
                }
	        }

	        echo '<br/>User created ...   <META HTTP-EQUIV=Refresh CONTENT="1; URL=users.php" >';

        }else{

	        echo '<br/>Error! failed creating user ...   <META HTTP-EQUIV=Refresh CONTENT="1; URL=users.php" >';
        }

        break;

    case'edit_user':

        $id                 = $_POST['id'];
        $user_name          = $_POST['user_name'];
        $user_email         = $_POST['user_email'];
        $company_name       = $_POST['company_name'];
        $user_language      = $_POST['user_language'];
        $user_password      = $_POST['user_password'];
        $user_type          = $_POST['user_type'];
        $user_permision     = $_POST['user_permision'];
        $secondary_email    = $_POST['secondary_email'];
        $user_phone         = $_POST['user_phone'];
        $user_country       = $_POST['user_country'];
        $user_address       = $_POST['user_address'];
        $user_notes         = $_POST['user_notes'];


        if ($user_password == "") {
            $parola = $_POST['ps'];
        } else {
            $parola = mc_encrypt($user_password);
        }

        $sqla = "UPDATE u_login set  	
                    user_name       = '$user_name',
                    user_email      = '$user_email',
                    company_name    = '$company_name',
                    user_password   = '$parola',
                    user_language   = '$user_language',
                    user_type       = '$user_type',
                    user_permision  = '$user_permision',
                    secondary_email = '$secondary_email',
                    user_phone      = '$user_phone',
                    user_country    = '$user_country',
                    user_address    = '$user_address',
                    user_notes      = '$user_notes'	  								
	    where id = '$id'";
        $update = mysqli_query($mysqli,$sqla);

        if($update){
	        echo ' Details updated ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=users.php" >';
        }else{
            echo 'Error! Please check your details and try again';
        }
        break;



    case'block':
        $id     = $_GET['id'];
        $nr     = $_GET['nr'];

        if($nr == "0"){
            $sql="update u_login set user_permision ='1'  WHERE id='$id' ";
            mysqli_query($mysqli,$sql);
        }else{
            $sql="update u_login set user_permision ='0'  WHERE id='$id' ";
	        mysqli_query($mysqli,$sql);
        }

        echo 'Account status changed ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=users.php" >';
    break;


    case'add_slide':

        $slide_order    = $_POST['slide_order'];
        $active_slide   = $_POST['active_slide'];
        $slide_name     = $_POST['slide_name'];
	    $slide_name_en  = $_POST['slide_name_en'];
	    $slide_title_en = $_POST['slide_title_en'];
        $slide_title    = $_POST['slide_title'];
        $slide_link     = $_POST['slide_link'];
        $notes          = $_POST['notes'];

        $sqlg   = "INSERT INTO slider (slide_order, active_slide, slide_name, slide_link, notes, slide_title, slide_name_en, slide_title_en)  VALUE  ('$slide_order', '$active_slide', '$slide_name', '$slide_link', '$notes', '$slide_title', '$slide_name_en', '$slide_title_en')";
        $insert = mysqli_query($mysqli, $sqlg);

        if($insert){
	        $id_sld = mysqli_insert_id($mysqli);

	        echo 'Slide details added. Redirecting ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=imagine_slide.php?id='.$id_sld.'" >';
        }else{
	        echo 'Error! Please try again ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=add_slider.php" >';
        }


    break;


    case'edit_slide':

	    $slide_order    = $_POST['slide_order'];
	    $active_slide   = $_POST['active_slide'];
	    $slide_name     = $_POST['slide_name'];
	    $slide_title    = $_POST['slide_title'];
	    $slide_name_en  = $_POST['slide_name_en'];
	    $slide_title_en = $_POST['slide_title_en'];
	    $slide_link     = $_POST['slide_link'];
	    $notes          = $_POST['notes'];
        $id             = $_POST['id'];

        $sql="UPDATE slider SET   	
            slide_order   = '$slide_order',
            active_slide  = '$active_slide', 
            slide_name    = '$slide_name', 
            slide_name_en = '$slide_name_en', 
            slide_title   = '$slide_title', 
            slide_title_en = '$slide_title_en', 
            slide_link    = '$slide_link', 
            notes         = '$notes' 
        where id = '$id'";
        $update = mysqli_query($mysqli,$sql);

        if($update){
	        echo 'Slide updated ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_slides.php" >';
        }


    break;

	case 'add_partner':

		$created_by     = $suser;
		$part_name      = $mysqli->real_escape_string($_POST['part_name']);
		$part_active    = $mysqli->real_escape_string($_POST['part_active']);
		$part_order     = $mysqli->real_escape_string($_POST['part_order']);
		$part_details   = $mysqli->real_escape_string($_POST['part_details']);
		$part_link      = $mysqli->real_escape_string($_POST['part_link']);
		$part_notes     = $mysqli->real_escape_string($_POST['part_notes']);

		$sql        = "INSERT INTO partners (part_order, part_active, part_name, part_details, part_link, created_at, created_by, part_notes) VALUE ('$part_order', '$part_active', '$part_name', '$part_details', '$part_link', NOW(), '$created_by', '$part_notes')";
		$insert     = mysqli_query($mysqli, $sql);

		if($insert) {
			$id_baza = mysqli_insert_id( $mysqli );

			if(isset($_FILES["fileToUpload"]) && $_FILES["fileToUpload"] !== ""){

				$target_dir     = "../public/images/logos/";
				$error          = 'Image uploaded';
				$target_file    = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk       = 1;
				$imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);

				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						$uploadOk = 1;
					} else {
						$error      = "File is not an image.";
						$uploadOk   = 0;
					}
				}

				// Check if file already exists
				if (file_exists($target_file)) {
					$error      = "Sorry, file already exists.";
					$uploadOk   = 0;
				}

				// Check file size
				if ($_FILES["fileToUpload"]["size"] > 500000) {
					$error      =  "Sorry, your file is too large.";
					$uploadOk   = 0;
				}

				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				   && $imageFileType != "gif" ) {
					$error      =  "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$uploadOk   = 0;
				}

				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					$error      =  "Sorry, your file was not uploaded.";
				} else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

						$partner_logo   = basename( $_FILES["fileToUpload"]["name"]);
						$sql            = "UPDATE  partners set  part_image = '$partner_logo' WHERE id = '$id_baza'";
						$update         = mysqli_query($mysqli, $sql);

					} else {
						$error   =  "Sorry, there was an error uploading your file.";
					}
				}

			}

			echo 'Partner created. '.$error.' ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_partners.php" >';

		}else{
			echo 'Error! Please try again ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=add_partner.php" >';
		}

		break;

	case 'update_partner':

		$created_by     = $suser;
		$id             = $_POST['id'];
		$part_name      = $mysqli->real_escape_string($_POST['part_name']);
		$part_active    = $mysqli->real_escape_string($_POST['part_active']);
		$part_order     = $mysqli->real_escape_string($_POST['part_order']);
		$part_details   = $mysqli->real_escape_string($_POST['part_details']);
		$part_link      = $mysqli->real_escape_string($_POST['part_link']);
		$part_notes     = $mysqli->real_escape_string($_POST['part_notes']);

		$sql = "UPDATE  partners set  
						part_order = '$part_order', 
						part_active = '$part_active', 
						part_name = '$part_name',
						part_details = '$part_details',
						part_link = '$part_link', 
						part_notes = '$part_notes'		 
					WHERE id = '$id'";
		$update = mysqli_query($mysqli, $sql);

		if($update) {
			$id_baza = mysqli_insert_id( $mysqli );

			if(isset($_FILES["fileToUpload"]) && $_FILES["fileToUpload"] !== ""){

				$target_dir     = "../public/images/logos/";
				$error          = 'Image uploaded';
				$target_file    = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk       = 1;
				$imageFileType  = pathinfo($target_file,PATHINFO_EXTENSION);

				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						$uploadOk = 1;
					} else {
						$error      = "File is not an image.";
						$uploadOk   = 0;
					}
				}

				// Check if file already exists
				if (file_exists($target_file)) {
					$error      = "Sorry, file already exists.";
					$uploadOk   = 0;
				}

				// Check file size
				if ($_FILES["fileToUpload"]["size"] > 500000) {
					$error      =  "Sorry, your file is too large.";
					$uploadOk   = 0;
				}

				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				   && $imageFileType != "gif" ) {
					$error      =  "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					$uploadOk   = 0;
				}

				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					$error      =  "Sorry, your file was not uploaded.";
				} else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

						$partner_logo   = basename( $_FILES["fileToUpload"]["name"]);
						$sql            = "UPDATE  partners set  part_image = '$partner_logo' WHERE id = '$id'";
						$update         = mysqli_query($mysqli, $sql);

					} else {
						$error   =  "Sorry, there was an error uploading your file.";
					}
				}

			}

			echo 'Partner updated. '.$error.' ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_partners.php" >';

		}else{
			echo 'Error! Please try again ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=add_partner.php?id='.$id.'" >';
		}

		break;


    case'general_1':

        $id = 1;

        $meta_title             = $_POST['meta_title'];
        $meta_description       = $_POST['meta_description'];
	    $meta_title_en          = $_POST['meta_title_en'];
	    $meta_description_en    = $_POST['meta_description_en'];

        $sqla="UPDATE general_info set 	    
            meta_title        = '$meta_title',           
            meta_description  = '$meta_description',
            meta_title_en     = '$meta_title_en',           
            meta_description_en  = '$meta_description_en'
        where id = '$id'";
        $update  = mysqli_query($mysqli, $sqla);

        if($update){
	        echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php">  Details updated ...';
        }else{
	        echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php">  Error! Details NOT updated ...';
        }


        break;

    case'general_3':

        $id=1;

        $auto_response_email      = $_POST['auto_response_email'];
        $auto_response_content    = $_POST['auto_response_content'];
	    $auto_response_content_en = $_POST['auto_response_content_en'];

        $sqla="UPDATE general_info set 	    
                auto_response_content   = '$auto_response_content', 
                auto_response_content_en = '$auto_response_content_en',            
                auto_response_email     = '$auto_response_email'
            where id=$id";
        $update = mysqli_query($mysqli, $sqla);

        if($update){
	        echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php"> Details updated ...';
        }else{
	        echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php"> Error! Details not updated ...';
        }


    break;

    case'general_2':

        $id = 1;
        $webmaster_code     = $_POST['webmaster_code'];
        $trackers_code      = $_POST['trackers_code'];

        $sqla   = "UPDATE general_info set  webmaster_code ='$webmaster_code',	trackers_code='$trackers_code' where id = '$id'";
        $update = mysqli_query($mysqli,$sqla);

        if($update){
	        echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php">  Details updated ...';
        }else{
	        echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php">  Error! Details NOT updated ...';
        }


    break;

    case'general_5':

        $id=1;

        $email_send_address = $_POST['email_send_address'];
        $general_phone      = $_POST['general_phone'];
        $general_mobile     = $_POST['general_mobile'];
        $general_email      = $_POST['general_email'];
        $general_address    = $_POST['general_address'];
	    $general_address_en = $_POST['general_address_en'];
        $general_footer     = $_POST['general_footer'];
        $general_footer_en  = $_POST['general_footer_en'];
        $footer_slogan      = $_POST['footer_slogan'];
	    $footer_slogan_en   = $_POST['footer_slogan_en'];
	    $link_facebook      = $_POST['link_facebook'];
	    $link_youtube       = $_POST['link_youtube'];
	    $link_instagram     = $_POST['link_instagram'];
	    $link_pinterest     = $_POST['link_pinterest'];
	    $link_twitter       = $_POST['link_twitter'];

	    $sqla="UPDATE general_info set 	   	
            email_send_address  = '$email_send_address',
            general_phone       = '$general_phone',
            general_email       = '$general_email',	
            general_mobile      = '$general_mobile', 
            footer_slogan       = '$footer_slogan',		
            footer_slogan_en    = '$footer_slogan_en',		
            general_address     = '$general_address',	
            general_address_en  = '$general_address_en',		
            general_footer      = '$general_footer',
            general_footer_en   = '$general_footer_en',
            link_facebook       = '$link_facebook ',				
            link_youtube    	= '$link_youtube',
            link_instagram    	= '$link_instagram',    
            link_pinterest     	= '$link_pinterest',
            link_twitter     	= '$link_twitter' 
        where id= '$id'";
        $update = mysqli_query($mysqli,$sqla);

        if($update){
            echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php">  Details updated ...';
        }else{
            echo '<META HTTP-EQUIV = "Refresh" Content = "1; URL =general_info.php">  Error! Details not updated ...';
        }

        break;


	case 'change_image_details':

		$id         = $_POST['id'];
		$project_id = $_POST['project_id'];
		$doc_alt    = $mysqli->real_escape_string($_POST['doc_alt']);
		$img_active = $_POST['img_active'];
		//$img_class  = $_POST['img_class'];
		//$main_image = $_POST['main_image'];

		$image_details = $mysqli->real_escape_string($_POST['image_details']);
		$image_details_en = $mysqli->real_escape_string($_POST['image_details_en']);

			$sqla="UPDATE gallery set 	   	
           			 doc_alt     	= '$doc_alt',			
            		 img_active    	= '$img_active',
            		 image_details  = '$image_details',
            		 image_details_en = '$image_details_en'           		 
        	where id= '$id'";
			$update = mysqli_query($mysqli,$sqla);

			/*
			if($main_image == "yes"){
				// crop the image and set it as main image for the project

				$sqlm       = "SELECT image FROM gallery WHERE  id = '$id' ";
				$resultm    = mysqli_query($mysqli,$sqlm);
				$rowm       = mysqli_fetch_array($resultm);
				$main_image = $rowm['image'];

				$new_image  = smartImageCrop("public/images/gallery",$main_image);

				$sqlpr      = "UPDATE pages set main_image  = 'gallery/$new_image' WHERE id = '$project_id'";
				$update_pr  = mysqli_query($mysqli,$sqlpr);
			}
			*/

			if($update){
				echo '<h1 style="text-align:center"><br/>Details updated</h1> 
					<script type="text/javascript">
						<!-- 
						setTimeout(					    
						    function refreshAndClose() {
						        window.opener.location.reload(true);
						        window.close();
						    },2000); 
						//--> 
					</script>';
			}

		break;


	case'delete_slide':

		$id     = $_GET['id'];
		$sql    = "DELETE FROM slider WHERE id = '$id' ";
		$delete = mysqli_query($mysqli,$sql);

		if($delete){
			echo 'Slide deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_slides.php" >';
		}


		break;

	case'delete_gallery':

		$id     = $_GET['id'];
		$sql    = "DELETE FROM gallery_details WHERE id = '$id' ";
		$delete = mysqli_query($mysqli,$sql);

		if($delete){
			echo 'Gallery deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_galleries.php" >';
		}


		break;

	case'delete_calendar':

		$id     = $_GET['id'];
		$sql    = "DELETE FROM calendar WHERE id = '$id' ";
		$delete = mysqli_query($mysqli,$sql);

		if($delete){
			echo 'Entry deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=calendar.php" >';
		}


		break;

	case'delete_message':

			$id     = $_GET['id'];
			$sql    = "DELETE FROM messages WHERE id = '$id' ";
			$delete = mysqli_query($mysqli,$sql);

			if($delete){
				echo 'Message deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=messages.php" >';
			}


		break;

	case'delete_page':

			$id     = $_GET['id'];
			$sql    = "DELETE FROM pages WHERE id = '$id' ";
			$delete = mysqli_query($mysqli,$sql);

			if($delete){

				// check to see if any requests to delete are associated with this page
				$sqla       = "UPDATE requests set project_removed  = 'yes' WHERE project_id = '$id'";
				$update     = mysqli_query($mysqli,$sqla);

				echo 'Page deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_pages.php" >';
			}


		break;

	case'delete_partner':

			$id     = $_GET['id'];
			$sql    = "DELETE FROM partners WHERE id = '$id' ";
			$delete = mysqli_query($mysqli,$sql);

			if($delete){
				echo 'Partner deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=all_partners.php" >';
			}


		break;

	case'delete_page_image':

		$id     = $_GET['id'];
		$sql    = "UPDATE pages SET main_image = '' WHERE id = '$id' ";
		$delete = mysqli_query($mysqli,$sql);

		if($delete){
			echo 'Image deleted ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=add_page.php?id='.$id.'" >';
		}


		break;

	case'delete_gallery_image':

		$id         = $_GET['id'];
		$img_name   = urldecode(base64_decode($_GET['n']));

		$sql    = "DELETE FROM gallery WHERE id = '$id' ";
		$delete = mysqli_query($mysqli,$sql);

		if($delete){

			unlink($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/".$img_name); // main image
			unlink($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/thumbs/".$img_name); // thumbnail

			echo '<h1 style="text-align:center"><br/>Image deleted</h1> 
				<script type="text/javascript">
					<!-- 
					setTimeout(					    
					    function refreshAndClose() {
					        window.opener.location.reload(true);
					        window.close();
					    },2000); 
					//--> 
				</script>';
		}



		break;


	case 'setMainImage':

		$response   = array();
		$boom       = explode("|",$_POST['imageId']);
		$image_id   = $boom[0];
		$project_id = $boom[1];

		$sqlm       = "SELECT * FROM gallery WHERE  id = '$image_id' ";
		$resultm    = mysqli_query($mysqli,$sqlm);
		$rowm       = mysqli_fetch_array($resultm);
		$main_image = $rowm['image'];

		$new_image  = smartImageCrop("public/images/gallery",$main_image);

		$sqla       = "UPDATE pages set main_image  = 'gallery/$new_image' WHERE id = '$project_id'";
        $update     = mysqli_query($mysqli,$sqla);

        if($update){
	        $response['error']      = 0;
	        $response['message']    = "SUCCESS! Image set as main for this project.";
        }else{
	        $response['error']      = 1;
	        $response['message']    = "Error! Please refresh the page and try again.";
        }

        echo json_encode($response);

		break;

	case 'hideImage':

		$response   = array();
		$boom       = explode("|",$_POST['imageId']);
		$image_id   = $boom[0];

		$sqla       = "UPDATE gallery set img_active  = 'no' WHERE id = '$image_id'";
        $update     = mysqli_query($mysqli,$sqla);

        if($update){
	        $response['error']      = 0;
	        $response['message']    = "SUCCESS! The image is now hidden.";
        }else{
	        $response['error']      = 1;
	        $response['message']    = "Error! Please refresh the page and try again.";
        }

        echo json_encode($response);

		break;


	case'deleteImage':

		$response   = array();
		$boom       = explode("|",$_POST['imageId']);
		$image_id   = $boom[0];

		$sqlm       = "SELECT * FROM gallery WHERE  id = '$image_id' ";
		$resultm    = mysqli_query($mysqli,$sqlm);
		$rowm       = mysqli_fetch_array($resultm);
		$main_image = $rowm['image'];

		$sql    = "DELETE FROM gallery WHERE id = '$image_id' ";
		$delete = mysqli_query($mysqli,$sql);

		if($delete){

			unlink($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/".$main_image); // main image
			unlink($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/thumbs/".$main_image); // thumbnail

			$response['error']      = 0;
			$response['message']    = "SUCCESS! The image was deleted.";
			$response['refresh']    = "on";
		}else{
			$response['error']      = 1;
			$response['message']    = "Error! Please refresh the page and try again.";
		}

		echo json_encode($response);


		break;


		case 'requestRemoval':

			$response   = array();
			$image_id   = $_POST['imageId'];
			$request_reason = "Please remove this project.";


			$sql        = "INSERT INTO requests (request_name, request_reason, request_date, project_id) VALUE ('$suser', '$request_reason', NOW(), '$image_id')";
			$insert     = mysqli_query($mysqli, $sql);

			if($insert){
				$response['error']      = 0;
				$response['message']    = "SUCCESS! Your request was send to an admin.";
			}else{
				$response['error']      = 1;
				$response['message']    = "Error! Please refresh the page and try again.";
			}

			echo json_encode($response);

			break;

	case 'cancelRemoval':


		$project_id   = $_GET['id'];

		$sqla       = "UPDATE requests set project_removed  = 'yes' WHERE project_id = '$project_id'";
		$update     = mysqli_query($mysqli,$sqla);

		if($update){
			echo 'Request removed ...  <META HTTP-EQUIV=Refresh CONTENT="1; URL=index.php" >';
		}



		break;

	case 'delete_mass_image':

		$ids    = $_POST['ids'];

		foreach ($ids as $image_id)
		{
			$sqlm       = "SELECT image FROM gallery WHERE  id = '$image_id' ";
			$resultm    = mysqli_query($mysqli,$sqlm);
			$rowm       = mysqli_fetch_array($resultm);
			$main_image = $rowm['image'];

			unlink($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/".$main_image);
			unlink($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/thumbs/".$main_image);

			$sql    = "DELETE FROM gallery WHERE id = '$image_id' ";
			$delete = mysqli_query($mysqli,$sql);


		}





		break;

}
?>