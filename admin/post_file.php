<?php
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}
require_once('config.php');


	$id_get         = $_GET['id'];
	$watermark_type = (int)$_GET['w'];
	$nume_document  = base64_decode($_GET['np']);
	$demo_mode      = false;
	$upload_dir     = '../public/images/gallery/';
	$dir            = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/";
	$allowed_ext    = array('jpg','jpeg','png','gif');
	$data           = date("Y.m.d");
	$ora            = date("H:i:s");

	if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
		exit_status('Error! Upload method not permited');
	}

	if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){

		$pic = $_FILES['pic'];

		if(!in_array(get_extension($pic['name']),$allowed_ext)){
			exit_status('Allowed extensions: '.implode(',',$allowed_ext).' !');
		}

		$ext            = pathinfo($pic['name'], PATHINFO_EXTENSION);
		$ext            = strtolower($ext);
		$nm             = explode($ext, $pic['name']);
		$random_number  = sanitazeStringForUrl($nm[0]."_".rand(11111,9999999));

		$sqla   = "INSERT INTO gallery (project_id,image,img_active,doc_type,doc_alt,folder) values ('$id_get', '".$random_number.".".$ext."','yes','$ext','$nume_document','image')";
		$insert = mysqli_query($mysqli,$sqla);

		if($insert) {

			$nume_nou_poza      = $random_number . "." . $ext;

			move_uploaded_file( $pic['tmp_name'], $upload_dir ."original/". $nume_nou_poza );
			chmod( $dir ."original/". $nume_nou_poza, 0777 );

			$filename               = $upload_dir ."original/". $nume_nou_poza;
			list( $width, $height ) = getimagesize( $filename );

			// creating new image 1350x900
			$newwidth   = "1350";
			$newheight  = floor( $height * ( $newwidth / $width ) );
			$newImage   = imagecreatetruecolor( $newwidth, $newheight );
			$source     = imagecreatefromjpeg( $filename );
			imagecopyresized( $newImage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height );
			$bigImageCreatedOk = imagejpeg( $newImage, $upload_dir . $nume_nou_poza, 72 );

			// create watermark only for the big image
			if($watermark_type > 0){
				watermark_image($dir.$nume_nou_poza, $watermark_type, $dir.$nume_nou_poza);
			}

			// creating thumb forced on height - no watermark
			$newheightTh    = "250";
			if($width >= $height){
				$newwidthTh = "370";
			}else{
				$newwidthTh = floor( $width * ( $newheightTh / $height ) );
			}
			$newImageTh     = imagecreatetruecolor( $newwidthTh, $newheightTh );
			$source         = imagecreatefromjpeg( $filename );
			imagecopyresized( $newImageTh, $source, 0, 0, 0, 0, $newwidthTh, $newheightTh, $width, $height );
			$thumbCreatedOk = imagejpeg( $newImageTh, $upload_dir ."thumbs/". $nume_nou_poza, 75 );

			// send confirmation
			if ($bigImageCreatedOk === true && $thumbCreatedOk === true) {
				unlink($dir."original/". $nume_nou_poza);
				exit_status( 'Success! Images uploaded' );
			} else {
				exit_status( 'Error! Corrupted image please retry' );
			}

		}
	}

function sanitazeStringForUrl($string){
	$str = strtolower(trim($string));
	$str = preg_replace('@[^a-zA-Z0-9]@', ' ', $str);
	$str = preg_replace('/(\s\s+|\-\-+)/', ' ', $str);
	return str_replace(' ','-',$str);
}

function watermark_image($target, $watermark_type, $newcopy) {

	switch ($watermark_type){
		case 1:
		default:
			$wtrmrk_file = $_SERVER['DOCUMENT_ROOT']."/public/images/logo-white.png";
			break;
		case 2:
			$wtrmrk_file = $_SERVER['DOCUMENT_ROOT']."/public/images/logo-black.png";
			break;
	}

	$watermark  = imagecreatefrompng($wtrmrk_file);
	imagealphablending($watermark, false);
	imagesavealpha($watermark, true);
	$img        = imagecreatefromjpeg($target);
	$img_w      = imagesx($img);
	$img_h      = imagesy($img);
	$wtrmrk_w   = imagesx($watermark);
	$wtrmrk_h   = imagesy($watermark);
	$right_poz  = 10;
	$bot_poz    = 10;
	/*
	$dst_x      = ($img_w / 2) - ($wtrmrk_w / 2); // For centering the watermark on any image
	$dst_y      = ($img_h / 2) - ($wtrmrk_h / 2); // For centering the watermark on any image
	*/
	$dst_x      = ($img_w) - ($wtrmrk_w - $right_poz);
	$dst_y      = ($img_h) - ($wtrmrk_h - $bot_poz);
	imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
	imagejpeg($img, $newcopy, 72);
	imagedestroy($img);
	imagedestroy($watermark);
}

function exit_status($str){
	echo json_encode(array('status'=>$str));
	exit;
}
function get_extension($file_name){
	$ext = explode('.', $file_name);
	$ext = array_pop($ext);
	return strtolower($ext);
}
?>