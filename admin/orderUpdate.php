<?php
session_start();
if(!$_SESSION['auth']){
	header('Location: login.php');
	die();
}
//include database class
include_once 'db.php';
$db = new DB();

//get images id and generate ids array
$idArray = explode(",",$_POST['ids']);

//update images order
$db->updateOrder($idArray);
?>