<?php
/*
------------------
Language: English
------------------
*/

$lang   = array();
$lb     = 'en';

// meniu
$lang['Dashboard']          = 'Dashboard';
$lang['ToWebsite']          = 'To Website';
$lang['Users']              = 'Users';
$lang['AddUser']            = 'Add a user';
$lang['AllUsers']           = 'All users';
$lang['SiteInformation']    = 'Site information';
$lang['GeneralInfo']        = 'General info';
$lang['GoodToKnow']         = 'Good to know';
$lang['FileUpload']         = 'File upload';
$lang['FrontPagSlider']     = 'Front page slider';
$lang['AddSliderImage']     = 'Add slider image';
$lang['EditSliderImage']    = 'Edit slider image';
$lang['MenuCalendar']       = 'Lease calendar';
$lang['MenuCalendarAdd']    = 'Add entry';
$lang['MenuCalendarAll']    = 'All entries';
$lang['MenuGallery']        = 'Galleries';
$lang['MenuGalleryFoto']    = 'Create new gallery';
$lang['MenuGalleryVideo']   = 'Create video gallery';
$lang['MenuAllGalleries']   = 'All galleries';
$lang['WebsitePages']       = 'Website pages';
$lang['AddArticle']         = 'Add new gallery';
$lang['AddActivity']        = 'Add new activity';
$lang['AllWebsitePages']    = 'See all pages';

// general
$lang['MyProfile']      = 'My profile';
$lang['FullScreen']     = 'Full screen';
$lang['DesignedBy']     = 'Designed by CipDesign.ro';
$lang['MoreDetails']    = 'More details';
$lang['Pages']          = 'Pages';
$lang['Articles']       = 'Galleries';

// add_user.php
$lang['UsersMainTitle']         = 'Users <small>info, permisions and type</small>';
$lang['UsersMainSubtitle']      = 'User info';
$lang['UsersTopFormTitle']      = 'Website users';
$lang['FormUserName']           = 'User name';
$lang['FormCompanyName']        = 'Company name';
$lang['FormLoginEmail']         = 'User login email';
$lang['FormLoginEmailHintText']         = 'Used at login. Must be valid'.
$lang['FormLoginEmailHintTitle']        = 'User main email';
$lang['FormInitialPassword']            = 'Initial password';
$lang['FormInitialPasswordHintText']    = 'The password will be automatically encoded. To reset it, create a new one else leave blank.';
$lang['FormInitialPasswordHintTitle']   = 'User password';
$lang['FormNotificariEmail']            = 'Email notifications';
$lang['FormNotificariEmailOpt1']        = 'no notification';
$lang['FormNotificariEmailOpt2']        = 'with notification';
$lang['FormNotificariEmailHint']        = 'An email containing all login details will be send to this user.';
$lang['FormUserType']                   = 'User type';
$lang['FormUserTypeOpt1']               = 'Admin (full admin rights)';
$lang['FormUserTypeOpt2']               = 'User (limited)';
$lang['FormUserTypeHint']               = 'Admins can manage users.';
$lang['FormUserActive']                 = 'Active login?';
$lang['FormUserActiveOpt1']             = 'Active account';
$lang['FormUserActiveOpt2']             = 'Inactive account';
$lang['FormUserActiveHint']             = 'The user can login only if the account is active.';
$lang['FormUserLanguage']               = 'Admin language';
$lang['FormUserLanguageOpt1']           = 'Romanian';
$lang['FormUserLanguageOpt2']           = 'English';
$lang['FormUserLanguageHint']           = 'The admin interface will be displayd in this language.';
$lang['FormSecondaryEmail']             = 'Secondary email';
$lang['FormSecondaryEmailHintText']     = 'Optional. Recommended.';
$lang['FormSecondaryEmailHintTitle']    = 'Secondary email address';
$lang['FormUserPhone']                  = 'User phone';
$lang['FormUserPhoneHintTitle']         = 'User phone';
$lang['FormUserPhoneHintText']          = 'Optional. Recommended.';
$lang['FormUserCountry']                = 'User country';
$lang['FormUserCountryHintTitle']       = 'User country';
$lang['FormUserCountryHintText']        = 'Optional. Recommended.';
$lang['FormUserAddress']                = 'User address';
$lang['FormUserAddressHintTitle']       = 'User address';
$lang['FormUserAddressHintText']        = 'Optional. Recommended.';
$lang['FormUserNotes']                  = 'User notes';
$lang['FormUserNotesHintTitle']         = 'User notes';
$lang['FormUserNotesHintText']          = 'This input is just for admin note. (max. 250 chars)';
$lang['FormSubmitBtnText']              = 'Register user';
$lang['FormResetBtn']                   = 'Reset data';
// edit form data
$lang['EditUsersMainTitle']         = 'Update user details <small> permission and contacts</small>';
$lang['EditUsersMainSubtitle']      = 'Change user details';
$lang['EditUsersTopFormTitle']      = 'Update details';
$lang['EditUsersFormPassword']          = 'User password';
$lang['EditUsersFormPasswordHintTitle'] = 'Change here to reset the existing password. Leave blank if you do not wish a password change';
$lang['EditUsersFormPasswordHintText']  = 'Change/reset password';
$lang['EditFormSubmitBtnText']          = 'Update details';
$lang['EditFormResetBtn']               = 'Reset to initial state';


// users.php
$lang['AllUsersMainTitle']         = 'Users <small>  info and permissions</small>';
$lang['AllUsersMainSubtitle']      = 'Users';
$lang['AllUsersBoxTitle']          = 'All system users';
$lang['AllUsersTablehead1']        = 'ID';
$lang['AllUsersTablehead2']        = 'User details';
$lang['AllUsersTablehead3']        = 'Title';
$lang['AllUsersTablehead4']        = 'Contacts';
$lang['AllUsersTablehead5']        = 'Actions';
$lang['AllUsersInfo1']             = 'Last login at';
$lang['AllUsersInfo2']             = 'User not logged in yet';
$lang['AllUsersInfo3']             = 'Account active';
$lang['AllUsersInfo4']             = 'Created at';
$lang['AllUsersInfo5']             = 'Notes';
$lang['AllUsersInfo6']             = 'Update user details';
$lang['AllUsersInfo7']             = 'Block this user account';
$lang['AllUsersInfo8']             = 'Are you sure that you want to block this user account?';
$lang['AllUsersInfo9']             = 'Block user account';


// faq.php
$lang['FaqPageTitle']           = 'Good to know <small> project related informations</small>';
$lang['FaqPageSubtitle']        = 'Info';
$lang['FaqPageTabTitle1']       = 'General info';
$lang['FaqPageTabTitle2']       = 'Users';
$lang['FaqPageTabTitle3']       = 'Website pages';
$lang['FaqPageTabTitle4']       = 'File upload';
$lang['FaqPageTabTitle5']       = 'Limitations';
$lang['FaqPageTab1SubTitle1']   = 'Dev info and support';
$lang['FaqPageTab1SubTitle2']   = 'Website info';
$lang['FaqPageTab1SubTitle3']   = 'Further development';
$lang['FaqPageTab1Text1']       = 'V. Ciprian (web developer)<br/>+44 0776.998.0346 (UK)<br/><a href="mailto:ciprian.m.visan@gmail.com">ciprian.m.visan@gmail.com</a><br/>www.eighty3.co.uk';
$lang['FaqPageTab1Text2']       = '<ul><li>MVC structure with custom admin panel</li><li>SEO on site (sitemap.xml, robotx.txt)</li><li>The website is google search optimised</li></ul>';
$lang['FaqPageTab1Text3']       = '<ul><li>The website structure is a modular one, new modules can be developed on request</li><li>The admin area is created to manage the front-end and all the users</li>	<li>The platform is a custom one, build from ground up and fully tested - is not a preformed structure</li>	<li>Technologies used: PHP (PDO, OOP),  HTML5, CSS3, JavaScript, AJAX)</li><li>Security: passed all pen-test and security metrics</li></ul>';
$lang['FaqPageTab2SubTitle1']   = 'Users management';
$lang['FaqPageTab2SubTitle2']   = 'Permissions';
$lang['FaqPageTab2Text1']       = '<ul><li>To manage users you need to have an active admin account</li><li>To add new users go to "Users" > "Add new user"</li><li>To manage users go to "Users" > "All users"</li><li>If you choose to "notify user" when the account was created select "with notification" in this case the user will receive an automated email containing the login credentials</li><li>Admins - unrestricted access to the admin panel</li><li>Partners - can only manage his details and projects</li><li>There is not limit in admin/partners number</li></ul>';
$lang['FaqPageTab2Text2']       = '<ul><li>Only active admins can change page details and manage other users</li></ul>';
$lang['FaqPageTab3SubTitle1']   = 'File upload';
$lang['FaqPageTab3Text1']       = '<ul><li>Multiple file upload is allowed</li><li>Drag&Drop support</li><li>No file limit</li><li>The files are only available for download</li></ul>';
$lang['FaqPageTab4SubTitle1']   = 'Known issues';
$lang['FaqPageTab4SubText1']    = '<ul><li>Admins can\'t manage partners content via partner interface - they have to do it via main admin panel</li><li>Last page edit will be visible on the fornt-end</li><li>Make sure the server has space for back-ups. If the server will run out of space NO bkps will be made</li><li>Site IP: '. $_SERVER['SERVER_ADDR'].'</li><li>Back-up is done daily in 2 distinct points - 12 hours apart</li></ul>';
$lang['FaqPageTab5SubTitle1']   = 'Site pages';
$lang['FaqPageTab5SubText1']    = '<ul><li>All front-end pages have a back-end (admin) entry</li><li>The changes will appear in real time on the front-end</li><li>The "Page visibility" property will determine if a page appears on the website</li><li>The "Page order"  property will determine in what order that page will be displayed on the website</li><li>There is no page limit - dynamic generaed pages</li></ul>';


// file_upload.php
$lang['FileUploadPageTitle']    = 'Multiple file upload <small> all common file extensions (zip, rar, 7z, pdf, images, docs, xls)</small>';
$lang['LoadFilesOnTheServer']   = 'Load files on the server';
$lang['SimpleUpload']           = 'Upload';
$lang['FileUploadText1']        = 'Upload multiple files at once<br>Simple Drag&Drop action (select your files and drop them in this page).<br>Your files will be save and easy to access via a URL. Excellent tool for files that are to large for email.';
$lang['FileUploadText2']        = '<h3>Notes</h3><ul><li>Max file size restricted by the server is <strong>'.(int)(ini_get("upload_max_filesize")).'MB</strong>.</li><li>For all image extensions (<strong>JPG, GIF, PNG</strong>) a preview will be generated</li><li>For all uploaded files the download will be forced (NO browser view).</li><li>To get a file url just "Right Click" on that file name and "Copy link adress"</li></ul>';
$lang['FileUploadBtn1']         = 'Add files ...';
$lang['FileUploadBtn2']         = 'Start upload';
$lang['FileUploadBtn3']         = 'Cancel';
$lang['FileUploadBtn4']         = 'Delete';
$lang['FileUploadBtn5']         = 'Start';

// add_slider.php
$lang['AddSlidePageTitle']      = 'Slides <small>images on front page</small>';
$lang['AddSlidePageSubtitle']   = 'Home page slides';
$lang['AddSlideFormTitle']      = 'Add new slide';
$lang['SlideFormAltText']       = 'Slide alternative text (SEO)';
$lang['SlideFormSlideText']       = 'Slide title';
$lang['SlideFormSlideTextHintTitle'] = 'If present will be ontop of the image';
$lang['SlideFormSlideTextHintText']  = 'Slide title';
$lang['SlideFormAltTextHintTitle'] = 'Hidden on the page. For search engines';
$lang['SlideFormAltTextHintText']  = 'Image alt tag(SEO)';
$lang['SlideFormActive']           = 'Slide visibility';
$lang['SlideFormActiveOpt1']       = 'yes';
$lang['SlideFormActiveOpt2']       = 'no';
$lang['SlideFormActiveHint']       = 'Is this image visible on the homepage slider';
$lang['SlideFormOrder']            = 'Order in slider';
$lang['SlideFormOrderHintTitle']   = 'Numerical';
$lang['SlideFormOrderHintText']    = 'Position in group';
$lang['SlideFormLink']             = 'Slide URL';
$lang['SlideFormLinkHintText']     = 'Optional. If present will redirect on click';
$lang['SlideFormLinkHintTitle']    = 'Image URL';
$lang['SlideFormNotes']            = 'Slide notes';
$lang['SlideFormNotesHintText']    = 'Optional - just for internal notes (250 char max)';
$lang['SlideFormNotesHintTitle']   = 'Notes';
$lang['SlideFormImage']            = 'Images';
$lang['SlideFormImageNotes']       = '<span class="label label-important">NOTE!</span><span>After the above details are added the system will redirect to the image crop page.<br/> Slide recommended size 1920x1080px</span>';
$lang['SlideFormAddSlideBtn']      = 'Add slide';
$lang['SlideFormResetSlideBtn']    = 'Reset';
$lang['EditSlidePageTitle']        = 'Edit slide details <small> image and details</small>';
$lang['EditSlidePageSubTitle']     = 'Update slide details';
$lang['SlideFormEditSlideBtn']     = 'Edit slide';
$lang['SlideFormEditResetSlideBtn']  = 'Reset';
$lang['EditSlideImageName']         = 'Slide';
$lang['EditSlideWarning']           = 'No images is uploaded';
$lang['EditSlideImageAction']       = 'Image action';
$lang['EditSlideImageHint']         = 'Edit this image';


// all_slides.php
$lang['AllSlidesPageTitle']      = 'Select slider image <small> edit image and image details</small>';
$lang['AllSlidesPageSubTitle']   = 'Select slide';
$lang['AllSlidesFormTitle']      = 'All slider images';
$lang['AllSlidesTable1']         = 'ID';
$lang['AllSlidesTable2']         = 'Slide details';
$lang['AllSlidesTable3']         = 'Visibility';
$lang['AllSlidesTable4']         = 'Order';
$lang['AllSlidesTable44']        = 'Actions';
$lang['AllSlidesTable5']         = 'in group';
$lang['AllSlidesTable6']         = 'Edit this slide';
$lang['AllSlidesTable7']         = 'Are you sure that you want to delete this slide?  You can set it as inactive.';



//add_calendar.php
$lang['AllCalendarPageTitle']      = 'Calendar arenda <small> date si contact</small>';
$lang['AllCalendarPageSubTitle']   = 'Calendar arenda';
$lang['AllCalendarFormTitle']      = 'Adauga intrare in calendar';
$lang['FormCalendarOrder']         = 'Ordine in grup';
$lang['FormCalendarOrderHint']     = 'Valoare numerica. Intrarile vor fi ordonate ascendent';
$lang['FormCalendarOrderHintTitle']= 'Ordine in tabel';
$lang['CalendarFormActive']        = 'Vizibilitate';
$lang['CalendarFormActiveOpt1']    = 'yes';
$lang['CalendarFormActiveOpt2']    = 'no';
$lang['CalendarFormActiveHint']    = 'Este aceasta intrare vizibila pe site in tabel?';
$lang['FormCalendarContact']            = 'Telefon contact';
$lang['FormCalendarContactHint']        = 'Telefon si detalii agent.';
$lang['FormCalendarContactHintTitle']   = 'Telefon agent';
$lang['FormCalendarAgent']              = 'Agent zonal';
$lang['FormCalendarAgentHint']          = 'Nume agent zonal.';
$lang['FormCalendarAgentHintTitle']     = 'Nume agent';
$lang['FormCalendarLocatie']            = 'Locatie';
$lang['FormCalendarLocatieHint']        = 'Locatie, aria, zona, oras.';
$lang['FormCalendarLocatieHintTitle']   = 'Oras, locatie';
$lang['FormCalendarDateProduse']            = 'Date produse';
$lang['FormCalendarDateProduseHint']        = 'Data singulara sau interval.';
$lang['FormCalendarDateProduseHintTitle']   = 'Data/Date';
$lang['FormCalendarDatePlati']              = 'Date plati';
$lang['FormCalendarDatePlatiHint']          = 'Data singulara sau interval.';
$lang['FormCalendarDatePlatiHintTitle']     = 'Data/Date';
$lang['FormAddCalendarBtn']                 = 'Adauga intrare';
$lang['FormEditCalendarBtn']                = 'Editare intrare';
$lang['FormResetCalendarBtn']               = 'Reseteaza datele';
$lang['EditAllCalendarPageTitle']           = 'Calendar arenda <small> date si contact</small>';
$lang['EditAllCalendarPageSubtitle']        = 'Editeaza intrare';
$lang['EditCalendarPageSubTitle']           = 'Modifica detalii intrare';


//calendar.php
$lang['AllCalendarsPageTitle']      = 'Calendar arenda <small> date si contact</small>';
$lang['AllCalendarsPageSubTitle']   = 'Toate intrarile calendarului de arenda';
$lang['AllCalendarsFormTitle']      = 'Intrari in calendar';
$lang['AllCalendarsTable1']         = 'ID';
$lang['AllCalendarsTable2']         = 'Detalii intrare';
$lang['AllCalendarsTable3']         = 'Vizibilitate';
$lang['AllCalendarsTable4']         = 'Ordine';
$lang['AllCalendarsTable44']        = 'Actiune';
$lang['AllCalendarsTable5']         = 'in group';
$lang['AllCalendarsTable6']         = 'Editeaza aceasta intrare';
$lang['AllCalendarsTable7']         = 'Esti sigur ca vrei sa stergi aceasta intrare?  Se poate marca cu vizibilitate NU.';


//all_pages.php
$lang['AllPagesTitle']          = 'Select page <small> content details</small>';
$lang['AllPagesSubTitle']       = 'Select page';
$lang['AllPagesFormTitle']      = 'All website pages';
$lang['AllPagesTable1']         = 'ID';
$lang['AllPagesTable2']         = 'Page details';
$lang['AllPagesTable3']         = 'Status';
$lang['AllPagesTable4']         = 'Dates';
$lang['AllPagesTable44']        = 'Actions';
$lang['AllPagesTable6']         = 'Update this page';
$lang['AllPagesTable7']         = 'Delete this page';
$lang['AllPagesTable8']         = 'Page without image';
$lang['AllPagesTable5']         = 'Are you sure that you want to delete this page? You can also set page visibility to NO.';
$lang['AllPagesTable9']         = 'Image gallery';
$lang['AllPagesTable10']        = 'Mark request as done';
$lang['AllPagesTable11']        = 'Title';
$lang['AllPagesTable12']        = 'Type';
$lang['AllPagesTable13']        = 'Visible on front-end';
$lang['AllPagesTable14']        = 'Order in group';
$lang['AllPagesTable15']        = 'Created at';
$lang['AllPagesTable16']        = 'Created by';
$lang['AllPagesTable17']        = 'not applicable';


//add_page.php
$lang['AddPageTitle']          = 'Site page <small> settings, image and content</small>';
$lang['AddPageSubTitle']       = 'Add page details';
$lang['AddPageFormTitle']      = 'Page details';
$lang['EditPageTitle']         = 'Update page details <small>content and images</small>';
$lang['EditPageSubTitle']      = 'Update page';
$lang['EditPageFormTitle']     = 'Update page details';
$lang['EditPageFormTitleHint']     = 'This will be shown on the page';
$lang['EditPageFormActive']        = 'Page visibility';
$lang['EditPageFormActiveOpt1']    = 'yes';
$lang['EditPageFormActiveOpt2']    = 'no';
$lang['EditPageFormActiveHint']    = 'This property will determine if this page will be visible on the website';
$lang['EditPageFormApproved']        = 'Gallery type';
$lang['EditPageFormApprovedOpt1']    = 'photo';
$lang['EditPageFormApprovedOpt2']    = 'video';
$lang['EditPageFormApprovedHint']    = 'This property will determine the type of the gallery';
$lang['EditPageFormOrder']           = 'Page order';
$lang['EditPageFormOrderHintTitle']  = 'Page order in group';
$lang['EditPageFormOrderHintText']   = 'Numerical value. (Will be displayed from small to big [ASC])';
$lang['EditPageFormMetaTitle']           = 'Page meta title';
$lang['EditPageFormMetaTitleHintTitle']  = 'Used for SEO';
$lang['EditPageFormMetaTitleHintText']   = 'This attribute will be use for page search engine optimisation. (max 250 char). Hidden on the page. Highly recommended';
$lang['EditPageFormMetaTitle']           = 'Page meta title';
$lang['EditPageFormMetaTitleHintTitle']  = 'Used for SEO';
$lang['EditPageFormMetaTitleHintText']   = 'This attribute will be use for page search engine optimisation. (max 250 char). Hidden on the page. Highly recommended.';
$lang['EditPageFormMetaDesc']           = 'Page meta description';
$lang['EditPageFormMetaDescHintTitle']  = 'Used for SEO';
$lang['EditPageFormMetaDescHintText']   = 'This attribute will be use for page search engine optimisation. (max 250 char). Hidden on the page. Highly recommended.';
$lang['EditPageFormContent']            = 'Page content';
$lang['EditPageFormImage']              = 'Page main image';
$lang['EditPageFormNotes']           = 'Page notes';
$lang['EditPageFormNotesHintTitle']  = 'Page notes';
$lang['EditPageFormNotesHintText']   = 'Only for back-end. (max 256 char). Attribute available only for admins.';
$lang['EditPageFormCustom']          = 'Custom page content';
$lang['EditPageFormCustomHintTitle'] = 'Page box';
$lang['EditPageFormCustomHintText']  = 'Custom page content. If empty wont show.';
$lang['EditPageFormBtnEdit']         = 'Update page details';
$lang['EditPageFormBtnReset']        = 'Reset details';
$lang['EditPageFormImageActions']    = 'Image actions';
$lang['EditPageFormImageAction1']    = 'Edit main image';
$lang['EditPageFormImageAction2']    = 'Change main image';
$lang['EditPageFormImageAction3']    = 'Delete this image';
$lang['EditPageFormImageAction4']    = 'Are you sure that you want to delete this image?';
$lang['EditPageFormImageAction5']    = 'Delete image';
$lang['EditPageFormImageAction6']    = 'Go to image gallery';
$lang['EditPageFormImageNote']       = '<span class="label label-important">NOTE!</span><span>This attribute is optional. After the page is created the system will redirect to the "crop image" feature. </span>';
$lang['EditPageFormImageAction7']    = 'Page image gallery';
$lang['EditPageFormImageAction8']    = '<span class="label label-important">NOTE!</span><span>This attribute is optional. After the page is created you can upload images intro this page gallery using a simple Drag&Drop.  </span>';


// afisare change_details
$lang['DisplayPageTitle']           = 'Change image details';
$lang['DisplayPageFormAlt']         = 'Image alt tag';
$lang['DisplayPageFormAltHint']     = 'This will be used for SEO images.';
$lang['DisplayPageFormVisibility']  = 'Image visible';
$lang['DisplayPageFormVisiOpt1']    = 'yes';
$lang['DisplayPageFormVisiOpt2']    = 'no';
$lang['DisplayPageFormVisiHint']    = 'Image wont show if is set to "no"';
$lang['DisplayPageFormConvertImg']  = 'Change to main image';
$lang['DisplayPageFormConvertOpt1'] = 'no';
$lang['DisplayPageFormConvertOpt2'] = 'yes';
$lang['DisplayPageFormConvertHint'] = 'This image will replace the "main project image" if is set to "yes"';
$lang['DisplayPageFormGal']         = 'Image category';
$lang['DisplayPageFormGalHint']     = 'If selected the category will become a image filter.';
$lang['DisplayPageEditDetails']     = 'Update details';
$lang['DisplayPageFormResetBtn']    = 'Reset';
