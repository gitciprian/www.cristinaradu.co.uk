<?php
/*
------------------
Language: Romanian
------------------
*/

$lang   = array();
$lb     ='ro';

// meniu
$lang['Dashboard']          = 'Panou de control';
$lang['ToWebsite']          = 'Catre site';
$lang['Users']              = 'Utilizatori';
$lang['AddUser']            = 'Adauga utilizator';
$lang['AllUsers']           = 'Toti utilizatorii';
$lang['SiteInformation']    = 'Informatii';
$lang['GeneralInfo']        = 'Generalitati';
$lang['GoodToKnow']         = 'Bine de stiut';
$lang['FileUpload']         = 'Incarca fisiere';
$lang['FrontPagSlider']     = 'Slider prima pagina';
$lang['AddSliderImage']     = 'Adauga imagine slide';
$lang['EditSliderImage']    = 'Editeaza imagine slide';
$lang['MenuCalendar']       = 'Calendar arenda';
$lang['MenuCalendarAdd']    = 'Adauga intrare';
$lang['MenuCalendarAll']    = 'Toate intrarile';
$lang['MenuGallery']        = 'Categorii pentru galerii';
$lang['MenuGalleryFoto']    = 'Adauga categorie galerie';
$lang['MenuGalleryVideo']   = 'Adauga galerie video';
$lang['MenuAllGalleries']   = 'Toate categoriile';
$lang['WebsitePages']       = 'Pagini site';
$lang['AddArticle']         = 'Creaza galerie noua';
$lang['AddActivity']        = 'Creaza activitate noua';
$lang['AllWebsitePages']    = 'Vezi toate paginile';


// general
$lang['MyProfile']      = 'Profilul meu';
$lang['FullScreen']     = 'Ecran complet';
$lang['DesignedBy']     = 'Dezvoltat de CipDesign.ro';
$lang['MoreDetails']    = 'Detalii';
$lang['Pages']          = 'Pagini site';
$lang['Articles']       = 'Galerii';

// add_user.php
$lang['UsersMainTitle']         = 'Utilizatori <small>info, detalii si permisiuni</small>';
$lang['UsersMainSubtitle']      = 'Informatii utilizator';
$lang['UsersTopFormTitle']      = 'Utilizatori website';
$lang['FormUserName']           = 'Nume utilizator';
$lang['FormCompanyName']        = 'Companie utilizator';
$lang['FormLoginEmail']         = 'Email principal utilizator';
$lang['FormLoginEmailHintText']         = 'Va fi folosit la autentificare. Trebuie sa fie valid'.
$lang['FormLoginEmailHintTitle']        = 'Adresa principala utilizator';
$lang['FormInitialPassword']            = 'Parola initiala';
$lang['FormInitialPasswordHintText']    = 'Parola va fi encodata automat. Pentru a o reseta, creati o parola noua. ';
$lang['FormInitialPasswordHintTitle']   = 'Parola utilizator';
$lang['FormNotificariEmail']            = 'Cu notificare';
$lang['FormNotificariEmailOpt1']        = 'fara email de notificare';
$lang['FormNotificariEmailOpt2']        = 'cu email de notificare';
$lang['FormNotificariEmailHint']        = 'Un email ce contine toate detaliile de login va fi trimis catre utilizator.';
$lang['FormUserType']                   = 'Tip utilizator';
$lang['FormUserTypeOpt1']               = 'Administrator (drepturi depline)';
$lang['FormUserTypeOpt2']               = 'Operator (drepturi limitate)';
$lang['FormUserTypeHint']               = 'Administratorii pot sa faca management de operatori.';
$lang['FormUserActive']                 = 'Utilizator activ?';
$lang['FormUserActiveOpt1']             = 'Cont activ (permite autentificare)';
$lang['FormUserActiveOpt2']             = 'Cont inactiv (nu permite autentificare)';
$lang['FormUserActiveHint']             = 'Numai useri activi se pot autentifica in sistem.';
$lang['FormUserLanguage']               = 'Limba admin';
$lang['FormUserLanguageOpt1']           = 'Romana';
$lang['FormUserLanguageOpt2']           = 'Engleza';
$lang['FormUserLanguageHint']           = 'Interfata de adminsitrare va fi afisata in aceasta limba.';
$lang['FormSecondaryEmail']             = 'Email secundar';
$lang['FormSecondaryEmailHintText']     = 'Optional. Recomandat.';
$lang['FormSecondaryEmailHintTitle']    = 'Adresa secundara';
$lang['FormUserPhone']                  = 'Telefon utilizator';
$lang['FormUserPhoneHintTitle']         = 'Telefon utilizator';
$lang['FormUserPhoneHintText']          = 'Optional. Recomandat.';
$lang['FormUserCountry']                = 'Tara utilizator';
$lang['FormUserCountryHintTitle']       = 'Tara utilizator';
$lang['FormUserCountryHintText']        = 'Optional. Recomandat.';
$lang['FormUserAddress']                = 'Adresa utilizator';
$lang['FormUserAddressHintTitle']       = 'Adresa utilizator';
$lang['FormUserAddressHintText']        = 'Optional. Recomandat.';
$lang['FormUserNotes']                  = 'Mentiuni utilizator';
$lang['FormUserNotesHintTitle']         = 'Mentiuni utilizator';
$lang['FormUserNotesHintText']          = 'Acest camp este disponibil doar administratorilor. (maxim 256 caractere)';
$lang['FormSubmitBtnText']              = 'Inregistreaza utilizator';
$lang['FormResetBtn']                   = 'Reseteaza datele';
// edit form data
$lang['EditUsersMainTitle']             = 'Actualizeaza date utilizator<small> permisiuni si contacte</small>';
$lang['EditUsersMainSubtitle']          = 'Editeaza date utilizator';
$lang['EditUsersTopFormTitle']          = 'Date existente';
$lang['EditUsersFormPassword']          = 'Parola utilizator';
$lang['EditUsersFormPasswordHintTitle'] = 'Schimba acest camp pentru a reseta parola. Lasa camp gol daca nu vrei sa resetezi parola.';
$lang['EditUsersFormPasswordHintText']  = 'Schimba/reseteaza parola';
$lang['EditFormSubmitBtnText']          = 'Actualizeaza detalii';
$lang['EditFormResetBtn']               = 'Reseteaza datele la valoarea initiala';

// users.php
$lang['AllUsersMainTitle']         = 'Utilizatori <small>  informatii si permisiuni</small>';
$lang['AllUsersMainSubtitle']      = 'Utilizatori website';
$lang['AllUsersBoxTitle']          = 'Toti utilizatorii sistemului';
$lang['AllUsersTablehead1']        = 'ID';
$lang['AllUsersTablehead2']        = 'Detalii utilizator';
$lang['AllUsersTablehead3']        = 'Titulatura';
$lang['AllUsersTablehead4']        = 'Detalii contact';
$lang['AllUsersTablehead5']        = 'Actiuni';
$lang['AllUsersInfo1']             = 'Ultima autentificare la';
$lang['AllUsersInfo2']             = 'Utilizator inca neautentificat';
$lang['AllUsersInfo3']             = 'Cont activ';
$lang['AllUsersInfo4']             = 'Creat la ';
$lang['AllUsersInfo5']             = 'Observatii';
$lang['AllUsersInfo6']             = 'Editeaza detalii utilizator';
$lang['AllUsersInfo7']             = 'Blocheaza cont';
$lang['AllUsersInfo8']             = 'Esti sigur ca vrei sa blochezi acest cont?';
$lang['AllUsersInfo9']             = 'Blocheaza cont utilizator';


// faq.php
$lang['FaqPageTitle']           = 'Bine de stiut <small> informatii legate de proiect</small>';
$lang['FaqPageSubtitle']        = 'Info';
$lang['FaqPageTabTitle1']       = 'Informatii generale';
$lang['FaqPageTabTitle2']       = 'Utilizatori';
$lang['FaqPageTabTitle3']       = 'Pagini site';
$lang['FaqPageTabTitle4']       = 'Upload de fisiere';
$lang['FaqPageTabTitle5']       = 'Limite cunoscute';
$lang['FaqPageTab1SubTitle1']   = 'Contact developer si suport';
$lang['FaqPageTab1SubTitle2']   = 'Informatii site';
$lang['FaqPageTab1SubTitle3']   = 'Dezvoltare in viitor';
$lang['FaqPageTab1Text1']       = 'V. Ciprian (web developer)<br/>+40 0723.237.182 (RO)<br/><a href="mailto:ciprian.cipdesign.ro">ciprian.cipdesign.ro</a><br/>www.cipdesign.ro';
$lang['FaqPageTab1Text2']       = '<ul><li>Structura site MVC cu zona de administrare custom</li><li>Optimizare SEO on site - conform cu ultima actualizare google (sitemap.xml, robotx.txt) </li><li>Siteul este optimizat pentru motoarele de cautare</li></ul>';
$lang['FaqPageTab1Text3']       = '<ul><li>Siteul este creat ca o structura modulara, oriacnd se pot adauga module noi</li><li>Zona de administrare este creata pentru a manageria continutul vizibil din front-end si utilizatorii.</li><li>Platforma este una dezvoltata de la 0, dezvoltata pentru a deservii nevoile custom, este testata permanent - NU este o structura preformata</li><li>Tehnologi folosite: PHP (PDO, OOP),  HTML5, CSS3, JavaScript, AJAX)</li>  <li>Securitate: validat de Security Metrics si testele de penetrare standard.</li></ul>';
$lang['FaqPageTab2SubTitle1']   = 'Management utilizatori';
$lang['FaqPageTab2SubTitle2']   = 'Permisiuni';
$lang['FaqPageTab2Text1']       = '<ul><li>Pentru a manageria utilizatorii trebuie sa aveti cont activ si titulatura de administrator</li><li>Pentru a adauga un utilizator nou mergeti la meniul din stanga "Utilizatori" > "Adauga utilizator"</li><li>Pentru a actualiza detaliile unui user mergeti la meniul din stanga  "Utilizatori" > "Toti utilizatorii"</li><li>Daca alegeti in momentul creari contului unui utilizator sa "trimiteti notificare email" atunci acel utilizator va primii pe email toate detaliile de autentificare</li><li>Nu exista un numar limitat de utilizatori</li></ul>';
$lang['FaqPageTab2Text2']       = '<ul><li>Doar administratorii activi pot sa modifice paginile siteului sau alti utilizatori</li></ul>';
$lang['FaqPageTab3SubTitle1']   = 'Incarcare fisiere';
$lang['FaqPageTab3Text1']       = '<ul><li>Este permisa incarcarea multipla de fisiere</li><li>Drag&Drop support</li><li>Nu exista o limita de fisiere</li><li>Fisierele vor fi descarcate automat</li></ul>';
$lang['FaqPageTab4SubTitle1']   = 'Limitari cunoscute';
$lang['FaqPageTab4SubText1']    = '<ul><li>Daca o pagina este editata simultan de mai multi utilizatori ultima editare va fi cea corecta</li><li>Asigurativa ca serverul are spatiu suficient de bkp. Daca nu exista spatiu bkp-urile nu se vor mai efectua.</li><li>Site IP: '. $_SERVER['SERVER_ADDR'] .'</li><li>Back-up se realizeaza zilnic de 2 ori pe zi la 12 ore distanta</li></ul>';
$lang['FaqPageTab5SubTitle1']   = 'Pagini site';
$lang['FaqPageTab5SubText1']    = '<ul><li>Toate paginile siteului au o intrare in zona de administrare</li><li>Modificarile facute in zona de administrare vor aparea in timp real pe site</li><li>Atributul "vizibilitate pagina" dicteaza daca pagina este vizibila sau nu pe site</li><li>Atributul "ordine pagina" daca exista va determina ordinea in care va aparea acea pagina in grup</li><li>Nu exista o limita de pagini - paginile sunt generate dinamic</li></ul>';


// file_upload.php
$lang['FileUploadPageTitle']    = 'Incarca fisiere pe server <small> sunt permise extensiile (zip, rar, 7z, pdf, images, docs, xls)</small>';
$lang['LoadFilesOnTheServer']   = 'Incarca fisier pe server';
$lang['SimpleUpload']           = 'Simplu de folosit';
$lang['FileUploadText1']        = 'Se pot incarca mai multe fisiere in acelas timp<br>Actiune simpla Drag&Drop (selectati fisierele si apoi doar "datile drumu" in aceasta pagina).<br>Fisierele dvs vor fi salvate si pot fi accesate oricand accesand URL-ul unic. Acest instrument este folositor cand atasamentele sunt prea mari pentru email.';
$lang['FileUploadText2']        = '<h3>Mentiuni</h3><ul><li>Limita maxima de dimensiune per fisier pe acest server este de <strong>'.(int)(ini_get("upload_max_filesize")).'MB</strong>.</li><li>Pentru toate imaginile (<strong>JPG, GIF, PNG</strong>) se va crea o galerie de previzualizare</li><li>Pentru toate fisierele se forteaza download-ul (nu se deschid in browser).</li><li>Pentru a copia link-ul unui fisier "Click dreapta" pe numele fisierului si apoi "Copy link adress/Copiaza adresa url"</li></ul>';
$lang['FileUploadBtn1']         = 'Adauga fisiere ...';
$lang['FileUploadBtn2']         = 'Porneste incarcarea';
$lang['FileUploadBtn3']         = 'Renunta';
$lang['FileUploadBtn4']         = 'Sterge';
$lang['FileUploadBtn5']         = 'Start';



// add_slider.php
$lang['AddSlidePageTitle']      = 'Slideuri <small>imagini prima pagina</small>';
$lang['AddSlidePageSubtitle']   = 'Slideuri prima pagina';
$lang['AddSlideFormTitle']      = 'Adauga slide nou';
$lang['SlideFormAltText']       = 'Slide text alternativ (SEO)';
$lang['SlideFormAltTextHintTitle'] = 'Apare ascuns in pagina si este folosit la optimizarea imaginii';
$lang['SlideFormAltTextHintText']  = 'Text alternativ imagine(SEO)';
$lang['SlideFormSlideText']       = 'Titlu slide';
$lang['SlideFormSlideTextHintTitle'] = 'Apare peste imagine daca este prezent.';
$lang['SlideFormSlideTextHintText']  = 'Apare ca titlu';
$lang['SlideFormActive']           = 'Vizibilitate slide';
$lang['SlideFormActiveOpt1']       = 'da';
$lang['SlideFormActiveOpt2']       = 'nu';
$lang['SlideFormActiveHint']       = 'Este aceasta imagine vizibila pe site?';
$lang['SlideFormOrder']            = 'Ordine slide';
$lang['SlideFormOrderHintTitle']   = 'Valoare numerica';
$lang['SlideFormOrderHintText']    = 'Pozitia in grup';
$lang['SlideFormLink']             = 'Slide link';
$lang['SlideFormLinkHintText']     = 'Optional. Daca este prezent la click va redirectiona catre pagina dorita';
$lang['SlideFormLinkHintTitle']    = 'Link peste imagine';
$lang['SlideFormNotes']            = 'Observatii';
$lang['SlideFormNotesHintText']    = 'Optional - doar pentru administratori (250 char max)';
$lang['SlideFormNotesHintTitle']   = 'Mentiuni';
$lang['SlideFormImage']            = 'Imagine';
$lang['SlideFormImageNotes']       = '<span class="label label-important">NOTE!</span><span> Dupa ce datele de mai sus sunt inregistrate sistemul va redirectiona catre zona de prelucrare imagine. Dimensiuni recomandate 1920x1080px</span>';
$lang['SlideFormAddSlideBtn']      = 'Adauga slide';
$lang['SlideFormResetSlideBtn']    = 'Reseteaza datele';
$lang['EditSlidePageTitle']        = 'Editeaza detaliile slideului <small> imagine si detalii</small>';
$lang['EditSlidePageSubTitle']     = 'Actualizeaza slide';
$lang['SlideFormEditSlideBtn']     = 'Editeaza detalii slide';
$lang['SlideFormEditResetSlideBtn']  = 'Reseteaza date slideului';
$lang['EditSlideImageName']         = 'Imagine slide';
$lang['EditSlideWarning']           = 'Nu exista o imagine incarcata';
$lang['EditSlideImageAction']       = 'Actiuni pe imagine';
$lang['EditSlideImageHint']         = 'Editeaza acesta imagine';


// all_slides.php
$lang['AllSlidesPageTitle']      = 'Selecteaza imagine slide <small> detalii si imagine</small>';
$lang['AllSlidesPageSubTitle']   = 'Selecteaza slide';
$lang['AllSlidesFormTitle']      = 'Toate imaginile slide-ului';
$lang['AllSlidesTable1']         = 'ID';
$lang['AllSlidesTable2']         = 'Detalii slide';
$lang['AllSlidesTable3']         = 'Vizibilitate';
$lang['AllSlidesTable4']         = 'Ordine';
$lang['AllSlidesTable44']        = 'Actiune';
$lang['AllSlidesTable5']         = 'in group';
$lang['AllSlidesTable6']         = 'Editeaza acest slide';
$lang['AllSlidesTable7']         = 'Esti sigur ca vrei sa stergi aceasta imagine?  Se poate marca cu vizibilitate NU.';


//add_gallery.php
$lang['AllCalendarPageTitle']      = 'Detalii galerii <small> date si contact</small>';
$lang['AllCalendarPageSubTitle']   = 'Galerii site';
$lang['AllCalendarFormTitle']      = 'Adauga categorie galerii';
$lang['FormCalendarOrder']         = 'Ordine in grup';
$lang['FormCalendarOrderHint']     = 'Valoare numerica. Intrarile vor fi ordonate ascendent';
$lang['FormCalendarOrderHintTitle']= 'Ordine in tabel';
$lang['CalendarFormActive']        = 'Vizibilitate';
$lang['CalendarFormActiveOpt1']    = 'yes';
$lang['CalendarFormActiveOpt2']    = 'no';
$lang['CalendarFormActiveHint']    = 'Este aceasta galerie vizibila pe site?';
$lang['FormCalendarContact']            = 'Telefon contact';
$lang['FormCalendarContactHint']        = 'Telefon si detalii agent.';
$lang['FormCalendarContactHintTitle']   = 'Telefon agent';
$lang['FormCalendarAgent']              = 'Nume galerie';
$lang['FormCalendarAgentHint']          = 'Nume agent zonal.';
$lang['FormCalendarAgentHintTitle']     = 'Nume agent';
$lang['FormCalendarLocatie']            = 'Nume galerie RO';
$lang['FormCalendarLocatieEN']          = 'Nume galerie EN';
$lang['FormCalendarLocatieHint']        = 'Numele acestei galerii asa cum apare ea pe site';
$lang['FormCalendarLocatieHintTitle']   = 'Nume scurt galerie';
$lang['FormCalendarDateProduse']            = 'Date produse';
$lang['FormCalendarDateProduseHint']        = 'Data singulara sau interval.';
$lang['FormCalendarDateProduseHintTitle']   = 'Data/Date';
$lang['FormCalendarDatePlati']              = 'Date plati';
$lang['FormCalendarDatePlatiHint']          = 'Data singulara sau interval.';
$lang['FormCalendarDatePlatiHintTitle']     = 'Data/Date';
$lang['FormAddCalendarBtn']                 = 'Adauga intrare';
$lang['FormEditCalendarBtn']                = 'Editare intrare';
$lang['FormResetCalendarBtn']               = 'Reseteaza datele';
$lang['EditAllCalendarPageTitle']           = 'Galerie site <small> date si contact</small>';
$lang['EditAllCalendarPageSubtitle']        = 'Editeaza intrare';
$lang['EditCalendarPageSubTitle']           = 'Modifica detalii intrare';


//calendar.php
$lang['AllCalendarsPageTitle']          = 'Detalii galerii <small> date si contact</small>';
$lang['AllCalendarsPageSubTitle']       = 'Toate galeriile din site';
$lang['AllCalendarsFormTitle']          = 'Galerii';
$lang['AllCalendarsTable1']             = 'ID';
$lang['AllCalendarsTable2']             = 'Detalii intrare';
$lang['AllCalendarsTable3']             = 'Vizibilitate';
$lang['AllCalendarsTable4']             = 'Ordine';
$lang['AllCalendarsTable44']            = 'Actiune';
$lang['AllCalendarsTable5']             = 'in group';
$lang['AllCalendarsTable6']             = 'Editeaza aceasta intrare';
$lang['AllCalendarsTable7']             = 'Esti sigur ca vrei sa stergi aceasta intrare?  Se poate marca cu vizibilitate NU.';


//all_pages.php
$lang['AllPagesTitle']          = 'Selecteaza pagina <small> detalii continut</small>';
$lang['AllPagesSubTitle']       = 'Selecteaza pagina';
$lang['AllPagesFormTitle']      = 'Toate paginile siteului';
$lang['AllPagesTable1']         = 'ID';
$lang['AllPagesTable2']         = 'Detalii';
$lang['AllPagesTable3']         = 'Status';
$lang['AllPagesTable4']         = 'Date';
$lang['AllPagesTable44']        = 'Actiuni';
$lang['AllPagesTable6']         = 'Actualizeaza pagina';
$lang['AllPagesTable7']         = 'Sterge pagina';
$lang['AllPagesTable8']         = 'Pagina fara imagine principala';
$lang['AllPagesTable5']         = 'Sigur doresti stergerea acestei pagini? Exista si varianta editarii acesteia cu vizibilitate NU';
$lang['AllPagesTable9']         = 'Galerie de imagini';
$lang['AllPagesTable10']        = 'Marcheaza ca realizat';
$lang['AllPagesTable11']        = 'Titlu';
$lang['AllPagesTable12']        = 'Tip';
$lang['AllPagesTable13']        = 'Apare pe site';
$lang['AllPagesTable14']        = 'Ordinea in grup';
$lang['AllPagesTable15']        = 'Creata la';
$lang['AllPagesTable16']        = 'Creata de';
$lang['AllPagesTable17']        = 'nu se aplica';


//add_page.php
$lang['AddPageTitle']          = 'Pagini site <small> imagini si continut</small>';
$lang['AddPageSubTitle']       = 'Adauga detalii pagina';
$lang['AddPageFormTitle']      = 'Detalii pagina';
$lang['EditPageTitle']         = 'Detalii pagina <small>continut si imagini</small>';
$lang['EditPageSubTitle']      = 'Actualizeaza pagina';
$lang['EditPageFormTitle']     = 'Actualizeaza detalii pagina';
$lang['EditPageFormTitleHint'] = 'Acest titlu va fi afisat in pagina';
$lang['EditPageFormActive']        = 'Vizibilitate pagina';
$lang['EditPageFormActiveOpt1']    = 'da';
$lang['EditPageFormActiveOpt2']    = 'nu';
$lang['EditPageFormActiveHint']    = 'Aceasta proprietate va determina daca pagina apare sau nu pe site';
$lang['EditPageFormApproved']        = 'Tip galerie';
$lang['EditPageFormApprovedOpt1']    = 'foto';
$lang['EditPageFormApprovedOpt2']    = 'video';
$lang['EditPageFormApprovedHint']    = 'Aceasta proprietate va determina tipul de galerie';
$lang['EditPageFormOrder']           = 'Ordine';
$lang['EditPageFormOrderHintTitle']  = 'Ordine pagina in grup';
$lang['EditPageFormOrderHintText']   = 'Valoare numerica. Se va afisa de la ascendent la descendent';
$lang['EditPageFormMetaTitle']           = 'Meta titlu pagina';
$lang['EditPageFormMetaTitleHintTitle']  = 'Folosit la SEO';
$lang['EditPageFormMetaTitleHintText']   = 'Acest atribut va fi folosit pentru optimizare pagina. (max 250 char). Apare ascuns in pagina. Recomandat.';
$lang['EditPageFormMetaDesc']           = 'Meta descriere pagina';
$lang['EditPageFormMetaDescHintTitle']  = 'Folosit la SEO';
$lang['EditPageFormMetaDescHintText']   = 'Acest atribut va fi folosit pentru optimizare pagina. (max 250 char). Apare ascuns in pagina. Recomandat.';
$lang['EditPageFormContent']            = 'Continut pagina';
$lang['EditPageFormImage']              = 'Imagine principala';
$lang['EditPageFormNotes']           = 'Observatii';
$lang['EditPageFormNotesHintTitle']  = 'Note';
$lang['EditPageFormNotesHintText']   = 'Doar pentru administratori. (max 256 char).';
$lang['EditPageFormCustom']          = 'Continut dedicat pagina';
$lang['EditPageFormCustomHintTitle'] = 'Atribut custom';
$lang['EditPageFormCustomHintText']  = 'Continut dedicat pagina. Daca este gol nu se afiseaza.';
$lang['EditPageFormBtnEdit']         = 'Editeaza continut pagina';
$lang['EditPageFormBtnReset']        = 'Reseteaza pagina';
$lang['EditPageFormBtnAdd']          = 'Editeaza continut pagina';
$lang['EditPageFormImageActions']    = 'Actiuni pe imagine';
$lang['EditPageFormImageAction1']    = 'Editeaza imagine';
$lang['EditPageFormImageAction2']    = 'Schimba imagine principala';
$lang['EditPageFormImageAction3']    = 'Sterge aceasta imagine';
$lang['EditPageFormImageAction4']    = 'Sigur doresti stergerea acestei imagini?';
$lang['EditPageFormImageAction5']    = 'Sterge imagine';
$lang['EditPageFormImageAction6']    = 'Mergi la galerie imagini';
$lang['EditPageFormImageNote']       = '<span class="label label-important">NOTA!</span><span> Acest atribut este optional. Dupa ce pagina va fi creata sistemul va directiona la functionalitatea de "taie imagine". </span>';
$lang['EditPageFormImageAction7']    = 'Galeria imagini';
$lang['EditPageFormImageAction8']    = '<span class="label label-important">NOTA!</span><span> Acest atribut este optional. Dupa ce pagina va fi creata galeria de imagini va fi deasemenea creata. Se pot incarca imagini in galerie folosinf "Drag&Drop".  </span>';


// afisare change_details
$lang['DisplayPageTitle']           = 'Schimba parametri imagine';
$lang['DisplayPageFormAlt']         = 'Text alternativ';
$lang['DisplayPageFormAltHint']     = 'Acest atribut va fi folosit la indexare iamgine.';
$lang['DisplayPageFormVisibility']  = 'Vizibilitate imagine';
$lang['DisplayPageFormVisiOpt1']    = 'da';
$lang['DisplayPageFormVisiOpt2']    = 'nu';
$lang['DisplayPageFormVisiHint']    = 'Imaginea nu va apare daca este selectat "nu"';
$lang['DisplayPageFormConvertImg']  = 'Marckeaza imagine ca principala';
$lang['DisplayPageFormConvertOpt1'] = 'nu';
$lang['DisplayPageFormConvertOpt2'] = 'da';
$lang['DisplayPageFormConvertHint'] = 'Acest atribut va determian daca aceasta imagine va fi "imagine centrala articol"';
$lang['DisplayPageFormGal']         = 'Categorie imagine';
$lang['DisplayPageFormGalHint']     = 'Daca este prezenta aceata categorie va devenii filtru in pagina de galerie foto.';
$lang['DisplayPageEditDetails']     = 'Actualizeaza detalii';
$lang['DisplayPageFormResetBtn']    = 'Reseteaza';