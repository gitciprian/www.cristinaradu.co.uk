<?php

function addentities($data){
	if(trim($data) != ''){
	$data = htmlentities($data, ENT_QUOTES);
	return str_replace('\\', '&#92;', $data);
	} else return $data;
}

function sanitazeStringForUrl($string)
{
	$str = strtolower(trim($string));
	$str = preg_replace('@[^a-zA-Z0-9]@', ' ', $str);
	$str = preg_replace('/(\s\s+|\-\-+)/', ' ', $str);
	return str_replace(' ','-',$str);
}

function Datasiora($Date){$_Date = "";
	$_Point = (int) date("m",strtotime($Date)) + 1 - 1;
	$AMonths = array ("January","February","March","April","May","June","July","August","September","October","November","December");
	$_Date .= date("d ",strtotime($Date));
	$_Date .= $AMonths[$_Point-1];
	$_Date .= date(" Y",strtotime($Date));
	$_Date .= date(" H:i",strtotime($Date));
	return $_Date;
}

function Datasc($Date){$_Date = "";
	$_Point = (int) date("m",strtotime($Date)) + 1 - 1;
	$AMonths = array ("January","February","March","April","May","June","July","August","September","October","November","December");
	$_Date .= date("d ",strtotime($Date));
	$_Date .= $AMonths[$_Point-1];
	$_Date .= date(" Y",strtotime($Date));
	//$_Date .= date(" H:i",strtotime($Date));
return $_Date;
}

function htmle ($val){
	return  preg_replace(array('/</', '/>/', '/"/', "/'/"), array('&lt;', '&gt;', '&quot;', '&#039;'), $val);
}

function page_type($val,$lb){

	if($lb == 'en'){
		switch ($val){
			case 1:
				$page_type  = "Web core page";
				break;
			case 2:
				$page_type  = "Web secondary page";
				break;
			case 3:
				$page_type  = "Service page";
				break;
			case 4:
				$page_type  = "Gallery";
				break;
		}
	}

	if($lb == 'ro'){
		switch ($val){
			case 1:
				$page_type  = "Pagina centrala";
				break;
			case 2:
				$page_type  = "Pagina secundara";
				break;
			case 3:
				$page_type  = "Pagina de activitati";
				break;
			case 4:
				$page_type  = "Galerie";
				break;
		}
	}

	return $page_type;
}

function galleryType($num){
	if(isset($num) && $num > 0){
		switch ($num){
			case 1:
				$type   = 'foto';
				break;
			case 2:
				$type   = 'video';
				break;
		}
		return $type;
	}else{
		return false;
	}
}
function galleryWatermark($num){
	if(isset($num)){
		switch ($num){
			case 0:
				$type   = 'fara watermark';
				break;
			case 1:
				$type   = 'alb';
				break;
			case 2:
				$type   = 'negru';
				break;
		}
		return $type;
	}else{
		return false;
	}
}

function zecimalenumar($number){
	$english_format_number = number_format($number, 2, '.', '');
	return $english_format_number;
}

function luni_in_romana($luna){
	$luni_engleza = array("January","February","March","April","May","June","July","August","September","October","November","December");
	$luni_romana = array("Ianuarie","Februarie","Martie","Aprilie","Mai","Iunie","Iulie","August","Septembrie","Octombrie","Noiembrie","Decembrie");
	$text = str_replace($luni_romana, $luni_engleza, $luna);
	$data_bread=date("j")." ".$text.", ".date("Y, g:i a");
	return $data_bread;
}

function smart_crop ($subject, $length = 256, $fill_string = '') {
	$subject        = trim($subject);
	$length         = intval($length);
	$fill_string    = trim($fill_string);
	if (strlen($subject) > $length) {
		$space_pos = strpos($subject, ' ', ($length - 3 * strlen($fill_string)));
	if ($space_pos > $length) {
		$space_pos  = $length - strlen($fill_string);
	}
	$subject        = substr($subject, 0, $space_pos) . $fill_string;
	}
	return $subject;
	// echo smart_crop ($rowbox['continut'], $length = 900, $fill_string = '...</p>') ;
}


// Encrypt Function
function mc_encrypt($encrypt){
	$encrypt = serialize($encrypt);
	$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
	$key = pack('H*', 'a8bfaad90db51f2480526735b631ae3ba883adaae6f088e5263714d99e16474f');
	$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
	$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
	$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
	return $encoded;
}

// Decrypt Function
function mc_decrypt($decrypt){
	$decrypt = explode('|', $decrypt.'|');
	$decoded = base64_decode($decrypt[0]);
	$iv = base64_decode($decrypt[1]);
	if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
	$key = pack('H*', 'a8bfaad90db51f2480526735b631ae3ba883adaae6f088e5263714d99e16474f');
	$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
	$mac = substr($decrypted, -64);
	$decrypted = substr($decrypted, 0, -64);
	$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
	if($calcmac!==$mac){ return false; }
	$decrypted = unserialize($decrypted);
	return $decrypted;
}

function read_key($var){
	return ucfirst(str_replace("_"," ",$var));
}

/*
 * Crop and center image based on what side fits best (only jpg)
 * @img_location is the path to the image ex: images/pages/image.jpg
 * @$img_name is the name of the image that you want to crop
 * @new_image_name iti will be assigned by default @img_name+'-main'
 * width an height are the new iamge sizes
 * example  smartImageCrop("public/images/gallery","koala-8364550.jpg","500","350");
 */

function smartImageCrop($img_location, $img_name, $image_new_width = 960,$image_new_height = 720){

   $existing_imge_path = $_SERVER['DOCUMENT_ROOT']."/".$img_location."/".$img_name;

   if(file_exists($existing_imge_path)){

       $image           = imagecreatefromjpeg($existing_imge_path);
	   $thumb_width     = $image_new_width;
	   $thumb_height    = $image_new_height;
	   $width           = imagesx($image);
	   $height          = imagesy($image);
	   $original_aspect = $width / $height;
	   $thumb_aspect    = $thumb_width / $thumb_height;

	   if ($original_aspect >= $thumb_aspect) {
		   // If image is wider than thumbnail (in aspect ratio sense)
		   $new_height  = $thumb_height;
		   $new_width   = $width / ($height / $thumb_height);

	   } else {
		   // If the thumbnail is wider than the image
		   $new_width   = $thumb_width;
		   $new_height  = $height / ($width / $thumb_width);
	   }

	   $thumb           = imagecreatetruecolor($thumb_width, $thumb_height);

	   // Resize and crop
	   imagecopyresampled($thumb,
		   $image,
		   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
		   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
		   0, 0,
		   $new_width, $new_height,
		   $width, $height);

	   $new_image_name  = str_replace(".jpg","-main.jpg",$img_name);
	   $new_imge_path   = $_SERVER['DOCUMENT_ROOT']."/".$img_location."/".$new_image_name;

	   if(imagejpeg($thumb, $new_imge_path, 80)){
		   return $new_image_name;
	   }else{
		   return "error";
	   }


   }else{
   	return "Image missing!";
   }

}

