<?php

/**
 * A simple PHP MVC skeleton
 *
 * @package php-mvc
 * @author Ciprian *
 * @license http://opensource.org/licenses/MIT MIT License
 */

// load the (optional) Composer auto-loader
if (file_exists('vendor/autoload.php')) {
    require 'vendor/autoload.php';
}

// load application config (error reporting etc.)
require 'application/config/config.php';

// load application class
require 'application/libs/application.php';
require 'application/libs/controller.php';

// load extras
require 'application/libs/Debug.php';
require 'application/libs/ViewHelper.php';
require 'application/libs/Request.php';

// start the application
$app = new Application();
