<?php

class MyaccountModel
{
	/**
	 * Every model needs a database connection, passed to the model
	 * @param object $db A PDO database connection
	 */
	function __construct($db) {
		try {
			$this->db = $db;
		} catch (PDOException $e) {
			exit('Database connection could not be established.');
		}
	}

	/**
	 * Get all details from database
	 */
	public function getAllDetails($partner_id)
	{
		if($partner_id){
			$sql = "SELECT * FROM u_login WHERE id = '$partner_id' ";
			$query = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}else{
			return 'partner id required';
		}

	}

	/**
	 * Get all partner messages from database
	 */
	public function getAllMessages($partner_id)
	{
		if($partner_id){
			$search_for = '"'.$partner_id.'"';
			$sql        = "SELECT * FROM messages WHERE users LIKE '%$search_for%'  ";
			$query      = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}else{
			return 'partner id required';
		}

	}

	/**
	 * Get individual message from database
	 */
	public function getSingleMessage($message_id)
	{
		if($message_id){
			$sql        = "SELECT * FROM messages WHERE id ='$message_id'  ";
			$query      = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}else{
			return 'message id required';
		}

	}

	/**
	 * Get  message attachments
	 */
	public function getMessageAttchments($message_id)
	{
		if($message_id){
			$sql        = "	SELECT * FROM gallery 
							WHERE project_id ='$message_id' 
							AND image !='' 
							AND img_active ='yes' 
							AND folder ='attach' 
						";
			$query      = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}else{
			return 'message id required';
		}

	}


	public function updatePartnerDetails($post)
	{
		foreach ($post as $k => $v){
			$$k = trim(strip_tags($v));
		}

		if ($user_password == "") {
			$parola = $_POST['old_pass'];
		} else {
			$parola = ViewHelper::mc_encrypt($user_password);
		}

		try {
			$sql = "UPDATE u_login SET 
						user_name 		= :user_name,  
						company_name	= :company_name,
						user_email		= :user_email,
						secondary_email	= :secondary_email,
						user_phone		= :user_phone,
						user_country	= :user_country,
						user_address	= :user_address,
						user_password	= :user_password
					WHERE id = '$id'";
			$query = $this->db->prepare($sql);
			$query->execute(
				array(
					':user_name'        => $user_name,
	                ':company_name'     => $company_name,
	                ':user_email'       => $user_email,
					':secondary_email'  => $secondary_email,
					':user_phone'       => $user_phone,
					':user_country'     => $user_country,
					':user_address'     => $user_address,
					':user_password'    => $parola
				)
			);

			if($query->rowCount() > 0){
				return true;
			}
		}
			catch(PDOException $e)
		{
			return $sql . "<br>" . $e->getMessage();
		}

	}

	public function updateMessageStatus($message_id)
	{

		if($message_id) {
			try {
				$sql   = "	UPDATE messages SET 
								opened 		= :opened,  
								opened_at	= :opened_at
							WHERE id = '$message_id'
						";
				$query = $this->db->prepare( $sql );
				$query->execute(
					array(
						':opened'       => 'yes',
						':opened_at'    => date("Y-m-d H:i:s")
					)
				);

				if ( $query->rowCount() > 0 ) {
					return true;
				}
			} catch ( PDOException $e ) {
				return $sql . "<br>" . $e->getMessage();
			}
		}


	}



	/*
	 * Count all the projects that belong to the partner
	 */
	public function getAmountOfProjects($partner_id, $active = false, $approved = false)
	{
		$sql = "SELECT COUNT(id) AS amount_of_projects FROM pages
				WHERE partner_id = '$partner_id'
				AND page_type = '4' ";
		if($active === true){
			$sql .= " AND page_active ='yes' ";
		}
		if($approved === true){
			$sql .= " AND page_approved ='no' ";
		}
		$query = $this->db->prepare($sql);
		$query->execute();

		// fetchAll() is the PDO method that gets all result rows
		return $query->fetch()->amount_of_projects;
	}

	/**
	 * Get  partner projects
	 */
	public function getPartnerProjects($partner_id,$approved = false,$pending = false)
	{
		if($partner_id){
			$sql        = "	SELECT * FROM pages 
							WHERE partner_id = '$partner_id'
							AND page_type = '4'							
						";
			if($approved === true){
				$sql .= " AND page_approved ='yes' ";
			}
			if($pending === true){
				$sql .= " AND page_approved ='no' ";
			}
			$sql .= " ORDER BY id DESC ";
			$query      = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}else{
			return 'partner id required';
		}

	}

	/**
	 * Get  individual project by id
	 */
	public function getProjectById($partner_id,$project_id)
	{
		if($partner_id){
			if($project_id) {
				$sql = "	SELECT * FROM pages 
							WHERE partner_id = '$partner_id'
							AND id = '$project_id'							
						";
				$query = $this->db->prepare( $sql );
				$query->execute();

				return $query->fetchAll();
			}else{
				return 'project id required';
			}
		}else{
			return 'partner id required';
		}

	}


	/*
	 * Create new project
	 * Params are send a post array
	 * notes has to be combined from job location, date, client
	 * images will be added after
	 */

	public function addNewProject($params)
	{

		if(count($params) > 0){
			try {

				$date               = date('Y-m-d H:i:s');
				$partner_id         = strip_tags($_POST['partner_id']);
				$page_title         = strip_tags($_POST['page_title']);
				$project_location   = strip_tags($_POST['project_location']);
				$project_date       = strip_tags($_POST['project_date']);
				$client_name        = strip_tags($_POST['client_name']);
				$created_by         = strip_tags($_POST['created_by']);
				$page_content       = $_POST['content'];
				$notes              = 'From '.$created_by.'; Location '.$project_location.'; Date: '.$project_date.'; Client name (optional): '.$client_name;


				$sql = "INSERT INTO pages (partner_id, page_type, page_meta_title, page_meta_description, page_title, page_content, created_at, created_by, page_extras, notes) VALUES (:partner_id, :page_type, :page_meta_title, :page_meta_description, :page_title, :page_content, :created_at, :created_by, :page_extras, :notes)";
				$query = $this->db->prepare($sql);
				$query->execute(array(
					':partner_id' => $partner_id,
					':page_type' => '4',
					':page_meta_title' => $page_title,
					':page_meta_description' => $page_title,
					':page_title' => $page_title,
					':page_content' => $page_content,
					':created_at' => $date,
					':created_by' => $created_by,
					':page_extras' => '{"short_description":""}',
					':notes' => $notes
				));

				if ( $query->rowCount() > 0 ) {

					$project_id = $this->db->lastInsertId();
					return $project_id;
				}
			} catch ( PDOException $e ) {
				return $sql . "<br>" . $e->getMessage();
			}


		}else{
			return false;
		}

	}


/*
	public function deleteSong($song_id)
	{
		$sql = "DELETE FROM song WHERE id = :song_id";
		$query = $this->db->prepare($sql);
		$query->execute(array(':song_id' => $song_id));
	}
*/


}
