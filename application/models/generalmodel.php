<?php

class GeneralModel
{

	function __construct($db) {
		try {
			$this->db = $db;
		} catch (PDOException $e) {
			exit('Database connection could not be established.');
		}
	}

	/**
	 * Get all variables from database
	 */
	public function getAllVariables()
	{
		$sql    = "SELECT * FROM general_info WHERE id = 1";
		$query  = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	/**
	 * Get all index slides
	 */
	public function getHomeSlides()
	{
		$sql    = "	SELECT * FROM slider 
					WHERE active_slide = 'yes'
					AND image !=''
					ORDER BY slide_order ASC
				  ";
		$query  = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function getPageContentById($id,$preview_on = false)
	{
		if($id){
			$sql    = "	SELECT * FROM pages WHERE id = '$id' ";
			if(!$preview_on) {
				$sql .= "  AND page_active ='yes' ";
			}
			$sql  .= " ORDER BY id ASC ";
			$query  = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}else{
			return false;
		}

	}

	public function getAllPageType($type, $limit = false, $subtype = false)
	{
		if($type){

			$sql    = "	SELECT * FROM pages 
				WHERE page_type = '$type'
				AND page_active ='yes'";
			if($subtype){
				$sql    .= " AND page_approved ='$subtype' ";
			}
			$sql    .= " ORDER BY page_order ASC ";
			if($limit){
				$sql    .= " LIMIT 0,$limit ";
			}
			$query  = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		}else{
			return false;
		}


	}

	// returns the entries from the calendar
	public function getAllCalendarEntryes()	{

			$sql    = "	SELECT * FROM calendar WHERE vizibilitate = 'yes' ORDER BY ordine ASC ";
			$query  = $this->db->prepare($sql);
			$query->execute();
			return $query->fetchAll();
	}

	// returns the entries from the calendar
	public function getPopularGalleryCategories($type = 1, $limit = false)	{

		$sql    = "	SELECT * FROM gallery_details 
					WHERE gallery_active = 'yes' 
					AND (gallery_name !='' OR gallery_name_en !='')
					AND gallery_type = '$type'					
					ORDER BY gallery_order ASC ";
		if($limit){
			$sql    .= " LIMIT 0,$limit ";
		}
		$query  = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function getAllActivePartners()
	{

		$sql    = "	SELECT * FROM partners 	WHERE part_active ='yes' ORDER BY part_order ASC ";
		$query  = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function getProjectImageGallery($project_id,$preview_on = false)
	{

		$sql    = "	SELECT * FROM gallery 	
					WHERE project_id ='$project_id' ";
		if(!$preview_on)	{
			$sql .=  "AND img_active = 'yes' ";
		}
		$sql    .=  "AND folder = 'image'
					AND image != ''
					ORDER BY img_order ASC ";
		$query  = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function getAllReviews($group = true, $offset=0, $amount=10)
	{
		if($offset == 0 || $offset == 1){
			$offset = 0;
		}else{
			$offset = ($offset - 1) * $amount;
		}

		if($group) {
			$sql = "SELECT * FROM s_reviews WHERE review_title !='' ORDER BY id ASC ";
		}else{
			$sql = "SELECT * FROM s_reviews WHERE review_title !='' ORDER BY id ASC LIMIT $offset, $amount  ";
		}
		$query  = $this->db->prepare($sql);
		$query->execute();
		$arr = $query->fetchAll();

		if(is_array($arr)){
			return $arr;
		}else{
			return 'not array';
		}
	}

	public function getAllProjects($group = true, $offset=0, $amount=6)
	{
		if($offset == 0 || $offset == 1){
			$offset = 0;
		}else{
			$offset = ($offset - 1) * $amount;
		}

		if($group) {
			$sql = "SELECT * FROM pages 
					WHERE page_type = '4' 
					AND page_active = 'yes'
					AND page_approved = 'yes'
					ORDER BY partner_id, page_order ASC ";
		}else{
			$sql = "SELECT * FROM pages 
					WHERE page_type = '4' 
					AND page_active = 'yes'
					AND page_approved = 'yes'
					ORDER BY partner_id, page_order ASC 
					LIMIT $offset, $amount  ";
		}
		$query  = $this->db->prepare($sql);
		$query->execute();
		$arr = $query->fetchAll();

		if(is_array($arr)){
			return $arr;
		}else{
			return 'not array';
		}
	}

	public function getImageDetailsByImageId($img_id)
	{

		$sql = "SELECT gallery.id AS img_id, gallery.image, gallery.image_details, gallery.image_details_en, gallery.created_at, pages.id AS page_id, pages.page_title, pages.page_title_en,	gallery_details.gallery_name, gallery_details.gallery_name_en
 				FROM gallery
				LEFT JOIN pages
				ON gallery.project_id = pages.id 	
				LEFT JOIN gallery_details
				ON 	pages.partner_id = gallery_details.id		
				WHERE gallery.id = '$img_id' 
				";
		$query  = $this->db->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function getRandomProjects($exclude_id=0,$number=4)
	{

		$sql = "SELECT id, main_image, page_title FROM pages 
				WHERE page_type = '4' 
				AND page_active = 'yes'
				AND page_approved = 'yes'";
		$sql .= " AND id !='$exclude_id' ";
		$sql .= " ORDER BY rand() ";
		$sql .= " LIMIT 0,$number ";
		$query  = $this->db->prepare($sql);
		$query->execute();
		$arr = $query->fetchAll();

		if(is_array($arr)){
			return $arr;
		}else{
			return 'not array';
		}
	}

	// special fucntions
	public function getAllGalleryCategories($lb)
	{
		$sql    = "SELECT id, gallery_name FROM gallery_details";

		if($lb == 'en')
		{
			$sql    = "SELECT id, gallery_name_en AS gallery_name FROM gallery_details";
		}
		$query  = $this->db->prepare($sql);
		$query->execute();
		$arr['categori']    = $query->fetchAll();
		return json_encode($arr);
	}

	// create a session valid with the gallery
	public function validateGallerySession($gal_pass,$post_pass,$project_id)
	{
		$url_return = $_SERVER["REQUEST_URI"];

		if($gal_pass == $post_pass) {
			$_SESSION[ 'project_' . $project_id ] = $project_id;
			header( "Location: $url_return " );
			exit;
		}else{
			header( "Location: $url_return/error " );
			exit;
		}

	}

}
