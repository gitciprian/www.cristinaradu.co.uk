<?php
/*
-----------------
Limba: Engleza
Autor: Visan Ciprian
Telefon: +44 0776.998.0346
Email: ciprian@cipdesign.ro
Site: www.cipdesign.ro
-----------------
*/

/******************************************** datele de mai jos se pot traduce ************************************************/

// generale
$lang['MoreDetails']    = '... read more';
$lang['AllGalleries']   = 'All';
$lang['Photo']          = 'Photo';
$lang['Video']          = 'Video';

// top meniu
$lang['Home']           = 'Home';
$lang['AboutMe']        = 'About Me';
$lang['Galleries']      = 'Galleries';
$lang['PhotoGallery']   = 'Photo Gallery';
$lang['VideoGallery']   = 'Video Gallery';
$lang['Contact']        = 'Contact';

// contact form
$lang['Nume']           = 'Name';
$lang['Email']          = 'Email';
$lang['Telefon']        = 'Phone';
$lang['Subiect']        = 'Subject';
$lang['Mesaj']          = 'Your Message';
$lang['Cod']            = 'Antispam code';
$lang['Trimite']        = 'Send Message';
$lang['codfals']        = 'Wrong antispam code';
$lang['SuccessMsg']     = 'Thank You. Your Message has been Submitted';



