<?php
/*
-----------------
Limba: Romana
Autor: Visan Ciprian
Telefon: +44 0776.998.0346
Email: ciprian@cipdesign.ro
Site: www.cipdesign.ro
-----------------
*/

/******************************************** datele de mai jos se pot traduce ************************************************/

// generale
$lang['MoreDetails']    = '... mai multe';
$lang['AllGalleries']   = 'Toate';
$lang['Photo']          = 'Foto';
$lang['Video']          = 'Video';

// top meniu
$lang['Home']           = 'Home';
$lang['AboutMe']        = 'Cine sunt';
$lang['Galleries']      = 'Galerii';
$lang['PhotoGallery']   = 'Galerie foto';
$lang['VideoGallery']   = 'Galerie video';
$lang['Contact']        = 'Contact';

// formular contact
$lang['Nume']           = 'Name';
$lang['Email']          = 'Email';
$lang['Telefon']        = 'Phone';
$lang['Subiect']        = 'Subject';
$lang['Mesaj']          = 'Message';
$lang['Cod']            = 'Antispam code';
$lang['Trimite']        = 'Submit';
$lang['codfals']        = 'Wrong antispam code';
$lang['SuccessMsg']     = 'Thank You. Your Message has been Submitted';



