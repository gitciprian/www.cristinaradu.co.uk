    <section class="inner-header divider layer-overlay overlay-dark-5"  data-bg-img="<?php echo IMAGE_PATH;?>about_bg.jpg?v=13">
        <div class="container">
            <div class="section-content pt-50 pb-50">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title text-white">Despre mine</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section: About -->
    <section id="about">
        <div class="container pb-0">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="text-uppercase font-28 letter-space-3 mt-0">Despre mine si <span class="text-theme-colored">programele</span> propuse.</h3>
                        <h5 class="text-uppercase font-weight-400">Aici scurt subtitlu sau ideea centrala a acestei pagini.<br/> Recomand un rand maxim.</h5>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut mole stie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermen. Ut mole stie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermen. </p>
                            </div>
                            <div class="col-md-6">
                                <p class="mb-sm-30">Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut mole stie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermen. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumb">
                            <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>despre_mine.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="row mt-40">
                    <div class="col-md-4">
                        <div class="icon-box p-0">
                            <a href="#" class="icon-border-effect effect-circled  icon icon-circled mb-0 mr-0 pull-left flip icon-lg">
                                <i class="flaticon-yoga-woman-flexing-body text-theme-colored font-54"></i>

                            </a>
                            <div class="ml-120">
                                <h5 class="icon-box-title mt-15 mb-10 text-uppercase">Optimism</h5>
                                <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-box p-0">
                            <a href="#" class="icon-border-effect effect-circled  icon icon-circled mb-0 mr-0 pull-left flip icon-lg">
                                <i class="flaticon-spa-tea-cup-with-flower text-theme-colored font-54"></i>
                            </a>
                            <div class="ml-120">
                                <h5 class="icon-box-title mt-15 mb-10 text-uppercase">Initiativa</h5>
                                <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-box p-0">
                            <a href="#" class="icon-border-effect effect-circled  icon icon-circled mb-0 mr-0 pull-left flip icon-lg">
                                <i class="flaticon-boxing-two text-theme-colored font-54"></i>
                            </a>
                            <div class="ml-120">
                                <h5 class="icon-box-title mt-15 mb-10 text-uppercase">Tenacitate</h5>
                                <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section: Divider -->
    <section>
        <div class="container pt-20">
            <div class="section-content text-center pt-30 pb-30 bg-orange">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="text-white text-left sm-text-center font-weight-400 pl-15 pl-sm-0 mt-0 mb-0 mb-sm-20">Pentru detalii suplimentare sau discutie libera...</h3>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="btn btn-theme-colored btn-flat text-uppercase">Te contactez eu</a>
                        <a href="#" class="btn btn-theme-colored btn-flat text-uppercase ml-20">Contacteaza-ma tu</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
