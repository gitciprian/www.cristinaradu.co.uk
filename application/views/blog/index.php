<div class="main-content">
	<section class="inner-header divider layer-overlay overlay-dark-5"  data-bg-img="<?php echo IMAGE_PATH;?>blog_bg.jpg">
		<div class="container">
			<div class="section-content pt-50 pb-50">
				<div class="row">
					<div class="col-md-12">
						<h3 class="title text-white">Articole, stiri, noutati</h3>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container mt-30 mb-30 pt-30 pb-30">
			<div class="row ">
				<div class="col-md-10 col-md-offset-1">
					<div class="blog-posts">
						<div class="col-md-12">
							<div class="row list-dashed">

								<article class="post mb-50 pb-30">
									<div class="entry-header">
										<div class="post-thumb">
											<img alt="" src="<?php echo IMAGE_PATH;?>blog_1.jpg" class="img-fullwidth img-responsive">
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="entry-content">
										<h5 class="entry-title text-uppercase mt-0"><a href="#">Aici titlu articol sau post</a></h5>
										<ul class="list-inline font-12 mb-20 mt-10">
											<li><i class="fa fa-calendar mr-5"></i> 8 Decembrie 2017 </li>
											<li><i class="fa fa-user ml-5 mr-5"></i> Lorenne</li>
										</ul>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, temporibus dignissimos. Explicabo minus ad similique. Corporis delectus, blanditiis hic maxime est fugit modi ad sapiente esse inventore dicta, soluta impedit a reiciendis consectetur <a href="#">[...]</a></p>
										<a class="btn btn-dark btn-theme-colored pull-right flip font-16" href="#"><i class="fa fa-angle-double-right"></i> Vezi detalii</a>
										<div class="clearfix"></div>
									</div>
								</article>

								<article class="post mb-50 pb-30">
									<div class="entry-header">
										<div class="post-thumb">
											<img alt="" src="<?php echo IMAGE_PATH;?>blog_2.jpg" class="img-fullwidth img-responsive">
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="entry-content">
										<h5 class="entry-title text-uppercase mt-0"><a href="#">Aici titlu articol sau post</a></h5>
										<ul class="list-inline font-12 mb-20 mt-10">
											<li><i class="fa fa-calendar mr-5"></i> 8 Decembrie 2017 </li>
											<li><i class="fa fa-user ml-5 mr-5"></i> Lorenne</li>
										</ul>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, temporibus dignissimos. Explicabo minus ad similique. Corporis delectus, blanditiis hic maxime est fugit modi ad sapiente esse inventore dicta, soluta impedit a reiciendis consectetur <a href="#">[...]</a></p>
										<a class="btn btn-dark btn-theme-colored pull-right flip font-16" href="#"><i class="fa fa-angle-double-right"></i> Vezi detalii</a>
										<div class="clearfix"></div>
									</div>
								</article>

								<article class="post clearfix mb-50 pb-30">
									<div class="entry-header">
										<div class="post-thumb">
											<img alt="" src="<?php echo IMAGE_PATH;?>blog_3.jpg" class="img-fullwidth img-responsive">
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="entry-content">
										<h5 class="entry-title text-uppercase mt-0"><a href="#">Aici titlu articol sau post</a></h5>
										<ul class="list-inline font-12 mb-20 mt-10">
											<li><i class="fa fa-calendar mr-5"></i> 8 Decembrie 2017 </li>
											<li><i class="fa fa-user ml-5 mr-5"></i> Lorenne</li>
										</ul>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, temporibus dignissimos. Explicabo minus ad similique. Corporis delectus, blanditiis hic maxime est fugit modi ad sapiente esse inventore dicta, soluta impedit a reiciendis consectetur <a href="#">[...]</a></p>
										<a class="btn btn-dark btn-theme-colored pull-right flip font-16" href="#"><i class="fa fa-angle-double-right"></i> Vezi detalii</a>
										<div class="clearfix"></div>
									</div>
								</article>

								<article class="post clearfix mb-50 pb-30">
									<div class="entry-header">
										<div class="post-thumb">
											<iframe src="https://player.vimeo.com/video/4696021?title=0&byline=0&portrait=0" width="500" height="281"></iframe>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="entry-content">
										<h5 class="entry-title text-uppercase mt-0"><a href="#">Articol cu postare video din Vime</a></h5>
										<ul class="list-inline font-12 mb-20 mt-10">
											<li><i class="fa fa-calendar mr-5"></i> 8 Decembrie 2017 </li>
											<li><i class="fa fa-user ml-5 mr-5"></i> Lorenne</li>
										</ul>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, temporibus dignissimos. Explicabo minus ad similique. Corporis delectus, blanditiis hic maxime est fugit modi ad sapiente esse inventore dicta, soluta impedit a reiciendis consectetur <a href="#">[...]</a></p>
										<a class="btn btn-dark btn-theme-colored pull-right flip font-16" href="#"><i class="fa fa-angle-double-right"></i> Vezi detalii</a>
										<div class="clearfix"></div>
									</div>
								</article>

								<article class="post clearfix mb-50 pb-30">
									<div class="entry-header">
										<div class="post-thumb">
											<iframe width="600" height="340" src="https://www.youtube.com/embed/iXAbte4QXKs" frameborder="0" allowfullscreen></iframe>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="entry-content">
										<h5 class="entry-title text-uppercase mt-0"><a href="#">Articol cu postare video din youtube</a></h5>
										<ul class="list-inline font-12 mb-20 mt-10">
											<li><i class="fa fa-calendar mr-5"></i> 8 Decembrie 2017 </li>
											<li><i class="fa fa-user ml-5 mr-5"></i> Lorenne</li>
										</ul>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, temporibus dignissimos. Explicabo minus ad similique. Corporis delectus, blanditiis hic maxime est fugit modi ad sapiente esse inventore dicta, soluta impedit a reiciendis consectetur <a href="#">[...]</a></p>
										<a class="btn btn-dark btn-theme-colored pull-right flip font-16" href="#"><i class="fa fa-angle-double-right"></i> Vezi detalii</a>
										<div class="clearfix"></div>
									</div>
								</article>
							</div>
						</div>
						<div class="col-md-12">
							<nav class="text-center">
								<ul class="pagination theme-colored">
									<li> <a aria-label="Previous" href="#"> <span aria-hidden="true">«</span> </a> </li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a href="#">...</a></li>
									<li> <a aria-label="Next" href="#"> <span aria-hidden="true">»</span> </a> </li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
