<div class="main-content">
	<section class="inner-header divider layer-overlay overlay-dark-5"  data-bg-img="<?php echo IMAGE_PATH;?>article_bg.jpg">
		<div class="container">
			<div class="section-content pt-50 pb-50">
				<div class="row">
					<div class="col-md-12">
						<h3 class="title text-white">Titlu articol sau nume postare</h3>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container mt-30 mb-30 pt-30 pb-30">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="blog-posts single-post">
						<article class="post clearfix mb-0">
							<div class="entry-header">
								<div class="post-thumb thumb">
									<img src="<?php echo IMAGE_PATH;?>article_default.jpg" alt="" class="img-responsive img-fullwidth">
								</div>
							</div>
							<div class="entry-content">
								<div class="entry-title pt-0">
									<h4><a href="#">Subtitlu sau scurta descriere articol</a></h4>
								</div>
								<div class="entry-meta mb-20">
									<ul class="list-inline">
										<li>Creat la: <span class="text-theme-colored"> 9/12/20175</span></li>
										<li>De catre: <span class="text-theme-colored">Lorenne</span></li>
									</ul>
								</div>
								<p class="mb-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<p class="mb-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<blockquote class="theme-colored pt-20 pb-20">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
									<footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
								</blockquote>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

								<div class="video-container text-center">
									<iframe src="https://www.youtube.com/embed/iXAbte4QXKs" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen width="560" height="315"></iframe>
								</div>
								<div class="clearfix pb-50"></div>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

								<div class="separator separator-rouned">
									<i class="fa fa-cog"></i>
								</div>

								<h4 class="pt-20">Galerie de imagini</h4>

								<!-- Portfolio Gallery Grid -->
								<div class="gallery-isotope grid-3 gutter-small clearfix" data-lightbox="gallery">

									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image"  href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
									<div class="gallery-item">
										<div class="thumb">
											<img class="img-fullwidth" src="http://placehold.it/400x300" alt="project">
											<div class="overlay-shade"></div>
											<div class="text-holder">
												<div class="title text-center">Descriere imagine</div>
											</div>
											<div class="icons-holder">
												<div class="icons-holder-inner">
													<div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
														<a data-lightbox="image" href="http://placehold.it/1350x720"><i class="fa fa-plus"></i></a>
													</div>
												</div>
											</div>
											<a class="hover-link" data-lightbox="image" href="http://placehold.it/400x300">View more</a>
										</div>
									</div>
								</div>
								<!-- End Portfolio Gallery Grid -->

								<div class="separator separator-rouned">
									<i class="fa fa-cog"></i>
								</div>



								<div class="mt-30 mb-0">
									<h5 class="pull-left mt-10 mr-20 text-theme-colored">Share:</h5>
									<ul class="styled-icons icon-circled m-0">
										<li><a href="#" data-bg-color="#3A5795"><i class="fa fa-facebook text-white"></i></a></li>
										<li><a href="#" data-bg-color="#55ACEE"><i class="fa fa-twitter text-white"></i></a></li>
										<li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus text-white"></i></a></li>
									</ul>
								</div>
							</div>
						</article>
						<div class="tagline p-0 pt-20 mt-5">
							<div class="row">
								<div class="col-md-12">
									<div class="tags">
										<p class="mb-0"><i class="fa fa-tags text-theme-colored"></i> <span>Tags:</span> Dieta, soare, echipa, sprit</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<style>
	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}

	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
</style>