<!-- Footer -->
<footer id="footer" class="footer divider parallax layer-overlay" data-bg-img="<?php echo IMAGE_PATH;?>footer_bg.jpg" data-parallax-ratio="0.7">
	<div class="container pb-60 pt-0">
		<div class="row text-center">
			<div class="col-md-6 col-md-offset-3">
				<div class="widget dark mb-50"> <img src="<?php echo IMAGE_PATH;?>logo-white.jpg" alt="" class="mt-10 mb-20">
					<p class="font-12">Aici un fel de slogan sau ceva legat de initiativa in a crea acest site. Vorporis dolor soluta officiis quam, repudiandae, culpa nostrum maiores dignissimos quod expedita, aliquid magnam tempore iste minima quaerat adipisci veniam.</p>
				</div>
			</div>
		</div>

		<div class="row mt-10">
			<div class="col-md-5">
				<h4 class="text-white">News & info</h4>
				<form id="mailchimp-subscription-form-footer" class="newsletter-form mt-20">
					<div class="input-group">
						<input type="email" value="" name="EMAIL" placeholder="Your Email" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer" >
						<span class="input-group-btn">
                                <button data-height="45px" class="btn btn-colored btn-theme-colored btn-xs m-0 font-14" type="submit">Aboneaza-te</button>
                            </span>
					</div>
				</form>
			</div>
			<div class="col-md-4 col-md-offset-3 mt-20">
				<div class="pull-right">
					<ul class="styled-icons icon-bordered small square list-inline mt-10">
						<li><a href=""><i class="fa fa-facebook text-white"></i></a></li>
						<li><a href=""><i class="fa fa-twitter text-white"></i></a></li>
						<li><a href=""><i class="fa fa-skype text-white"></i></a></li>
						<li><a href=""><i class="fa fa-youtube text-white"></i></a></li>
					</ul>
				</div>
				<div class="pull-left">
					<h5 class="text-white">Contact rapid</h5>
					<h4 class="text-gray">0723.23.2356</h4>
				</div>
			</div>
		</div>
		<div class="text-center">
			<h3 class="text-white">
				<a href="mailto:lorenne@thechangewithinu.com">lorenne@thechangewithinu.com</a>
			</h3>
		</div>
	</div>
	<div class="container-fluid copy-right p-20 bg-black-333">
		<div class="row text-center">
			<div class="col-md-12">
				<p class="font-11 text-white m-0">Copyright &copy;2017 TheChangeWithinU.com</p>
			</div>
		</div>
	</div>
</footer>

<a class="scrollToTop" href=""><i class="fa fa-angle-up"></i></a>
</div>

<script src="<?php echo JS_PATH;?>jquery-ui.min.js"></script>
<script src="<?php echo JS_PATH;?>bootstrap.min.js"></script>
<script src="<?php echo JS_PATH;?>jquery-plugin-collection.js"></script>
<link  href="<?php echo JS_PATH;?>revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo JS_PATH;?>revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo JS_PATH;?>revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?php echo JS_PATH;?>revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo JS_PATH;?>revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- Revolution Slider 5.x EXTENSIONS  -->
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo JS_PATH;?>revolution-slider/js/extensions/revolution.extension.video.min.js"></script>

<script src="<?php echo JS_PATH;?>custom.js<?php echo VER;?>"></script>
</body>
</html>