<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title>Meta titlu pagina - programs and balance</title>
	<link href="<?php echo IMAGE_PATH;?>favicon.png" rel="shortcut icon" type="image/png">
	<link href="<?php echo CSS_PATH;?>bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>jquery-ui.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>animate.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>css-plugin-collections.css" rel="stylesheet"/>
	<link href="<?php echo CSS_PATH;?>menuzord-skins/menuzord-boxed.css" rel="stylesheet" id="menuzord-menu-skins"/>
	<link href="<?php echo CSS_PATH;?>style-main.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>colors/theme-skin-green.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>preloader.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>responsive.css" rel="stylesheet" type="text/css">
	<link href="<?php echo CSS_PATH;?>style.css<?php echo VER;?>" rel="stylesheet" type="text/css">
	<script src="<?php echo JS_PATH;?>jquery-2.2.4.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="">
<div id="wrapper">
	<!-- preloader -->
	<div id="preloader">
		<div id="spinner">
			<img src="<?php echo IMAGE_PATH;?>preloaders/12.gif" alt="">
		</div>
		<div id="disable-preloader" class="btn btn-default btn-sm"></div>
	</div>
	<!-- Header -->
	<header id="header" class="header">
		<div class="header-top sm-text-center bg-theme-colored">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<nav>
							<ul class="list-inline sm-text-center text-left flip mt-5">
								<li> <a class="text-white" href="">FAQ</a> </li>
								<li class="text-white">|</li>
								<li> <a class="text-white" href="">Chat</a> </li>
								<li class="text-white">|</li>
								<li> <a class="text-white" href="">Contact</a> </li>
							</ul>
						</nav>
					</div>
					<div class="col-md-6">
						<div class="widget m-0 mt-5 no-border">
							<ul class="list-inline text-right sm-text-center">
								<li class="pl-10 pr-10 mb-0 pb-0">
									<div class="header-widget text-white"><i class="fa fa-envelope-o"></i> lorenne@thechangewithinu.com </div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 text-right flip sm-text-center">
						<div class="widget m-0">
							<ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
								<li class="mb-0 pb-0"><a href=""><i class="fa fa-facebook"></i></a></li>
								<li class="mb-0 pb-0"><a href=""><i class="fa fa-twitter"></i></a></li>
								<li class="mb-0 pb-0"><a href=""><i class="fa fa-instagram"></i></a></li>
								<li class="mb-0 pb-0"><a href=""><i class="fa fa-linkedin text-white"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-nav">
			<div class="header-nav-wrapper navbar-scrolltofixed bg-lighter">
				<div class="container">
					<nav id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand pull-left flip" href="javascript:void(0)"><img src="<?php echo IMAGE_PATH;?>logo.png?v=1" alt=""></a>
						<ul class="menuzord-menu">
							<li class="active">
								<a href="">Home</a>
							</li>
							<li>
								<a href="">Despre mine</a>
							</li>
							<li>
								<a href="">Programe</a>
							</li>
							<li>
								<a href="">Nutritie</a>
							</li>
							<li>
								<a href="">Magazin</a>
								<ul class="dropdown">
									<li><a href="">Produse</a>
										<ul class="dropdown">
											<li><a href="">Categoria 1</a></li>
											<li><a href="">Categoria 2</a></li>
											<li><a href="">Categoria 3</a></li>
											<li><a href="">Categoria 4</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a href="">Blog</a>
							</li>
							<li>
								<a href="">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
