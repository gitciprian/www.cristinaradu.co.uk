    <div class="main-content">
        <!-- Section: home -->
        <section id="home" class="divider">
            <div class="container-fluid p-0">
                <!-- Slider Revolution Start -->
                <div class="rev_slider_wrapper">
                    <div class="rev_slider" data-version="5.0">
                        <ul>
                            <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                                <img src="<?php echo IMAGE_PATH;?>slides/slide_1.jpg?v=1"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>

                                <div class="tp-caption tp-resizeme text-uppercase font-raleway text-center text-white"
                                     id="rs-1-layer-1"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['-80']"
                                     data-fontsize="['64','64','54','24']"
                                     data-lineheight="['95']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:700;"><span class="text-theme-colored">Fitness</span> & Sport
                                </div>
                                <div class="tp-caption tp-resizeme text-center font-raleway text-white"
                                     id="rs-1-layer-2"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['0']"
                                     data-fontsize="['18']"
                                     data-lineheight="['34']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1400"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:500;">Programe standard si personalizate<br> adaptate pentru fiecare in parte
                                </div>
                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption tp-resizeme"
                                     id="rs-1-layer-3"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['80']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1600"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-default btn-lg btn-circled mr-10" href="">Inscriete acum</a> <a class="btn btn-colored btn-lg btn-circled btn-theme-colored" href="">Detalii</a>
                                </div>
                            </li>

                            <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                                <img src="<?php echo IMAGE_PATH;?>slides/slide_2.jpg?v=1"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>

                                <div class="tp-caption tp-resizeme text-uppercase font-raleway text-center text-white"
                                     id="rs-2-layer-1"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['-80']"
                                     data-fontsize="['64','64','54','24']"
                                     data-lineheight="['95']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:700;">Echilibru & <span class="text-theme-colored">Armonie</span>
                                </div>
                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption tp-resizeme text-center font-raleway text-white"
                                     id="rs-2-layer-2"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['0']"
                                     data-fontsize="['18']"
                                     data-lineheight="['34']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1200"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:500;">Auto-cunoastere, echilibru, armonie <br> trup si spirit
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption tp-resizeme"
                                     id="rs-2-layer-3"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['80']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1400"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-default btn-lg btn-circled mr-10" href="">Inscriete acum</a> <a class="btn btn-colored btn-lg btn-circled btn-theme-colored" href="">Detalii</a>
                                </div>
                            </li>

                            <!-- SLIDE 3 -->
                            <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo IMAGE_PATH;?>slides/slide_3.jpg?v=1"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>

                                <div class="tp-caption tp-resizeme text-uppercase font-raleway text-center text-white"
                                     id="rs-3-layer-1"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['-80']"
                                     data-fontsize="['64','64','54','24']"
                                     data-lineheight="['95']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:700;">Atitudine pozitiva
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption tp-resizeme text-center font-raleway text-white"
                                     id="rs-3-layer-2"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['0']"
                                     data-fontsize="['18']"
                                     data-lineheight="['34']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1200"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:500;">Aici o scurta descriere<br> sau se poate lasa necompeltat
                                </div>
                                <div class="tp-caption tp-resizeme"
                                     id="rs-3-layer-3"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['80']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1400"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-circled btn-theme-colored" href="">Detalii</a>
                                </div>
                            </li>

                            <!-- SLIDE 4 -->
                            <li data-index="rs-4" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo IMAGE_PATH;?>slides/slide_5.jpg?v=1"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>

                                <div class="tp-caption tp-resizeme text-uppercase font-raleway text-center text-white"
                                     id="rs-4-layer-1"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['-80']"
                                     data-fontsize="['64','64','54','24']"
                                     data-lineheight="['95']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:700;">Lucru in echipa
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption tp-resizeme text-center font-raleway text-white"
                                     id="rs-4-layer-2"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['0']"
                                     data-fontsize="['18']"
                                     data-lineheight="['34']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1200"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; font-weight:500;">Aici o scurta descriere<br> sau se poate lasa necompeltat
                                </div>
                                <div class="tp-caption tp-resizeme"
                                     id="rs-4-layer-3"
                                     data-x="['center']"
                                     data-hoffset="['0']"
                                     data-y="['middle']"
                                     data-voffset="['80']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1400"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-circled btn-theme-colored" href="">Detalii</a>
                                </div>
                            </li>

                        </ul>
                    </div><!-- end .rev_slider -->
                </div>
                <!-- end .rev_slider_wrapper -->
                <script>
                    $(document).ready(function(e) {
                        var revapi = $(".rev_slider").revolution({
                            sliderType:"standard",
                            jsFileLocation: "<?php echo JS_PATH;?>revolution-slider/js/",
                            sliderLayout: "auto",
                            dottedOverlay: "none",
                            delay: 4500,
                            navigation: {
                                keyboardNavigation: "off",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 1,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                                arrows: {
                                    style: "gyges",
                                    enable: true,
                                    hide_onmobile: false,
                                    hide_onleave: true,
                                    hide_delay: 200,
                                    hide_delay_mobile: 1200,
                                    tmp: '',
                                    left: {
                                        h_align: "left",
                                        v_align: "center",
                                        h_offset: 0,
                                        v_offset: 0
                                    },
                                    right: {
                                        h_align: "right",
                                        v_align: "center",
                                        h_offset: 0,
                                        v_offset: 0
                                    }
                                },
                                bullets: {
                                    enable: true,
                                    hide_onmobile: true,
                                    hide_under: 800,
                                    style: "hebe",
                                    hide_onleave: false,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 0,
                                    v_offset: 30,
                                    space: 5,
                                    tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                                }
                            },
                            responsiveLevels: [1240, 1024, 778],
                            visibilityLevels: [1240, 1024, 778],
                            gridwidth: [1170, 1024, 778, 480],
                            gridheight: [720, 768, 960, 720],
                            lazyType: "none",
                            parallax:"mouse",
                            parallaxBgFreeze:"off",
                            parallaxLevels:[2,3,4,5,6,7,8,9,10,1],
                            shadow: 0,
                            spinner: "off",
                            stopLoop: "on",
                            stopAfterLoops: 0,
                            stopAtSlide: -1,
                            shuffle: "off",
                            autoHeight: "off",
                            fullScreenAutoWidth: "off",
                            fullScreenAlignForce: "off",
                            fullScreenOffsetContainer: "",
                            fullScreenOffset: "0",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                nextSlideOnWindowFocus: "off",
                                disableFocusListener: false,
                            }
                        });
                    });
                </script>
                <!-- Slider Revolution Ends -->
            </div>
        </section>

        <!-- Section: WelCome -->
        <section id="about" style="background: #fff">
            <div class="container pb-20">
                <div class="section-title">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- <i class="flaticon-medical-27 font-72 text-white-f1"></i> -->
                            <h2 class="title text-uppercase">The Change Within <span class="text-black font-weight-300">YOU</span></h2>
                            <p class="text-uppercase letter-space-4">Alatura-te comunitatii noastre.</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row mtli-row-clearfix">
                        <div class="col-md-6 mb-sm-20">
                            <div class="thumb">
                                <img src="<?php echo IMAGE_PATH;?>sub_slider_2.png" alt="nu este cumparata inca">
                            </div>
                        </div>
                        <div class="col-md-6 pl-0 sm-pl-15 sm-text-center">
                            <div class="icon-box iconbox-theme-colored p-0 ml-160 ml-sm-0">
                                <a href="" class="icon icon-bordered icon-circled mb-0 mr-0 pull-left flip sm-pull-none icon-md">
                                    <i class="flaticon-yoga-woman-on-her-knees-looking-up font-34"></i>
                                </a>
                                <div class="ml-100 ml-sm-0">
                                    <h5 class="icon-box-title mt-15 mb-0 text-uppercase letter-space-3">Echilibru Corp & Spirit</h5>
                                    <p class="text-gray">Aici o scurta descriere despre acest punct. Recomand text care sa contina cuvinte cheie.</p>
                                </div>
                            </div>
                            <div class="icon-box iconbox-theme-colored p-0 ml-120 ml-sm-0">
                                <a href="" class="icon icon-bordered icon-circled mb-0 mr-0 pull-left flip sm-pull-none">
                                    <i class="flaticon-gym-slim-body-with-timer font-34"></i>
                                </a>
                                <div class="ml-100 ml-sm-0">
                                    <h5 class="icon-box-title mt-15 mb-0 text-uppercase letter-space-3">Programe de exercitii</h5>
                                    <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
                                </div>
                            </div>
                            <div class="icon-box iconbox-theme-colored p-0 ml-80 ml-sm-0">
                                <a href="" class="icon icon-bordered icon-circled mb-0 mr-0 pull-left flip sm-pull-none">
                                    <i class="flaticon-nutrition-salad-bowl-hand-drawn-food  font-34"></i>
                                </a>
                                <div class="ml-100 ml-sm-0">
                                    <h5 class="icon-box-title mt-15 mb-0 text-uppercase letter-space-3">Programe de nutritie</h5>
                                    <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
                                </div>
                            </div>
                            <div class="icon-box iconbox-theme-colored p-0 ml-40 ml-sm-0">
                                <a href="" class="icon icon-bordered icon-circled mb-0 mr-0 pull-left flip sm-pull-none">
                                    <i class="flaticon-gym-muscular-male-outline-with-heart-and-lifeline font-34"></i>
                                </a>
                                <div class="ml-100 ml-sm-0">
                                    <h5 class="icon-box-title mt-15 mb-0 text-uppercase letter-space-3">Informatii, unelte, produse</h5>
                                    <p class="text-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="whychoose">
            <div class="container-fluid pt-0 pb-0 pt-sm-0">
                <div class="section-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 p-0 mb-xs-30">
                            <div class="img-icon-box text-center divider mt-sm-0">
                                <div class="img-icon-details pt-50">
                                    <i class="flaticon-gym-gymnast-practice font-72"></i>
                                    <h4 class="title text-uppercase font-weight-400 mt-0"><a class="text-white" href="">Avantaje Membri</a></h4>
                                    <div class="img-icon-content pl-40 pr-40">
                                        <p class="text-white">Descarcare gratuita e-book, articole, informari & extra stuff sau orice se doreste</p>
                                        <a class="btn btn-default btn-lg btn-circled mr-10" href="">Detalii</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 p-0 mb-xs-30">
                            <div class="img-icon-box text-center divider mt-sm-0">
                                <div class="img-icon-details pt-50">
                                    <i class="flaticon-gym-bodybuilder-carrying-weight-on-one-hand-outline font-72"></i>
                                    <h4 class="title text-uppercase font-weight-400 mt-0"><a class="text-white" href="">Echipamente & produse</a></h4>
                                    <div class="img-icon-content pl-40 pr-40">
                                        <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est modi, saepe hic esse maxime quasi sapiente ex debitis quis dolorum.</p>
                                        <a class="btn btn-default btn-lg btn-circled mr-10" href="">Detalii</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 p-0 mb-xs-30">
                            <div class="img-icon-box text-center divider mt-sm-0">
                                <div class="img-icon-details pt-50">
                                    <i class="flaticon-gym-gymnast-muscular-arm-outline-with-a-timer font-72"></i>
                                    <h4 class="title text-uppercase font-weight-400 mt-0"><a class="text-white" href="">Tonus +</a></h4>
                                    <div class="img-icon-content pl-40 pr-40">
                                        <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est modi, saepe hic esse maxime quasi sapiente ex debitis quis dolorum.</p>
                                        <a class="btn btn-default btn-lg btn-circled mr-10" href="">Detalii</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-3 p-0 mb-xs-30">
                            <div class="img-icon-box text-center divider mt-sm-0 ">
                                <div class="img-icon-details pt-50">
                                    <i class="flaticon-gym-heart-with-muscular-male-gymnast-arms font-72"></i>
                                    <h4 class="title text-uppercase font-weight-400 mt-0"><a class="text-white" href="">Beneficii ...</a></h4>
                                    <div class="img-icon-content pl-40 pr-40">
                                        <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est modi, saepe hic esse maxime quasi sapiente ex debitis quis dolorum.</p>
                                        <a class="btn btn-default btn-lg btn-circled mr-10" href="">Detalii</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="classes">
            <div class="container pb-50">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="title text-uppercase">Inscriete in <span class="text-black font-weight-300">program</span></h2>
                            <p class="text-uppercase letter-space-4">Fa parte din comunitatea nostra</p>
                        </div>
                    </div>
                </div>
                <div class="row mtli-row-clearfix">
                    <div class="col-md-4 col-sm-6 mb-30">
                        <div class="item">
                            <div class="class-item box-hover-effect effect1 mb-md-30 bg-lighter">
                                <div class="thumb mb-20"> <img alt="featured project" src="<?php echo IMAGE_PATH;?>programs/program_1.jpg" class="img-responsive img-fullwidth">
                                </div>
                                <div class="content text-center flip p-25 pt-0">
                                    <h4 class="text-uppercase text-theme-colored font-weight-800 letter-space-2">Consultanta si support</h4>
                                    <a class="btn btn-flat btn-sm border-theme-colored mt-30" href="">Detalii</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-30">
                        <div class="item">
                            <div class="class-item box-hover-effect effect1 mb-md-30 bg-lighter">
                                <div class="thumb mb-20"> <img alt="featured project" src="<?php echo IMAGE_PATH;?>programs/program_2.jpg" class="img-responsive img-fullwidth">
                                </div>
                                <div class="content text-center flip p-25 pt-0">
                                    <h4 class="text-uppercase text-theme-colored font-weight-800 letter-space-2">Nutritie & alimentatie</h4>
                                    <a class="btn btn-flat btn-sm border-theme-colored mt-30" href="">Detalii</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-30">
                        <div class="item">
                            <div class="class-item box-hover-effect effect1 mb-md-30 bg-lighter">
                                <div class="thumb mb-20"> <img alt="featured project" src="<?php echo IMAGE_PATH;?>programs/program_3.jpg" class="img-responsive img-fullwidth">
                                </div>
                                <div class="content text-center flip p-25 pt-0">
                                    <h4 class="text-uppercase text-theme-colored font-weight-800 letter-space-2">Fitness & cardio</h4>
                                    <a class="btn btn-flat btn-sm border-theme-colored mt-30" href="">Detalii</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-30">
                        <div class="item">
                            <div class="class-item box-hover-effect effect1 mb-md-30 bg-lighter">
                                <div class="thumb mb-20"> <img alt="featured project" src="<?php echo IMAGE_PATH;?>programs/program_4.jpg" class="img-responsive img-fullwidth">
                                </div>
                                <div class="content text-center flip p-25 pt-0">
                                    <h4 class="text-uppercase text-theme-colored font-weight-800 letter-space-2">Diete echilibrate</h4>
                                    <a class="btn btn-flat btn-sm border-theme-colored mt-30" href="">Detalii</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-30">
                        <div class="item">
                            <div class="class-item box-hover-effect effect1 mb-md-30 bg-lighter">
                                <div class="thumb mb-20"> <img alt="featured project" src="<?php echo IMAGE_PATH;?>programs/program_5.jpg" class="img-responsive img-fullwidth">
                                </div>
                                <div class="content text-center flip p-25 pt-0">
                                    <h4 class="text-uppercase text-theme-colored font-weight-800 letter-space-2">Meditatie & balans</h4>
                                    <a class="btn btn-flat btn-sm border-theme-colored mt-30" href="">Detalii</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-30">
                        <div class="item">
                            <div class="class-item box-hover-effect effect1 mb-md-30 bg-lighter">
                                <div class="thumb mb-20"> <img alt="featured project" src="<?php echo IMAGE_PATH;?>programs/program_6.jpg" class="img-responsive img-fullwidth">
                                </div>
                                <div class="content text-center flip p-25 pt-0">
                                    <h4 class="text-uppercase text-theme-colored font-weight-800 letter-space-2">Tooluri si echipamente</h4>
                                    <a class="btn btn-flat btn-sm border-theme-colored mt-30" href="">Detalii</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-theme-colored orange">
            <div class="container pt-30 pb-30">
                <div class="section-content">
                    <div class="row sm-text-center">
                        <div class="col-md-12 text-center">
                            <h2 class="m-0 text-uppercase text-white font-weidth-400">Alege programul care ti se potriveste!</h2>
                            <a href="#" class="btn btn-gray btn-theme-colored  btn-xl">Inregistreaza-te acum! <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container pb-0">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="<?php echo IMAGE_PATH;?>weightloss_2.jpg" alt="">
                        </div>
                        <div class="col-md-6 pb-sm-70 pt-sm-50">
                            <h3 class="mt-0">Calculator <span class="text-theme-colored">BMI</span></h3>
                            <p>Acest tool va permite sa aflati rapid coeficientul BMI (body mass index) in ce prag va aflati, si ce trebuie as faceti mai departe</p>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#bmi_metric_version" aria-controls="bmi_metric_version" role="tab" data-toggle="tab">Standard metric</a>
                                </li>
                                <li role="presentation">
                                    <a href="#bmi_English_version" aria-controls="bmi_English_version" role="tab" data-toggle="tab">Standard (US, UK)</a>
                                </li>
                                <li role="presentation">
                                    <a href="#bmi_table" aria-controls="bmi_table" role="tab" data-toggle="tab">Tabel observatii</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content border-gray-lightgray pr-0">

                                <div role="tabpanel" class="tab-pane active" id="bmi_metric_version">
                                    <form id="form_bmi_metric_calculator" class="bg-img-right-bottom bg-no-repeat pr-40" >
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Greutate </label>
                                                    <input type="number" step="any" class="form-control" required name="bmi_metric_weight_kg" value="" placeholder="in kg">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Inaltime </label>
                                                    <input type="number" step="any" class="form-control" required name="bmi_metric_height_cm" value="" placeholder="in cm">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Varsta </label>
                                                    <input type="number" class="form-control" required name="bmi_metric_age" value="" placeholder="in ani impliniti">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Gen &nbsp;</label>
                                                    <br>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="bmi_metric_gender" value="f" checked>Feminin
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="bmi_metric_gender" value="m">Masculin
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div style="display: none;" role="alert" class="alert alert-success" id="bmi_metric_calculator_form_result"> </div>
                                                <button type="submit" class="btn btn-sm btn-theme-colored text-uppercase" data-loading-text="Please wait...">Calculeaza</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div role="tabpanel" class="tab-pane " id="bmi_English_version">
                                    <form id="form_bmi_standard_calculator" class="bg-img-right-bottom bg-no-repeat pr-20">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Greutate </label>
                                                    <input type="number" step="any" class="form-control" required name="bmi_standard_weight_lbs" value="" placeholder="in lbs">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label>Inaltime </label>
                                                    <input type="number" step="any" class="form-control" required name="bmi_standard_height_ft" value="" placeholder="in ft">
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <input type="number" step="any" class="form-control" required name="bmi_standard_height_in" value="" placeholder="inch">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Varsta </label>
                                                    <input type="number" class="form-control" required name="bmi_standard_age" value="" placeholder="in ani impliniti">
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label>Gen &nbsp;</label>
                                                    <br>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="bmi_standard_gender" value="f" checked>Feminin
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="bmi_standard_gender" value="m">Masculin
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div style="display: none;" role="alert" class="alert alert-success" id="bmi_standard_calculator_form_result"> </div>
                                                <button type="submit" class="btn btn-sm btn-theme-colored text-uppercase" data-loading-text="Please wait...">Calculeaza</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="bmi_table">
                                    <p>Valorile coeficientului BMI conform standardului Organizatiei Mondiala a Sanatatii (WHO). Standardul se aplica pentru barbati si femei cu varsta peste 18 ani.</p>
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr><th>Standard</th><th>Coeficient BMI - kg/m<sup>2</sup></th></tr>
                                        <tr><td>Sub-ponderal sever</td><td>&lt; 16</td></tr>
                                        <tr><td>Sub-ponderal moderat</td><td>16 - 17</td></tr>
                                        <tr><td>Sub-ponderal</td><td>17 - 18.5</td></tr>
                                        <tr><td>Normal</td><td>18.5 - 25</td></tr>
                                        <tr><td>Supra-ponderal</td><td>25 - 30</td></tr>
                                        <tr><td>Obez categoria I</td><td>30 - 35</td></tr>
                                        <tr><td>>Obez categoria II</td><td>35 - 40</td></tr>
                                        <tr><td>>Obez categoria III</td><td>&gt; 40</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section: Produse -->
        <section id="gallery" class="bg-light">
            <div class="container pb-0">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="title text-uppercase">Produse <span class="text-black font-weight-300">recomandate</span></h2>
                            <p class="text-uppercase letter-space-4">Gama noastara de produse profesionale.</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Works Filter -->
                            <div class="portfolio-filter">
                                <a href="" class="active" data-filter="*">Toate</a>
                                <a href="#branding" class="" data-filter=".branding">Aerobic</a>
                                <a href="#design" class="" data-filter=".design">Static</a>
                                <a href="#photography" class="" data-filter=".photography">DInamic</a>
                            </div>

                            <div id="grid" class="gallery-isotope grid-4 clearfix">
                                <div class="gallery-item design">
                                    <div class="thumb">
                                        <div class="flexslider-wrapper" data-direction="vertical">
                                            <div class="flexslider">
                                                <ul class="slides">
                                                    <li><a href="<?php echo IMAGE_PATH;?>products/product_10.jpg" title="Galerie de imagini produs - imaginea 1"><img src="<?php echo IMAGE_PATH;?>products/product_10.jpg" alt=""></a></li>
                                                    <li><a href="<?php echo IMAGE_PATH;?>products/product_11.jpg" title="Galerie de imagini produs - imaginea 2"><img src="<?php echo IMAGE_PATH;?>products/product_11.jpg" alt=""></a></li>
                                                    <li><a href="<?php echo IMAGE_PATH;?>products/product_12.jpg" title="Galerie de imagini produs - imaginea 3"><img src="<?php echo IMAGE_PATH;?>products/product_12.jpg" alt=""></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a href=""><i class="fa fa-picture-o"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="gallery-item photography">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>products/product_1.jpg" alt="project">
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Echipamente de exterior</h5>
                                            <p>Aici o scurta descriere produs</p>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a data-lightbox="image" href="<?php echo IMAGE_PATH;?>products/product_1.jpg"><i class="fa fa-plus"></i></a>
                                                    <a href=""><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="hover-link" data-lightbox="image" href="<?php echo IMAGE_PATH;?>products/product_1.jpg">Mai multe detalii</a>
                                    </div>
                                </div>

                                <div class="gallery-item branding">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>products/product_2.jpg" alt="project">
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a href=""><i class="fa fa-link"></i></a>
                                                    <a href=""><i class="fa fa-heart-o"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="hover-link" data-lightbox="image" href="<?php echo IMAGE_PATH;?>products/product_2.jpg">Mai multe detalii</a>
                                    </div>
                                </div>

                                <div class="gallery-item design">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>products/product_3.jpg" alt="project">
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a href=""><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="hover-link" data-lightbox="image" href="<?php echo IMAGE_PATH;?>products/product_3.jpg">Mai multe detalii</a>
                                    </div>
                                </div>

                                <div class="gallery-item photography">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>products/product_4.jpg" alt="project">
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=vDUUD9_Ipjc"><i class="fa fa-youtube-play"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="gallery-item branding">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>products/product_5.jpg" alt="project">
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=fDWFVI8PQOI"><i class="fa fa-play"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="gallery-item design">
                                    <div class="thumb">
                                        <div class="flexslider-wrapper">
                                            <div class="flexslider">
                                                <ul class="slides">
                                                    <li><a href="<?php echo IMAGE_PATH;?>products/product_6.jpg" title="Galerie de imagini produs - imaginea 1"><img src="<?php echo IMAGE_PATH;?>products/product_6.jpg" alt=""></a></li>
                                                    <li><a href="<?php echo IMAGE_PATH;?>products/product_7.jpg" title="Galerie de imagini produs - imaginea 2"><img src="<?php echo IMAGE_PATH;?>products/product_7.jpg" alt=""></a></li>
                                                    <li><a href="<?php echo IMAGE_PATH;?>products/product_8.jpg" title="Galerie de imagini produs - imaginea 3"><img src="<?php echo IMAGE_PATH;?>products/product_8.jpg" alt=""></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a href=""><i class="fa fa-picture-o"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="gallery-item photography">
                                    <div class="thumb">
                                        <img class="img-fullwidth" src="<?php echo IMAGE_PATH;?>products/product_9.png" alt="project">
                                        <div class="overlay-shade"></div>
                                        <div class="text-holder text-center">
                                            <h5 class="title">Aici nume produs sau altele</h5>
                                        </div>
                                        <div class="icons-holder">
                                            <div class="icons-holder-inner">
                                                <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                                                    <a data-lightbox="image" href="<?php echo IMAGE_PATH;?>products/product_9.png"><i class="fa fa-plus"></i></a>
                                                    <a href=""><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="hover-link" data-lightbox="image" href="<?php echo IMAGE_PATH;?>products/product_9.png">Mai multe detalii</a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section: Blog -->
        <section id="blog">
            <div class="container">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <h2 class="title text-uppercase">Articole & <span class="text-black font-weight-300">noutati</span></h2>
                            <p class="text-uppercase letter-space-4">Aici scurta descriere zona de blog</p>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-4">
                            <article class="post clearfix">
                                <div class="entry-header">
                                    <div class="post-thumb thumb"> <img class="img-responsive img-fullwidth" alt="" src="<?php echo IMAGE_PATH;?>blog/blog_1.jpg"> </div>
                                    <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                                        <div class="display-table">
                                            <div class="display-table-cell">
                                                <ul class="font-12">
                                                    <li>
                                                        <a href="" class="text-white"><i class="fa fa-arrow-right"></i> <br>Citeste</a>
                                                    </li>
                                                    <li class="mt-20">
                                                        <a href="" class="text-white"><i class="fa fa-comments-o"></i> <br>10 Comentarii</a>
                                                    </li>
                                                    <li class="mt-20">
                                                        <a href="" class="text-white"><i class="fa fa-share-square-o"></i> <br> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-content p-20">
                                    <h4 class="entry-title text-white text-uppercase"><a href="">Titlu articol</a></h4>
                                    <ul class="entry-date list-inline mt-10 mb-10">
                                        <li><a href="" class="text-theme-colored">5 Decembrie, 2017</a></li>
                                        <li><a href="">Autor de: <span class="text-theme-colored">Lorenne</span></a></li>
                                    </ul>
                                    <a href="" class="btn-read-more btn-sm">Citeste...</a>
                                    <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article class="post clearfix">
                                <div class="entry-header">
                                    <div class="post-thumb thumb"> <img class="img-responsive img-fullwidth" alt="" src="<?php echo IMAGE_PATH;?>blog/blog_2.jpg"> </div>
                                    <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                                        <div class="display-table">
                                            <div class="display-table-cell">
                                                <ul class="font-12">
                                                    <li>
                                                        <a href="" class="text-white"><i class="fa fa-arrow-right"></i> <br>Citeste</a>
                                                    </li>
                                                    <li class="mt-20">
                                                        <a href="" class="text-white"><i class="fa fa-comments-o"></i> <br>10 Comentarii</a>
                                                    </li>
                                                    <li class="mt-20">
                                                        <a href="" class="text-white"><i class="fa fa-share-square-o"></i> <br> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-content p-20">
                                    <h4 class="entry-title text-white text-uppercase"><a href="">Cum sa scoti ce e mai bun din tine</a></h4>
                                    <ul class="entry-date list-inline mt-10 mb-10">
                                        <li><a href="" class="text-theme-colored">14 August, 2017</a></li>
                                        <li><a href="">Autor: <span class="text-theme-colored">Lorenne</span></a></li>
                                    </ul>
                                    <a href="" class="btn-read-more btn-sm">Citeste...</a>
                                    <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article class="post clearfix">
                                <div class="entry-header">
                                    <div class="post-thumb thumb">
                                        <img class="img-responsive img-fullwidth" alt="" src="<?php echo IMAGE_PATH;?>blog/blog_3.jpg">
                                    </div>
                                    <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                                        <div class="display-table">
                                            <div class="display-table-cell">
                                                <ul class="font-12">
                                                    <li>
                                                        <a href="" class="text-white"><i class="fa fa-arrow-right"></i> <br>Citeste</a>
                                                    </li>
                                                    <li class="mt-20">
                                                        <a href="" class="text-white"><i class="fa fa-comments-o"></i> <br>10 Comentarii</a>
                                                    </li>
                                                    <li class="mt-20">
                                                        <a href="" class="text-white"><i class="fa fa-share-square-o"></i> <br> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-content p-20">
                                    <h4 class="entry-title text-white text-uppercase"><a href="">Post title here</a></h4>
                                    <ul class="entry-date list-inline mt-10 mb-10">
                                        <li><a href="" class="text-theme-colored">21 mai, 2017</a></li>
                                        <li><a href="">Lorenne: <span class="text-theme-colored">amazon.com</span></a></li>
                                    </ul>
                                    <a href="" class="btn-read-more btn-sm">Citeste...</a>
                                    <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- end main-content -->

