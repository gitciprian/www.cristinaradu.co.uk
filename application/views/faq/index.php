<!-- Start main-content -->
<div class="main-content">
	<section class="inner-header divider layer-overlay overlay-dark-5" data-bg-img="<?php echo IMAGE_PATH;?>faq_bg.jpg">
		<div class="container">
			<div class="section-content pt-50 pb-50">
				<div class="row">
					<div class="col-md-12">
						<h3 class="title text-white">FAQ</h3>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div id="accordion1" class="panel-group accordion">
						<div class="panel">
							<div class="panel-title">
								<a data-parent="#accordion1" data-toggle="collapse" href="#accordion11" class="active" aria-expanded="true"> <span class="open-sub"></span>
									<strong>Q. Ce trebuie sa fac sa ma inscriu in program?</strong>
								</a>
							</div>
							<div id="accordion11" class="panel-collapse collapse in" role="tablist" aria-expanded="true">
								<div class="panel-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis</p>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-title">
								<a class="collapsed" data-parent="#accordion1" data-toggle="collapse" href="#accordion12" aria-expanded="false"> <span class="open-sub"></span>
									<strong>Q. Pot sa urmez mai multe programe?</strong>
								</a>
							</div>
							<div id="accordion12" class="panel-collapse collapse" role="tablist" aria-expanded="false" style="height: 0px;">
								<div class="panel-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis</p>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-title">
								<a data-parent="#accordion1" data-toggle="collapse" href="#accordion13" class="collapsed" aria-expanded="false"> <span class="open-sub"></span>
									<strong>Q. Cum si de unde pot achizitiona produse recomandate?</strong>
								</a>
							</div>
							<div id="accordion13" class="panel-collapse collapse" role="tablist" aria-expanded="false">
								<div class="panel-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis</p>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-title">
								<a data-parent="#accordion1" data-toggle="collapse" href="#accordion14" class="collapsed" aria-expanded="false"> <span class="open-sub"></span>
									<strong>Q. Cum poti intra in legatura cu mine?</strong>
								</a>
							</div>
							<div id="accordion14" class="panel-collapse collapse" role="tablist" aria-expanded="false">
								<div class="panel-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis</p>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-title">
								<a data-parent="#accordion1" data-toggle="collapse" href="#accordion15" class="collapsed" aria-expanded="false"> <span class="open-sub"></span>
									<strong>Q. Orice alta intreabare sau zona care crezi ca trebuie explicata. Nu exista un numar fix de intrebari.</strong>
								</a>
							</div>
							<div id="accordion15" class="panel-collapse collapse" role="tablist" aria-expanded="false">
								<div class="panel-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- end main-content -->