<div class="header-title white" data-parallax="scroll" data-position="top" data-natural-height="600" data-natural-width="1920" data-image-src="<?php echo IMAGE_PATH;?>long-2.jpg">
	<div class="container">
		<div class="title-base">
			<hr class="anima" />
			<h1><?php echo $page_title;?></h1>
			<p><?php echo $scurta_descriere;?></p>
		</div>
	</div>
</div>

<div class="section-empty">
	<div class="container content">
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo IMAGE_PATH;?><?php echo $main_image;?>" alt="<?php echo $page_title;?>">
			</div>
			<div class="col-md-8">
				<?php echo $page_content;?>
				<?php if($youtube_video_url != ""):?>
					<hr class="space" />
					<iframe width="100%" height="400" src="<?php echo $youtube_video_url;?>" frameborder="0" allowfullscreen></iframe>
				<?php endif;?>
			</div>
		</div>
		<hr class="space" />
		<?php if($url_material_download !=""):?>
			<div class="row">
				<div class="col-md-4">
					<div class="title-base text-right">
						<hr />
						<h4>Materiale auxiliare</h4>
					</div>
				</div>
				<div class="col-md-8">
					<a class="btn btn-sm btn-default" href="<?php echo $url_material_download;?>"> Descarca atasament </a>
				</div>
			</div>
		<?php endif;?>
	</div>
</div>