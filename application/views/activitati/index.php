<div class="header-title white ken-burn-center" data-parallax="scroll" data-position="top" data-image-src="<?php echo IMAGE_PATH;?>long-3.jpg" data-natural-width="1920" data-natural-height="600">
    <div class="container">
        <div class="title-base">
            <hr class="anima" />
            <h1>Activitatile noastre</h1>
            <p><?php echo $page_title;?></p>
        </div>
    </div>
</div>
<div class="section-empty">
    <div class="container content">
        <?php echo $page_content;?><br/>
        <div class="grid-list columns-list">
            <div class="grid-box row">

                <?php foreach ($all_services as $service):

	                $all_jsons          = json_decode($service->page_extras,true);
	                $scurta_descriere   = $all_jsons['scurta_descriere'];

                ?>
                    <div class="grid-item col-md-6">
                        <div class="advs-box advs-box-side boxed-inverse" data-anima="fade-left" data-trigger="hover">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="img-box"><img src="<?php echo IMAGE_PATH;?><?php echo $service->main_image;?>" alt="<?php echo $service->page_title;?>" /></div>
                                </div>
                                <div class="col-md-8">
                                    <h3><?php echo $service->page_title;?></h3>
                                    <hr class="anima">
                                    <p><?php echo $scurta_descriere;?></p>
                                    <a class="btn-text" href="/activitati/detalii/<?php echo ViewHelper::sanitazeStringForUrl($service->page_title);?>/<?php echo $service->id;?>"> Detalii </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <div class="clear"></div>
            </div>
            <ul class="pagination pagination-grid hide-first-last" data-page-items="6" data-pagination-anima="show-scale" data-options="scrollTop:true"></ul>
        </div>
    </div>
</div>
