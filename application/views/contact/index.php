<div class="main-content">
    <section class="inner-header divider layer-overlay overlay-dark-5"  data-bg-img="<?php echo IMAGE_PATH;?>contact_bg.jpg?v=13">
        <div class="container">
            <div class="section-content pt-50 pb-50">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title text-white">Contact</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="divider">
        <div class="container">
            <div class="row pt-30">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-map-2 text-theme-colored"></i></a>
                                <div class="media-body">
                                    <h5 class="mt-0">Locatii</h5>
                                    <p>Bucuresti, Romania</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-chat text-theme-colored"></i></a>
                                <div class="media-body">
                                    <h5 class="mt-0">WhatsApp/Phone</h5>
                                    <p>+44 0723.23.23.23</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="pe-7s-mail text-theme-colored"></i></a>
                                <div class="media-body">
                                    <h5 class="mt-0">Email</h5>
                                    <p>lorenne@TheChangeWithinU.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="icon-box left media bg-deep p-30 mb-20"> <a class="media-left pull-left" href="#"> <i class="fa fa-skype text-theme-colored"></i></a>
                                <div class="media-body">
                                    <h5 class="mt-0">Video Call</h5>
                                    <p>Skype.TheChangeWithinU</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <h3 class="line-bottom mt-0 mb-30">Interesati de o discutie?</h3>
                    <!-- Contact Form -->
                    <form id="contact_form" name="contact_form" class="" action="" method="post">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Nume <small>*</small></label>
                                    <input name="form_name" class="form-control" type="text" placeholder="Numele dvs" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Email <small>*</small></label>
                                    <input name="form_email" class="form-control required email" type="email" placeholder="Adresa de email">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Subiect <small>*</small></label>
                                    <input name="form_subject" class="form-control required" type="text" placeholder="... motivul pentru care ma contactati">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Telefon</label>
                                    <input name="form_phone" class="form-control" type="text" placeholder="Numar de telefon">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Mesaj</label>
                            <textarea name="form_message" class="form-control required" rows="5" placeholder="Mesajul dvs"></textarea>
                        </div>
                        <div class="form-group">
                            <input name="form_botcheck" class="form-control" type="hidden" value="" />
                            <button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Please wait...">Trimite mesaj</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid pt-0 pb-0">
            <div class="row">
                <iframe class="google-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d91160.56672522558!2d26.024597994229303!3d44.43792692050995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1f93abf3cad4f%3A0xac0632e37c9ca628!2sBucure%C8%99ti!5e0!3m2!1sro!2sro!4v1504873629186" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>
</div>
