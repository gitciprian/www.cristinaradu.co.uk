<div class="header-title white ken-burn" data-parallax="scroll" data-position="top" data-natural-height="550" data-natural-width="1920" data-image-src="<?php echo IMAGE_PATH;?><?php echo ($bg_image !="" ? $bg_image : 'long-4.jpg' );?>">
    <div class="container">
        <div class="title-base">
            <hr class="anima" />
            <h1><?php echo $lang['PhotoGalleries'];?></h1>
            <p><?php echo ($lb == 'ro' ? $page_title : $page_title_en);?></p>
        </div>
    </div>
</div>
<?php if($page_content !=""):?>
    <div class="section-bg-color">
        <div class="container content text-center">
	        <?php echo ($lb == 'ro' ? $page_content : $page_content_en);?>
        </div>
    </div>
<?php endif;?>
<div class="section-empty">
    <div class="container content">
        <div class="maso-list gallery">
            <div class="navbar navbar-inner">
                <div class="navbar-toggle"><i class="fa fa-bars"></i><span>Menu</span><i class="fa fa-angle-down"></i></div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav nav-center over ms-minimal inner maso-filters">
                        <li class="current-active active"><a data-filter="maso-item"><?php echo $lang['AllGalleries'];?></a></li>
                        <?php if($active_categories > 0):?>
                            <?php foreach ($gallery_categories as $category):?>
                                <li><a data-filter="cat<?php echo $category->id;?>"><?php echo $lb == 'ro' ? $category->gallery_name : $category->gallery_name_en;?></a></li>
	                        <?php endforeach;?>
                        <?php endif;?>
                        <li><a class="maso-order" data-sort="asc"><i class="fa fa-arrow-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="maso-box row" data-lightbox-anima="fade-top">

                <?php if($active_galleries > 0):?>
                    <?php foreach ($all_galleries as $gallery):
                        if($gallery !=""):
                            $page_extras    = json_decode($gallery->page_extras, true);
                            if(count($page_extras) > 0)
                            {
                                foreach ($page_extras as $key => $val)
                                {
                                    $$key = $val;
                                }
                            }
                        ?>
                            <div class="maso-item col-md-4 cat<?php echo $gallery->partner_id;?>">
                                <div class="img-box adv-img adv-img-classic-box white">
	                                <?php echo ($parola_galerie !="" ? '<div class="gallery-locked-index"><img class="img-locked" title="'.$lang['PasswordProtected'].'" src="'.IMAGE_PATH.'icon-locked.png"></div>' : '');?>
                                    <a href="/foto/detalii/<?php echo ViewHelper::sanitazeStringForUrl($lb == 'ro' ? $gallery->page_title : $gallery->page_title_en);?>/<?php echo $gallery->id;?>">
                                        <img src="<?php echo IMAGE_PATH;?><?php echo $gallery->main_image;?>" alt="<?php echo ($lb == 'ro' ? $gallery->page_title : $gallery->page_title_en);?>">
                                        <div class="caption">
                                            <div class="caption-inner">
                                                <h2><?php echo ($lb == 'ro' ? $gallery->page_title : $gallery->page_title_en);?></h2>
                                                <p class="sub-text"><?php echo ViewHelper::getKeyValue($all_cat_names,$gallery->partner_id);?></p>
                                                <p class="big-text hidden-xs"><?php echo ViewHelper::textLimit($lb == 'ro' ? $scurta_descriere : $scurta_descriere_en, 75);?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php else:?>
                    <h4 class="text-center"><?php echo $lang['GeneralEmpty'];?></h4>
                <?php endif;?>
                <div class="clear"></div>
            </div>
            <div class="list-nav">
                <a href="#" class="circle-button btn-sm load-more-maso" data-pagination-anima="fade-bottom" data-page-items="6"> <?php echo $lang['PaginationMore'];?> <i class="fa fa-arrow-down"></i> </a>
            </div>
        </div>
    </div>
</div>