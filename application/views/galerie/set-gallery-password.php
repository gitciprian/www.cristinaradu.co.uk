<div class="header-title white ken-burn" data-parallax="scroll" data-position="top" data-image-src="<?php echo IMAGE_PATH;?><?php echo ($bg_image !="" ? $bg_image : 'long-1.jpg');?>" data-natural-width="1980" data-natural-height="600">
	<div class="container">
		<div class="title-base">
			<hr class="anima" />
            <h1><?php echo ($lb == 'ro' ? $page_title : $page_title_en);?></h1>
            <p><?php echo ($lb == 'ro' ? $scurta_descriere : $scurta_descriere_en);?></p>
		</div>
	</div>
</div>
<div class="section-empty">
	<div class="container content">
		<div class="row">
			<div class="col-md-12 text-center">
				<p><?php echo $lang['PassDescription'];?></p>
				<div class="col-md-4 col-md-offset-4 text-center">
					<form action="" method="post">
						<div class="form-group">
							<label for="pwd"><?php echo $lang['PassTitle'];?>:</label>
							<input type="password" name="gallery_password" class="form-control" id="pwd" required>
						</div>
						<button type="submit" class="btn btn-default"><?php echo $lang['PassButton'];?></button>
					</form>
				</div>
				<div class="clearfix"></div><br/>
				<p><?php echo $lang['PassHint'];?></p>
			</div>
		</div>
		<hr class="space" />
	</div>
</div>

<script src="https://cdn.jsdelivr.net/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert/1.1.3/sweetalert.css">

<script>
    $( document ).ready(function() {
		<?php if(isset($_GET['url'])):?>
			<?php if (strpos($_GET['url'], 'error') !== false):?>
	        swal("Ooops", "<?php echo $lang['PassError'];?>", "error");
			<?php endif;?>
		<?php endif;?>
    });
</script>