<div class="header-title white ken-burn" data-parallax="scroll" data-position="top" data-image-src="<?php echo IMAGE_PATH;?><?php echo ($bg_image !="" ? $bg_image : 'long-1.jpg');?>" data-natural-width="1980" data-natural-height="600">
	<div class="container">
		<div class="title-base">
			<hr class="anima" />
            <h1><?php echo ($lb == 'ro' ? $page_title : $page_title_en);?></h1>
            <p><?php echo ($lb == 'ro' ? $scurta_descriere : $scurta_descriere_en);?></p>
		</div>
	</div>
</div>
<div class="section-empty">
	<div class="container content">
		<div class="row">
			<div class="col-md-8">
				<?php if($how_many_videos > 0):?>
					<div class="grid-list gallery">
						<div class="grid-box row" data-lightbox-anima="fade-right">
							<?php foreach ($project_videos as $video):
                                if($video !=""):
                            ?>
                                    <div class="grid-item col-md-6">
                                        <a class="img-box mfp-iframe i-center img-scale-up" href="www.youtube.com/watch?v=<?php echo $video;?>">
                                            <i class="fa fa-film"></i>
                                            <img src="https://img.youtube.com/vi/<?php echo $video;?>/hqdefault.jpg" alt="" />
                                        </a>
                                    </div>
                                <?php endif;?>
							<?php endforeach;?>
							<div class="clearfix"></div>
						</div>
					</div>
				<?php else :?>
					<h4><?php echo $lang['GalleryEmpty'];?></h4>
					<div class="clearfix"></div>
				<?php endif;?>
			</div>
			<div class="col-md-4 portfolio-details fixed-area text-justify" data-topscroll="50" data-bottom="300">
				<h4><?php echo $lang['VideoDefaultTitle'];?></h4>
				<hr class="space m" />
				<?php echo ($lb == 'ro' ? $page_content : $page_content_en);?>
				<hr class="space m" />
				<ul class="fa-ul ">
                    <li><i class="fa-li im-user"></i><b><?php echo ($lb == 'ro' ? 'Autor' : 'Author');?>:</b> <?php echo $autor;?></li>
					<li><i class="fa-li im-structure"></i><b><?php echo ($lb == 'ro' ? 'Video in galerie' : 'Video in gallery');?>:</b> <?php echo $how_many_videos;?></li>
                    <li><i class="fa-li im-calendar"></i><b><?php echo ($lb == 'ro' ? 'Creata la' : 'Created at');?>:</b> <?php echo ViewHelper::Datasc($created_at);?></li>
					<li><i class="fa-li im-copyright"></i><b>Copyright:</b> Free - non comercial</li>
				</ul>
				<hr class="space m" />
				<div class="btn-group social-group">
					<?php if($link_facebook != ""): ?><a target="_blank" href="<?php echo $link_facebook;?>" data-social="share-facebook" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook text-s circle"></i></a><?php endif;?>
					<?php if($link_youtube != ""): ?><a target="_blank" href="<?php echo $link_youtube;?>" data-social="share-youtube" data-toggle="tooltip" data-placement="top" title="Youtube"><i class="fa fa-youtube text-s circle"></i></a><?php endif;?>
					<?php if($link_pinterest != ""): ?><a target="_blank" href="<?php echo $link_pinterest;?>" data-social="share-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest text-s circle"></i></a><?php endif;?>
					<?php if($link_instagram != ""): ?><a target="_blank" href="<?php echo $link_instagram;?>" data-social="share-instagram" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram text-s circle"></i></a><?php endif;?>
					<?php if($link_twitter != ""): ?><a target="_blank" href="<?php echo $link_twitter;?>" data-social="share-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter text-s circle"></i></a><?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>