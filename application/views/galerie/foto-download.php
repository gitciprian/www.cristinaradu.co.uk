<div class="section-empty">
	<div class="container content">
		<div class="row">
			<div class="col-md-6">
				<img src="<?php echo IMAGE_PATH;?>gallery/<?php echo $image;?>" alt="<?php echo $page_title;?>" class="img-responsive" style="margin: 0"/>
			</div>
			<div class="col-md-6">
				<div class="content">
					<hr class="space s" />
					<div class="row">
						<div class="col-md-12">
							<div class="title-base text-left">
								<hr />
								<h4><?php echo ($lb == 'ro' ? $page_title : $page_title_en);?></h4>
							</div>
						</div>
						<div class="col-md-12">
							<div class="tag-row">
								<span><i class="fa fa-calendar"></i> <a href="#"><?php echo ViewHelper::Datasc($created_at);?></a></span>
								<span><i class="fa fa-bookmark"></i> <a href="#"><?php echo ($lb == 'ro' ? $gallery_name : $gallery_name_en);?></a></span>
							</div>
						</div>
					</div>
					<hr class="space m" />
					<div class="text-left">
						<a href="<?php echo IMAGE_PATH;?>gallery/<?php echo $image;?>" download="v-hudubet-<?php echo $image_id;?>-<?php echo $image;?>" class="btn btn-sm  anima-button" type="button"><i class="im-download"></i><?php echo $lang['ImageDownload'];?></a>
						<a href="mailto:<?php echo $general_email;?>?subject=I want to buy this image (id <?php echo $image_id;?>)" class="btn btn-sm  anima-button" type="button"><i class="im-dollar-sign"></i><?php echo $lang['ImageBuy'];?></a>

                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" style="display: inline">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="WPPT9TMFMZMLW">
                            <button class="btn btn-sm  anima-button" type="submit"><i class="im-paypal"></i><?php echo $lang['ImageDonate'];?></button>
                            <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
                        </form>

                    </div>
					<hr class="space m" />
						<?php echo ($lb == 'ro' ? $image_details : $image_details_en);?>
					<hr class="space s" />
					<div class="social-group-button">
						<div class="social-button" data-anima="pulse-vertical" data-trigger="hover"><i class="fa fa-share-alt circle text-s"></i></div>
						<div class="btn-group social-group">
							<?php if($link_facebook != ""): ?><a target="_blank" href="<?php echo $link_facebook;?>"><i data-social="share-facebook" class="fa fa-facebook circle"></i></a><?php endif;?>
							<?php if($link_twitter != ""): ?><a target="_blank" href="<?php echo $link_twitter;?>"><i data-social="share-twitter" class="fa fa-twitter circle"></i></a><?php endif;?>
							<?php if($link_youtube != ""): ?><a target="_blank" href="<?php echo $link_youtube;?>"><i data-social="share-google" class="fa fa-google circle"></i></a><?php endif;?>
							<?php if($link_pinterest != ""): ?><a target="_blank" href="<?php echo $link_pinterest;?>"><i data-social="share-pinterest" class="fa fa-pinterest circle"></i></a><?php endif;?>
							<?php if($link_instagram != ""): ?><a target="_blank" href="<?php echo $link_instagram;?>"><i data-social="share-instagram" class="fa fa-instagram circle"></i></a><?php endif;?>
						</div>
					</div>
                    <hr class="space s" />
                    <a href="/foto/detalii/<?php echo ViewHelper::sanitazeStringForUrl($lb == 'ro' ? $page_title : $page_title_en);?>/<?php echo $page_id;?>" class="btn-text"><?php echo $lang['BackToGallery'];?></a>
				</div>
			</div>
		</div>
	</div>
</div>
