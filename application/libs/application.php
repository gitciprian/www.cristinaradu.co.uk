<?php

class Application
{

    private $url_controller     = null;
    private $url_action         = null;
    private $url_parameter_1    = null;
    private $url_parameter_2    = null;
    private $url_parameter_3    = null;

    public function __construct()
    {
        // create array with URL parts in $url
        $this->splitUrl();

	    // check to see if the controller exists if not look for a lang equivalence
	    $url_controller     = $this->urlToSameController($this->url_controller);

        // check for controller: does such a controller exist ?
        if (file_exists('./application/controller/' . $url_controller . '.php')) {

            require './application/controller/' . $url_controller . '.php';
	        $url_controller     = new $url_controller();

            if (method_exists($url_controller, $this->url_action)) {

                // call the method and pass the arguments to it
                if (isset($this->url_parameter_3))
                {
                    // will translate to something like $this->home->method($param_1, $param_2, $param_3);
	                $url_controller->{$this->url_action}($this->url_parameter_1, $this->url_parameter_2, $this->url_parameter_3);

                } elseif (isset($this->url_parameter_2))
                {
                    // will translate to something like $this->home->method($param_1, $param_2);
	                $url_controller->{$this->url_action}($this->url_parameter_1, $this->url_parameter_2);

                } elseif (isset($this->url_parameter_1))
                {
                    // will translate to something like $this->home->method($param_1);
	                $url_controller->{$this->url_action}($this->url_parameter_1);

                } else {
                    // if no parameters given, just call the method without parameters, like $this->home->method();
	                $url_controller->{$this->url_action}();
                }
            } else {

	            // default/fallback: call the index() method of a selected controller
            	if (
            		$this->url_action === NULL || // for default
	                $this->url_action == 1 || // for language 1 - ro
	                $this->url_action == 2 // for language 2 - en
	                )
	            {
		            $url_controller->index();
	            } else{
		            // 404
		            require './application/controller/error.php';
		            $error = new Error();
		            $error->index();
	            }

            }

        } else {

	        // invalid URL, so simply show home/index or 404
	        if($url_controller === NULL)
	        {
		        require './application/controller/home.php';
		        $home = new Home();
		        $home->index();
	        }else{
	        	// 404
		        require './application/controller/error.php';
		        $error = new Error();
		        $error->index();
	        }

        }
    }

    /**
     * Get and split the URL
     */
    private function splitUrl()
    {
        if (isset($_GET['url'])) {

            // split URL
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            $this->url_controller   = (isset($url[0]) ? $url[0] : null);
            $this->url_action       = (isset($url[1]) ? $url[1] : null);
            $this->url_parameter_1  = (isset($url[2]) ? $url[2] : null);
            $this->url_parameter_2  = (isset($url[3]) ? $url[3] : null);
            $this->url_parameter_3  = (isset($url[4]) ? $url[4] : null);

            // for debugging. uncomment this if you have problems with the URL
	        /*
             echo 'Controller: ' . $this->url_controller . '<br />';
             echo 'Action: ' . $this->url_action . '<br />';
             echo 'Parameter 1: ' . $this->url_parameter_1 . '<br />';
             echo 'Parameter 2: ' . $this->url_parameter_2 . '<br />';
             echo 'Parameter 3: ' . $this->url_parameter_3 . '<br />';
	        */
        }
    }


	/*
	 * Look for lang alternatives in controller names - special hack
	 */
	private function urlToSameController($url_controller)
	{
		if(isset($url_controller)){

			/*
			 * change this if you want to have multiple URLs pointing to the same controller
			 * this is meant to be used to change the URL depending on language

				switch ($url_controller)
				{
					case 'despre-mine':
					case 'about-me':

						$url_controller = 'despre-mine';
						break;
				}
			*/

			return $this->urlToController($url_controller);

		}


	}


	/**
	 * Get and split the URL
	 */
	private function urlToController($url_controller)
	{
		if(isset($url_controller)){
			$url_controller = preg_replace("/[^[:alnum:][:space:]]/u", '', $url_controller);
			//$url_controller = preg_replace("/[^a-zA-Z0-9]+/", "", $url_controller);
		}

		return $url_controller;
	}
}
