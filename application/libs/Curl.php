<?php
class Curl {

    public $url = '';
    public $aData = array();
    public $type = 'POST';
    public $userAgent = '';
    public $customHeader = '';

    function get() {

        /*if($this->userAgent == '') {
            $this->userAgent = $_SERVER['HTTP_USER_AGENT'];
        }*/

        $aResponse = array();
        $aResponse['INFO'] = array();
        $aResponse['INFO']['URL'] = $this->url;
        $aResponse['INFO']['TYPE'] = $this->type;
        $aResponse['RESPONSE'] = array();

        if($this->url == "") {
            $aResponse['INFO']['ERROR'] = "INVALID URL - $this->url";
            return $aResponse;
        }

        $aTransportData = http_build_query($this->aData);

        /*  echo $url;
          echo $aTransportData;*/
        switch($this->type) {

            case "POST":

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL,$this->url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 300);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $aTransportData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                //curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
                if($this->customHeader != '') {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->customHeader);
                }
                $response = curl_exec ($ch);
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                curl_close ($ch);

                break;

            case "GET":

                $ch = curl_init();

                $this->url .= '?' . http_build_query($this->aData);
                //echo $this->url;

                curl_setopt($ch, CURLOPT_URL,$this->url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                if($this->customHeader != '') {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->customHeader);
                }

                $response = curl_exec ($ch);
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                curl_close ($ch);

                break;

            default:
                return false;
                break;

        }

        $aResponse['INFO']['HTTP_CODE'] = $httpcode;
        $aResponse['RESPONSE'] = $response;

        return $aResponse;


    }





}