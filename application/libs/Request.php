<?php

class Request
{



    public static function filterISO($value)
    {

        //Debug::dump($value);
        $value = html_entity_decode($value, ENT_COMPAT, 'ISO-8859-15');
        $value = htmlspecialchars($value, ENT_QUOTES, 'ISO-8859-15');
        $value = filter_var($value, FILTER_SANITIZE_STRING);
        //Debug::dump($value, 'RETURNING');
        return $value;
    }
    public static function filter($value)
    {

        //Debug::dump($value);
        $value = html_entity_decode($value, ENT_COMPAT, 'UTF-8');
        $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        $value = filter_var($value, FILTER_SANITIZE_STRING);
        //Debug::dump($value, 'RETURNING');
        return $value;
    }
    public static function getParam($paramName = false, $defaultValue = false, $isISO = false)
    {

        if ($paramName === false) {
            return false;
        }
        if ($isISO) {
            if (isset($_REQUEST[$paramName]) && $_REQUEST[$paramName] != "") {
//            Debug::dump($_REQUEST[$paramName]);
                return Request::filterISO($_REQUEST[$paramName]);

            } else {
//            Debug::dump($defaultValue);
                return $defaultValue;
            }
        } else {
            if (isset($_REQUEST[$paramName]) && $_REQUEST[$paramName] != "") {
//            Debug::dump($_REQUEST[$paramName]);
                return Request::filter($_REQUEST[$paramName]);

            } else {
//            Debug::dump($defaultValue);
                return $defaultValue;
            }
        }


    }
    public static function getUnfilteredParam($paramName = false, $defaultValue = false)
    {

        if ($paramName === false) {
            return false;
        }

        return $_REQUEST[$paramName];

    }


}



    