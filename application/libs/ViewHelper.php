<?php
class ViewHelper
{

    /*
     * Class to handle all view helper functions
     */
    function __construct()
    {

    }


    public static function buildGoogleAnalyticsTag() {

        $html = '
            <!-- google analytics -->
       
        ';
        return $html;

    }



    /*
     * function to build pagination
     * params
     * $total           : total of items to pagination
     * $hitsPerPage     : how many hits per page
     * $hitsPerPage     : how many hits per page
     * $selectedPage    : which page is selected item
     * $clickClass      : name of class (css) for selected item
     */
    public static function buildPagination($total = false, $hitsPerPage = 10, $selectedPage = 1, $action="", $niceDisplay=true, $displayAmount= 2) {


        if(!$total) {
            return '';
        }

        $nPages = ceil($total / $hitsPerPage);
        $html = '<div class="row text-center"><div class="col-lg-12"><ul class="pagination">';

	    if($niceDisplay) {

		    $selectedClass = 'active';

		    if($selectedPage > 1) {

			    $html .= '<li><a href="'.$action.'/1" title="Go to first page">&laquo;</a></li>';
			    $html .= '<li><a href="'.$action.'/' . ($selectedPage > 1 ? ($selectedPage - 1) : 1) . '"  title="Go to previous page"><</a></li>';
		    }

		    for($i = 1; $i <= $nPages; $i++) {

			    $showPageButton = false;
			    if($selectedPage == $i) {
				    $selectedClass = 'active';
			    } else {
				    $selectedClass = '';
			    }

			    if($i >= ($selectedPage - $displayAmount) &&  $i <= ($selectedPage + $displayAmount)) {
				    $showPageButton = true;
			    }


			    if($showPageButton) {
				    $html .= '<li class="'.$selectedClass.'"><a href="'.$action.'/' . $i . '">' . $i . '</a></li>';
			    }
		    }


		    if($selectedPage < $nPages) {
			    $html .= '<li><a href="'.$action.'/' . ($selectedPage < $nPages ? ($selectedPage + 1) : $nPages) . '"  title="Go to next page">></a></li>';
			    $html .= '<li><a href="'.$action.'/' . $nPages . '" title="Go to last page">&raquo;</a></li>';
		    }



	    } else {
		    /* just all page buttons no trimming / next / prev */
		    for($i = 1; $i <= $nPages; $i++) {

			    if($selectedPage == $i) {
				    $selectedClass = 'active';
			    } else {
				    $selectedClass = '';
			    }

			    $html .= '<li class="'.$selectedClass.'"><a href="'.$action.'/'.$i.'">'.$i.'</a></li>';
		    }
	    }



        $html .= '</ul></div></div>';
        return $html;

    }

    /*
     * function to limit a texts length
     * also filters the variable
     * params
     * $text          : text to format
     * $length        : length if string
     */
    public static function textLimit($text = false, $length = 250, $replacement=' ...') {

        if(!$text) {
            return false;
        }
        $text = Request::filter($text);
        //Debug::dump($text);

        $text = trim($text);
        $length = intval($length);

        $replacement = trim($replacement);

        if (strlen($text) > $length) {

            $space_pos = false;

            if(strpos($text, ' ')) {
                @$space_pos = strpos($text, ' ', ($length - 3 * strlen($replacement)));
            }

            if ($space_pos && $space_pos > $length) {
                $space_pos = $length - strlen($replacement);
            }
            $text = substr($text, 0, $space_pos) . $replacement;
        }

        return $text;

    }

    /*
     * function to format a unix timestamp
     * also filters the variable
     * params
     * $unix            : timestamp to format
     * $format          : format string for php date
     */
    public static function formatDate($unix=false, $format='jS M Y') {
        if(!$unix) {
            return false;
        }
        return date($format, $unix);

    }


    /*
     * function to format a number
     * also filters the variable
     * params
     * $number          : number to format
     * $dp              : decimal places
     */
    public static function formatNumber($number = false, $dp = 2) {

        if(!$number) {
            return false;
        }
        $number = Request::filter($number);
        return number_format($number, $dp);

    }


	/*trim long titles*/
	function trim_text($input, $length, $ellipses = true, $strip_html = true) {
		//strip tags, if desired
		if ($strip_html) {
			$input = strip_tags($input);
		}

		//no need to trim, already shorter than trim length
		if (strlen($input) <= $length) {
			return $input;
		}

		//find last space within length
		$last_space = strrpos(substr($input, 0, $length), ' ');
		$trimmed_text = substr($input, 0, $last_space);

		//add ellipses (...)
		if ($ellipses) {
			$trimmed_text .= '...';
		}

		return $trimmed_text;
	}

	/*
    *check a vars value for !isset and null
    */
	function checkVarVal($val=false)
	{
		if (!$val || !isset($val) || $val == "") {
			return false;
		} else {
			return true;
		}
	}

	/* function to convert month # to string */
	function MonthString($monthNumber='') {

		$aMonths = explode(" ","'' Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec");

		if($monthNumber == "") {
			return false;
		}

		$monthNumber = ltrim($monthNumber, "0");
		return $aMonths[$monthNumber];

	}

	/*
	* Function to convert a number into readable Currency
	*
	* @param string $n   			The number
	* @param string $n_decimals	The decimal position
	* @return string           	The formatted Currency Amount
	*
	* Returns string type, rounded number - same as php number_format()):
	*
	* Examples:
	*		format_amount(54.377, 2) 	returns 54.38
	*		format_amount(54.004, 2) 	returns 54.00
	*		format_amount(54.377, 3) 	returns 54.377
	*		format_amount(54.00007, 3) 	returns 54.00
	*/
	function format_amount($n, $n_decimals) {
		return ((floor($n) == round($n, $n_decimals)) ? number_format($n).'.00' : number_format($n, $n_decimals));
	}

	/*
	 * Function to Encrypt user sensitive data for storing in the database
	 *
	 * @param string	$value		The text to be encrypted
	 * @param 			$encodeKey	The Key to use in the encryption
	 * @return						The encrypted text
	 */
	function encryptIt($value) {
		// The encodeKey MUST match the decodeKey
		$encodeKey = 'Li1KUqJ4tgX14dS,A9ejk?uwnXaNSD@fQ+!+D.f^`Jy';
		$encoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($encodeKey), $value, MCRYPT_MODE_CBC, md5(md5($encodeKey))));
		return($encoded);
	}

	/*
	 * Function to decrypt user sensitive data for displaying to the user
	 *
	 * @param string	$value		The text to be decrypted
	 * @param 			$decodeKey	The Key to use for decryption
	 * @return						The decrypted text
	 */
	function decryptIt($value) {
		// The decodeKey MUST match the encodeKey
		$decodeKey = 'Li1KUqJ4tgX14dS,A9ejk?uwnXaNSD@fQ+!+D.f^`Jy';
		$decoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($decodeKey), base64_decode($value), MCRYPT_MODE_CBC, md5(md5($decodeKey))), "\0");
		return($decoded);
	}

	function clean($string) {
		return $string = str_replace(',', '', $string); // Replaces all spaces with hyphens.
	}

	function Percentage($value){
		return round($value * 100). "%";
	}

	function Percentages($value){
		return round($value * 100);
	}

	function returnPercent($percentage,$totalValue){

		$percentage = $percentage == 0 ? '0.001' : $percentage;
		$totalValue = $totalValue == 0 ? '0.001' : $totalValue;

		$new_percent = ($percentage * 100) / $totalValue;
		$new_percent = round($new_percent,2)."%";
		return $new_percent;
	}


	// uniformizeaza nume
	function cleanNames($value){
		return ucwords(strtolower($value));
	}

	// calculeaza varsta din data nasterii
	function extractAge($dob){

		$new_date = date('Y-m-d', strtotime('+18 years', strtotime($dob)));
		return $new_date;
	}

	// uniformizeaza email
	function cleanEmail($value){
		return strtolower($value);
	}

	public static function sanitazeStringForUrl($string){

		$str = strtolower(trim($string));
		$str = preg_replace('@[^a-zA-Z0-9]@', ' ', $str);
		$str = preg_replace('/(\s\s+|\-\-+)/', ' ', $str);

		return str_replace(' ','-',$str);
	}

	function sanitazeStringForTitle($string){
		$str = str_replace('-', ' ', $string);
		return ucfirst($str);
	}

	public static function imageClass($number_of_items){
		if($number_of_items <= 20){
			$img_class  = "col-md-6";
		}
		if($number_of_items > 20 && $number_of_items <= 51){
			$img_class  = "col-md-4";
		}
		if($number_of_items > 51){
			$img_class  = "col-md-3";
		}

		return $img_class;
	}

	static function Datasiora($Date){

		$_Date = "";
		$_Point = (int) date("m",strtotime($Date)) + 1 - 1;
		$AMonths = array ("January","February","March","April","May","June","July","August","September","October","November","December");
		$_Date .= date("d ",strtotime($Date));
		$_Date .= $AMonths[$_Point-1];
		$_Date .= date(" Y",strtotime($Date));
		$_Date .= date(" H:i",strtotime($Date));

		return $_Date;
	}

	static function Datasc($Date){

		$_Date = "";
		$_Point = (int) date("m",strtotime($Date)) + 1 - 1;
		$AMonths = array ("January","February","March","April","May","June","July","August","September","October","November","December");
		$_Date .= date("d ",strtotime($Date));
		$_Date .= $AMonths[$_Point-1];
		$_Date .= date(" Y",strtotime($Date));
		//$_Date .= date(" H:i",strtotime($Date));

		return $_Date;
	}

	static function  getKeyValue($array,$key){
		$json   = json_decode($array);
		foreach($json->categori as $cat){
			if($cat->id == $key){
				return $cat->gallery_name;
			}
		}
	}


	function getMonths($lb){

		if ($lb == "ro" || $lb == "") {
			$months = array(1 => 'Ianuarie', 2 => 'Februarie', 3 => 'Martie', 4 => 'Aprilie', 5 => 'Mai', 6 => 'Iunie', 7 => 'Iulie', 8 => 'August', 9 => 'Septembrie', 10 => 'Octombrie', 11 => 'Noiembrie', 12 => 'Decembrie');
		} else {
			$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
		}

		return $months;
	}

	//validate date
	function validateDate($date, $format = 'Y-m-d H:i:s'){

		$d      = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	// return a string without last character
	function removeLastChar($string, $number_of_characters = 1){

		$short_str  = substr($string, 0, -$number_of_characters);
		return $short_str;
	}

	public static function get_url_param($var_name){

		if(isset($var_name)) {
			$my_var = $_REQUEST[$var_name];
			$my_var = (int) ( $my_var );
			return $my_var;
		}else{
			return false;
		}

	}

	public static function splitAllUrl($param=1)
	{
		if (isset($_GET['url'])) {

			// split URL
			$url = rtrim($_GET['url'], '/');
			$url = filter_var($url, FILTER_SANITIZE_URL);
			$url = explode('/', $url);

			switch ($param){
				case '0':
					return $parameter_0 = (isset($url[0]) ? $url[0] : null);
					break;
				default:
				case '1':
					return $parameter_1 = (isset($url[1]) ? $url[1] : null);
					break;
				case '2':
					return $parameter_2 = (isset($url[2]) ? $url[2] : null);
					break;
				case '3':
					return $parameter_3 = (isset($url[3]) ? $url[3] : null);
					break;
			}

		}
	}

	public static function detectSelectedLanguage($lang){

		switch ($lang)
		{
			default:
			case '1':
				$lb = 'ro';
				break;

			case '2':
				$lb = 'en';
				break;
		}

		$_SESSION['lang']   = $lb;
		setcookie("lang", $lb,  time() + (3600 * 24 * 30), '/');

		return $lb;

	}

	public static function getDetectedLanguage()
	{

		if(isSet($_SESSION['lang']))
		{
			$lang  = $_SESSION['lang'];
		}
		elseif (isSet($_COOKIE['lang']))
		{
			$lang  = $_COOKIE['lang'];
		}
		else
		{
			$lang = 'ro';
		}

		return $lang;
	}

	public static function encrypt_decrypt($action, $string)
	{

		$output = false;

		$encrypt_method     = "AES-256-CBC";
		$secret_key         = 'sensors-12345!@#$%asdfgQWERT!@#$%^&*()_+';
		$secret_iv          = 'sensors-12345!@#$%asdfgQWERT+_)(*&^%$#@!';

		// hash
		$key = hash('sha256', $secret_key);

		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		if( $action == 'encrypt' )
		{
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_url_encode($output);
		}
		else if( $action == 'decrypt' )
		{
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}

		return $output;
	}

	public static function base64_url_encode($input) {
		return strtr(base64_encode($input), '+/=', '._-');
	}

	public static function base64_url_decode($input) {
		return base64_decode(strtr($input, '._-', '+/='));
	}

	/*
	$plain_txt      = "This is my plain text";
	$encrypted_txt  = encrypt_decrypt('encrypt', $plain_txt);
	$decrypted_txt  = encrypt_decrypt('decrypt', $encrypted_txt);
	*/

	public static function isJson($string)
	{
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public  static function filterGetParams($param)
	{
		return filter_input( INPUT_GET, $param, FILTER_SANITIZE_URL );
	}

	public static function redirect($url)
	{
		//Clean URL
		$uri = htmlspecialchars($_SERVER['REQUEST_URI'],ENT_QUOTES,'UTF-8');
		$uri = preg_replace('~&amp;~','&',$uri);
		header("Location: $url");
	}

}
