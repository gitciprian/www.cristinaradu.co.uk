<?php
class Login
{
	private $ENCRYPTION_KEY = 'a8bfaad90db51f2480526735b631ae3ba883adaae6f088e5263714d99e16474f';
	public $db              = null;


	function __construct()
	{
		$this->openDatabaseConnection();
	}

	private function openDatabaseConnection()
	{
		$options    = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
		$this->db   = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
	}

		public function login($umail,$upass)
		{
			try
			{
				if($this->validate_email($umail)){

					$sql        = "SELECT * FROM u_login WHERE user_email = '$umail'  AND user_permision ='1'";
					$query      = $this->db->prepare($sql);
					$query->execute();

					if($query->rowCount() > 0)
					{

						$all_user_data = $query->fetchAll();

						foreach ($all_user_data as $user) {

							$user_email   = $user->user_email;
							$user_name    = $user->user_name;
							$user_type    = $user->user_type;
							$user_lang    = $user->user_language;
							$user_id      = $user->id;
							$db_pass      = $user->user_password;
						}

						$md5pass = $this->mc_decrypt($db_pass);

						if($md5pass == $upass){

							$_SESSION['auth'] = true;
							$_SESSION['user_email']   = $user_email;
							$_SESSION['user_name']    = $user_name;
							if($user_lang == ""){
								$_SESSION['user_language']= "ro";
							}else{
								$_SESSION['user_language']= $user_lang;
							}
							$_SESSION['user_type']    = $user_type;
							$_SESSION['user_id']      = $user_id;

							$this->log_login_time();

							return true;

						} else {
							return false;
						}

					}

				}


			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
			}
		}

		// Encrypt Function
		public function mc_encrypt($encrypt){
			$encrypt = serialize($encrypt);
			$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
			$key = pack('H*', $this->ENCRYPTION_KEY);
			$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
			$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
			$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
			return $encoded;
		}

		// Decrypt Function
		public function mc_decrypt($decrypt){
			$decrypt = explode('|', $decrypt.'|');
			$decoded = base64_decode($decrypt[0]);
			$iv = base64_decode($decrypt[1]);
			if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
			$key = pack('H*', $this->ENCRYPTION_KEY);
			$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
			$mac = substr($decrypted, -64);
			$decrypted = substr($decrypted, 0, -64);
			$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
			if($calcmac!==$mac){ return false; }
			$decrypted = unserialize($decrypted);
			return $decrypted;
		}

		public function is_loggedin()
		{
			if(isset($_SESSION['auth']))
			{
				return true;
			}
		}

		public function log_login_time()
		{
			if(isset($_SESSION['auth']))
			{

				$sql = "INSERT INTO u_auth (user_id, user_name) VALUES (:user_id, :user_name)";
				$query = $this->db->prepare($sql);
				$query->execute(array(':user_id' => $_SESSION['user_id'], ':user_name' => $_SESSION['user_name']));

			}
		}

		public function validate_email($email)
		{
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return true;
			}
		}

		public function redirect($url)
		{
			//Clean URL
			$uri = htmlspecialchars($_SERVER['REQUEST_URI'],ENT_QUOTES,'UTF-8');
			$uri = preg_replace('~&amp;~','&',$uri);
			header("Location: $url");
		}

		public function logout()
		{
			session_destroy();
			session_regenerate_id();
			unset($_SESSION['auth']);
			return true;
		}

}
?>