<?php

class Language extends Controller {

	public function index()
	{
		/*
		 * Get the referral URL and after setting the language redirect back to it
		 */
		$referral_url   = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/' ;

		/*
		 * Get the lang from the URL (first param after the action)
		 */
		$lang_from_url  = ViewHelper::splitAllUrl(1);

		/*
		 * Set the language as a variable and store it in session or/and cookie
		 */
		$lb             = ViewHelper::detectSelectedLanguage($lang_from_url) ;

		ViewHelper::redirect($referral_url);

	}
}