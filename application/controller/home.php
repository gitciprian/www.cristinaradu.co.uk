<?php

class Home extends Controller
{
    /**
     * PAGE: index
     */
    public function index()
    {
    	/*
	    $active             = '1';
	    $lb                 = ViewHelper::getDetectedLanguage() ;
	    $general_model      = $this->loadModel('GeneralModel');
	    $page_details       = $general_model->getPageContentById($active);
	    $page_galleries     = $general_model->getAllPageType(4,8);
	    $variables          = $general_model->getAllVariables();
	    $slides             = $general_model->getHomeSlides();
	    $how_many_slides    = count($slides);

	    //Debug::dump($page_galleries);

	    foreach ( $variables as $var )
	    {
	    	foreach ($var as $k => $v)
	    	{
			    $$k = $v;
	    	}
		    $general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
		    $general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
	    }

	    foreach ( $page_details as $var )
	    {
		    foreach ($var as $k => $v)
		    {
			    if($k == "page_extras")
			    {
				    $page_extras    = json_decode($v, true);
				    if(count($page_extras) > 0)
				    {
					    foreach ($page_extras as $key => $val)
					    {
						    $$key = $val;
					    }
				    }
			    } else {
				    $$k = $v;
			    }
		    }
	    }

	    if (isset($page_meta_title))
	    {
		    $page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
	    } else {
		    $page_meta_title    = $general_page_meta_title;
	    }

	    if (isset($page_meta_description))
	    {
		    $page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
	    } else {
		    $page_meta_description    = $general_page_meta_description;
	    }

	    require 'application/lang/lang.'.$lb.'.php';
    	*/
        require 'application/views/_templates/header.php';
        require 'application/views/home/index.php';
        require 'application/views/_templates/footer.php';
    }


}
