<?php

class Foto extends Controller
{

	public function index()
	{

		$active             = 3;
		$lb                 = ViewHelper::getDetectedLanguage() ;
		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($active);
		$variables          = $general_model->getAllVariables();
		$gallery_categories = $general_model->getPopularGalleryCategories(1,5);
		$active_categories  = count($gallery_categories);
		$all_galleries      = $general_model->getAllPageType(4, false, 1);
		$active_galleries   = count($all_galleries);
		$all_cat_names      = $general_model->getAllGalleryCategories($lb);

		//var_dump($all_cat_names);

		foreach ( $page_details as $var )
		{
			foreach ($var as $k => $v)
			{
				if($k == "page_extras")
				{
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0)
					{
						foreach ($page_extras as $key => $val)
						{
							$$key = $val;
						}
					}
				} else {
					$$k = $v;
				}
			}
		}

		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
			$general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
		}

		if (isset($page_meta_title))
		{
			$page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
		} else {
			$page_meta_title    = $general_page_meta_title;
		}

		if (isset($page_meta_description))
		{
			$page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
		} else {
			$page_meta_description    = $general_page_meta_description;
		}

		require 'application/lang/lang.'.$lb.'.php';
		require 'application/views/_templates/header.php';
		require 'application/views/galerie/index-foto.php';
		require 'application/views/_templates/footer.php';
	}


	// detalii galerie
	public function detalii()
	{

		$active         = 3;
		$lb             = ViewHelper::getDetectedLanguage() ;
		$project_id     = ViewHelper::splitAllUrl("3");
		$project_id     = (int)$project_id;

		if(!is_int($project_id) || $project_id < 10)
		{
			ViewHelper::redirect("/error/");
			exit();
		}

		$preview_on     = (ViewHelper::splitAllUrl("4") == 'preview' ? true : false);
		$load_page      = true;

		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($project_id,$preview_on);

		if(count($page_details) < 1)
		{
			ViewHelper::redirect("/error/");
			exit();
		}

		$variables          = $general_model->getAllVariables();
		$project_images     = $general_model->getProjectImageGallery($project_id,$preview_on);
		$how_many_images    = count($project_images);

		$img_class          = ViewHelper::imageClass($how_many_images);


		//Debug::dump($_SESSION);

		foreach ( $page_details as $var )
		{
			foreach ($var as $k => $v)
			{
				if($k == "page_extras")
				{
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0){
						foreach ($page_extras as $key => $val)
						{
							$$key = $val;
						}
					}
				} else {
					$$k = $v;
				}
			}
		}

		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
			$general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
		}

		if (isset($page_meta_title))
		{
			$page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
		} else {
			$page_meta_title    = $general_page_meta_title;
		}

		if (isset($page_meta_description))
		{
			$page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
		} else {
			$page_meta_description    = $general_page_meta_description;
		}

		if(isset($_POST['gallery_password']) && $_POST['gallery_password'] !="")
		{
			$general_model->validateGallerySession($parola_galerie,$_POST['gallery_password'],$project_id);
		}

		if($parola_galerie !="")
		{
			if(isset($_SESSION['project_'.$project_id]) && $_SESSION['project_'.$project_id] == $project_id)
			{
				$load_page = true;
			}else{
				$load_page  = false;
			}
		}

		if($load_page === true)
		{
			$load_template  = 'detalii-foto.php';
		}else{
			$load_template  = 'set-gallery-password.php';
		}

		require 'application/lang/lang.'.$lb.'.php';
		require 'application/views/_templates/header.php';
		require 'application/views/galerie/'.$load_template;
		require 'application/views/_templates/footer.php';
	}

	/*
	 * download image page
	 */
	public function download()
	{

		$active             = 3;
		$lb                 = ViewHelper::getDetectedLanguage() ;
		$general_model      = $this->loadModel('GeneralModel');
		$variables          = $general_model->getAllVariables();
		$enc_image_id       = ViewHelper::splitAllUrl("2");
		$image_id           = (int)ViewHelper::base64_url_decode($enc_image_id);

		if(!is_int($image_id) || $image_id == 0)
		{
			ViewHelper::redirect("/error/");
			exit();
		}

		$image_details      = $general_model->getImageDetailsByImageId($image_id);

		foreach ( $image_details as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
		}

		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$page_meta_title        = ($lb == 'ro' ? $var->meta_title : $var->meta_title_en);
			$page_meta_description  = ($lb == 'ro' ? $var->meta_description : $var->meta_description_en);
		}

		require 'application/lang/lang.'.$lb.'.php';
		require 'application/views/_templates/header.php';
		require 'application/views/galerie/foto-download.php';
		require 'application/views/_templates/footer.php';
	}

}
