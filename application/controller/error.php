<?php

class Error extends Controller
{
	/**
	 * PAGE: index
	 */
	public function index()
	{
		header("HTTP/1.0 404 Not Found");

		/*
		$active             = '0';
		$lb                 = ViewHelper::getDetectedLanguage() ;
		$general_model      = $this->loadModel('GeneralModel');
		$variables          = $general_model->getAllVariables();

		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
			$general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
		}

		if (isset($page_meta_title))
		{
			$page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
		} else {
			$page_meta_title    = $general_page_meta_title;
		}

		if (isset($page_meta_description))
		{
			$page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
		} else {
			$page_meta_description    = $general_page_meta_description;
		}

		require 'application/lang/lang.'.$lb.'.php';
		*/
		require 'application/views/_templates/header.php';
		require 'application/views/404/index.php';
		require 'application/views/_templates/footer.php';
	}


}
