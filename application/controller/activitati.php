<?php

class Activitati extends Controller
{

	public function index()
	{

		$active = 3;
		require 'application/config/constants.php';

		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($active);
		$all_services       = $general_model->getAllPageType('3');
		$variables          = $general_model->getAllVariables();
		$total_services     = count($all_services);

		//Debug::dump($all_services);

		foreach ( $page_details as $var ) {

			foreach ($var as $k => $v){

				if($k == "page_extras"){
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0){
						foreach ($page_extras as $key => $val){
							$$key = $val;
						}
					}
				}else{
					$$k = $v;
				}

			}

		}

		foreach ( $variables as $var ) {
			foreach ($var as $k => $v){
				$$k = $v;
			}
			$general_page_meta_title       = $var->meta_title;
			$general_page_meta_description = $var->meta_description;
		}

		require 'application/views/_templates/header.php';
		require 'application/views/activitati/index.php';
		require 'application/views/_templates/footer.php';
	}


	public function detalii()
	{


		$active         = 3;
		require 'application/config/constants.php';

		$project_id     = ViewHelper::splitAllUrl("3");
		$project_id     = (int)$project_id;
		$preview_on     = (ViewHelper::splitAllUrl("4") == 'preview' ? true : false);

		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($project_id,$preview_on);
		$variables          = $general_model->getAllVariables();

		//Debug::dump($project_id);

		foreach ( $page_details as $var ) {

			foreach ($var as $k => $v){

				if($k == "page_extras"){
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0){
						foreach ($page_extras as $key => $val){
							$$key = $val;
						}
					}
				}else{
					$$k = $v;
				}

			}

		}

		foreach ( $variables as $var ) {
			foreach ($var as $k => $v){
				$$k = $v;
			}
			$general_page_meta_title       = $var->meta_title;
			$general_page_meta_description = $var->meta_description;
		}

		require 'application/views/_templates/header.php';
		require 'application/views/activitati/detaliu-activitate.php';
		require 'application/views/_templates/footer.php';
	}

}
