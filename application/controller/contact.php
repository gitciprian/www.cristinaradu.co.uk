<?php

class Contact extends Controller
{

	public function index()
	{
/*
		$active             = 5;
		$lb                 = ViewHelper::getDetectedLanguage() ;
		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($active);
		$variables          = $general_model->getAllVariables();

		//Debug::dump($variables);

		foreach ( $page_details as $var )
		{
			foreach ($var as $k => $v)
			{
				if($k == "page_extras")
				{
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0)
					{
						foreach ($page_extras as $key => $val)
						{
							$$key = $val;
						}
					}
				} else {
					$$k = $v;
				}
			}
		}

		// get macro-variables
		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
			$general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
		}

		if (isset($page_meta_title))
		{
			$page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
		} else {
			$page_meta_title    = $general_page_meta_title;
		}

		if (isset($page_meta_description))
		{
			$page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
		} else {
			$page_meta_description    = $general_page_meta_description;
		}

		require 'application/lang/lang.'.$lb.'.php';
*/
		require 'application/views/_templates/header.php';
		require 'application/views/contact/index.php';
		require 'application/views/_templates/footer.php';
	}

	public function sendform()
	{

		$auto_response_content = $auto_response_content_en = $email_send_address = $auto_response_email = $general_email ='';

		$active             = 5;
		$lb                 = ViewHelper::getDetectedLanguage() ;
		$general_model      = $this->loadModel('GeneralModel');
		$variables          = $general_model->getAllVariables();

		// get macro-variables
		foreach ( $variables as $var ) {
			foreach ($var as $k => $v){
				$$k = $v;
			}
		}

		require 'application/libs/php-mailer/PHPMailerAutoload.php';

		$secret         = "6LfhOzsUAAAAAJSDR9OElIV2SgP82rRvC5lNtOPf";
		$remoteip       = $_SERVER["REMOTE_ADDR"];
		$url            = "https://www.google.com/recaptcha/api/siteverify";

		// Form info
		$response       = $_POST["g-recaptcha-response"];
		$form_name      = Request::filter($_POST["form_name"]);
		$form_email     = Request::filter($_POST["form_email"]);
		$form_message   = Request::filter($_POST["form_message"]);
		$adresaPrimire  = $general_email; //$general_email - cupry1@gmail.com

		// Curl Request
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, array(
			'secret' => $secret,
			'response' => $response,
			'remoteip' => $remoteip
		));
		$curlData = curl_exec($curl);
		curl_close($curl);

		// Parse data
		$recaptcha = json_decode($curlData, true);
		if ($recaptcha["success"]){

			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			$mail->setLanguage('en', 'php-mailer/language/phpmailer.lang-en.php');
			$mail->Host = 'mail.smtp2go.com';
			$mail->SMTPAuth = true;
			$mail->Username = 'safe-ro-mail';
			$mail->Password = 'PYXaz6VS3OzH%fT$^3w9';
			$mail->SMTPSecure = 'TLS';
			$mail->Port = 80; //465, 443, 8465
			$mail->isHTML(true);
			$mail->setFrom($email_send_address, 'Valentin Hudubet');
			$mail->addReplyTo($auto_response_email, 'Valentin Hudubet');
			$mail->addAddress($adresaPrimire);
			$mail->addBCC('cupry1@gmail.com');
			$mail->Subject = "Mesaj nou pe formularul de contact din site";

			$html   = "";
			$html   .= "
				<table width='100%' align='left' border='0' cellspacing='3' cellpadding='3'>
					<tr>
						<td align='left'><img src='https://www.valentinhudubet.com/public/images/logo-black.png' /> </td>
					</tr>
					<tr>
						<td align='left'>&nbsp;</td>
					</tr>
					<tr>
						<td align='left'>
							Mesaj de la: ".$form_name."<br/>							
							Email: ".$form_email."<br/>	
							IP contact: ".$remoteip."<br/><br/>
							************************************<br/>
							Mesaj: ".nl2br($form_message)."<br/>
							************************************
						</td>
					</tr>
				</table>
			";

			$message  = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>'.$mail->Subject.'</title></head><body style="max-width:800px;text-align:left; margin:10px">' . $html . '</body></html>';

			$mail->Body = $message;

			if (!$mail->send()) {

				header('Location: /contact/notok');

			} else {

				// send autoresponse
				$mail = new PHPMailer;
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->setLanguage('en', 'php-mailer/language/phpmailer.lang-en.php');
				$mail->Host = 'mail.smtp2go.com';
				$mail->SMTPAuth = true;
				$mail->Username = 'safe-ro-mail';
				$mail->Password = 'PYXaz6VS3OzH%fT$^3w9';
				$mail->SMTPSecure = 'TLS';
				$mail->Port = 80; //465, 443, 8465
				$mail->isHTML(true);
				$mail->setFrom($email_send_address, 'Valentin Hudubet');
				$mail->addReplyTo($auto_response_email, 'Valentin Hudubet');
				$mail->addAddress($form_email);
				$mail->Subject = "Multumim pentru mesajul dvs.";

				$auto_response_content  = $lb == 'ro' ? $auto_response_content : $auto_response_content_en;

				$html   = "";
				$html   .= "
				<table width='100%' align='left' border='0' cellspacing='3' cellpadding='3'>
					<tr>
						<td align='left'><img src='https://www.valentinhudubet.com/public/images/logo-black.png' /> </td>
					</tr>
					<tr>
						<td align='left'>&nbsp;</td>
					</tr>
					<tr>
						<td align='left'>".nl2br($auto_response_content)."</td>
					</tr>
				</table>
			";

				$message  = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>'.$mail->Subject.'</title></head><body style="max-width:800px;text-align:left; margin:10px">' . $html . '</body></html>';
				$mail->Body = $message;
				$mail->send();

				header('Location: /contact/ok');

			}

		}else{
			header('Location: /contact/notok');
		}

	}

}