<?php

class Video extends Controller
{

	public function index()
	{

		$active             = 4;
		$lb                 = ViewHelper::getDetectedLanguage() ;
		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($active);
		$variables          = $general_model->getAllVariables();
		$gallery_categories = $general_model->getPopularGalleryCategories(2,5);
		$active_categories  = count($gallery_categories);
		$all_galleries      = $general_model->getAllPageType(4, false, 2);
		$active_galleries   = count($all_galleries);
		$all_cat_names      = $general_model->getAllGalleryCategories($lb);

		//Debug::dump($page_foto_gallery);

		foreach ( $page_details as $var )
		{
			foreach ($var as $k => $v)
			{
				if($k == "page_extras")
				{
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0)
					{
						foreach ($page_extras as $key => $val)
						{
							$$key = $val;
						}
					}
				} else {
					$$k = $v;
				}
			}
		}

		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
			$general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
		}

		if (isset($page_meta_title))
		{
			$page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
		} else {
			$page_meta_title    = $general_page_meta_title;
		}

		if (isset($page_meta_description))
		{
			$page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
		} else {
			$page_meta_description    = $general_page_meta_description;
		}

		require 'application/lang/lang.'.$lb.'.php';
		require 'application/views/_templates/header.php';
		require 'application/views/galerie/index-video.php';
		require 'application/views/_templates/footer.php';
	}


	// detalii galerie
	public function detalii()
	{

		$active         = 4;
		$lb             = ViewHelper::getDetectedLanguage() ;

		$project_id     = ViewHelper::splitAllUrl("3");
		$project_id     = (int)$project_id;

		if(!is_int($project_id) || $project_id < 10)
		{
			ViewHelper::redirect("/error/");
			exit();
		}

		$preview_on     = (ViewHelper::splitAllUrl("4") == 'preview' ? true : false);
		$general_model      = $this->loadModel('GeneralModel');
		$page_details       = $general_model->getPageContentById($project_id,$preview_on);

		if(count($page_details) < 1)
		{
			ViewHelper::redirect("/error/");
			exit();
		}

		$variables          = $general_model->getAllVariables();
		$how_many_videos    = 0;
		$load_page      = true;

		//unset($_SESSION['project_10']);

		//Debug::dump($page_details);

		foreach ( $page_details as $var )
		{
			foreach ($var as $k => $v)
			{
				if($k == "page_extras")
				{
					$page_extras    = json_decode($v, true);
					if(count($page_extras) > 0)
					{
						foreach ($page_extras as $key => $val)
						{
							$$key = $val;
						}
					}
				}else {
					$$k = $v;
				}
			}
		}

		foreach ( $variables as $var )
		{
			foreach ($var as $k => $v)
			{
				$$k = $v;
			}
			$general_page_meta_title       = $lb == 'ro' ? $var->meta_title : $var->meta_title_en;
			$general_page_meta_description = $lb == 'ro' ? $var->meta_description : $var->meta_description_en;
		}

		if (isset($page_meta_title))
		{
			$page_meta_title    = $lb == 'ro' ? $page_meta_title : $page_meta_title_en;
		} else {
			$page_meta_title    = $general_page_meta_title;
		}

		if (isset($page_meta_description))
		{
			$page_meta_description    = $lb == 'ro' ? $page_meta_description : $page_meta_description_en;
		} else {
			$page_meta_description    = $general_page_meta_description;
		}

		// afisare videoclip in pagina
		if($video_codes !="")
		{
			$project_videos     = explode(", ",$video_codes);
			foreach ($project_videos as $video)
			{
				if($video !="")
				{
					$how_many_videos++;
				}
			}
		}


		if(isset($_POST['gallery_password']) && $_POST['gallery_password'] !="")
		{
			$general_model->validateGallerySession($parola_galerie,$_POST['gallery_password'],$project_id);
		}

		if($parola_galerie !="")
		{
			if(isset($_SESSION['project_'.$project_id]) && $_SESSION['project_'.$project_id] == $project_id)
			{
				$load_page = true;
			}else{
				$load_page  = false;
			}
		}

		if($load_page === true)
		{
			$load_template  = 'detalii-video.php';
		}else{
			$load_template  = 'set-gallery-password.php';
		}

		require 'application/lang/lang.'.$lb.'.php';
		require 'application/views/_templates/header.php';
		require 'application/views/galerie/'.$load_template;
		require 'application/views/_templates/footer.php';

	}

}
